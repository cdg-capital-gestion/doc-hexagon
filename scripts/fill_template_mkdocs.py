##########################################################################
###################### COMMENTAIRE ACTIVITE  #############################
##########################################################################
import pathlib

import jinja2

HX_HERE = pathlib.Path(__file__).absolute().parent


def _init_template_environment(searchpath: pathlib.Path=HX_HERE, encoding: str='utf-8') -> jinja2.Environment:
    loader = jinja2.FileSystemLoader(searchpath=str(searchpath), encoding=encoding)
    env = jinja2.Environment(loader=loader, autoescape=jinja2.select_autoescape(['jinja']))
    return env


def fill_template(module_dot_path: str) -> str:
    env = _init_template_environment()
    tpl = env.get_template('template_mkdocs_module.jinja')

    result = tpl.render(module_path=module_dot_path)
    return result


if __name__ == '__main__':
    rs = fill_template('courbe')
    print(rs)
