# Fonctions d'automatisation de la génération d'une structure de base pour la documentation en MKDocs
from pathlib import Path
import subprocess
import yaml

from hexagon.scripts.fill_template_mkdocs import fill_template

utils_folder = Path(__file__).absolute().parent
SRC_FOLDER_PATH = utils_folder.parent
DOC_FOLDER_PATH = utils_folder.parent.parent.joinpath('docs')


def launch_pdoc(doc_project_name: str='doc-hexagon'):
    dst_path = Path(SRC_FOLDER_PATH.parent.parent).joinpath(doc_project_name, 'public')
    # cmd1 = "mamba activate dev"
    cmd = f"pdoc --html -f --config show_source_code=False {str(SRC_FOLDER_PATH)} -o {str(dst_path)}"
    try:
        print('launching pdoc generation ....')
        print(cmd)
        subprocess.run(cmd, shell=True, check=True, capture_output=True)
        print(f'pdoc generation done: {dst_path}')
    
    except Exception as err:
        raise err


def is_pyfile(path: Path) -> bool:
    if path.stem != '__init__' and path.suffix == '.py' and not path.name.startswith('.'):
        return True

    return False


def walk_folder(fdpath: Path) -> list:
    rv = []
    for sub in fdpath.iterdir():

        if sub.is_dir() and not sub.stem.startswith('__') and not sub.stem == 'test':
            rv.append(walk_folder(sub))

        elif sub.is_file() and is_pyfile(sub):
            rv.append(sub)

    return rv


def make_relative_to_src(lpaths):
    rv = []
    src = SRC_FOLDER_PATH
    for el in lpaths:
        if isinstance(el, list):
            rv.extend(make_relative_to_src(el))

        else:
            rv.append(el.relative_to(src))

    return rv


def make_doc_entry(lpaths: list, create=False):
    rv = []
    for relpath in lpaths:
        mdpath = relpath.with_suffix('.md')
        if str(mdpath.parent) == '.':
            dot_path = mdpath.stem
        else:
            pth = str(mdpath).split('.')[0]
            dot_path = '.'.join(pth.split('/'))

        mdcontent = fill_template(dot_path)
        docpath = DOC_FOLDER_PATH.joinpath(mdpath)
        rv.append(mdpath)
        if create:
            if not docpath.parent.exists():
                docpath.parent.mkdir(parents=True, exist_ok=True)

            with docpath.open('w') as fd:
                fd.write(mdcontent)

    if not create:
        return rv

    print('Done')


def build_nav_file(nav_path):
    mdpaths = make_doc_entry(make_relative_to_src(walk_folder(SRC_FOLDER_PATH)), create=False)
    rv_list = []
    for p in mdpaths:
        if str(p.parent) == '.':
            key = p.stem.capitalize()
        else:
            key = p.parent.stem.capitalize() + '.' + p.stem
        dico = {key: str(p)}
        rv_list.append(dico)

    rv = {'nav': rv_list}
    with open(nav_path, 'w') as fd:
        yaml.dump(rv, fd)
    print('Done')
