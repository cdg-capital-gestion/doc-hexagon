SELECT 
    PUBLIC.INDICE_VALUE.ID as ID_HISTORIQUE,
    PUBLIC.INDICE_VALUE.DATE_MARCHE,
    PUBLIC.INDICE.ID as ID_INDICE,
    PUBLIC.INDICE.CODE,
    PUBLIC.INDICE_VALUE.VALUE,
    PUBLIC.INDICE.DESCRIPTION,
    PUBLIC.INDICE.MARCHE,
    PUBLIC.INDICE.PLACE_COTATION,
    PUBLIC.INDICE.DATE_CREATION,
    PUBLIC.INDICE.DATE_REFERENCE,
    PUBLIC.INDICE.VALEUR_BASE,
    PUBLIC.INDICE.COMMENT,
    PUBLIC.INDICE.TICKER,
    PUBLIC.INDICE.ID_PAYS,
    PUBLIC.INDICE.ID_DEVISE

FROM PUBLIC.INDICE, PUBLIC.INDICE_VALUE

WHERE PUBLIC.INDICE.ID = PUBLIC.INDICE_VALUE.ID_INDICE

ORDER BY PUBLIC.INDICE_VALUE.DATE_MARCHE, PUBLIC.INDICE.ID
