SELECT TITRE.ID AS "titre.id",
    FONDS.ID AS "fonds.id",
    ETAT_TITRE.DATE_MARCHE AS DATE_MARCHE,
    FONDS.CODE AS "fonds.code",
    FONDS.CODE_ISIN AS "fonds.code_isin",
    FONDS.DESCRIPTION AS "fonds.description",
    FONDS.CATEGORIE AS "fonds.categorie",
    FONDS.PROMOTEUR AS "fonds.promoteur",
    TITRE.CODE AS "titre.code",
    TITRE.ISIN AS "titre.isin",
    TITRE.DESCRIPTION AS "titre.description",
    TITRE.CATEGORIE AS "titre.categorie",
    TITRE.CLASSE AS "titre.classe",
    TITRE.TAUX_FACIAL AS "titre.taux_facial",
    TITRE.EMISSION AS "titre.emission",
    TITRE.JOUISSANCE AS "titre.jouissance",
    TITRE.ECHEANCE AS "titre.echeance",
    TITRE.SPREAD AS "titre.spread",
    TITRE.NOMINAL AS "titre.nominal",
    TITRE.QT_EMISE AS "titre.qt_emise",
    TITRE.PERIODICITE AS "titre.periodicite",
    TITRE.COTE AS "titre.cote",
    TITRE.GARANTI AS "titre.garanti",
    TITRE.TYPE_COUPON AS "titre.type_coupon",
    TITRE.METHODE_VALO AS "titre.methode_valo",
    TITRE.REVISION AS "titre.revision",
    TITRE.RANG AS "titre.rang",
    TITRE.MODE_REMBOURSEMENT AS "titre.mode_remboursement",
    TITRE.PLACE_COTATION AS "titre.place_cotation",
    ETAT_TITRE.COURS AS "titre.cours",
    ETAT_TITRE.COURS * INVENTAIRE.QUANTITE AS "titre.cours_ligne",
    ETAT_TITRE.SENSIBILITE AS "titre.sensibilite",
    ETAT_TITRE.DURATION AS "titre.duration",
    ETAT_TITRE.CONVEXITE AS "titre.convexite",
    ETAT_TITRE.TAUX_COURBE AS "titre.taux_courbe",
    ETAT_TITRE.TAUX_VALORISATION AS "titre.taux_valorisation",
    ETAT_TITRE.DATE_REFERENCE AS "titre.date_reference",
    ETAT_TITRE.COUPON_COURU AS "titre.coupon_couru",
    ETAT_TITRE.MATURITE_RESIDUELLE AS "titre.maturite_residuelle",
    ETAT_TITRE.SPREAD_VALORISATION AS "titre.spread_valorisation",
    ETAT_TITRE.DEVISE_VALORISATION AS "titre.devise_valorisation",
    INVENTAIRE.QUANTITE AS "titre.quantite",
    INVENTAIRE.PDR_BRUT AS "titre.pdr_brut",
    INVENTAIRE.PDR_NET AS "titre.pdr_net",
    EMETTEUR.CODE AS "emetteur.code",
    EMETTEUR.ISIN AS "emetteur.isin",
    EMETTEUR.DESCRIPTION AS "emetteur.description",
    SECTEUR.CODE AS "secteur.code",
    SECTEUR.DESCRIPTION AS "secteur.description",
    DEVISE.CODE AS "devise.code",
    DEVISE.DESCRIPTION AS "devise.description",
    PAYS.ALPHA3 AS "pays.code",
    PAYS_EMETTEUR.ALPHA3 AS "emetteur.pays.code"

FROM PUBLIC.TITRE AS TITRE,
    PUBLIC.FONDS AS FONDS,
    PUBLIC.ETAT_TITRE AS ETAT_TITRE,
    PUBLIC.INVENTAIRE_FONDS AS INVENTAIRE,
    PUBLIC.EMETTEUR AS EMETTEUR,
    PUBLIC.SECTEUR AS SECTEUR,
    PUBLIC.DEVISE AS DEVISE,
    PUBLIC.PAYS AS PAYS,
    PUBLIC.PAYS AS PAYS_EMETTEUR

WHERE TITRE.ID_EMETTEUR = EMETTEUR.ID
    AND TITRE.ID_DEVISE = DEVISE.ID
    AND TITRE.ID_PAYS = PAYS.ID
    AND ETAT_TITRE.ID_TITRE = TITRE.ID
    AND EMETTEUR.ID_SECTEUR = SECTEUR.ID
    AND EMETTEUR.ID_PAYS = PAYS_EMETTEUR.ID
    AND INVENTAIRE.ID_TITRE = TITRE.ID
    AND INVENTAIRE.ID_FONDS = FONDS.ID
    AND INVENTAIRE.DATE_MARCHE = ETAT_TITRE.DATE_MARCHE

ORDER BY ETAT_TITRE.DATE_MARCHE,
    FONDS.ID,
    TITRE.CLASSE,
    TITRE.CATEGORIE,
    TITRE.ID
