WITH historique AS (
    SELECT EF.id_fonds, EF.DATE_MARCHE, EF.VL,
    row_number() OVER (ORDER BY EF.id_fonds, EF.DATE_MARCHE) as rid
    
    FROM
        PUBLIC.ETAT_FONDS AS EF
    
    WHERE EF.date_marche >= '2021-12-31'
    AND ef.date_marche <= '2022-03-25'
)

SELECT 
    h1.id_fonds,
    h1.date_marche as date_reference,
    h2.date_marche as date_marche,
    h1.vl as vl_reference,
    h2.vl as vl_marche,
    h2.vl / h1.vl - 1 as variation

from historique as h1, historique as h2
where h1.id_fonds = h2.id_fonds
and h2.rid - h1.rid = 1

ORDER BY h1.id_fonds, h1.date_marche


##############################################################################

SELECT 
    EF1.DATE_MARCHE AS DATE_REFERENCE,
    EF2.DATE_MARCHE AS DATE_MARCHE,
    EF1.VL AS VL_REFERENCE,
    EF2.VL AS VL_MARCHE,
    EF2.VL / EF1.VL - 1 AS VARIATION

FROM 
    PUBLIC.ETAT_FONDS AS EF1,
    PUBLIC.ETAT_FONDS AS EF2

WHERE 
    EF1.ID_FONDS = EF2.ID_FONDS AND 
    ef1.id_fonds = 32 AND
    EF1.DATE_MARCHE < EF2.DATE_MARCHE

AND EF2.DATE_MARCHE IN (
    '2021-12-31', '2022-01-04', '2022-01-06', '2022-01-10',
    '2022-01-13', '2022-01-17', '2022-01-19', '2022-01-21',
    '2022-01-25', '2022-01-27', '2022-01-31', '2022-02-02',
    '2022-02-04', '2022-02-08', '2022-02-10', '2022-02-14',
    '2022-02-16', '2022-02-18', '2022-02-22', '2022-02-24',
    '2022-02-28', '2022-03-02', '2022-03-04', '2022-03-08',
    '2022-03-10', '2022-03-14', '2022-03-16', '2022-03-18',
    '2022-03-22', '2022-03-24')
AND EF1.DATE_MARCHE IN (
    '2021-12-31', '2022-01-04', '2022-01-06', '2022-01-10',
    '2022-01-13', '2022-01-17', '2022-01-19', '2022-01-21',
    '2022-01-25', '2022-01-27', '2022-01-31', '2022-02-02',
    '2022-02-04', '2022-02-08', '2022-02-10', '2022-02-14',
    '2022-02-16', '2022-02-18', '2022-02-22', '2022-02-24',
    '2022-02-28', '2022-03-02', '2022-03-04', '2022-03-08',
    '2022-03-10', '2022-03-14', '2022-03-16', '2022-03-18',
    '2022-03-22', '2022-03-24')
