#######################################################################
######################### NOUVEAUX TITRES #############################

SELECT
    t.code code,
    t.code_isin isin,
    t.classe classe,
    t.categorie categorie,
    t.description description,
    t.emetteur code_emetteur,
    t.nominal nominal,
    t.valeur_taux taux_facial,
    t.date_emission emission,
    t.date_jouissance jouissance,
    t.date_echeance echeance,
    t.spread_emission spread,
    t.nombre_titre_emis qt_emise,
    t.garantie garanti,
    t.periodicite_coupon periodicite,
    t.type_cotation cote,
    t.ticker ticker,
    t.groupe_1 code_pays,
    t.devise_cotation code_devise,
    t.type_taux type_coupon,
    t.type_taux methode_valo,
    t.date_revision revision,
    t.periodicite_rembou mode_remboursement

FROM tp.titre t

WHERE t.code NOT IN ('IREPO')
AND t.code NOT LIKE 'DAT%'
AND t.date_creation > to_date('25/11/2022', 'dd/mm/yyyy')


######################################################################
########################## ETAT TITRES ###############################

SELECT
    vt.date_valo date_marche,
    vt.date_reference date_reference,
    vt.titre code_titre,
    vt.cours cours,
    vt.sensibilite sensibilite,
    vt.duration duration,
    vt.convexite convexite,
    (vt.taux_courbe - t.spread_emission/100) taux_courbe,
    vt.taux_courbe taux_valorisation,
    t.spread_emission spread_valorisation,
    vt.titredevise devise_valorisation,
    vt.coupon_couru coupon_couru,
    (t.date_echeance - vt.date_valo) maturite_residuelle

FROM tp.valo_titre vt, tp.titre t

WHERE vt.titre = t.code
AND vt.titre not LIKE 'DAT%'
-- Exclut les titres qui créent du bruit
AND vt.titre not IN ('1183')
AND MONTHS_BETWEEN(vt.date_valo , vt.date_reference) < 0.7
AND vt.date_valo <= to_date('29/07/2022', 'dd/mm/yyyy')
AND vt.date_valo > to_date('22/07/2022', 'dd/mm/yyyy')

ORDER BY vt.date_valo ASC, vt.titre ASC

#######################################################################
########################## ETAT DES FONDS #############################

/*
PS: Il Manque la sensibilité et la duration, ces deux mesures seront recalculées
au niveau de la couche applicative tout en laissant la possibilité de fournir notre propre valeur.
*/

SELECT
    pf.date_fp date_marche,
    pf.portefeuille code_fonds,
    pf.vl vl,
    pf.actif_net_av_vl actif_net,
    pf.actif_net_ap_vl actif_net_ap_sr,
    pf.actif_brut actif_brut,
    pf.nombre_parts_av_vl nombre_parts,
    pf.nombre_parts_ap_vl nombre_parts_ap_sr,
    pf.banque_jour banque,
    (pf.frais_gestion + pf.frais_gestion2 + pf.frais_gestion3) fdg,
    pf.cumul_provisions_charges provision_fdg,
    pf.agios agios,
    pf.cumul_provisions_agios provision_agios,
    pf.frais_gestion fdg_ckg,
    pf.frais_gestion2 fdg_depositaire,
    pf.frais_gestion3 fdg_ammc

FROM tp.histo_portef_fp_group pf

WHERE pf.type_vl = 'COM'
AND pf.date_fp <= to_date('29/07/2022', 'dd/mm/yyyy')
AND pf.date_fp > to_date('22/07/2022', 'dd/mm/yyyy')
--AND pf.portefeuille = 'IJT_PRO'
ORDER BY pf.date_fp ASC, pf.portefeuille ASC


############################################################################
############################# INVENTAIRE FONDS #############################
############################################################################

SELECT
    pf.date_valo date_marche,
    pf.portefeuille code_fonds,
    pf.titre code_titre,
    pf.actif quantite,
    pf.pdr_brut pdr_brut,
    pf.pdr_net pdr_net

FROM tp.valo_portefeuille pf

WHERE pf.actif != 0
AND pf.titre != 'IREPO'
-- Titres a exclure
AND pf.date_valo <= to_date('29/07/2022', 'dd/mm/yyyy')
AND pf.date_valo > to_date('22/07/2022', 'dd/mm/yyyy')
/*AND pf.portefeuille = 'FCP_ALF'*/

ORDER BY pf.date_valo ASC, pf.portefeuille ASC


#######################################################################
########################## OPERATIONS MARCHE TEMPORAIRES ##############

SELECT
    ho.num_operation id,
    ho.date_operation date_operation,
    ho.portefeuille code_fonds,
    ho.titre code_titre,
    ho.poste poste_operation,
    ho.depositaire depositaire,
    ho.intermediaire intermediaire,
    ho.contrepartie contrepartie,
    ho.quantite quantite,
    ho.interets interets,
    ho.cours cours_unitaire,
    ho.montant_1 cours_operation_net,
    ho.montant_7 cours_operation_brut,
    ho.frais_tot frais_operation,
    ho.negotaux taux_negoce,
    ho.taux_placement taux_placement,
    ho.devise_1 devise_reglement,
    ho.negospreadtaux spread_operation,
    ho.negoppc ppc,
    ho.nbre_jour_placement duree,
    ho.date_echeance date_fin,
    ho.date_livraison date_rl,
    ho.valeur1 coupon_couru_unitaire,
    ho.pmv_brut pmv_brut,
    ho.pmv_net pmv_net,
    ho.taux_1 cours_devise

FROM tp.histo_operation ho

WHERE ho.statut = 'V'
AND ho.poste LIKE '%REPO%'
AND ho.poste NOT LIKE '%B'
AND ho.date_operation <= to_date('29/07/2022', 'dd/mm/yyyy')
AND ho.date_operation > to_date('22/07/2022', 'dd/mm/yyyy')
ORDER BY ho.date_operation ASC, ho.poste ASC


#####################################################################
########################## OPERATIONS MARCHE ########################

SELECT
    ho.num_operation id,
    ho.date_operation date_operation,
    ho.portefeuille code_fonds,
    ho.titre code_titre,
    ho.poste poste_operation,
    ho.depositaire depositaire,
    ho.intermediaire intermediaire,
    ho.contrepartie contrepartie,
    ho.quantite quantite,
    ho.interets interets,
    ho.cours cours_unitaire,
    ho.montant_1 cours_operation_net,
    ho.montant_7 cours_operation_brut,
    ho.frais_tot frais_operation,
    ho.negotaux taux_negoce,
    ho.taux_placement taux_placement,
    ho.devise_1 devise_reglement,
    ho.negospreadtaux spread_operation,
    ho.negoppc ppc,
    ho.nbre_jour_placement duree,
    ho.date_echeance date_fin,
    ho.date_livraison date_rl,
    ho.valeur1 coupon_couru_unitaire,
    ho.pmv_brut pmv_brut,
    ho.pmv_net pmv_net,
    ho.taux_1 cours_devise

FROM tp.histo_operation ho

WHERE ho.statut = 'V'
AND ho.poste NOT IN (
    'CES', 'SOUS', 'CESC', 'SOUSC', 'STRANS', 'ETRANS', 'FDG_VAR-',
    'FRDEPO-', 'OBLAM_E', 'OBLAM_S', 'REMA_ACT', 'REMA_OBL', 'SOMA_OBL',
    'FRCDVM-', 'SOMA_ACT', 'PRORLA-', 'PRORLO-', 'PROV_AUTO-',
    'FRBANQ-', 'FREMET-', 'FRGES-', 'PROAGIOS+', 'ECH_DAT', 'PROTVA-',
    'FRBANQ+', 'FREMET+', 'FRGES+', 'PROAGIOS-', 'ECH_BDC', 'PRORLA+',
    'VDTRM', 'ADTRMB', 'VIRINT-', 'VIRINT+', 'PRECOMFDG-',
    'PRECOMFDG+', 'DAT', 'BDC', 'VDEV', 'ADEV', 'APPCAP', 'APPCA')
AND ho.poste NOT LIKE '%REPO%'
AND ho.poste NOT LIKE '%B'
AND ho.date_operation <= to_date('29/07/2022', 'dd/mm/yyyy')
AND ho.date_operation > to_date('22/07/2022', 'dd/mm/yyyy')
ORDER BY ho.date_operation ASC, ho.poste ASC

#####################################################################
########################## OPERATIONS PASSIF ########################

SELECT
    hr.num_reglement id,
    hr.date_operation date_operation,
    hr.mode_reglement poste_operation,
    hr.description description,
    hr.entite code_fonds,
    hr.montant montant_operation,
    hr.tiers tiers,
    hr.devise devise_reglement

FROM tp.histo_reglement hr

WHERE flag_stade = 'R'

AND date_operation <= to_date('29/07/2022', 'dd/mm/yyyy')
AND date_operation > to_date('22/07/2022', 'dd/mm/yyyy')
AND mode_reglement IN ('DISTRIB-', 'DIVID+', 'SOUS+', 'RACH-', 'FDG_VAR-')
ORDER BY hr.date_operation, hr.entite


#####################################################################
########################## OPERATIONS PASSIF 2  ########################

SELECT
    hr.num_reglement id,
    hr.date_operation date_operation,
    hr.mode_reglement poste_operation,
    hr.description description,
    hr.entite code_fonds,
    hr.montant montant_operation,
    hr.tiers tiers,
    hr.devise devise_reglement,
    CASE hr.mode_reglement 
    WHEN 'SOUS+' THEN hr.montant * 1
    ELSE -1 * hr.montant END as montant_abs

FROM tp.histo_reglement hr

WHERE flag_stade = 'R'

AND date_operation <= to_date('29/07/2022', 'dd/mm/yyyy')
AND date_operation > to_date('22/07/2022', 'dd/mm/yyyy')
AND mode_reglement IN ('SOUS+', 'RACH-')
ORDER BY hr.date_operation, hr.entite

#######################################################################
######################### UPDATE TITRES #############################

SELECT
    t.code code,
    t.code_isin isin,
    t.classe classe,
    t.categorie categorie,
    t.description description,
    t.emetteur code_emetteur,
    t.nominal nominal,
    t.valeur_taux taux_facial,
    t.date_emission emission,
    t.date_jouissance jouissance,
    t.date_echeance echeance,
    t.spread_emission spread,
    t.nombre_titre_emis qt_emise,
    t.garantie garanti,
    t.periodicite_coupon periodicite,
    t.type_cotation cote,
    t.ticker ticker,
    t.groupe_1 code_pays,
    t.devise_cotation code_devise,
    t.type_taux type_coupon,
    t.type_taux methode_valo,
    t.date_revision revision,
    t.periodicite_rembou mode_remboursement

FROM tp.titre t

WHERE t.code NOT IN ('IREPO')
AND t.code NOT LIKE 'DAT%'
AND t.flag_actif = 'O'
AND t.date_revision > Date '2023-04-08'
AND t.date_revision <= Date '2023-07-29'

AND t.date_revision > to_date('08/04/2023', 'dd/mm/yyyy')
AND t.date_revision <= to_date('29/07/2023', 'dd/mm/yyyy')

#######################################################################
########################## ECHEANCIER TITRES ##########################


SELECT
    te.titre code_titre,
    te.date_tombee date_coupon,
    te.capital_restant capital,
    te.capital_amortis amortissement,
    te.coupon_brut coupon,
    te.taux taux_facial

FROM tp.titre_evenement te

WHERE te.titre IN ()

###########################################################################
########################## NOUVEAU OPCVM (CREATE) ##########################

SELECT
    t.code code,
    ti.code_isin code_isin,
    t.description description,
    t.periode_vl periodicite,
    t.forme_juridique forme_juridique,
    t.type_gestion type_gestion,
    t.date_creation date_creation,
    t.date_premiere_vl date_premiere_vl,
    t.taux_frais_gestion * 100 taux_fdg,
    t.taux_frais_gestion_2 * 100 taux_depositaire,
    t.taux_frais_gestion_3 * 100 taux_ammc,
    t.type_resultat affectation_resultat,
    ti.categorie categorie,
    ti.groupe_12 code_sesam,
    ti.groupe_6 code_agrement

FROM tp.tiers t, tp.titre ti

WHERE t.code = ti.code
AND t.est_opcvm = 'O'
AND t.flag_actif = 'O'
AND t.code = 'isr_selec'


###########################################################################
######################## NOUVEAU EMETTEUR (CREATE) ########################

SELECT
    t.code code,
    t.description description,
    t.zone_text13 isin,
    t.secteur_economique code_secteur
    
FROM tp.tiers t

WHERE t.flag_actif = 'O'
AND t.est_emetteur = 'O'
AND t.code = 'EFTENERGIA'


#################################################################
########################## FDG VARIABLES ########################

SELECT
    hr.num_reglement id,
    hr.date_operation date_operation,
    hr.mode_reglement poste_operation,
    hr.description description,
    hr.entite code_fonds,
    hr.montant montant_operation,
    hr.tiers tiers,
    hr.devise devise_reglement

FROM tp.histo_reglement hr

WHERE flag_stade = 'R'

AND date_operation <= to_date('31/12/2020', 'dd/mm/yyyy')
AND date_operation > to_date('30/04/2020', 'dd/mm/yyyy')
AND mode_reglement IN ('FDG_VAR-')
ORDER BY hr.date_operation, hr.entite

####################################################################
########################## DEVISE COURS ############################

# Décaler d'un jour

SELECT
    dev.date_cotation date_marche,
    dev.devise_1 code_devise,
    dev.type_cotation unite_devise,
    dev.devise_2 code_devise_base,
    dev.type_cotation unite_devise_locale,
    dev.ask cours

FROM tp.histo_devise_cours dev

WHERE dev.date_cotation <= to_date('29/07/2022', 'dd/mm/yyyy')
AND dev.date_cotation > to_date('22/07/2022', 'dd/mm/yyyy')

ORDER BY dev.date_cotation

#####################################################################
########################## Composition MBI ##########################

SELECT
    hi.date_cotation date_marche,
    hi.indice code_strate,
    hi.titre code_titre,
    hi.cours cours,
    hi.quantite quantite,
    hi.total_titres quantite_totale,
    hi.nominal nominal,
    hi.gisement gisement ,
    hi.valorisation cours_ligne,
    hi.poids poids_strate

FROM tp.histo_indice_fp hi

WHERE hi.indice = 'MBI'
/*WHERE hi.indice IN ('MBICT', 'MBIMT', 'MBIMLT', 'MBILT')*/
AND hi.date_cotation <= to_date('29/07/2022', 'dd/mm/yyyy')
AND hi.date_cotation > to_date('22/07/2022', 'dd/mm/yyyy')

ORDER BY hi.date_cotation, hi.indice, hi.titre

###########################################################################
########################## OP COUVERTURE DEVISE ##########################

SELECT
    ho.num_operation id,
    ho.date_operation date_operation,
    ho.portefeuille code_fonds,
    ho.poste poste_operation,
    ho.contrepartie contrepartie,
    ho.cours cours_spot,
    ho.montant_1 cours_operation_devise,
    ho.montant_2 cours_operation_locale,
    ho.montant_7 cours_terme,
    ho.devise_1 devise_reglement,
    ho.devise_2 devise_locale,
    ho.nbre_jour_placement duree,
    ho.date_echeance date_fin,
    ho.date_livraison date_rl

FROM tp.histo_operation ho

WHERE ho.statut = 'F'
AND ho.portefeuille = 'CDG_ACT'
AND ho.poste IN ('VDTRMB', 'ADTRMB')
AND ho.date_echeance > to_date('22/07/2022', 'dd/mm/yyyy')
AND ho.date_operation <= to_date('22/07/2022', 'dd/mm/yyyy')
ORDER BY ho.date_operation ASC, ho.poste ASC


###########################################################################
########################## REFERENTIEL OPCVM (UPDATE) ##########################

SELECT
    t.code code,
    t.zone_text13 code_isin,
    t.description,
    t.taux_frais_gestion_2 taux_depositaire,
    t.taux_frais_gestion_3 taux_ammc,
    t.taux_frais_gestion taux_ckg,
    t.type_gestion,
    t.forme_juridique

FROM tp.tiers t

WHERE t.est_opcvm = 'O'
AND t.flag_actif = 'O'

###########################################################################
########################## REFERENTIEL EMETTEURS ##########################

SELECT
    tiers.code code,
    tiers.description description,
    tiers.zone_text13 isin,
    tiers.secteur_economique code_secteur,
    tiers.nationalite code_pays,
    tiers.rating rating

FROM tp.tiers tiers
WHERE tiers.est_emetteur = 'O'
AND tiers.flag_actif = 'O'
AND tiers.code IN ('EFTUTILITIES',
'EBARID_IMM')


############################################################################
########################## REFERENTIEL TIERS EXCEL #########################

SELECT
    /*te.entite_pere entite_pere,*/
    te.code code_ext,
    te.description description,
    /*te.nationalite nationalite,*/
    te.type_economique type_economique,
    te.zone_text11 apparente,
    te.zone_text12 concurrentiel

FROM tp.tiers te
--WHERE te.flag_actif = 'O'
--AND te.est_client = 'O'
WHERE te.code IN (
    '199_05',
    'CL1006',
    'CL1009'

)


##############################################################################
########################## REFERENTIEL TIERS CLIENTS #########################

SELECT
    te.entite_pere entite_pere,
    te.code code_ext,
    te.description description,
    /*te.nationalite nationalite,*/
    DECODE(te.type_residence, 'R', 'True', 'NR', 'False', 'False')  residence,
    DECODE(te.zone_text11, 'A', 'True', 'NA', 'False', 'False') apparente,
    DECODE(te.zone_text12, 'C', 'True', 'NC', 'False', 'False')  concurrentiel,
    te.type_economique type_economique

FROM tp.tiers te
WHERE te.flag_actif = 'O'
AND te.est_client = 'O'
AND te.code IN (
    '28', '28', '65', '194', '269', '317', '328',
    'CL1008', 'CL1024', 'CL1031', 'CL1033',
    'CL1033', 'CL1052', 'CL1088', 'CL1095',
    'CL1096', 'CL1098', 'CL1101', 'CL1114', 'CL1115'
)

##############################################################################
########################## REFERENTIEL COMPTE ESPECE #########################
SELECT *
FROM tp.compte_espece ce
WHERE rownum < 250
AND ce.entite = '2'


##############################################################################
########################## REFERENTIEL COMPTE TITRE #########################
SELECT *
FROM tp.compte_titre ct
WHERE rownum < 250
AND ct.entite = '2'

#######################################################################
########################## ECHEANCIER TITRES ##########################


SELECT
    te.titre code_titre,
    te.date_tombee date_coupon,
    te.capital_restant capital,
    te.capital_amortis amortissement,
    te.coupon_brut coupon,
    te.taux taux_facial

FROM tp.titre_evenement te

WHERE te.titre IN ()

#######################################################################
######################### Count OP Temporaires ########################

SELECT count(*) nombre_operations

FROM tp.histo_operation ho

WHERE ho.statut = 'V'
AND ho.poste LIKE 'RREPO%'
AND ho.poste NOT LIKE '%B'
AND ho.date_operation < to_date('01/01/2019', 'dd/mm/yyyy')
AND ho.date_operation > to_date('01/01/2015', 'dd/mm/yyyy')
ORDER BY ho.date_operation ASC, ho.poste ASC


#######################################################################
######################### FRAIS R/L Dépositaire #######################

SELECT
    ho.num_operation,
    ho.date_operation,
    ho.poste,
    ho.portefeuille code_fonds,
    ho.titre code_titre,
    ho.quantite,
    ho.cours,
    ho.montant_1 volume_operation,
    ho.intermediaire,
    hc.nature_frais,
    hc.code_bareme,
    hc.taux_bareme,
    hc.taux_applique,
    hc.taux_tva,
    hc.courtage_ttc_cv

FROM tp.histo_operation ho, tp.histo_courtage hc
WHERE ho.num_operation = hc.op_finance
AND courtage_ttc_cv != 0
AND hc.nature_frais = 2
AND ho.date_operation <= to_date('31/12/2018', 'dd/mm/yyyy')
AND ho.date_operation >= to_date('01/01/2018', 'dd/mm/yyyy')
AND ho.depositaire in ('DCDGCAP')

ORDER BY ho.date_operation, ho.portefeuille, ho.poste


###########################################################################
##################### LAB/FT Date de dernière opération ###################

SELECT
    hr.tiers tiers, hr.mode_reglement, hr.date_operation

FROM tp.histo_reglement hr

WHERE hr.date_operation IN (
    SELECT MAX(date_operation) max_date

    FROM tp.histo_reglement

    WHERE flag_stade = 'R'
    AND mode_reglement IN ('SOUS+', 'RACH-')
    AND date_operation <= to_date('22/07/2022', 'dd/mm/yyyy')
    AND date_operation > to_date('24/04/2018', 'dd/mm/yyyy')

    GROUP BY tiers
)

ORDER BY hr.tiers, hr.date_operation

###########################################################################

SELECT
    hr.num_reglement id,
    hr.date_operation date_operation,
    hr.mode_reglement poste_operation,
    hr.montant montant_operation,
    hr.tiers tiers,
    hr.devise devise_reglement

FROM tp.histo_reglement hr

WHERE flag_stade = 'R'

AND date_operation <= to_date('22/07/2022', 'dd/mm/yyyy')
AND date_operation > to_date('24/04/2018', 'dd/mm/yyyy')
AND mode_reglement IN ('SOUS+', 'RACH-')

ORDER BY hr.date_operation, hr.entite

###########################################################################

SELECT
    pf.date_valo date_marche,
    pf.portefeuille code_fonds,
    pf.titre code_titre,
    t.code_isin code_isin,
    pf.actif quantite,
    t.periodicite_rembou periodicite_remboursement

FROM tp.valo_portefeuille pf, tp.titre t

WHERE pf.actif != 0
AND pf.titre != 'IREPO'
AND pf.titre = t.code
AND pf.portefeuille = 'ADM_CAS'
AND pf.date_valo = to_date('22/07/2022', 'dd/mm/yyyy')

###########################################################################
SELECT
    pf.date_valo date_marche,
    pf.portefeuille code_fonds,
    pf.titre code_titre,
    pf.actif quantite

FROM tp.valo_portefeuille pf

WHERE pf.actif != 0
AND pf.titre != 'IREPO'
-- Titres a exclure
AND pf.date_valo = to_date('22/07/2022', 'dd/mm/yyyy')
AND pf.portefeuille IN (
    'CDG_PAR', 'FCP_SOL', 'CDG_SOL', 'CAM_MON', 
    'CDG_CIT', 'AXA_EQU', 'BAR_TRE', 'CDG_SER'
)

ORDER BY pf.date_valo ASC, pf.portefeuille ASC

####################### CHECK HISTO BENCHMARK XML ##########################
SELECT *
FROM tp.temp_histo_benchmark hb
WHERE hb.date_valeur >= to_date('27/12/2019', 'dd/mm/yyyy')
-- AND hb.portefeuille = 'Small_Cap'

####################### CHECK HISTO FDG CLIENTS ##########################

select *
FROM tp.histo_fdg_client hfc
where date_vl >= TO_DATE ('26/12/2014', 'dd/mm/yyyy')
AND date_vl <= TO_DATE ('26/06/2015', 'dd/mm/yyyy')

####################### Oracle DB Tales Inspect ##########################

SELECT 
    table_name,
    column_name,
    data_type,
    data_length,
    data_precision,
    nullable

FROM USER_TAB_COLUMNS 

WHERE table_name = 'titre'

##############################################################################
# IT'S FKG CASE SENSITIVE

SELECT 
    col.column_id, 
    col.owner as schema_name,
    col.table_name, 
    col.column_name, 
    col.data_type, 
    col.data_length, 
    col.data_precision, 
    col.data_scale, 
    col.nullable

FROM sys.all_tab_columns col

INNER JOIN sys.all_tables t
ON col.owner = t.owner 
AND col.table_name = t.table_name

WHERE col.owner = 'TP'
AND col.table_name = 'HISTO_CONTRIBUTION'
ORDER BY col.column_id

###################### BDC CREATE ###########################
SELECT
    ho.num_operation id,
    ho.poste poste_operation,
    ho.portefeuille code_fonds,
    ho.depositaire depositaire,
    ho.contrepartie code_emetteur,
    ho.date_operation emission,
    ho.date_echeance echeance,
    ho.montant_1 nominal,
    ho.taux_placement taux_placement,
    ho.interets interets

FROM tp.histo_operation ho

WHERE ho.statut = 'V'
AND ho.poste IN ('DAT', 'BDC')
AND ho.poste NOT LIKE '%B'
AND ho.date_operation > to_date('{debut:%d/%m/%Y}', 'dd/mm/yyyy')
AND ho.date_operation <= to_date('{fin:%d/%m/%Y}', 'dd/mm/yyyy')
ORDER BY ho.portefeuille ASC, ho.date_operation ASC
    
