Document Chargé depuis le fichier: /Users/mero/Library/Application Support/Hexagon/config/system.yaml
***************** ORM DBACCESSS ON ***************
---
description: |
    API documentation for modules: hexagon, hexagon.calendrier, hexagon.calendrier.aggregates, hexagon.calendrier.dao, hexagon.calendrier.enumerations, hexagon.calendrier.exceptions, hexagon.calendrier.fridays, hexagon.calendrier.market, hexagon.core, hexagon.core.date_periode, hexagon.core.entities, hexagon.core.enumerations, hexagon.core.repository, hexagon.core.valueobjects, hexagon.courbe, hexagon.courbe.common, hexagon.courbe.courbe, hexagon.courbe.enumerations, hexagon.courbe.exceptions, hexagon.courbe.interpolator, hexagon.deposit, hexagon.devise, hexagon.etl, hexagon.etl.csv_util, hexagon.etl.data_importer, hexagon.etl.excel, hexagon.etl.exceptions, hexagon.etl.template, hexagon.etl.transform, hexagon.etl.transform.gisement_mbi, hexagon.etl.transform.maroclear_excel, hexagon.etl.transform.transform_asfim, hexagon.etl.transform.transform_mbi, hexagon.etl.transform.xml_maroclear, hexagon.etl.yaml_utils, hexagon.infrastructure, hexagon.infrastructure.config, hexagon.infrastructure.config.config_file_handler, hexagon.infrastructure.config.config_manager, hexagon.infrastructure.config.domains, hexagon.infrastructure.config.domains.analytics, hexagon.infrastructure.config.domains.baseconfig, hexagon.infrastructure.config.domains.courbe, hexagon.infrastructure.config.domains.default_factories, hexagon.infrastructure.config.domains.etl, hexagon.infrastructure.config.domains.performance, hexagon.infrastructure.config.domains.rapports, hexagon.infrastructure.config.domains.reporting, hexagon.infrastructure.config.domains.system, hexagon.infrastructure.config.domains.veille, hexagon.infrastructure.config.exceptions, hexagon.infrastructure.db, hexagon.infrastructure.db.db_access, hexagon.infrastructure.db.db_utils, hexagon.infrastructure.db.exceptions, hexagon.infrastructure.db.models, hexagon.infrastructure.db.models.base_mixin, hexagon.infrastructure.db.models.clients, hexagon.infrastructure.db.models.data_items, hexagon.infrastructure.db.models.documents, hexagon.infrastructure.db.models.market, hexagon.infrastructure.db.models.opcvm, hexagon.infrastructure.db.models.reporting, hexagon.infrastructure.db.models.views, hexagon.infrastructure.db.models_utils, hexagon.infrastructure.exceptions, hexagon.infrastructure.query_executor, hexagon.infrastructure.text_query_executor, hexagon.infrastructure.timeseries, hexagon.performance, hexagon.performance.aggregates, hexagon.performance.benchmark, hexagon.performance.benchmark-old, hexagon.performance.calendar_fonds, hexagon.performance.comparator, hexagon.performance.datastructures, hexagon.performance.devise, hexagon.performance.enumerations, hexagon.performance.exceptions, hexagon.performance.fonds, hexagon.performance.indice, hexagon.performance.repository, hexagon.repository, hexagon.repository.exceptions, hexagon.repository.loader_inventaire, hexagon.repository.loader_operation, hexagon.repository.referentiel, hexagon.repository.store, hexagon.utils, hexagon.utils.app_utils, hexagon.utils.attrs_util, hexagon.utils.collection_helpers, hexagon.utils.dir_utils, hexagon.utils.enumerations, hexagon.utils.exceptions, hexagon.utils.stringify, hexagon.veille, hexagon.veille.analytics, hexagon.veille.entities, hexagon.veille.range_analytics.

lang: en

classoption: oneside
geometry: margin=1in
papersize: a4

linkcolor: blue
links-as-notes: true
...


    
# Module `hexagon` {#hexagon}

# Présentation Hexagon - version **2023.04.1**

Hexagon est application orientée "Data", développée en interne au sein de CDG Capital Gestion
pour répondre principalement aux besoin des reporting OPCVM.

Dans ce sens et avec le temps Hexagon a pris une dimension plus large et doit répondre à d'autres besoins
vu qu'il produit déjà les chiffres nécessaires.

Chose qui a poussé à une réécriture totale d'Hexagon core en **Middle-End** c-à-d entre une librairie de fonctions et de classes et une application à part entière
avec les interactions et les règles de gestion.

L'approche adoptée est innovante et risquée en même temps - version '2023.04'-CalVer cependant les
résultats obtenus sont intéressants d'un point de vue "System Design" à savoir le fonctionnement en mode hybride:

- **Choix de Python (3.11+):**
    - Supporte un haut niveau d'abstraction facilitant la conception et la modélisation du système.
    - Facilite la traduction des concepts mathématiques grâce à la richesse
        des structures de données de base (<code>list</code>, <code>set</code>, <code>dict</code>, <code>deque</code>, <code>defaultdict</code>, <code>ChainMap</code>, <code>heapq</code>, <code>Enum</code>)
    - Spécification mathématiquement correcte des valeurs nulles "None" (Singleton et évalué à False)
    - Multi-paradigme: Supporte la programmation fonctionnelle, comportementale et Orientée objet, Asynchrone
    - Le concept des itérateurs est inhérent au design du langage
    - Supporte les fonctions, les coroutines et les générateurs as first class citizens
    - Riche écosystème de librairies pour des besoins spécifiques:
        (Webserver, Filesystem, Charting, Data Analysis, Scraping, WebRequests, GUI, DataBases, ...)
    - **Sqlalchemy:** **Le meilleur ORM tous langages confondus** une librairie hors norme qui mérite d'être cité.

- **Mode interactif:**
    - Accès aux fonctionnalités bas niveau d'Hexagon (Modules et domaines définis)
    - Execution interactive des fonctionnalités
    - Offre des capacités de développer des scripts personnalisés (Utilisateurs initiés)
    - Aide interactive pour tous les modules Hexagon
    - Simulation et Analytics

- **Domain Driven Design (DDD):**
    
    Généralement le DDD n'est pas utilisé pour un système analytique mais les principes qu'il prône restent valables.

    Afin d'atteindre le Middle-End, Hexagon a adapté le DDD en définissant le domaine des OPCVM et d'un marché financier
    à part entière (Fonds, Portefeuille, Dates, Indices, AMMC, Devise, Souscripteur, Opération, ....)
    
    Suite à ce refactoring (radical), Hexagon s'est doté des fonctionnalités ou plutôt des propriétés suivantes:
    
    - Définition claire et uniforme des entités mises en jeu dans Hexagon. Titre, Fonds, Performance, Percent, ...
    - Réduction des dépendances externes.
    - Possibilité de configurer chaque module at runtime en mode interactif sans avoir à redémarrer l'application
    - Utilisation de la théorie des ensembles pour les Données/Entités reférentiel (Fonds, Indice, Titre)
    - Meilleure implémentation du concept des "Assimilations" (X est assimilable à Y) pour les entités du référentiel:
        - Un Fonds est une entité unique mais joue des rôles différents selon le contexte (Titre, Contrepartie, Emetteur, OPCVM)
        C'est la raison principale derrière l'adoption du DDD, Hexagon définit des CoreEntities multirôles et à identité unique
    - Séparation des Converters et Adapters entre les boundaries de chaque domaine.
    - Application plus Testable (Chaque domaine définit son langage)
    - Possiblité de faire des simulations basées sur des scénarios.
    - Gestion des fichiers de configuration par store/contexte et séparation du code source en
        utilisant les dossiers par défaut de chaque OS:
        - Windows: ~Home/AppData/Local/Hexagon
        - Mac OSX: ~Home/Library/Application Support/Hexagon
        - Linux/Red Hat: ~Home/.config/Hexagon
    - Séparation claire entre les modèles conceptuels et leurs contreparties “données"
    - Optimisation de l'ordre de 30~80X plus rapide le tout en dessous de 200 Mo de RAM.
    - Ajout de Converters pour supporter les services applicatifs: **WEB, Desktop et API**
    - Support de la localisation des données exportées en CSV (Séparateur décimal, des milliers, devise locale)
    - Intefaçage plus général avec les besoins haut niveau (Application WEB, Application Desktop, API)
    - Support de Json, Yaml et TOML comme structures de fichiers.
    - Traçabilité des données générées (Ajout de propriétés par défaut aux fichiers Excel).


## Vue d'ensemble: Architecture (Physique et conceptuelle)

### Architecture Physique

Hexagon est composé d'une base de données et de son code source qui constitue sa librairie

- **Base De Données**

    La base de données Hexagon a été conçue afin de structurer,
    isoler et apporter une spécification claire aux données essentielles à son fonctionnement.
    Conçue en adhérant fortement aux règles des Formes normales (3-4NF), la BDD offre de meilleures
    performances et une administration simplifiée.

    **Fiche Technique:**

    - Type: PostgresSQL
    - Version: 14+
    - Extensions: HStore, JSON
    - Client: PGAdmin 4
    - ApplicationUser: hexagon_services
    - Fréquence d'alimentation: Hebdomadaire
    - Taille du Backup: 120-140 Mo
    - Taille de la BDD: 2 Go
    - Fréquence du Backup: Hebdomadaire
    - OS: Wondows/Linux/Mac OSX
    - Version Initiale: 11

- **Hexagon-src code-**:

    Le package Hexagon a été conçu pour implémenter la logique qui exploite les données brutes de la BDD
    afin de les transformer en valeurs exploitables selon le besoin.
    
    Il est composé de plusieurs modules qui se composent afin de répondre aux différents besoins,
    le package implémente aussi l'ensemble des fonctions helpers/utils du projet.

    **Fiche Technique:**

    - Language: Python
    - Version: 3.11+
    - Package Manager: Mamba, pip
    - Shell: Jupyter Qtconsole, ptpython
    - Versioning: Git
    - CollabPlatform: GitLab
    - Linters: Jedi, pyflakes, LSP-pylsp
    - DocGenerator: pdoc3
    - DBAccess: psycopg2 + sqlalchemy
    - Profiling: cProfile, snakeviz, pyinstrument, 
    - Main Dependencies: sqlalchemy, attrs, cattrs, python-dateutil, more-itertools, xlsxwriter, numpy
    - OS: Wondows/Linux/Mac OSX

### Architecture conceptuelle -Version Courte-

En résumé Les principaux modules d'Hexagon sont:

- <code>[hexagon.infrastructure](#hexagon.infrastructure "hexagon.infrastructure")</code>:
    - <code>[hexagon.infrastructure.db](#hexagon.infrastructure.db "hexagon.infrastructure.db")</code>:
    - <code>[hexagon.infrastructure.config](#hexagon.infrastructure.config "hexagon.infrastructure.config")</code>:
- <code>[hexagon.performance](#hexagon.performance "hexagon.performance")</code>:
- <code>[hexagon.veille](#hexagon.veille "hexagon.veille")</code>:
- <code>hexagon.analytics</code>:
- <code>[hexagon.etl](#hexagon.etl "hexagon.etl")</code>:


    
## Sub-modules

* [hexagon.calendrier](#hexagon.calendrier)
* [hexagon.core](#hexagon.core)
* [hexagon.courbe](#hexagon.courbe)
* [hexagon.deposit](#hexagon.deposit)
* [hexagon.devise](#hexagon.devise)
* [hexagon.etl](#hexagon.etl)
* [hexagon.infrastructure](#hexagon.infrastructure)
* [hexagon.performance](#hexagon.performance)
* [hexagon.repository](#hexagon.repository)
* [hexagon.utils](#hexagon.utils)
* [hexagon.veille](#hexagon.veille)






    
# Module `hexagon.calendrier` {#hexagon.calendrier}




    
## Sub-modules

* [hexagon.calendrier.aggregates](#hexagon.calendrier.aggregates)
* [hexagon.calendrier.dao](#hexagon.calendrier.dao)
* [hexagon.calendrier.enumerations](#hexagon.calendrier.enumerations)
* [hexagon.calendrier.exceptions](#hexagon.calendrier.exceptions)
* [hexagon.calendrier.fridays](#hexagon.calendrier.fridays)
* [hexagon.calendrier.market](#hexagon.calendrier.market)






    
# Module `hexagon.calendrier.aggregates` {#hexagon.calendrier.aggregates}







    
## Classes


    
### Class `CalendarDate` {#hexagon.calendrier.aggregates.CalendarDate}



> `class CalendarDate(date_marche: datetime.date, periode: str = None)`


CalendarDate(date_marche: datetime.date, periode: str = None)





    
#### Instance variables


    
##### Variable `date_marche` {#hexagon.calendrier.aggregates.CalendarDate.date_marche}

Return an attribute of instance, which is of type owner.

    
##### Variable `periode` {#hexagon.calendrier.aggregates.CalendarDate.periode}

Return an attribute of instance, which is of type owner.



    
### Class `CalendarPeriode` {#hexagon.calendrier.aggregates.CalendarPeriode}



> `class CalendarPeriode(date_marche: datetime.date, date_reference: datetime.date, periode: str = None)`


Une performance est une variation d'une valeur entre deux dates
et qui correspond à une période ayant un sens à l'utilisateur.

=> CalendarPeriode is the date part of the equation

Atributes:

- date_marche (<code>date</code>): date de valeur de la performance
- date_reference (<code>date</code>): date de référence de la performance
- periode (<code>str</code>): attribut à but indicatif, peut prendre any valeur



    
#### Descendants

* [hexagon.performance.aggregates.PerformanceDelta](#hexagon.performance.aggregates.PerformanceDelta)
* [hexagon.performance.aggregates.PerformanceSpot](#hexagon.performance.aggregates.PerformanceSpot)
* [hexagon.performance.aggregates.PerformanceValue](#hexagon.performance.aggregates.PerformanceValue)



    
#### Instance variables


    
##### Variable `date_marche` {#hexagon.calendrier.aggregates.CalendarPeriode.date_marche}

Return an attribute of instance, which is of type owner.

    
##### Variable `date_reference` {#hexagon.calendrier.aggregates.CalendarPeriode.date_reference}

Return an attribute of instance, which is of type owner.

    
##### Variable `njours` {#hexagon.calendrier.aggregates.CalendarPeriode.njours}

Retourne le nombre de jours entre la date de marché et la date de référence


###### Returns

<code>int</code>: Le nombre de jours entre les deux dates

    
##### Variable `periode` {#hexagon.calendrier.aggregates.CalendarPeriode.periode}

Return an attribute of instance, which is of type owner.



    
#### Methods


    
##### Method `islower` {#hexagon.calendrier.aggregates.CalendarPeriode.islower}



    
> `def islower(self, adate: datetime.date) -> bool`


Vérifie si la date est <code>strictement</code> inférieure à <code>self.date\_reference</code>

Args:

- adate (<code>date</code>): Date à vérifier

    
##### Method `isupper` {#hexagon.calendrier.aggregates.CalendarPeriode.isupper}



    
> `def isupper(self, adate: datetime.date) -> bool`


Vérifie si la date est <code>strictement</code> supérieurs à <code>self.date\_marche</code>

Args:

- adate (<code>date</code>): Date à vérifier

    
##### Method `to_dict` {#hexagon.calendrier.aggregates.CalendarPeriode.to_dict}



    
> `def to_dict(self, *, keys: list[str] = None) -> dict`


Retourne les attributs de l'objet as [attribute: value] pairs of a dict
keys = (date_marche, date_reference, periode)


###### Returns

<code>dict</code>: Représentation de l'instance en dict[attribute] = value

    
### Class `CalendarState` {#hexagon.calendrier.aggregates.CalendarState}



> `class CalendarState(ref: str, daily_min: datetime.date, daily_max: datetime.date, weekly_min: datetime.date, weekly_max: datetime.date, daily_count: int, weekly_count: int)`


Représente l'état actuel du calendrier pour un indice, fonds, ... (self.ref)









    
# Module `hexagon.calendrier.dao` {#hexagon.calendrier.dao}







    
## Classes


    
### Class `CalDaoRegEntry` {#hexagon.calendrier.dao.CalDaoRegEntry}



> `class CalDaoRegEntry(entity: Any, date_entity: Any, right_key: str, left_key: str = 'id', date_attr: str = 'date_marche')`


CalDaoRegEntry(entity, date_entity, right_key, left_key, date_attr)


    
#### Ancestors (in MRO)

* [builtins.tuple](#builtins.tuple)




    
#### Instance variables


    
##### Variable `date_attr` {#hexagon.calendrier.dao.CalDaoRegEntry.date_attr}

Alias for field number 4

    
##### Variable `date_entity` {#hexagon.calendrier.dao.CalDaoRegEntry.date_entity}

Alias for field number 1

    
##### Variable `entity` {#hexagon.calendrier.dao.CalDaoRegEntry.entity}

Alias for field number 0

    
##### Variable `left_key` {#hexagon.calendrier.dao.CalDaoRegEntry.left_key}

Alias for field number 3

    
##### Variable `right_key` {#hexagon.calendrier.dao.CalDaoRegEntry.right_key}

Alias for field number 2



    
### Class `CalendarDao` {#hexagon.calendrier.dao.CalendarDao}



> `class CalendarDao(dbsession: sqlalchemy.orm.session.Session)`





    
#### Ancestors (in MRO)

* [hexagon.infrastructure.query_executor.QueryExecutor](#hexagon.infrastructure.query_executor.QueryExecutor)





    
#### Static methods


    
##### `Method query_date_entity` {#hexagon.calendrier.dao.CalendarDao.query_date_entity}



    
> `def query_date_entity(date_entity, id_key: str, id_value: int, date_attr: str = 'date_marche') -> sqlalchemy.sql.selectable.Select`




    
##### `Method query_entity` {#hexagon.calendrier.dao.CalendarDao.query_entity}



    
> `def query_entity(entity, date_entity, filter_key: str, filter_value: Any, right_key: str, date_attr: str = 'date_marche', left_key: str = 'id') -> sqlalchemy.sql.selectable.Select`





    
#### Methods


    
##### Method `get_entity_dates` {#hexagon.calendrier.dao.CalendarDao.get_entity_dates}



    
> `def get_entity_dates(self, entity: Any, filter_key: str, filter_value: Any) -> list[datetime.date]`






    
# Module `hexagon.calendrier.enumerations` {#hexagon.calendrier.enumerations}







    
## Classes


    
### Class `FrequenceCalendar` {#hexagon.calendrier.enumerations.FrequenceCalendar}



> `class FrequenceCalendar(value, names=None, *, module=None, qualname=None, type=None, start=1, boundary=None)`


Fréquences supportées pour générer un vecteur de dates de performances
entre 2 dates. Chaque fréquence respecte le jargon du marché.

Attributes:

- QUOTIDIENNE (<code>str</code>): Fréquence quotidienne (seulement pour le mode quotidien)
- HEBDOMADAIRE (<code>str</code>): Fréquence hebdomadaire selon le mode du calendrier
- MENSUELLE (<code>str</code>): Fréquence mensuelle en respectant le MTD selon le mode
- TRIMESTRIELLE (<code>str</code>): Fréquence trimestrielle en respectant le QTD selon le mode
- SEMESTRIELLE (<code>str</code>): Fréquence semestrielle en respectant le STD selon le mode
- ANNUELLE (<code>str</code>): Fréquence annuelle en respectant le YTD selon le mode


    
#### Ancestors (in MRO)

* [enum.StrEnum](#enum.StrEnum)
* [builtins.str](#builtins.str)
* [enum.ReprEnum](#enum.ReprEnum)
* [enum.Enum](#enum.Enum)



    
#### Class variables


    
##### Variable `ANNUELLE` {#hexagon.calendrier.enumerations.FrequenceCalendar.ANNUELLE}



    
##### Variable `HEBDOMADAIRE` {#hexagon.calendrier.enumerations.FrequenceCalendar.HEBDOMADAIRE}



    
##### Variable `MENSUELLE` {#hexagon.calendrier.enumerations.FrequenceCalendar.MENSUELLE}



    
##### Variable `QUOTIDIENNE` {#hexagon.calendrier.enumerations.FrequenceCalendar.QUOTIDIENNE}



    
##### Variable `SEMESTRIELLE` {#hexagon.calendrier.enumerations.FrequenceCalendar.SEMESTRIELLE}



    
##### Variable `TRIMESTRIELLE` {#hexagon.calendrier.enumerations.FrequenceCalendar.TRIMESTRIELLE}






    
### Class `ModeCalendar` {#hexagon.calendrier.enumerations.ModeCalendar}



> `class ModeCalendar(value, names=None, *, module=None, qualname=None, type=None, start=1, boundary=None)`


Enumère les Deux modes du calendrier de performances de L'ASFIM

Attributes:

- HEBDOMADAIRE (<code>str</code>): Mode Calendar Weekly (utilise une fenêtre de semaines)
- QUOTIDIEN (<code>str</code>): Mode Daily (Decale de la période et prend le jour d'avant pour un jour férié)
- VL (<code>str</code>): Mode seulement pour CalendarFonds qui est traduit depuis la périodicité du Fonds.


    
#### Ancestors (in MRO)

* [enum.StrEnum](#enum.StrEnum)
* [builtins.str](#builtins.str)
* [enum.ReprEnum](#enum.ReprEnum)
* [enum.Enum](#enum.Enum)



    
#### Class variables


    
##### Variable `HEBDOMADAIRE` {#hexagon.calendrier.enumerations.ModeCalendar.HEBDOMADAIRE}



    
##### Variable `QUOTIDIEN` {#hexagon.calendrier.enumerations.ModeCalendar.QUOTIDIEN}



    
##### Variable `VL` {#hexagon.calendrier.enumerations.ModeCalendar.VL}






    
### Class `PeriodesMarket` {#hexagon.calendrier.enumerations.PeriodesMarket}



> `class PeriodesMarket(value, names=None, *, module=None, qualname=None, type=None, start=1, boundary=None)`


Enumère les périodes utilisées par nos métiers

Attributes:

- MTD (<code>str</code>): Month To date
- QTD (<code>str</code>): Quarter To date
- STD (<code>str</code>): Semester To date
- YTD (<code>str</code>): Year To date


    
#### Ancestors (in MRO)

* [enum.StrEnum](#enum.StrEnum)
* [builtins.str](#builtins.str)
* [enum.ReprEnum](#enum.ReprEnum)
* [enum.Enum](#enum.Enum)



    
#### Class variables


    
##### Variable `MTD` {#hexagon.calendrier.enumerations.PeriodesMarket.MTD}



    
##### Variable `QTD` {#hexagon.calendrier.enumerations.PeriodesMarket.QTD}



    
##### Variable `STD` {#hexagon.calendrier.enumerations.PeriodesMarket.STD}



    
##### Variable `YTD` {#hexagon.calendrier.enumerations.PeriodesMarket.YTD}








    
# Module `hexagon.calendrier.exceptions` {#hexagon.calendrier.exceptions}







    
## Classes


    
### Class `CalendarDateNotFound` {#hexagon.calendrier.exceptions.CalendarDateNotFound}



> `class CalendarDateNotFound(...)`


Quand l'historique des dates est court ou la date est introuvable


    
#### Ancestors (in MRO)

* [hexagon.calendrier.exceptions.CalendarError](#hexagon.calendrier.exceptions.CalendarError)
* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)






    
### Class `CalendarError` {#hexagon.calendrier.exceptions.CalendarError}



> `class CalendarError(...)`


Base Class for Calendar related Exceptions


    
#### Ancestors (in MRO)

* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)


    
#### Descendants

* [hexagon.calendrier.exceptions.CalendarDateNotFound](#hexagon.calendrier.exceptions.CalendarDateNotFound)







    
# Module `hexagon.calendrier.fridays` {#hexagon.calendrier.fridays}







    
## Classes


    
### Class `Fridays` {#hexagon.calendrier.fridays.Fridays}



> `class Fridays()`







    
#### Class variables


    
##### Variable `begin` {#hexagon.calendrier.fridays.Fridays.begin}



    
##### Variable `freq` {#hexagon.calendrier.fridays.Fridays.freq}



    
##### Variable `memo` {#hexagon.calendrier.fridays.Fridays.memo}



    
##### Variable `until` {#hexagon.calendrier.fridays.Fridays.until}



    
##### Variable `weekday` {#hexagon.calendrier.fridays.Fridays.weekday}





    
#### Static methods


    
##### `Method build_rrule` {#hexagon.calendrier.fridays.Fridays.build_rrule}



    
> `def build_rrule() -> dateutil.rrule.rrule`


rrule object avec la configuration des vendredis et fréquence hebdo


###### Returns

<code>rrule.rrule</code>: rrule object


    
#### Methods


    
##### Method `fridays_before` {#hexagon.calendrier.fridays.Fridays.fridays_before}



    
> `def fridays_before(self, end: datetime.date) -> sortedcontainers.sortedset.SortedSet`


Retourne les vendredis du calendrier avant end.
Le set est mémorisé avec la clé (None, end)

Args:

- end (<code>datetime.date</code>): Date de fin


###### Returns

<code>SortedSet</code>: Collection ordonnée des vendredis avant la date end

    
##### Method `fridays_between` {#hexagon.calendrier.fridays.Fridays.fridays_between}



    
> `def fridays_between(self, begin: datetime.date, end: datetime.date) -> sortedcontainers.sortedset.SortedSet`


Retourne les vendredis du calendrier entre begin et end.
Le set est mémorisé avec la clé (begin, end)

Args:

- begin (<code>datetime.date</code>): Date de début
- end (<code>datetime.date</code>): Date de fin


###### Returns

<code>SortedSet</code>: Collection ordonnée des vendredis entre les dates [begin et end]

    
##### Method `fridays_from` {#hexagon.calendrier.fridays.Fridays.fridays_from}



    
> `def fridays_from(self, begin: datetime.date) -> sortedcontainers.sortedset.SortedSet`


Retourne les vendredis du calendrier depuis la date begin
Le set est mémorisé avec la clé (begin, None)

Args:

- begin (<code>datetime.date</code>): Date de début


###### Returns

<code>SortedSet</code>: Collection ordonnée des vendredis depuis la date begin



    
# Module `hexagon.calendrier.market` {#hexagon.calendrier.market}

Module principal de calcul des périodes à des dates selon le calendrier de l'ASFIM




    
## Functions


    
### Function `refresh_cache_calendar` {#hexagon.calendrier.market.refresh_cache_calendar}



    
> `def refresh_cache_calendar()`





    
## Classes


    
### Class `MarketCalendar` {#hexagon.calendrier.market.MarketCalendar}



> `class MarketCalendar(daily_dates: list[datetime.date], mode_calendar: hexagon.calendrier.enumerations.ModeCalendar | str, ref: str = None)`


Génere les dates de référence a utiliser pour les dates de performance financières.

Args:

- daily_dates (List): Liste de dates passée en paramètre qui servira comme réference pour le calcul des dates hebdomadaires.



    
#### Descendants

* [hexagon.performance.calendar_fonds.CalendarFonds](#hexagon.performance.calendar_fonds.CalendarFonds)


    
#### Class variables


    
##### Variable `JARGON_PERIODES_MAPPER` {#hexagon.calendrier.market.MarketCalendar.JARGON_PERIODES_MAPPER}




    
#### Instance variables


    
##### Variable `vecteur_dates` {#hexagon.calendrier.market.MarketCalendar.vecteur_dates}

Si mode_calendar = <code>ModeCalendar.HEBDOMADAIRE</code>, Retourne <code>self.weekly\_dates</code>

Si mode_calendar = <code>ModeCalendar.QUOTIDIEN</code>, Retourne <code>self.daily\_dates</code>

Returns:

- <code>SortedSet</code>: Vecteur des dates selon le mode du calendrier.


    
#### Static methods


    
##### `Method build_hebdo_from_quotid` {#hexagon.calendrier.market.MarketCalendar.build_hebdo_from_quotid}



    
> `def build_hebdo_from_quotid(daily_dates: sortedcontainers.sortedset.SortedSet[datetime.date], freq: int = 2, weekday: int = FR) -> sortedcontainers.sortedset.SortedSet`


Construit une liste de dates Hebdomadaires depuis la liste quotidienne.
Nécessite une liste de jours ouvrables financiers comme repère.


###### Args

daily_dates (<code>SortedSet</code>[`datetime.date`]): Liste des jours ouvrables.
freq (<code>str</code>): <code>rrule.WEEKLY</code> pour les vendredis

###### Returns

<code>SortedSet</code>: liste de dates hebdo.

    
##### `Method clean_weekends` {#hexagon.calendrier.market.MarketCalendar.clean_weekends}



    
> `def clean_weekends(daily_dates: list[datetime.date], weekends: tuple[int] = (5, 6)) -> list[datetime.date]`




    
##### `Method clear_cache` {#hexagon.calendrier.market.MarketCalendar.clear_cache}



    
> `def clear_cache()`




    
##### `Method from_config` {#hexagon.calendrier.market.MarketCalendar.from_config}



    
> `def from_config(mode_calendar: hexagon.calendrier.enumerations.ModeCalendar | str, dbsession: sqlalchemy.orm.session.Session) -> Self`




    
##### `Method from_devise` {#hexagon.calendrier.market.MarketCalendar.from_devise}



    
> `def from_devise(code_devise: str, mode_calendar: hexagon.calendrier.enumerations.ModeCalendar | str, dbsession: sqlalchemy.orm.session.Session) -> Self`




    
##### `Method from_fonds` {#hexagon.calendrier.market.MarketCalendar.from_fonds}



    
> `def from_fonds(code_fonds: str, mode_calendar: hexagon.calendrier.enumerations.ModeCalendar | str, dbsession: sqlalchemy.orm.session.Session) -> Self`




    
##### `Method from_fonds_uid` {#hexagon.calendrier.market.MarketCalendar.from_fonds_uid}



    
> `def from_fonds_uid(fonds_uid: int, mode_calendar: hexagon.calendrier.enumerations.ModeCalendar | str, dbsession: sqlalchemy.orm.session.Session) -> Self`




    
##### `Method from_indice` {#hexagon.calendrier.market.MarketCalendar.from_indice}



    
> `def from_indice(code_indice: str, mode_calendar: hexagon.calendrier.enumerations.ModeCalendar | str, dbsession: sqlalchemy.orm.session.Session) -> Self`




    
##### `Method periode_to_hebdo` {#hexagon.calendrier.market.MarketCalendar.periode_to_hebdo}



    
> `def periode_to_hebdo(periode: str = '1:S') -> int`


Transforme une periode en nombre d'observations selon le calendrier de l'ASFIM.

l'ASFIM utilise une fenetre de taille fixe:
    - 1 sem = 1 observation
    - N mois = (4 * N) + mod(N, 3)
    - N ans = (52 * N) + mod(N, 5)

```python
>>> periode_to_hebdo(periode='1:A') = 52  # obs
>>> periode_to_hebdo(periode='5:A') = 261 # obs
>>> periode_to_hebdo(periode='1:M') = 4 # obs
>>> periode_to_hebdo(periode='3:M') = 13 # obs
```


Args:

- periode (<code>str</code>): Periode avec comme séparateur ":".


###### Returns

<code>int</code>: le nombre de semaines correspondant à la période.

###### Raises

<code>CalendarError</code>: If periode not in ('S', 'M', 'A')

    
##### `Method periode_to_quotid` {#hexagon.calendrier.market.MarketCalendar.periode_to_quotid}



    
> `def periode_to_quotid(periode: str = '1:J', future: bool = False) -> dateutil.relativedelta.relativedelta`


Construit un relativedelta historique pour la periode passée en paramètre.
A utiliser pour déduire les dates de performance marché.

```python
>>> periode_to_quotid(period='9:M') => relativedelta(-9 months)
>>> periode_to_quotid(period='3:A') => relativedelta(-3 years)
```


Args:

- future (<code>bool</code>): Si future retourne un relative delta positif à ajouter.
- periode (<code>str</code>): Periode avec comme séparateur ":".


###### Returns

<code>relativedelta</code>: un relativedelta correspondant à la periode passée en paramètre.

###### Raises

<code>CalendarError</code>: If periode not in ('J', S', 'M', 'A')


    
#### Methods


    
##### Method `between` {#hexagon.calendrier.market.MarketCalendar.between}



    
> `def between(self, start: datetime.date, end: datetime.date, byfreq: hexagon.calendrier.enumerations.FrequenceCalendar | str = 'H', aslist: bool = False) -> list[hexagon.calendrier.aggregates.CalendarPeriode]`


Retourne les dates entre 'start' et 'end' selon la fréquence choisie.

Args:

- start (<code>datetime.date</code>): date de début
- end (<code>datetime.date</code>): date de fin
- byfreq (<code>str</code>, H): par défaut 'H' (hebdomadaire), must be in ('H', 'Q', 'M', 'T', 'S', 'A')


###### Returns

<code>list</code>[`datetime.date`]: liste de dates entre les 2 dates selon la fréquence choisie.

    
##### Method `build_weekly_dates` {#hexagon.calendrier.market.MarketCalendar.build_weekly_dates}



    
> `def build_weekly_dates(self)`


Initialise la liste des dates hebdomadaires à partie de la liste des dates quotidiennes.

Supprime les fin de semestres pour les dates différentes des Vendredi.

    
##### Method `date_apres` {#hexagon.calendrier.market.MarketCalendar.date_apres}



    
> `def date_apres(self, adate: datetime.date) -> datetime.date`


Utilise la bisection pour retourner la date juste après celle fournie selon le mode du calendrier.
si Hebdomadaire retourne la prochaine semaine
si Quotidien retourne le jour d'après


###### Returns

<code>datetime.date</code>: La date suivante

    
##### Method `date_apres_ou_date` {#hexagon.calendrier.market.MarketCalendar.date_apres_ou_date}



    
> `def date_apres_ou_date(self, adate: datetime.date) -> datetime.date`


utilise la bisection pour retourner la date juste après celle fournie selon le mode du calendrier.
si Hebdomadaire retourne la prochaine semaine
si Quotidien retourne le jour d'après


###### Returns

<code>datetime.date</code>: La date suivante

    
##### Method `date_après` {#hexagon.calendrier.market.MarketCalendar.date_après}



    
> `def date_après(self, adate: datetime.date) -> datetime.date`


Utilise la bisection pour retourner la date juste après celle fournie selon le mode du calendrier.
si Hebdomadaire retourne la prochaine semaine
si Quotidien retourne le jour d'après


###### Returns

<code>datetime.date</code>: La date suivante

    
##### Method `date_avant` {#hexagon.calendrier.market.MarketCalendar.date_avant}



    
> `def date_avant(self, adate: datetime.date) -> datetime.date`


Utilise la bisection pour retourner la date juste avant celle fournie selon le mode du calendrier.

Si <code>ModeCalendar.HEBDOMADAIRE</code> retourne la semaine précédente,
Si ModeCalendar.QUOTIDIEN retourne le jour de la veille


###### Returns

<code>datetime.date</code>: La date d'avant

    
##### Method `date_avant_ou_date` {#hexagon.calendrier.market.MarketCalendar.date_avant_ou_date}



    
> `def date_avant_ou_date(self, adate: datetime.date) -> datetime.date`


utilise la bisection pour retourner la date juste avant celle fournie selon le mode du calendrier.
si Hebdomadaire retourne la semaine précédente
si Quotidien retourne le jour de la veille


###### Returns

<code>datetime.date</code>: La date d'avant

    
##### Method `date_reference` {#hexagon.calendrier.market.MarketCalendar.date_reference}



    
> `def date_reference(self, date_marche: datetime.date, periode: hexagon.calendrier.enumerations.PeriodesMarket | str) -> hexagon.calendrier.aggregates.CalendarPeriode`


Retourne la date de réference associée à la date de valeur, la période et la périodicité de l'instrument
afin de calculer la performance selon le calendrier de l'ASFIM.

Args:

- date_marche (<code>datetime.date</code>): date de valeur de l'évaluation de la performance
- periode (<code>PeriodesMarket</code>| <code>str</code>): Période décrite selon le tableau des périodes supportées


###### Returns

<code>CalendarPeriode</code>: Les dates date_reference et date_marche

    
##### Method `date_reference_many` {#hexagon.calendrier.market.MarketCalendar.date_reference_many}



    
> `def date_reference_many(self, date_marche: datetime.date, periodes: list[str], strict: bool = False) -> list[hexagon.calendrier.aggregates.CalendarPeriode]`


Retourne les dates de références pour plusieurs périodes.
La période est omise des résultats, si:

- La période est inadéquate au mode du calendrier ex: (période Jour pour le calendrier Hebdomadaire)
- La période demandée dépasse la limite de l'historique du calendrier

Si <code>strict</code> = True: Une exception est raised

Args:

- date_marche (<code>datetime.date</code>): Date de référence
- periodes (<code>list</code>[`str`]): Liste des périodes: ['1:S', '3:M', '26:S', '52:S', '1:A', '3:A']
- strict (<code>bool</code>, False): Garantit l'exhaustivité des périodes pour le résultat.


###### Returns

<code>list</code>[`CalendarPeriode`]: Liste des dates valides

###### Raises

<code>CalendarDateNotFound</code>, <code>CalendarError</code>

    
##### Method `date_référence` {#hexagon.calendrier.market.MarketCalendar.date_référence}



    
> `def date_référence(self, date_marche: datetime.date, periode: hexagon.calendrier.enumerations.PeriodesMarket | str) -> hexagon.calendrier.aggregates.CalendarPeriode`


Retourne la date de réference associée à la date de valeur, la période et la périodicité de l'instrument
afin de calculer la performance selon le calendrier de l'ASFIM.

Args:

- date_marche (<code>datetime.date</code>): date de valeur de l'évaluation de la performance
- periode (<code>PeriodesMarket</code>| <code>str</code>): Période décrite selon le tableau des périodes supportées


###### Returns

<code>CalendarPeriode</code>: Les dates date_reference et date_marche

    
##### Method `getstate` {#hexagon.calendrier.market.MarketCalendar.getstate}



    
> `def getstate(self) -> hexagon.calendrier.aggregates.CalendarState`


Retourne une description de l'état du Calendar


###### Returns

<code>CalendarState</code>: Etat du Calendar de reference (self.ref)

    
##### Method `intersection` {#hexagon.calendrier.market.MarketCalendar.intersection}



    
> `def intersection(self, other: Self, mode_calendar: hexagon.calendrier.enumerations.ModeCalendar = None) -> Self`


Construit une instance avec l'intersection des dates des 2 calendriers.

Fournir mode_calendar pour forcer le calendrier de l'intersection
sinon ils seront alignés au Mode Quotidien Si un des 2 calendriers est quotidien

Args:

- other (<code>Self</code>): MarketCalendar à croiser avec cette instance
- mode_calendar (<code>ModeCalendar</code>, None): si renseigné force le mode à utiliser


###### Returns

<code>Self</code>: Instance avec l'intersection des 2 calendriers

###### Raises

<code>CalendarError</code>: Si other n'est pas du même type que MarketCalendar

    
##### Method `is_hebdo` {#hexagon.calendrier.market.MarketCalendar.is_hebdo}



    
> `def is_hebdo(self, date_marche: datetime.date) -> bool`


Vérifie si la date passée est une date Hebdomadaire selon le calendrier Asfim

    
##### Method `is_quotidien` {#hexagon.calendrier.market.MarketCalendar.is_quotidien}



    
> `def is_quotidien(self, date_marche: datetime.date) -> bool`


Vérifie si la date passée est une date Quotidienne Ouvrable

    
##### Method `mtd` {#hexagon.calendrier.market.MarketCalendar.mtd}



    
> `def mtd(self, date_marche: datetime.date) -> hexagon.calendrier.aggregates.CalendarPeriode`


Retourne la date de début du mois auquel appartient la date passée en parametre.

Args:

- date_marche (<code>datetime.date</code>): date de valeur


###### Returns

<code>CalendarPeriode</code>: La date de début et de fin du month to date.

    
##### Method `mtd_date` {#hexagon.calendrier.market.MarketCalendar.mtd_date}



    
> `def mtd_date(self, date_marche: datetime.date) -> hexagon.calendrier.aggregates.CalendarPeriode`


Retourne la date de début du Mois comme date de début et la date passée en paramètre
comme date de fin si cette dernière est antérieure à la date de fin du mois courant.

```python
>>> self.mtd_date('27/03/2020')  = CalendarPeriode(date_reference=28/02/2020, date_marche=27/03/2020, periode='mtd')
```


Args:

- date_marche (<code>datetime.date</code>): date de valeur


###### Returns

<code>CalendarPeriode</code>: La date de début et la date demandée en date_marche.

    
##### Method `njours_ouvrables` {#hexagon.calendrier.market.MarketCalendar.njours_ouvrables}



    
> `def njours_ouvrables(self, debut: datetime.date, fin: datetime.date) -> int`


Retourne le nombre d'observations des Jours Ouvrable selon le mode du Calendar

Instancié: Si Quotidien, prend les jours Quotidiens, sinon le vecteur des dates Hebdomadaires
Args:

- debut (<code>datetime.date</code>): Date de début
- fin (<code>datetime.date</code>): Date de fin

    
##### Method `periode_annee` {#hexagon.calendrier.market.MarketCalendar.periode_annee}



    
> `def periode_annee(self, annee: int, periode: str = None) -> hexagon.calendrier.aggregates.CalendarPeriode`


Détermine la période <code>Annuelle</code> de l'année rensignée selon le mode du calendrier

Construit la période selon la forme {Année}. 2023, 2022, etc

Args:

- annee (<code>int</code>): L'année de la période
- mois (<code>int</code>): Le mois de la période
- periode (<code>str</code>, None): Période pour nommer la période demandée


###### Raises

<code>CalendarError</code>: Si l'année n'y figure pas

    
##### Method `periode_mois` {#hexagon.calendrier.market.MarketCalendar.periode_mois}



    
> `def periode_mois(self, annee: int, mois: int, periode: str = None) -> hexagon.calendrier.aggregates.CalendarPeriode`


Détermine la période <code>Mensuelle</code> du mois de l'année rensignée selon le mode du calendrier

Construit la période selon la forme {Année}-{Mois}{numéro}. 2023-M1, 2022-M3, 2022-M8

Args:

- annee (<code>int</code>): L'année de la période
- mois (<code>int</code>): Le mois de la période
- periode (<code>str</code>, None): Période pour nommer la période demandée


###### Raises

<code>CalendarError</code>: Si le mois est mal défini

    
##### Method `periode_semestre` {#hexagon.calendrier.market.MarketCalendar.periode_semestre}



    
> `def periode_semestre(self, annee: int, mois: int, periode: str = None) -> hexagon.calendrier.aggregates.CalendarPeriode`


Détermine la période <code>Semestrielle</code> du mois de l'année rensignée selon le mode du calendrier

Construit la période selon la forme {Année}-{Semestre}{numéro}. 2023-S1, 2022-S2, etc

Args:

- annee (<code>int</code>): L'année de la période
- mois (<code>int</code>): Le mois de la période
- periode (<code>str</code>, None): Période pour nommer la période demandée


###### Raises

<code>CalendarError</code>: Si le mois est mal défini

    
##### Method `periode_trimestre` {#hexagon.calendrier.market.MarketCalendar.periode_trimestre}



    
> `def periode_trimestre(self, annee: int, mois: int, periode: str = None) -> hexagon.calendrier.aggregates.CalendarPeriode`


Détermine la période <code>Trimestrielle</code> du mois de l'année rensignée selon le mode du calendrier

Construit la période selon la forme {Année}-{Trimestre}{numéro}. 2023-T1, 2022-T3, etc

Args:

- annee (<code>int</code>): L'année de la période
- mois (<code>int</code>): Le mois de la période
- periode (<code>str</code>, None): Période pour nommer la période demandée


###### Raises

<code>CalendarError</code>: Si le mois est mal défini

    
##### Method `période_année` {#hexagon.calendrier.market.MarketCalendar.période_année}



    
> `def période_année(self, annee: int, periode: str = None) -> hexagon.calendrier.aggregates.CalendarPeriode`


Détermine la période <code>Annuelle</code> de l'année rensignée selon le mode du calendrier

Construit la période selon la forme {Année}. 2023, 2022, etc

Args:

- annee (<code>int</code>): L'année de la période
- mois (<code>int</code>): Le mois de la période
- periode (<code>str</code>, None): Période pour nommer la période demandée


###### Raises

<code>CalendarError</code>: Si l'année n'y figure pas

    
##### Method `période_mois` {#hexagon.calendrier.market.MarketCalendar.période_mois}



    
> `def période_mois(self, annee: int, mois: int, periode: str = None) -> hexagon.calendrier.aggregates.CalendarPeriode`


Détermine la période <code>Mensuelle</code> du mois de l'année rensignée selon le mode du calendrier

Construit la période selon la forme {Année}-{Mois}{numéro}. 2023-M1, 2022-M3, 2022-M8

Args:

- annee (<code>int</code>): L'année de la période
- mois (<code>int</code>): Le mois de la période
- periode (<code>str</code>, None): Période pour nommer la période demandée


###### Raises

<code>CalendarError</code>: Si le mois est mal défini

    
##### Method `période_semestre` {#hexagon.calendrier.market.MarketCalendar.période_semestre}



    
> `def période_semestre(self, annee: int, mois: int, periode: str = None) -> hexagon.calendrier.aggregates.CalendarPeriode`


Détermine la période <code>Semestrielle</code> du mois de l'année rensignée selon le mode du calendrier

Construit la période selon la forme {Année}-{Semestre}{numéro}. 2023-S1, 2022-S2, etc

Args:

- annee (<code>int</code>): L'année de la période
- mois (<code>int</code>): Le mois de la période
- periode (<code>str</code>, None): Période pour nommer la période demandée


###### Raises

<code>CalendarError</code>: Si le mois est mal défini

    
##### Method `période_trimestre` {#hexagon.calendrier.market.MarketCalendar.période_trimestre}



    
> `def période_trimestre(self, annee: int, mois: int, periode: str = None) -> hexagon.calendrier.aggregates.CalendarPeriode`


Détermine la période <code>Trimestrielle</code> du mois de l'année rensignée selon le mode du calendrier

Construit la période selon la forme {Année}-{Trimestre}{numéro}. 2023-T1, 2022-T3, etc

Args:

- annee (<code>int</code>): L'année de la période
- mois (<code>int</code>): Le mois de la période
- periode (<code>str</code>, None): Période pour nommer la période demandée


###### Raises

<code>CalendarError</code>: Si le mois est mal défini

    
##### Method `qtd` {#hexagon.calendrier.market.MarketCalendar.qtd}



    
> `def qtd(self, date_marche: datetime.date) -> hexagon.calendrier.aggregates.CalendarPeriode`


Retourne la date de début et de fin du trimestre auquel appartient la date passée en parametre.

Args:

- date_marche (<code>datetime.date</code>): date de valeur


###### Returns

<code>CalendarPeriode</code>: La date de début et de fin du quarter to date (Trimestre)

    
##### Method `qtd_date` {#hexagon.calendrier.market.MarketCalendar.qtd_date}



    
> `def qtd_date(self, date_marche: datetime.date) -> hexagon.calendrier.aggregates.CalendarPeriode`


Retourne la date de début du Trimestre comme date de début et la date passée en paramètre
comme date de fin si cette dernière est antérieure à la date de fin du trimestre courant.

```python
>>> self.qtd_date('27/03/2020')  = CalendarPeriode(date_reference=27/12/2019, date_marche=27/03/2020, periode='qtd')
```


Args:

- date_marche (<code>datetime.date</code>): date de valeur


###### Returns

<code>CalendarPeriode</code>: La date de début et la date demandée en date_marche.

    
##### Method `std` {#hexagon.calendrier.market.MarketCalendar.std}



    
> `def std(self, date_marche: datetime.date) -> hexagon.calendrier.aggregates.CalendarPeriode`


Retourne la date de début et de fin du semestre auquel appartient la date passée en parametre.

Args:

- date_marche (<code>datetime.date</code>): date de valeur


###### Returns

<code>CalendarPeriode</code>: La date de début et de fin du semester to date (Semestre).

    
##### Method `std_date` {#hexagon.calendrier.market.MarketCalendar.std_date}



    
> `def std_date(self, date_marche: datetime.date) -> hexagon.calendrier.aggregates.CalendarPeriode`


Retourne la date de début du Semestre comme date de début et la date passée en paramètre
comme date de fin si cette dernière est antérieure à la date de fin du semestre courant.

```python
>>> self.std_date('27/03/2020')  = CalendarPeriode(date_reference=27/09/2019, date_marche=27/03/2020, periode='std')
```


Args:

- date_marche (<code>datetime.date</code>): date de valeur


###### Returns

<code>CalendarPeriode</code>: La date de début et la date demandée en date_marche.

    
##### Method `union` {#hexagon.calendrier.market.MarketCalendar.union}



    
> `def union(self, other: Self, mode_calendar: hexagon.calendrier.enumerations.ModeCalendar = None) -> Self`


Construit une instance avec l'union des dates des 2 calendriers.

Fournir mode_calendar pour forcer le calendrier de l'union
sinon ils seront alignés au Mode Quotidien Si un des 2 calendriers est quotidien

Args:

- other (<code>Self</code>): MarketCalendar à croiser avec cette instance
- mode_calendar (<code>ModeCalendar</code>, None): si renseigné force le mode à utiliser


###### Returns

<code>Self</code>: Instance avec l'union des 2 calendriers

###### Raises

<code>CalendarError</code>: Si other n'est pas du même type MarketCalendar

    
##### Method `ytd` {#hexagon.calendrier.market.MarketCalendar.ytd}



    
> `def ytd(self, date_marche: datetime.date) -> hexagon.calendrier.aggregates.CalendarPeriode`


Retourne la date de début et de fin de l'année à laquelle appartient la date passée en paramètre.

```python
>>> self.ytd('01/06/2019') = CalendarPeriode(date_reference=28/12/2018, date_marche=27/12/2019, periode='ytd')
```


Args:

- date_marche (<code>datetime.date</code>): date de valeur


###### Returns

<code>CalendarPeriode</code>: La date de début et de fin du year to date.

    
##### Method `ytd_date` {#hexagon.calendrier.market.MarketCalendar.ytd_date}



    
> `def ytd_date(self, date_marche: datetime.date) -> hexagon.calendrier.aggregates.CalendarPeriode`


Retourne la date de début d'année comme date de début et la date passée en paramètre
comme date de fin si cette dernière est antérieure à la date de fin d'année.

```python
>>> self.ytd_date('01/06/2019')  = CalendarPeriode(date_reference=28/12/2018, date_marche=01/06/2019, periode='ytd')
```


Args:

- date_marche (<code>datetime.date</code>): date de valeur


###### Returns

<code>CalendarPeriode</code>: La date de début et la date demandée en date_marche.



    
# Module `hexagon.core` {#hexagon.core}

=========================
Domain Driven Reminder
=========================

# Core Domain

Le **Core Domain** définit les entités ou leurs abstractions, ces entités définissent le périmètre
conceptuel du projet Hexagon - **Plateforme Data et de calcul avec Capacité d'automatisation de 
la production des reporting**.

Le core domain offrira une interface commune de communication entre les différents sous domaines,
il est conçu autour du domaine/monde de la finance et plus spécifiquement **la gestion des OCPVM**.

Principalement il sera composé de CoreEntity, CoreEntityIterables, CoreValue et CoreValueIterables.

Les entités sont définies par les propriétés suivantes:

  - **CoreEntity**:
      - Identifiables
      - Peuvent être mutables
      - Composite

  - **CoreValue**:
      - Représente ou décrit une mesure
      - Equivalent
      - Remplaçable
      - De préférence *Immutable*

### Principes et Guidelines

  1. Le choix des noms des **Dossiers**, **Fichiers**, **Classes**, **Variables**, ...
  Doit être le plus proche possible de la réalité ou du jargon utilisé par les collaborateurs du métier.
  2. Equilibrer entre les noms en *Français* et en *Anglais*
  3. Rester cohérent sur la définition et l'intention désirée.
  4. Doivent être des Classes Python, Enums, DataClasses, et la librairie standard 
  5. Ne doivent dépendre d'aucune librairie tierce:
      - PAS de **Sqlalchemy**, **pandas.DataFrame**, **Pydantic**, **Openpyxl**, **Pendulum**, Rien, Nada ..
      - **EXCEPTION** pour numpy.array, attrs et sortedcontainers


==================================================
# Domain Driven Design Reminder
==================================================

## Entities
  - Identifiable
  - May be mutable
  - Composite

## Values
  - Measurable/Describe
  - Equivalent
  - Replaceable
  - Self-contained
  - Preferably Immutable

## Aggregates
  - Consistency boundaries
  - Transaction/Unit of work modifies only one aggregate
  - not just object clusters
  - May only have one entity

## Aggregate Root Entity
  - Maintains Aggregate consistency
  - Hosts commands/methods which modify the aggregate
  - Target for inbound aggregate references
  - all inter-aggregates references are by root ID

## Domain Events
  - Something which happens which domain experts care about
  - Significant state transitions
  - Publish/Subscribe rahter than subject-observer

  Side note:
    Not needed in hexagon which is not a transactional system, however there are still 
    some internal events but are better implemented as exceptions,
    this way the caller is subscribed by design.

## Factories
  - Facilitate entity and Aggregate construction
  - Allow us to express the ubiquitous language - verbs rather than nouns (constructors)
  - Hide construction details. ex: ID Generation, collections instantiation

## Repositories
  - Stores aggregates
  - Usually one repo per Aggregate type
  - Abstraction over persistence mechanism
  - Architecturally significant

## Bounded Context
  - Scope of a ubiquitous language
  - Segregated models
  - Independent, autonomous implementations
  - Align with technical components
  - Loosely coupled

## Context Maps
  - Integration between Bounded Contexts
    - Logical Mapping (concepts)
    - Physical Mapping (actual software)
  - Many integration patterns:
    - Anti-corruption layer
    - Open host service formalize as Published Language


    
## Sub-modules

* [hexagon.core.date_periode](#hexagon.core.date_periode)
* [hexagon.core.entities](#hexagon.core.entities)
* [hexagon.core.enumerations](#hexagon.core.enumerations)
* [hexagon.core.repository](#hexagon.core.repository)
* [hexagon.core.valueobjects](#hexagon.core.valueobjects)






    
# Module `hexagon.core.date_periode` {#hexagon.core.date_periode}






    
## Functions


    
### Function `periodes_from_str` {#hexagon.core.date_periode.periodes_from_str}



    
> `def periodes_from_str(str_periodes: str, list_separator: str = ',', periode_separator: str = ':', sens: int = -1) -> list[hexagon.core.date_periode.DatePeriode]`





    
## Classes


    
### Class `CalendarPeriode` {#hexagon.core.date_periode.CalendarPeriode}



> `class CalendarPeriode(periode: str)`





    
#### Ancestors (in MRO)

* [hexagon.core.date_periode.DatePeriode](#hexagon.core.date_periode.DatePeriode)






    
### Class `DatePeriode` {#hexagon.core.date_periode.DatePeriode}



> `class DatePeriode(periode: str, separator: str, sens: int)`






    
#### Descendants

* [hexagon.core.date_periode.CalendarPeriode](#hexagon.core.date_periode.CalendarPeriode)
* [hexagon.courbe.courbe_utils.CourbePeriode](#hexagon.courbe.courbe_utils.CourbePeriode)





    
#### Methods


    
##### Method `to_days` {#hexagon.core.date_periode.DatePeriode.to_days}



    
> `def to_days(self, date_valeur: datetime.date) -> int`




    
##### Method `to_relativedelta` {#hexagon.core.date_periode.DatePeriode.to_relativedelta}



    
> `def to_relativedelta(self) -> dateutil.relativedelta.relativedelta`




    
##### Method `to_relativedict` {#hexagon.core.date_periode.DatePeriode.to_relativedict}



    
> `def to_relativedict(self) -> dict`




    
##### Method `to_timedelta` {#hexagon.core.date_periode.DatePeriode.to_timedelta}



    
> `def to_timedelta(self, date_valeur: datetime.date) -> datetime.timedelta`




    
##### Method `validate` {#hexagon.core.date_periode.DatePeriode.validate}



    
> `def validate(self)`


Valide le format et la périodidcté de la période qui est supportée.
affecte les attributs: self.nombre et self.step


###### Raises

<code>[DatePeriodeError](#hexagon.core.date\_periode.DatePeriodeError "hexagon.core.date\_periode.DatePeriodeError")</code>
:   • Si le séparateur est invalide
                  • Si la période n'est pas un entier positif
                  • Si le sens n'est pas IN (-1, 1)



    
### Class `DatePeriodeError` {#hexagon.core.date_periode.DatePeriodeError}



> `class DatePeriodeError(...)`


Common base class for all non-exit exceptions.


    
#### Ancestors (in MRO)

* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)






    
### Class `StepPeriode` {#hexagon.core.date_periode.StepPeriode}



> `class StepPeriode(value, names=None, *, module=None, qualname=None, type=None, start=1, boundary=None)`


Périodicités supportées pour l'interpolation
    * 4:J = 4 Jours
    * 4:S = 4 Semaines
    * 4:M = 4 Mois
    * 4:A = 4 Ans


    
#### Ancestors (in MRO)

* [enum.StrEnum](#enum.StrEnum)
* [builtins.str](#builtins.str)
* [enum.ReprEnum](#enum.ReprEnum)
* [enum.Enum](#enum.Enum)



    
#### Class variables


    
##### Variable `A` {#hexagon.core.date_periode.StepPeriode.A}



    
##### Variable `J` {#hexagon.core.date_periode.StepPeriode.J}



    
##### Variable `M` {#hexagon.core.date_periode.StepPeriode.M}



    
##### Variable `S` {#hexagon.core.date_periode.StepPeriode.S}








    
# Module `hexagon.core.entities` {#hexagon.core.entities}







    
## Classes


    
### Class `CAggregate` {#hexagon.core.entities.CAggregate}



> `class CAggregate()`


Method generated by attrs for class CAggregate.







    
### Class `CValue` {#hexagon.core.entities.CValue}



> `class CValue()`


Method generated by attrs for class CValue.







    
### Class `Cactions` {#hexagon.core.entities.Cactions}



> `class Cactions(uid: int = -1, code: str = None, description: str = None, code_isin: str = None, classe: str = None, categorie: str = None, emetteur: str = None, secteur: str = None, devise=None, ticker: str = None, type_cotation: hexagon.core.enumerations.TypeCotation = 0, place_cotation: str = None)`


Method generated by attrs for class Cactions.


    
#### Ancestors (in MRO)

* [hexagon.core.entities.Ctitre](#hexagon.core.entities.Ctitre)
* [hexagon.core.entities.CoreEntity](#hexagon.core.entities.CoreEntity)




    
#### Instance variables


    
##### Variable `place_cotation` {#hexagon.core.entities.Cactions.place_cotation}

Return an attribute of instance, which is of type owner.

    
##### Variable `ticker` {#hexagon.core.entities.Cactions.ticker}

Return an attribute of instance, which is of type owner.

    
##### Variable `type_cotation` {#hexagon.core.entities.Cactions.type_cotation}

Return an attribute of instance, which is of type owner.



    
### Class `Cbenchmark` {#hexagon.core.entities.Cbenchmark}



> `class Cbenchmark(uid: int = -1, code: str = None, description: str = None, fonds: hexagon.core.entities.Cfonds = _Nothing.NOTHING)`


Method generated by attrs for class Cbenchmark.


    
#### Ancestors (in MRO)

* [hexagon.core.entities.CoreEntity](#hexagon.core.entities.CoreEntity)


    
#### Descendants

* [hexagon.performance.datastructures.PFBenchmark](#hexagon.performance.datastructures.PFBenchmark)


    
#### Class variables


    
##### Variable `type_benchmark` {#hexagon.core.entities.Cbenchmark.type_benchmark}




    
#### Instance variables


    
##### Variable `fonds` {#hexagon.core.entities.Cbenchmark.fonds}

Return an attribute of instance, which is of type owner.



    
### Class `Cdevise` {#hexagon.core.entities.Cdevise}



> `class Cdevise(uid: int = -1, code: str = None, description: str = None)`


Method generated by attrs for class Cdevise.


    
#### Ancestors (in MRO)

* [hexagon.core.entities.CoreEntity](#hexagon.core.entities.CoreEntity)


    
#### Descendants

* [hexagon.performance.datastructures.PFDevise](#hexagon.performance.datastructures.PFDevise)





    
### Class `Cfonds` {#hexagon.core.entities.Cfonds}



> `class Cfonds(uid: int = -1, code: str = None, description: str = None, code_isin: str = None, categorie: hexagon.core.enumerations.CategorieFonds = OPCVM, periodicite: hexagon.core.enumerations.PeriodiciteVL = H, affectation_resultat: hexagon.core.enumerations.AffectationResultat = MIXTE, type_gestion: hexagon.core.enumerations.TypeGestion = ND, forme_juridique: hexagon.core.enumerations.FormeJuridique = FCP, date_creation: datetime.date = None, date_premiere_vl: datetime.date = None, date_premiere_souscription: datetime.date = None)`


Method generated by attrs for class Cfonds.


    
#### Ancestors (in MRO)

* [hexagon.core.entities.CoreEntity](#hexagon.core.entities.CoreEntity)


    
#### Descendants

* [hexagon.performance.datastructures.PFFonds](#hexagon.performance.datastructures.PFFonds)



    
#### Instance variables


    
##### Variable `affectation_resultat` {#hexagon.core.entities.Cfonds.affectation_resultat}

Return an attribute of instance, which is of type owner.

    
##### Variable `categorie` {#hexagon.core.entities.Cfonds.categorie}

Return an attribute of instance, which is of type owner.

    
##### Variable `code_isin` {#hexagon.core.entities.Cfonds.code_isin}

Return an attribute of instance, which is of type owner.

    
##### Variable `date_creation` {#hexagon.core.entities.Cfonds.date_creation}

Return an attribute of instance, which is of type owner.

    
##### Variable `date_premiere_souscription` {#hexagon.core.entities.Cfonds.date_premiere_souscription}

Return an attribute of instance, which is of type owner.

    
##### Variable `date_premiere_vl` {#hexagon.core.entities.Cfonds.date_premiere_vl}

Return an attribute of instance, which is of type owner.

    
##### Variable `forme_juridique` {#hexagon.core.entities.Cfonds.forme_juridique}

Return an attribute of instance, which is of type owner.

    
##### Variable `is_distribuant` {#hexagon.core.entities.Cfonds.is_distribuant}



    
##### Variable `periodicite` {#hexagon.core.entities.Cfonds.periodicite}

Return an attribute of instance, which is of type owner.

    
##### Variable `type_gestion` {#hexagon.core.entities.Cfonds.type_gestion}

Return an attribute of instance, which is of type owner.



    
### Class `Cindice` {#hexagon.core.entities.Cindice}



> `class Cindice(uid: int = -1, code: str = None, description: str = None, devise: str = None)`


Method generated by attrs for class Cindice.


    
#### Ancestors (in MRO)

* [hexagon.core.entities.CoreEntity](#hexagon.core.entities.CoreEntity)


    
#### Descendants

* [hexagon.performance.datastructures.PFIndice](#hexagon.performance.datastructures.PFIndice)



    
#### Instance variables


    
##### Variable `devise` {#hexagon.core.entities.Cindice.devise}

Return an attribute of instance, which is of type owner.



    
### Class `CindiceComposite` {#hexagon.core.entities.CindiceComposite}



> `class CindiceComposite(uid: int = -1, code: str = None, description: str = None, composition: list[hexagon.core.entities.Cindice] = _Nothing.NOTHING)`


Method generated by attrs for class CindiceComposite.


    
#### Ancestors (in MRO)

* [hexagon.core.entities.CoreEntity](#hexagon.core.entities.CoreEntity)


    
#### Descendants

* [hexagon.performance.datastructures.PFIndiceComposite](#hexagon.performance.datastructures.PFIndiceComposite)



    
#### Instance variables


    
##### Variable `composition` {#hexagon.core.entities.CindiceComposite.composition}

Return an attribute of instance, which is of type owner.



    
### Class `Cinventaire` {#hexagon.core.entities.Cinventaire}



> `class Cinventaire(uid: int = -1, code: str = None, description: str = None, date_marche: datetime.date = None, date_inventaire: datetime.date = None, fonds: hexagon.core.entities.Cfonds = _Nothing.NOTHING, composition: list[hexagon.core.entities.Ctitre] = _Nothing.NOTHING, devise: str = None)`


Method generated by attrs for class Cinventaire.


    
#### Ancestors (in MRO)

* [hexagon.core.entities.CoreEntity](#hexagon.core.entities.CoreEntity)




    
#### Instance variables


    
##### Variable `composition` {#hexagon.core.entities.Cinventaire.composition}

Return an attribute of instance, which is of type owner.

    
##### Variable `date_inventaire` {#hexagon.core.entities.Cinventaire.date_inventaire}

Return an attribute of instance, which is of type owner.

    
##### Variable `date_marche` {#hexagon.core.entities.Cinventaire.date_marche}

Return an attribute of instance, which is of type owner.

    
##### Variable `devise` {#hexagon.core.entities.Cinventaire.devise}

Return an attribute of instance, which is of type owner.

    
##### Variable `fonds` {#hexagon.core.entities.Cinventaire.fonds}

Return an attribute of instance, which is of type owner.



    
### Class `Cobligation` {#hexagon.core.entities.Cobligation}



> `class Cobligation(uid: int = -1, code: str = None, description: str = None, code_isin: str = None, classe: str = None, categorie: str = None, emetteur: str = None, secteur: str = None, devise=None, nominal: float = None, emission: datetime.date = None, jouissance: datetime.date = None, echeance: datetime.date = None, revision: datetime.date = None, taux_facial: float = None, spread: float = None, type_garantie: hexagon.core.enumerations.TypeGarantie = 0, type_cotation: hexagon.core.enumerations.TypeCotation = 0, qt_emise: float = None, periodicite_coupon: hexagon.core.enumerations.PeriodiciteCoupon = A, type_coupon: hexagon.core.enumerations.TypeCoupon = FIXE, rang: hexagon.core.enumerations.RangObligation = SENIOR, mode_remboursement: hexagon.core.enumerations.ModeRemboursement = INFINE, periodicite_remboursement: hexagon.core.enumerations.PeriodiciteCoupon = A)`


Method generated by attrs for class Cobligation.


    
#### Ancestors (in MRO)

* [hexagon.core.entities.Ctitre](#hexagon.core.entities.Ctitre)
* [hexagon.core.entities.CoreEntity](#hexagon.core.entities.CoreEntity)




    
#### Instance variables


    
##### Variable `echeance` {#hexagon.core.entities.Cobligation.echeance}

Return an attribute of instance, which is of type owner.

    
##### Variable `emission` {#hexagon.core.entities.Cobligation.emission}

Return an attribute of instance, which is of type owner.

    
##### Variable `jouissance` {#hexagon.core.entities.Cobligation.jouissance}

Return an attribute of instance, which is of type owner.

    
##### Variable `mode_remboursement` {#hexagon.core.entities.Cobligation.mode_remboursement}

Return an attribute of instance, which is of type owner.

    
##### Variable `nominal` {#hexagon.core.entities.Cobligation.nominal}

Return an attribute of instance, which is of type owner.

    
##### Variable `periodicite_coupon` {#hexagon.core.entities.Cobligation.periodicite_coupon}

Return an attribute of instance, which is of type owner.

    
##### Variable `periodicite_remboursement` {#hexagon.core.entities.Cobligation.periodicite_remboursement}

Return an attribute of instance, which is of type owner.

    
##### Variable `qt_emise` {#hexagon.core.entities.Cobligation.qt_emise}

Return an attribute of instance, which is of type owner.

    
##### Variable `rang` {#hexagon.core.entities.Cobligation.rang}

Return an attribute of instance, which is of type owner.

    
##### Variable `revision` {#hexagon.core.entities.Cobligation.revision}

Return an attribute of instance, which is of type owner.

    
##### Variable `seniorite` {#hexagon.core.entities.Cobligation.seniorite}



    
##### Variable `spread` {#hexagon.core.entities.Cobligation.spread}

Return an attribute of instance, which is of type owner.

    
##### Variable `taux_facial` {#hexagon.core.entities.Cobligation.taux_facial}

Return an attribute of instance, which is of type owner.

    
##### Variable `type_cotation` {#hexagon.core.entities.Cobligation.type_cotation}

Return an attribute of instance, which is of type owner.

    
##### Variable `type_coupon` {#hexagon.core.entities.Cobligation.type_coupon}

Return an attribute of instance, which is of type owner.

    
##### Variable `type_garantie` {#hexagon.core.entities.Cobligation.type_garantie}

Return an attribute of instance, which is of type owner.



    
### Class `CoreEntity` {#hexagon.core.entities.CoreEntity}



> `class CoreEntity(uid: int = -1, code: str = None, description: str = None)`


Method generated by attrs for class CoreEntity.



    
#### Descendants

* [hexagon.core.entities.Cbenchmark](#hexagon.core.entities.Cbenchmark)
* [hexagon.core.entities.Cdevise](#hexagon.core.entities.Cdevise)
* [hexagon.core.entities.Cfonds](#hexagon.core.entities.Cfonds)
* [hexagon.core.entities.Cindice](#hexagon.core.entities.Cindice)
* [hexagon.core.entities.CindiceComposite](#hexagon.core.entities.CindiceComposite)
* [hexagon.core.entities.Cinventaire](#hexagon.core.entities.Cinventaire)
* [hexagon.core.entities.Csouscripteur](#hexagon.core.entities.Csouscripteur)
* [hexagon.core.entities.Ctitre](#hexagon.core.entities.Ctitre)
* [hexagon.performance.datastructures.FondsCashOut](#hexagon.performance.datastructures.FondsCashOut)
* [hexagon.performance.datastructures.PFEntity](#hexagon.performance.datastructures.PFEntity)
* [hexagon.veille.entities.VeilleFonds](#hexagon.veille.entities.VeilleFonds)



    
#### Instance variables


    
##### Variable `code` {#hexagon.core.entities.CoreEntity.code}

Return an attribute of instance, which is of type owner.

    
##### Variable `core_entity` {#hexagon.core.entities.CoreEntity.core_entity}



    
##### Variable `description` {#hexagon.core.entities.CoreEntity.description}

Return an attribute of instance, which is of type owner.

    
##### Variable `uid` {#hexagon.core.entities.CoreEntity.uid}

Return an attribute of instance, which is of type owner.


    
#### Static methods


    
##### `Method from_any` {#hexagon.core.entities.CoreEntity.from_any}



    
> `def from_any(other: typing.Any | collections.abc.Mapping | collections.abc.Sequence, mapping: collections.abc.Mapping = None) -> Self`


Construit une instance depuis un objet ayant les mêmes attributs, ça permet d'éviter
cls(uid=other.id, code=other.code, ....)
un mapping peut être passé pour traduire les attributs de Self vers other:
i.e: {'uid': 'id', 'code': 'code_fonds'}


###### Args

**```other```** :&ensp;<code>Any</code>
:   Objet supportant getattr()


**```mapping```** :&ensp;<code>dict</code>, optional
:   Mapping des self.champs vers other.other.champs:
    {'uid': 'id', 'code': 'code_xy', 'description': 'desc'}




    
#### Methods


    
##### Method `cache_key` {#hexagon.core.entities.CoreEntity.cache_key}



    
> `def cache_key(self) -> str`




    
### Class `Csouscripteur` {#hexagon.core.entities.Csouscripteur}



> `class Csouscripteur(uid: int = -1, code: str = None, description: str = None, nom: str = None)`


Method generated by attrs for class Csouscripteur.


    
#### Ancestors (in MRO)

* [hexagon.core.entities.CoreEntity](#hexagon.core.entities.CoreEntity)



    
#### Class variables


    
##### Variable `type_souscripteur` {#hexagon.core.entities.Csouscripteur.type_souscripteur}




    
#### Instance variables


    
##### Variable `nom` {#hexagon.core.entities.Csouscripteur.nom}

Return an attribute of instance, which is of type owner.



    
### Class `Ctitre` {#hexagon.core.entities.Ctitre}



> `class Ctitre(uid: int = -1, code: str = None, description: str = None, code_isin: str = None, classe: str = None, categorie: str = None, emetteur: str = None, secteur: str = None, devise=None)`


Method generated by attrs for class Ctitre.


    
#### Ancestors (in MRO)

* [hexagon.core.entities.CoreEntity](#hexagon.core.entities.CoreEntity)


    
#### Descendants

* [hexagon.core.entities.Cactions](#hexagon.core.entities.Cactions)
* [hexagon.core.entities.Cobligation](#hexagon.core.entities.Cobligation)



    
#### Instance variables


    
##### Variable `categorie` {#hexagon.core.entities.Ctitre.categorie}

Return an attribute of instance, which is of type owner.

    
##### Variable `classe` {#hexagon.core.entities.Ctitre.classe}

Return an attribute of instance, which is of type owner.

    
##### Variable `code_isin` {#hexagon.core.entities.Ctitre.code_isin}

Return an attribute of instance, which is of type owner.

    
##### Variable `devise` {#hexagon.core.entities.Ctitre.devise}

Return an attribute of instance, which is of type owner.

    
##### Variable `emetteur` {#hexagon.core.entities.Ctitre.emetteur}

Return an attribute of instance, which is of type owner.

    
##### Variable `secteur` {#hexagon.core.entities.Ctitre.secteur}

Return an attribute of instance, which is of type owner.





    
# Module `hexagon.core.enumerations` {#hexagon.core.enumerations}

Enumérations des principales propriétés des concepts du domaine <code>[hexagon.core](#hexagon.core "hexagon.core")</code> tel que:

**<code>[CategorieFonds](#hexagon.core.enumerations.CategorieFonds "hexagon.core.enumerations.CategorieFonds")</code>**, **<code>[TypeGestion](#hexagon.core.enumerations.TypeGestion "hexagon.core.enumerations.TypeGestion")</code>**, **<code>[ModeRemboursement](#hexagon.core.enumerations.ModeRemboursement "hexagon.core.enumerations.ModeRemboursement")</code>**, **<code>[TypeCoupon](#hexagon.core.enumerations.TypeCoupon "hexagon.core.enumerations.TypeCoupon")</code>**, ...





    
## Classes


    
### Class `AffectationResultat` {#hexagon.core.enumerations.AffectationResultat}



> `class AffectationResultat(value, names=None, *, module=None, qualname=None, type=None, start=1, boundary=None)`


Type d'Affection des résultats de l'OPCVM.
Distribuant, Capitalisant ...


#### Attributes

- CAPITALISATION (str='CAPITALISATION'): Le Fonds est Capitalisant
- DISTRIBUTION (str='DISTRIBUTION'): Le Fonds est Distribuant
- MIXTE (str='MIXTE'): Le Fonds est Mixte. Capitalisant et distribuant


    
#### Ancestors (in MRO)

* [enum.StrEnum](#enum.StrEnum)
* [builtins.str](#builtins.str)
* [enum.ReprEnum](#enum.ReprEnum)
* [enum.Enum](#enum.Enum)



    
#### Class variables


    
##### Variable `CAPITALISATION` {#hexagon.core.enumerations.AffectationResultat.CAPITALISATION}



    
##### Variable `DISTRIBUTION` {#hexagon.core.enumerations.AffectationResultat.DISTRIBUTION}



    
##### Variable `MIXTE` {#hexagon.core.enumerations.AffectationResultat.MIXTE}






    
### Class `CategorieFonds` {#hexagon.core.enumerations.CategorieFonds}



> `class CategorieFonds(value, names=None, *, module=None, qualname=None, type=None, start=1, boundary=None)`


Catégories des OPCVM selon la classification de l'AMMC


#### Attributes

- MONETAIRE (str='MON'): Catégorie MONETAIRE
- OCT (str='OCT'): Catégorie OCT
- OMLT (str='OMLT'): Catégorie OMLT
- ACTIONS (str='ACT'): Catégorie ACTIONS
- DIVERSIFIE (str='DIV'): Catégorie DIVERSIFIE
- CONTRACTUEL (str='CON'): Catégorie CONTRACTUEL
- OPCVM (str='OPCVM'): Catégorie OPCVM, Valeur par défaut si le champs n'est pas Défini


    
#### Ancestors (in MRO)

* [enum.StrEnum](#enum.StrEnum)
* [builtins.str](#builtins.str)
* [enum.ReprEnum](#enum.ReprEnum)
* [enum.Enum](#enum.Enum)



    
#### Class variables


    
##### Variable `ACTIONS` {#hexagon.core.enumerations.CategorieFonds.ACTIONS}



    
##### Variable `CONTRACTUEL` {#hexagon.core.enumerations.CategorieFonds.CONTRACTUEL}



    
##### Variable `DIVERSIFIE` {#hexagon.core.enumerations.CategorieFonds.DIVERSIFIE}



    
##### Variable `MONETAIRE` {#hexagon.core.enumerations.CategorieFonds.MONETAIRE}



    
##### Variable `OCT` {#hexagon.core.enumerations.CategorieFonds.OCT}



    
##### Variable `OMLT` {#hexagon.core.enumerations.CategorieFonds.OMLT}



    
##### Variable `OPCVM` {#hexagon.core.enumerations.CategorieFonds.OPCVM}






    
### Class `FormeJuridique` {#hexagon.core.enumerations.FormeJuridique}



> `class FormeJuridique(value, names=None, *, module=None, qualname=None, type=None, start=1, boundary=None)`


Forme juridique des OPCVM tel que défini par l'AMMC, SICAV ou FCP


#### Attributes

- FCP (str='FCP'): Fonds commun en placements
- SICAV (str='SICAV'): Société d'investissement collectif à actions variables
- ND (str='ND'): Non Défini/Valeur non renseignée


    
#### Ancestors (in MRO)

* [enum.StrEnum](#enum.StrEnum)
* [builtins.str](#builtins.str)
* [enum.ReprEnum](#enum.ReprEnum)
* [enum.Enum](#enum.Enum)



    
#### Class variables


    
##### Variable `FCP` {#hexagon.core.enumerations.FormeJuridique.FCP}



    
##### Variable `ND` {#hexagon.core.enumerations.FormeJuridique.ND}



    
##### Variable `SICAV` {#hexagon.core.enumerations.FormeJuridique.SICAV}






    
### Class `Include` {#hexagon.core.enumerations.Include}



> `class Include(value, names=None, *, module=None, qualname=None, type=None, start=1, boundary=None)`


Enumère les inclusions des intervalles au sens mathématique.
Cette classe est à utiliser wherever we have to define an interval.


#### Attributes

- BOTH (str): Inclut les 2 Bornes [min-max]. Interalle [fermé-fermé]
- LEFT (str): Inclut La Borne gauche [min-max[. Interalle [fermé-ouvert[
- RIGHT (str): Inclut La Borne droite ]min-max]. Interalle ]ouvert-fermé]
- NONE (str): Exclut les 2 Bornes ]min-max[. Interalle ]ouvert-ouvert[


    
#### Ancestors (in MRO)

* [enum.StrEnum](#enum.StrEnum)
* [builtins.str](#builtins.str)
* [enum.ReprEnum](#enum.ReprEnum)
* [enum.Enum](#enum.Enum)



    
#### Class variables


    
##### Variable `BOTH` {#hexagon.core.enumerations.Include.BOTH}



    
##### Variable `LEFT` {#hexagon.core.enumerations.Include.LEFT}



    
##### Variable `NONE` {#hexagon.core.enumerations.Include.NONE}



    
##### Variable `RIGHT` {#hexagon.core.enumerations.Include.RIGHT}






    
### Class `ModeRemboursement` {#hexagon.core.enumerations.ModeRemboursement}



> `class ModeRemboursement(value, names=None, *, module=None, qualname=None, type=None, start=1, boundary=None)`


Le mode de remboursement du capital de l'obligation. INFINE, AMOR, PERPET...


#### Attributes

- INFINE (str='INFINE'): Le capital est remboursé à l'echéance avec le dernier coupon
- AMORTISSABLE (str='AMORTISSABLE'): Le capital est amorti linéairement à chaque coupon
- PERPETUELLE (str='PERPETUELLE'): Dépend de l'émission. Le capital n'est pas remboursé, l'obligation tourne à perpétuité


    
#### Ancestors (in MRO)

* [enum.StrEnum](#enum.StrEnum)
* [builtins.str](#builtins.str)
* [enum.ReprEnum](#enum.ReprEnum)
* [enum.Enum](#enum.Enum)



    
#### Class variables


    
##### Variable `AMORTISSABLE` {#hexagon.core.enumerations.ModeRemboursement.AMORTISSABLE}



    
##### Variable `INFINE` {#hexagon.core.enumerations.ModeRemboursement.INFINE}



    
##### Variable `PERPETUELLE` {#hexagon.core.enumerations.ModeRemboursement.PERPETUELLE}






    
### Class `PeriodiciteCoupon` {#hexagon.core.enumerations.PeriodiciteCoupon}



> `class PeriodiciteCoupon(value, names=None, *, module=None, qualname=None, type=None, start=1, boundary=None)`


Périodicité du coupon de l'obligation. Les périodiciét standardes A, S, T, ...


#### Attributes

- BIMENSUELLE (str='B'): Le coupon est bimensuel
- MENSUELLE (str='M'): Le coupon est mensuel
- TRIMESTRIELLE (str='T'): Le coupon est trimestriel
- SEMESTRIELLE (str='S'): Le coupon est semestriel
- ANNUELLE (str='A'): Le coupon est annuel


    
#### Ancestors (in MRO)

* [enum.StrEnum](#enum.StrEnum)
* [builtins.str](#builtins.str)
* [enum.ReprEnum](#enum.ReprEnum)
* [enum.Enum](#enum.Enum)



    
#### Class variables


    
##### Variable `ANNUELLE` {#hexagon.core.enumerations.PeriodiciteCoupon.ANNUELLE}



    
##### Variable `BIMENSUELLE` {#hexagon.core.enumerations.PeriodiciteCoupon.BIMENSUELLE}



    
##### Variable `MENSUELLE` {#hexagon.core.enumerations.PeriodiciteCoupon.MENSUELLE}



    
##### Variable `SEMESTRIELLE` {#hexagon.core.enumerations.PeriodiciteCoupon.SEMESTRIELLE}



    
##### Variable `TRIMESTRIELLE` {#hexagon.core.enumerations.PeriodiciteCoupon.TRIMESTRIELLE}






    
### Class `PeriodiciteVL` {#hexagon.core.enumerations.PeriodiciteVL}



> `class PeriodiciteVL(value, names=None, *, module=None, qualname=None, type=None, start=1, boundary=None)`


Périodicité des VL des OPCVM. Quotidien, Hebdomadaire, ND


#### Attributes

- QUOTIDIEN (str='Q'): Fonds Quotidien
- HEBDOMADAIRE (str='H'): Fonds Hebdomadaire
- ND (str='ND'): Non Défini/Valeur non renseignée


    
#### Ancestors (in MRO)

* [enum.StrEnum](#enum.StrEnum)
* [builtins.str](#builtins.str)
* [enum.ReprEnum](#enum.ReprEnum)
* [enum.Enum](#enum.Enum)



    
#### Class variables


    
##### Variable `HEBDOMADAIRE` {#hexagon.core.enumerations.PeriodiciteVL.HEBDOMADAIRE}



    
##### Variable `ND` {#hexagon.core.enumerations.PeriodiciteVL.ND}



    
##### Variable `QUOTIDIEN` {#hexagon.core.enumerations.PeriodiciteVL.QUOTIDIEN}






    
### Class `RangObligation` {#hexagon.core.enumerations.RangObligation}



> `class RangObligation(value, names=None, *, module=None, qualname=None, type=None, start=1, boundary=None)`


Le rang ou la séniorité de l'obligation. Senior, Subordonnée


#### Attributes

- SENIOR (str='SENIOR'): L'obligation est Normale. Le premier lors dune liquidation de l'emetteur
- SUBORDONEE (str='SUBORDONEE'): L'obligation est subordonnée. Le dernier lors d'une liquidation.


    
#### Ancestors (in MRO)

* [enum.StrEnum](#enum.StrEnum)
* [builtins.str](#builtins.str)
* [enum.ReprEnum](#enum.ReprEnum)
* [enum.Enum](#enum.Enum)



    
#### Class variables


    
##### Variable `SENIOR` {#hexagon.core.enumerations.RangObligation.SENIOR}



    
##### Variable `SUBORDONEE` {#hexagon.core.enumerations.RangObligation.SUBORDONEE}






    
### Class `TypeCotation` {#hexagon.core.enumerations.TypeCotation}



> `class TypeCotation(value, names=None, *, module=None, qualname=None, type=None, start=1, boundary=None)`


Type de cotation du Titre financier: Coté/Non Coté


#### Attributes

- COTE (int=1): Le titre est coté à la SBVC
- NONCOTE (int=0): Le titre n'est pas coté à SBVC


    
#### Ancestors (in MRO)

* [enum.IntEnum](#enum.IntEnum)
* [builtins.int](#builtins.int)
* [enum.ReprEnum](#enum.ReprEnum)
* [enum.Enum](#enum.Enum)



    
#### Class variables


    
##### Variable `COTE` {#hexagon.core.enumerations.TypeCotation.COTE}



    
##### Variable `NONCOTE` {#hexagon.core.enumerations.TypeCotation.NONCOTE}






    
### Class `TypeCoupon` {#hexagon.core.enumerations.TypeCoupon}



> `class TypeCoupon(value, names=None, *, module=None, qualname=None, type=None, start=1, boundary=None)`


Type du coupon de l'Obligation. Fixe ou Révisable


#### Attributes

- FIX (str='FIXE'): Taux Fixe
- FIXE (str='FIX'): Description
- REVISABLE (str='REV'): Description


    
#### Ancestors (in MRO)

* [enum.StrEnum](#enum.StrEnum)
* [builtins.str](#builtins.str)
* [enum.ReprEnum](#enum.ReprEnum)
* [enum.Enum](#enum.Enum)



    
#### Class variables


    
##### Variable `FIX` {#hexagon.core.enumerations.TypeCoupon.FIX}



    
##### Variable `FIXE` {#hexagon.core.enumerations.TypeCoupon.FIXE}



    
##### Variable `REVISABLE` {#hexagon.core.enumerations.TypeCoupon.REVISABLE}






    
### Class `TypeGarantie` {#hexagon.core.enumerations.TypeGarantie}



> `class TypeGarantie(value, names=None, *, module=None, qualname=None, type=None, start=1, boundary=None)`


Type de garantie de l'obligation. Garanti par le trésor/l'état ou non Garanti


#### Attributes

- GARANTIE (int=1): L'obligation est garanti par l'état
- NONGARANTIE (int=0): L'obligation est non garanti par l'état


    
#### Ancestors (in MRO)

* [enum.IntEnum](#enum.IntEnum)
* [builtins.int](#builtins.int)
* [enum.ReprEnum](#enum.ReprEnum)
* [enum.Enum](#enum.Enum)



    
#### Class variables


    
##### Variable `GARANTIE` {#hexagon.core.enumerations.TypeGarantie.GARANTIE}



    
##### Variable `NONGARANTIE` {#hexagon.core.enumerations.TypeGarantie.NONGARANTIE}






    
### Class `TypeGestion` {#hexagon.core.enumerations.TypeGestion}



> `class TypeGestion(value, names=None, *, module=None, qualname=None, type=None, start=1, boundary=None)`


Type de gestion du Fonds. Grand publique ou Dédié


#### Attributes

- PUBLIQUE (str='P'): Fonds Grand public (GP: chez l'ASFIM)
- DEDIE (str='D'): Fonds Dédié (FNPP: chez l'ASFIM)
- ND (str='ND'): Non Défini/Valeur non renseignée


    
#### Ancestors (in MRO)

* [enum.StrEnum](#enum.StrEnum)
* [builtins.str](#builtins.str)
* [enum.ReprEnum](#enum.ReprEnum)
* [enum.Enum](#enum.Enum)



    
#### Class variables


    
##### Variable `DEDIE` {#hexagon.core.enumerations.TypeGestion.DEDIE}



    
##### Variable `ND` {#hexagon.core.enumerations.TypeGestion.ND}



    
##### Variable `PUBLIQUE` {#hexagon.core.enumerations.TypeGestion.PUBLIQUE}








    
# Module `hexagon.core.repository` {#hexagon.core.repository}






    
## Functions


    
### Function `struct_core_entity` {#hexagon.core.repository.struct_core_entity}



    
> `def struct_core_entity(core_cls: hexagon.core.entities.CoreEntity) -> sqlalchemy.sql.selectable.Select`


Requête par défaut des Core entities


###### Args

**```cls_```** :&ensp;<code>CoreEntity</code>
:   Classe utilisée pour construir l'Entité



###### Returns

<code>Select</code>
:   Requête Sqlalchemy




    
## Classes


    
### Class `CoreEntityRepository` {#hexagon.core.repository.CoreEntityRepository}



> `class CoreEntityRepository(core_entity: hexagon.core.entities.CoreEntity, dbsession: sqlalchemy.orm.session.Session)`





    
#### Ancestors (in MRO)

* [hexagon.infrastructure.query_executor.QueryExecutor](#hexagon.infrastructure.query_executor.QueryExecutor)





    
#### Static methods


    
##### `Method get_dbmodel` {#hexagon.core.repository.CoreEntityRepository.get_dbmodel}



    
> `def get_dbmodel(core_cls: hexagon.core.entities.CoreEntity) -> sqlalchemy.orm.decl_api.Base`




    
##### `Method struct_core_fonds` {#hexagon.core.repository.CoreEntityRepository.struct_core_fonds}



    
> `def struct_core_fonds() -> sqlalchemy.sql.selectable.Select`




    
##### `Method struct_core_titre` {#hexagon.core.repository.CoreEntityRepository.struct_core_titre}



    
> `def struct_core_titre() -> sqlalchemy.sql.selectable.Select`





    
#### Methods


    
##### Method `load_core_entity` {#hexagon.core.repository.CoreEntityRepository.load_core_entity}



    
> `def load_core_entity(self, uid: list[int] = None, code: list[str] = None) -> list[hexagon.core.entities.CoreEntity]`




    
##### Method `load_core_fonds` {#hexagon.core.repository.CoreEntityRepository.load_core_fonds}



    
> `def load_core_fonds(self, uid: list[int] = None, code: list[str] = None, categorie: list[str] = None, type_gestion: list[str] = None, affectation_resultat: list[str] = None) -> list[hexagon.core.entities.Cfonds]`




    
##### Method `load_core_titre` {#hexagon.core.repository.CoreEntityRepository.load_core_titre}



    
> `def load_core_titre(self, uid: list[int] = None, code: list[str] = None, classe: list[str] = None, categorie: list[str] = None, emetteur: list[str] = None, secteur: list[str] = None, devise: list[str] = None) -> list[hexagon.core.entities.Ctitre]`






    
# Module `hexagon.core.valueobjects` {#hexagon.core.valueobjects}







    
## Classes


    
### Class `CdeviseCours` {#hexagon.core.valueobjects.CdeviseCours}



> `class CdeviseCours(vuid: int = -1, refuid: int = -1, date_marche: datetime.date = None, ref: hexagon.core.entities.Cdevise = None, code: str = None, cours: float = 1, code_devise_locale: str = 'MAD')`


Method generated by attrs for class CdeviseCours.


    
#### Ancestors (in MRO)

* [hexagon.core.valueobjects.CoreValue](#hexagon.core.valueobjects.CoreValue)




    
#### Instance variables


    
##### Variable `code` {#hexagon.core.valueobjects.CdeviseCours.code}

Return an attribute of instance, which is of type owner.

    
##### Variable `code_devise_locale` {#hexagon.core.valueobjects.CdeviseCours.code_devise_locale}

Return an attribute of instance, which is of type owner.

    
##### Variable `cours` {#hexagon.core.valueobjects.CdeviseCours.cours}

Return an attribute of instance, which is of type owner.


    
#### Static methods


    
##### `Method at_date` {#hexagon.core.valueobjects.CdeviseCours.at_date}



    
> `def at_date(date_marche: datetime.date, dbsession: sqlalchemy.orm.session.Session) -> list[typing.Self]`




    
##### `Method struct_query` {#hexagon.core.valueobjects.CdeviseCours.struct_query}



    
> `def struct_query() -> sqlalchemy.sql.selectable.Select`





    
### Class `CoreValue` {#hexagon.core.valueobjects.CoreValue}



> `class CoreValue(vuid: int = -1, refuid: int = -1, ref: hexagon.core.entities.CoreEntity = None, date_marche: datetime.date = None)`


Method generated by attrs for class CoreValue.



    
#### Descendants

* [hexagon.core.valueobjects.CdeviseCours](#hexagon.core.valueobjects.CdeviseCours)
* [hexagon.veille.entities.VeilleAggregate](#hexagon.veille.entities.VeilleAggregate)
* [hexagon.veille.entities.VeilleValue](#hexagon.veille.entities.VeilleValue)



    
#### Instance variables


    
##### Variable `date_marche` {#hexagon.core.valueobjects.CoreValue.date_marche}

Return an attribute of instance, which is of type owner.

    
##### Variable `ref` {#hexagon.core.valueobjects.CoreValue.ref}

Return an attribute of instance, which is of type owner.

    
##### Variable `refuid` {#hexagon.core.valueobjects.CoreValue.refuid}

Return an attribute of instance, which is of type owner.

    
##### Variable `vuid` {#hexagon.core.valueobjects.CoreValue.vuid}

Return an attribute of instance, which is of type owner.





    
# Module `hexagon.courbe` {#hexagon.courbe}




    
## Sub-modules

* [hexagon.courbe.common](#hexagon.courbe.common)
* [hexagon.courbe.courbe](#hexagon.courbe.courbe)
* [hexagon.courbe.enumerations](#hexagon.courbe.enumerations)
* [hexagon.courbe.exceptions](#hexagon.courbe.exceptions)
* [hexagon.courbe.interpolator](#hexagon.courbe.interpolator)






    
# Module `hexagon.courbe.common` {#hexagon.courbe.common}







    
## Classes


    
### Class `CourbeBase` {#hexagon.courbe.common.CourbeBase}



> `class CourbeBase(points_courbe: list[hexagon.courbe.common.PointCourbe] = None, date_courbe: datetime.date = None, auto_sort: bool = True)`


Représente une courbe des taux dont les points sont des PointCourbe
Cette classe offre des fonctionnalités adaptées à la manipulation de la courbe
Implémente une interface similaire à celle des list


#### Attributes

**```points_courbe```** :&ensp;<code>list</code>
:   liste d'objets PointCourbe




    
#### Ancestors (in MRO)

* [collections.abc.Collection](#collections.abc.Collection)
* [collections.abc.Sized](#collections.abc.Sized)
* [collections.abc.Iterable](#collections.abc.Iterable)
* [collections.abc.Container](#collections.abc.Container)





    
#### Static methods


    
##### `Method from_records` {#hexagon.courbe.common.CourbeBase.from_records}



    
> `def from_records(records: list[dict], date_courbe: datetime.date = None) -> Self`


Instancie une nouvelle courbe depuis une Liste de dict dict[maturite: taux] = (maturite: taux)


###### Args

**```records```** :&ensp;<code>list\[dict]</code>
:   Liste de dict: record = {'maturite': maturite,  'taux': taux}



###### Returns

<code>[CourbeBase](#hexagon.courbe.common.CourbeBase "hexagon.courbe.common.CourbeBase")</code>
:   Nouvelle instance avec comme input la liste de dicts fournie



    
##### `Method from_rows` {#hexagon.courbe.common.CourbeBase.from_rows}



    
> `def from_rows(collection: list[tuple[int, float]], date_courbe: datetime.date = None) -> Self`


Instancie une nouvelle courbe depuis une Liste de tuples (maturite, taux)


###### Args

**```collection```** :&ensp;<code>list</code>
:   Liste du couple (maturite, taux)



###### Returns

<code>[CourbeBase](#hexagon.courbe.common.CourbeBase "hexagon.courbe.common.CourbeBase")</code>
:   Nouvelle instance avec comme input la liste de tuples fournie




    
#### Methods


    
##### Method `add_point` {#hexagon.courbe.common.CourbeBase.add_point}



    
> `def add_point(self, maturite: int, taux: float)`


Ajoute un point courbe à la courbe des taux
Procède à la création du PointCourbe depuis les inputs


###### Args

**```maturite```** :&ensp;<code>int</code>
:   maturite en jours


**```taux```** :&ensp;<code>float</code>
:   taux en nombre naturel ==> 2.5% = 0.025



###### Raises

<code>CourbePointError</code>
:   Si la maturité existe dans la courbe



    
##### Method `borne_inf` {#hexagon.courbe.common.CourbeBase.borne_inf}



    
> `def borne_inf(self, point: hexagon.courbe.common.PointCourbe) -> hexagon.courbe.common.PointCourbe`


Retourne le point dont la maturité
est juste supérieure à la maturité du point passé en paramètre -> le min des maxs


###### Args

**```point```** :&ensp;<code>[PointCourbe](#hexagon.courbe.common.PointCourbe "hexagon.courbe.common.PointCourbe")</code>
:   point à rechercher (par maturite)



###### Returns

<code>[PointCourbe](#hexagon.courbe.common.PointCourbe "hexagon.courbe.common.PointCourbe")</code>
:   Le point Courbe de la borne inférieure



###### Raises

<code>ValueError</code>
:   Si la courbe est vide ou que la maturité recherchée est très grande



    
##### Method `borne_sup` {#hexagon.courbe.common.CourbeBase.borne_sup}



    
> `def borne_sup(self, point: hexagon.courbe.common.PointCourbe) -> hexagon.courbe.common.PointCourbe`


Retourne le point dont la maturité
est juste inférieure à la maturité du point passé en paramètre -> le max des mins


###### Args

**```point```** :&ensp;<code>[PointCourbe](#hexagon.courbe.common.PointCourbe "hexagon.courbe.common.PointCourbe")</code>
:   point à rechercher (par maturite)



###### Returns

<code>[PointCourbe](#hexagon.courbe.common.PointCourbe "hexagon.courbe.common.PointCourbe")</code>
:   Le point Courbe de la borne supérieure



###### Raises

<code>ValueError</code>
:   Si la courbe est vide ou que la maturité recherchée est très petite



    
##### Method `duplicate` {#hexagon.courbe.common.CourbeBase.duplicate}



    
> `def duplicate(self) -> Self`


Retourne une nouvelle instance avec duplication des points courbe


###### Returns

<code>[CourbeBase](#hexagon.courbe.common.CourbeBase "hexagon.courbe.common.CourbeBase")</code>
:   Nouvelle instance clone



    
##### Method `insert` {#hexagon.courbe.common.CourbeBase.insert}



    
> `def insert(self, pos: int, point: hexagon.courbe.common.PointCourbe)`


Insère une PointCourbe à la position <code>pos</code>
Permet d'implémenter la même interface qu'une liste python


###### Args

**```pos```** :&ensp;<code>int</code>
:   Zero-Based Index position


point (`PointCourbe): Point à insérer dans la collection.

###### Raises

<code>ValueError</code>
:   Si un point de même maturité existe déjà dans la courbe



    
##### Method `replace_point` {#hexagon.courbe.common.CourbeBase.replace_point}



    
> `def replace_point(self, point_courbe: hexagon.courbe.common.PointCourbe)`


Remplace un point de même maturité, la maturité doit être présente dans la courbe


###### Args

**```point_courbe```** :&ensp;<code>[PointCourbe](#hexagon.courbe.common.PointCourbe "hexagon.courbe.common.PointCourbe")</code>
:   Le point doit avoir la même maturité qu'un point présent



###### Raises

<code>CourbePointError</code>
:   Si un point de même maturité n'existe pas dans la liste



    
##### Method `supprime_maturite` {#hexagon.courbe.common.CourbeBase.supprime_maturite}



    
> `def supprime_maturite(self, maturite: int) -> hexagon.courbe.common.PointCourbe`


Supprime un point ayant la maturite <code>maturite</code> et retourne le point supprimé


###### Args

**```maturite```** :&ensp;<code>int</code>
:   Maturite à supprimer en Jours



    
##### Method `supprime_point` {#hexagon.courbe.common.CourbeBase.supprime_point}



    
> `def supprime_point(self, point_courbe: hexagon.courbe.common.PointCourbe)`


Supprime le point passe en paramètre, le point doit être présent!


###### Args

**```point_courbe```** :&ensp;<code>[PointCourbe](#hexagon.courbe.common.PointCourbe "hexagon.courbe.common.PointCourbe")</code>
:   Point à supprimer



    
##### Method `to_arrays` {#hexagon.courbe.common.CourbeBase.to_arrays}



    
> `def to_arrays(self) -> dict[str, numpy.ndarray]`


Construit un dictionnaire contenant les vecteurs de maturité et des taux
ex: dico = {'maturite': np.array(maturités), 'taux': np.array(taux)}


###### Returns

<code>dict</code>
:   dict['maturite', 'taux']



    
### Class `PointCourbe` {#hexagon.courbe.common.PointCourbe}



> `class PointCourbe(maturite: int = 1, taux: float = 0.0)`


Représente un point de la courbe des taux

La comparaison se fait sur la base de la maturité seulement


#### Attributes

**```maturite```** :&ensp;`int=1`
:   La maturite du point en Jours


**```taux```** :&ensp;`float=0.0`
:   Le taux du point en unités 2% => 0.02







    
#### Instance variables


    
##### Variable `maturite` {#hexagon.courbe.common.PointCourbe.maturite}

Return an attribute of instance, which is of type owner.

    
##### Variable `taux` {#hexagon.courbe.common.PointCourbe.taux}

Return an attribute of instance, which is of type owner.



    
#### Methods


    
##### Method `duplicate` {#hexagon.courbe.common.PointCourbe.duplicate}



    
> `def duplicate(self) -> Self`




    
##### Method `to_dict` {#hexagon.courbe.common.PointCourbe.to_dict}



    
> `def to_dict(self) -> dict[int, float]`


Retourne les attributs de l'objet as [attribute: value] pairs of a dict
keys = (maturite, taux)


###### Returns

<code>dict</code>
:   Représentation de l'instance en dict[attribute] = value



    
##### Method `with_maturite` {#hexagon.courbe.common.PointCourbe.with_maturite}



    
> `def with_maturite(self, maturite: int) -> Self`


Retourne une nouvelle instance avec la nouvelle maturité et garde l'ancien taux


###### Args

**```maturite```** :&ensp;<code>int</code>
:   Maturité correspondant au taux



###### Returns

<code>[PointCourbe](#hexagon.courbe.common.PointCourbe "hexagon.courbe.common.PointCourbe")</code>
:   Une nouvelle instance avec la nouvelle maturité



    
##### Method `with_taux` {#hexagon.courbe.common.PointCourbe.with_taux}



    
> `def with_taux(self, taux: float) -> Self`


Retourne une nouvelle instance avec le nouveaux taux et garde l'ancienne maturité


###### Args

**```taux```** :&ensp;<code>float</code>
:   Taux correspondant à la maturité



###### Returns

<code>[PointCourbe](#hexagon.courbe.common.PointCourbe "hexagon.courbe.common.PointCourbe")</code>
:   Une nouvelle instance avec le nouveaux taux





    
# Module `hexagon.courbe.courbe` {#hexagon.courbe.courbe}







    
## Classes


    
### Class `CourbeBAM` {#hexagon.courbe.courbe.CourbeBAM}



> `class CourbeBAM(date_courbe: datetime.date, courbe: hexagon.courbe.common.CourbeBase, *, mode_actuariel: hexagon.courbe.enumerations.ModeActuariel = 366, mode_courbe: hexagon.courbe.enumerations.ModeCourbe = ZC)`










    
#### Methods


    
##### Method `interpolate_maturite` {#hexagon.courbe.courbe.CourbeBAM.interpolate_maturite}



    
> `def interpolate_maturite(self, maturite: int, *, mode: hexagon.courbe.enumerations.ModeCourbe = BAM) -> hexagon.courbe.common.PointCourbe`




    
##### Method `interpolate_maturite_many` {#hexagon.courbe.courbe.CourbeBAM.interpolate_maturite_many}



    
> `def interpolate_maturite_many(self, maturite_liste: list[int], *, mode: hexagon.courbe.enumerations.ModeCourbe = BAM) -> list[hexagon.courbe.common.PointCourbe]`


Interpole plusieurs maturités selon le mode demandé

###### Args

**```maturite_liste```** :&ensp;<code>list</code>
:   Liste des maturités à rechercher



###### Returns

list[`PointCourbe`]: Liste des interpolations des maturités

    
##### Method `interpolate_periode` {#hexagon.courbe.courbe.CourbeBAM.interpolate_periode}



    
> `def interpolate_periode(self, periode: str, *, mode: hexagon.courbe.enumerations.ModeCourbe = BAM) -> hexagon.courbe.common.PointCourbe`




    
##### Method `interpolate_periode_many` {#hexagon.courbe.courbe.CourbeBAM.interpolate_periode_many}



    
> `def interpolate_periode_many(self, periode_liste: list[str], *, mode: hexagon.courbe.enumerations.ModeCourbe = BAM) -> list[hexagon.courbe.common.PointCourbe]`


Interpole plusieurs périodes telles que définies par relativedelta et selon le mode de la courbe

###### Args

**```periode_liste```** :&ensp;<code>list</code>
:   Liste des périodes à rechercher list[Dict]



###### Returns

list[`PointCourbe`]: Liste des interpolations des maturités

    
##### Method `tenors` {#hexagon.courbe.courbe.CourbeBAM.tenors}



    
> `def tenors(self, mode: hexagon.courbe.enumerations.ModeCourbe = BAM, detail: bool = False) -> list[hexagon.courbe.common.PointCourbe]`


Construit les Tenors classiques (3:M, 6:M, 1A, 2A, 5A, 10A, 15A, 20A, 25A et 30A)
passer datail = True pour des tenors avec un pas plus détaillé.


###### Args

mode (str, 'BAM'): Mode de la courbe à utiliser 'BAM' VS 'ZC'
**```detail```** :&ensp;<code>bool, False</code>
:   Si True, Contruit des tenors détaillés année / année



###### Returns

<code>list</code>
:   list[`PointCourbe`] Liste des tenors en PointCourbe





    
# Module `hexagon.courbe.enumerations` {#hexagon.courbe.enumerations}







    
## Classes


    
### Class `CourbeOptions` {#hexagon.courbe.enumerations.CourbeOptions}



> `class CourbeOptions(value, names=None, *, module=None, qualname=None, type=None, start=1, boundary=None)`


Create a collection of name/value pairs.

Example enumeration:

```python
>>> class Color(Enum):
...     RED = 1
...     BLUE = 2
...     GREEN = 3
```


Access them by:

- attribute access::

```python
>>> Color.RED
<Color.RED: 1>
```


- value lookup:

```python
>>> Color(1)
<Color.RED: 1>
```


- name lookup:

```python
>>> Color['RED']
<Color.RED: 1>
```


Enumerations can be iterated over, and know how many members they have:

```python
>>> len(Color)
3
```


```python
>>> list(Color)
[<Color.RED: 1>, <Color.BLUE: 2>, <Color.GREEN: 3>]
```


Methods can be added to enumerations, and members can have their own
attributes -- see the documentation for details.


    
#### Ancestors (in MRO)

* [enum.Enum](#enum.Enum)



    
#### Class variables


    
##### Variable `FLATPOINT` {#hexagon.courbe.enumerations.CourbeOptions.FLATPOINT}



    
##### Variable `MAXZC` {#hexagon.courbe.enumerations.CourbeOptions.MAXZC}



    
##### Variable `TENORS` {#hexagon.courbe.enumerations.CourbeOptions.TENORS}



    
##### Variable `TENORS_DETAIL` {#hexagon.courbe.enumerations.CourbeOptions.TENORS_DETAIL}






    
### Class `ModeActuariel` {#hexagon.courbe.enumerations.ModeActuariel}



> `class ModeActuariel(value, names=None, *, module=None, qualname=None, type=None, start=1, boundary=None)`


Nombre de jours à considérer comme étant Actuariel.

ACT365: Actuariel à partir de 365 ou 366 pour année bisextile

ACT366: Actuariel à partir de 366 quelque soit l'année


    
#### Ancestors (in MRO)

* [enum.IntEnum](#enum.IntEnum)
* [builtins.int](#builtins.int)
* [enum.ReprEnum](#enum.ReprEnum)
* [enum.Enum](#enum.Enum)



    
#### Class variables


    
##### Variable `ACT365` {#hexagon.courbe.enumerations.ModeActuariel.ACT365}



    
##### Variable `ACT366` {#hexagon.courbe.enumerations.ModeActuariel.ACT366}






    
### Class `ModeCourbe` {#hexagon.courbe.enumerations.ModeCourbe}



> `class ModeCourbe(value, names=None, *, module=None, qualname=None, type=None, start=1, boundary=None)`


Mode de la courbe à utiliser. ZC pour Zéro-Coupon


    
#### Ancestors (in MRO)

* [enum.StrEnum](#enum.StrEnum)
* [builtins.str](#builtins.str)
* [enum.ReprEnum](#enum.ReprEnum)
* [enum.Enum](#enum.Enum)



    
#### Class variables


    
##### Variable `BAM` {#hexagon.courbe.enumerations.ModeCourbe.BAM}



    
##### Variable `ZC` {#hexagon.courbe.enumerations.ModeCourbe.ZC}






    
### Class `TraitementCourbe` {#hexagon.courbe.enumerations.TraitementCourbe}



> `class TraitementCourbe(value, names=None, *, module=None, qualname=None, type=None, start=1, boundary=None)`


Seuil de maturité minimal pour considérer que la courbe est plate.

Defaut: 56 Jours, Si 1 aucun traitement n'est fait.


    
#### Ancestors (in MRO)

* [enum.IntEnum](#enum.IntEnum)
* [builtins.int](#builtins.int)
* [enum.ReprEnum](#enum.ReprEnum)
* [enum.Enum](#enum.Enum)



    
#### Class variables


    
##### Variable `BRUTE` {#hexagon.courbe.enumerations.TraitementCourbe.BRUTE}



    
##### Variable `FLAT56` {#hexagon.courbe.enumerations.TraitementCourbe.FLAT56}








    
# Module `hexagon.courbe.exceptions` {#hexagon.courbe.exceptions}







    
## Classes


    
### Class `CourbeError` {#hexagon.courbe.exceptions.CourbeError}



> `class CourbeError(...)`


Base Class for all Courbe related Exceptions


    
#### Ancestors (in MRO)

* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)


    
#### Descendants

* [hexagon.courbe.exceptions.CourbePointError](#hexagon.courbe.exceptions.CourbePointError)
* [hexagon.courbe.exceptions.InterpolatorError](#hexagon.courbe.exceptions.InterpolatorError)





    
### Class `CourbePointError` {#hexagon.courbe.exceptions.CourbePointError}



> `class CourbePointError(...)`


Exception générale pour l'ensemble des erreurs liées à la manipulation de **la Courbe**


    
#### Ancestors (in MRO)

* [hexagon.courbe.exceptions.CourbeError](#hexagon.courbe.exceptions.CourbeError)
* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)






    
### Class `InterpolatorError` {#hexagon.courbe.exceptions.InterpolatorError}



> `class InterpolatorError(...)`


Exception pour l'interpolateur.


#### Raisons

- Erreur d'interpolation
- Courbe passée mal construite
- Vérification


    
#### Ancestors (in MRO)

* [hexagon.courbe.exceptions.CourbeError](#hexagon.courbe.exceptions.CourbeError)
* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)








    
# Module `hexagon.courbe.interpolator` {#hexagon.courbe.interpolator}







    
## Classes


    
### Class `CourbeInterpolator` {#hexagon.courbe.interpolator.CourbeInterpolator}



> `class CourbeInterpolator(courbe: hexagon.courbe.common.CourbeBase, date_courbe: datetime.date, process_taux: bool = True)`


Cette classe représente un Interpolateur spécifique aux taux dans la mesure
où elle prend en considération la nature des taux à interpoler.
    
    * Si la borne supérieure d'interpolation est un taux actuariel et que la maturité demandée
        est monétaire la borne sup est transformée en un taux monétaire avant interpolation.
    
    * Si la borne inférieure d'interpolation est un taux monétaire et que la maturité demandée
        est actuarielle la borne inf est transformée en un taux actuariel avant interpolation.


#### Attributes

**```courbe```** :&ensp;<code>CourbeBase</code>
:   Instance de la Courbe


**```date_courbe```** :&ensp;<code>date</code>
:   Date de la courbe


**```order```** :&ensp;<code>int</code>, default=<code>1</code>
:   L'ordre d'interpolation


**```process_taux```** :&ensp;<code>bool</code>, default=<code>True</code>
:   Si doit proceder à l'ajustement des taux de même type
    Si taux demandé monétaire, monétarise le taux actuariel adjacent et vice-versa









    
#### Methods


    
##### Method `build_interpolator` {#hexagon.courbe.interpolator.CourbeInterpolator.build_interpolator}



    
> `def build_interpolator(self, courbe: hexagon.courbe.common.CourbeBase)`


Construit l'interpolateur de la courbe des taux passée en param


###### Args

**```courbe```** :&ensp;<code>CourbeBase</code>
:   La courbe à utiliser pour contruire l'interpolateur



###### Returns

<code>InterpolatedUnivariateSpline</code>
:   Un interpolateur de scipy



###### Raises

<code>ValueError</code>
:   Si la courbe contient des doublons



    
##### Method `interpolate_maturite` {#hexagon.courbe.interpolator.CourbeInterpolator.interpolate_maturite}



    
> `def interpolate_maturite(self, maturite_cible: int) -> hexagon.courbe.common.PointCourbe`


Calcule le taux interpolé adéquat pour la liste de la courbe fournie.

Cette méthode prend en considération les taux non homogènes (Taux Monétaire VS taux Actuariel)

Par défaut cette fonction effectue une interpolation et extrapolation selon <code>self.ordre</code>


###### Args

**```maturite_cible```** :&ensp;<code>int</code>
:   Maturité à interpoler



###### Returns

<code>PointCourbe</code>
:   Taux interpolé ou extrapolé.



    
##### Method `interpolate_maturite_many` {#hexagon.courbe.interpolator.CourbeInterpolator.interpolate_maturite_many}



    
> `def interpolate_maturite_many(self, maturite_liste: list[int]) -> list[hexagon.courbe.common.PointCourbe]`


Interpole plusieurs maturités


###### Args

**```maturite_liste```** :&ensp;<code>list</code>
:   Liste des maturités à rechercher



###### Returns

<code>list</code>
:   Liste de PointCourbe -> list[PointCourbe]



    
##### Method `interpolate_periode` {#hexagon.courbe.interpolator.CourbeInterpolator.interpolate_periode}



    
> `def interpolate_periode(self, periode: str) -> hexagon.courbe.common.PointCourbe`


Procède à l'interpolation en prenant comme parametre un dict au sens de relativedelta

exemple:
    >>> periode = {'years': 2, 'months': 6, 'days': 25}
    >>> interpolate_periode(periode)


###### Args

**```periode```** :&ensp;<code>dict</code>
:   Dict d'une période i.e: periode = {'years': 2}, {'years': 4, 'months': 6}



###### Returns

<code>PointCourbe</code>
:   Taux interpolé ou extrapolé.



    
##### Method `interpolate_periode_many` {#hexagon.courbe.interpolator.CourbeInterpolator.interpolate_periode_many}



    
> `def interpolate_periode_many(self, periode_liste: list[dict]) -> list[hexagon.courbe.common.PointCourbe]`


Interpole plusieurs périodes telles que définies par relativedelta


###### Args

**```periode_liste```** :&ensp;<code>list</code>
:   Liste des périodes à rechercher list[Dict]



###### Returns

<code>list</code>
:   Liste de PointCourbe -> list[PointCourbe]





    
# Module `hexagon.deposit` {#hexagon.deposit}






    
## Functions


    
### Function `calculate_interest` {#hexagon.deposit.calculate_interest}



    
> `def calculate_interest(nominal: float, taux_placement: float, emission: datetime.date, echeance: datetime.date, periodicite: str) -> float`


Calcule les interets du Dépot (DAT/BDC) sur la période fournie


###### Args

**```nominal```** :&ensp;<code>float</code>
:   Le nominal du dépôt


**```taux_placement```** :&ensp;<code>float</code>
:   Taux de placement, sert de base pour le calcul des interets


**```emission```** :&ensp;<code>date</code>
:   Date d'émission ou date de départ du calcul des interets


**```echeance```** :&ensp;<code>date</code>
:   Date d'échéance ou date de remboursement du dépôt


**```periodicite```** :&ensp;<code>str</code>
:   Périodicité de paiement des interêts



###### Returns

<code>float</code>
:   Les interets sur la période



###### Raises

<code>ValueError</code>
:   Si la période est supérieure à 1 AN



    
### Function `create_deposit` {#hexagon.deposit.create_deposit}



    
> `def create_deposit(code_fonds: str, nominal: float, taux_placement: float, emission: datetime.date, echeance: datetime.date, code_emetteur: str = None, *, periodicite: str = 'A', interets: float = None, dbsession: sqlalchemy.orm.session.Session)`


Crée un DAT avec les mentions fournies, Si code emetteur non 'None', crée automatiquement un BDC


###### Args

**```code_fonds```** :&ensp;<code>str</code>
:   le code du Fonds selon la création sur MANAR.


**```nominal```** :&ensp;<code>float</code>
:   le montant investi en DAT ou BDC.


**```taux_placement```** :&ensp;<code>float</code>
:   Le taux de rémunération du dépot, permet de calculer les intérêts de l'investissement.


**```code_emetteur```** :&ensp;<code>None</code>, optional
:   Renseigner code_emetteur dans le cas d'un BDC, default to 'None'


**```emission```** :&ensp;<code>str, date</code>
:   Date d'emission du titre


**```echeance```** :&ensp;<code>str, date</code>
:   Date d'echéance du titre


**```interets```** :&ensp;<code>float, None</code>
:   Si interets fournis saute l'étape du calcul des interêts



    
### Function `create_reference_deposit` {#hexagon.deposit.create_reference_deposit}



    
> `def create_reference_deposit(code_fonds: str, code_emetteur: str = None, dbsession: sqlalchemy.orm.session.Session = None) -> dict`


Génère un code interne pour les DAT et les BDC vu qu'ils n'ont pas de code ISIN Marché.

Le besoin de la génération d'un code unique est du au fait que MANAR considère les DAT et BDC comme
des opérations temporaires de début 'emission' et de fin 'echéance'.

* L'idée est de traiter les DAT et les BDC comme des Titres financiers afin de les intégrer
  dans l'inventaire, dans le calcul des ratios règlementaires et dans les ratios de risque.


###### Args

**```code_fonds```** :&ensp;<code>str</code>
:   le code du Fonds selon la création sur MANAR.


**```nominal```** :&ensp;<code>float</code>
:   le montant investi en DAT ou BDC.


**```code_emetteur```** :&ensp;<code>None</code>, optional
:   Renseigner code_emetteur dans le cas d'un BDC, default to 'None'



###### Returns

<code>dict</code>
:   Dict ayant comme keys, [code, description, classe, categorie].



###### Raises

<code>ValueError</code>
:   Description



    
### Function `is_bdc` {#hexagon.deposit.is_bdc}



    
> `def is_bdc(code_emetteur: str = None) -> bool`


Vérifie si un dépot est un BDC ou un DAT.


###### Args

**```code_emetteur```** :&ensp;<code>None</code>, optional
:   Renseigner code_emetteur dans le cas d'un BDC, default to 'None'



###### Returns

<code>bool</code>
:   True pour un BDC, False pour un DAT.






    
# Module `hexagon.devise` {#hexagon.devise}






    
## Functions


    
### Function `load_table_devise` {#hexagon.devise.load_table_devise}



    
> `def load_table_devise(date_marche: datetime.date, code_devise_locale: str, dbsession: sqlalchemy.orm.session.Session) -> list[sqlalchemy.engine.row.Row]`


Construit une table des cours de devise par rapport à une devise de Base,
    par défaut correspond au MAD ou sur <code>config.AnalyticsConfig</code>

Args:

- date_marche (<code>datetime.date</code>): date de valeur des cours de devise
- code_devise_locale (<code>str</code>): code iso 3 caractères de la devise souhaitée


###### Returns

<code>list</code>[`Row`]: Resultat de la requete

    
### Function `load_table_devise_many` {#hexagon.devise.load_table_devise_many}



    
> `def load_table_devise_many(ldates: list[datetime.date], code_devise_locale: str, dbsession: sqlalchemy.orm.session.Session) -> list[sqlalchemy.engine.row.Row]`


Construit une table des cours de devise par rapport à une devise de Base,
    par défaut correspond au MAD ou sur analytics.yaml [devise]

Args:

- date_marche (<code>datetime.date</code>): date de valeur des cours de devise
- code_devise_locale (<code>str</code>): code iso 3 caractères de la devise souhaitée


###### Returns

<code>list</code>[`Row`]: Resultat de la requete


    
## Classes


    
### Class `DeviseDateNotFound` {#hexagon.devise.DeviseDateNotFound}



> `class DeviseDateNotFound(...)`


Common base class for all non-exit exceptions.


    
#### Ancestors (in MRO)

* [hexagon.devise.DeviseError](#hexagon.devise.DeviseError)
* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)






    
### Class `DeviseError` {#hexagon.devise.DeviseError}



> `class DeviseError(...)`


Common base class for all non-exit exceptions.


    
#### Ancestors (in MRO)

* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)


    
#### Descendants

* [hexagon.devise.DeviseDateNotFound](#hexagon.devise.DeviseDateNotFound)





    
### Class `DeviseTable` {#hexagon.devise.DeviseTable}



> `class DeviseTable(date_marche: datetime.date, dbsession: sqlalchemy.orm.session.Session)`


Classe permettant de manipuler et de convertir les devises.

DeviseTable fonctionne en déclarant:

- Une devise locale <code>self.code\_devise\_locale</code>
- L'unité de cette devise i.e: (EUR, 1), (JPY, 100), (NOK, 100), (DZD, 100)

Attributes:

- date_marche (<code>datetime.date</code>): Date de Valeur ou date de conversion
- code_devise_locale (<code>str</code>): La devise Locale ALPHA-3
- unite_devise_locale (<code>str</code>): L'unite de la devise Locale, pour le MAD=1, pour le YEN Japonais=100
- map_devise (<code>dict</code>): Mapping des cours de conversion entre les devises étrangères et la devise Locale
- table_devise (<code>list</code>): Contient les données telles que stockées au niveau de la base


    
#### Ancestors (in MRO)

* [hexagon.devise.DeviseTableBase](#hexagon.devise.DeviseTableBase)






    
### Class `DeviseTableBase` {#hexagon.devise.DeviseTableBase}



> `class DeviseTableBase(date_marche: datetime.date, table_devise: list = None, code_devise_locale: str = None, unite_devise_locale: float = 1, *, dbsession: sqlalchemy.orm.session.Session)`


Classe permettant de manipuler et de convertir les devises.

DeviseTable fonctionne en déclarant:

- Une devise locale <code>self.code\_devise\_locale</code>
- L'unité de cette devise i.e: (EUR, 1), (JPY, 100), (NOK, 100), (DZD, 100)

Attributes:

- date_marche (<code>datetime.date</code>): Date de Valeur ou date de conversion
- code_devise_locale (<code>str</code>): La devise Locale ALPHA-3
- unite_devise_locale (<code>str</code>): L'unite de la devise Locale, pour le MAD=1, pour le YEN Japonais=100
- map_devise (<code>dict</code>): Mapping des cours de conversion entre les devises étrangères et la devise Locale
- table_devise (<code>list</code>): Contient les données telles que stockées au niveau de la base



    
#### Descendants

* [hexagon.devise.DeviseTable](#hexagon.devise.DeviseTable)





    
#### Methods


    
##### Method `convert_montant_devise` {#hexagon.devise.DeviseTableBase.convert_montant_devise}



    
> `def convert_montant_devise(self, montant: float, devise_montant: str) -> float`


Convertit un montant exprimé en devise vers la devise de base de l'objet DeviseTable

Args:

- montant (<code>float</code>): Le montant en devise étrangère
- devise_montant (<code>str</code>): code de devise du montant en 3 caractères i.e MAD, USD, EUR ....


###### Returns

<code>float</code>: le montant converti en devise base 'self.code_devise_locale'

    
##### Method `get_cours_devise` {#hexagon.devise.DeviseTableBase.get_cours_devise}



    
> `def get_cours_devise(self, devise_cible: str) -> float`


Retourne le cours de conversion de la devise passée en paramètre code ALPHA-3


###### Args

devise_cible (<code>str</code>): Code de la devise

###### Returns

<code>float</code>: le cours de conversion de la devise en date du self.date_marche

    
##### Method `get_link_devise` {#hexagon.devise.DeviseTableBase.get_link_devise}



    
> `def get_link_devise(self, from_devise: str, to_devise: str) -> float`


Chaine les cours de deux devises étrangères en utilisant comme devise
    étalon la devise locale. ex: MAD

Args:

- from_devise (<code>str</code>): Code de la devise source
- to_devise (<code>str</code>): Code de la devise cible


###### Returns

<code>float</code>: Le cours de conversion entre la devise source et la devise cible

    
##### Method `get_unite_devise` {#hexagon.devise.DeviseTableBase.get_unite_devise}



    
> `def get_unite_devise(self, devise_cible: str) -> int`


Retourne l'unité de base la devise passée en paramètre i.e: 100 Yen, 100 Couronnes etc....


###### Args

devise_cible (<code>str</code>): Devise désirée

###### Returns

<code>int</code>: l'unite de base de la devise en date du self.date_marche

    
##### Method `init_map_devise` {#hexagon.devise.DeviseTableBase.init_map_devise}



    
> `def init_map_devise(self)`




    
##### Method `invert_cours_devise` {#hexagon.devise.DeviseTableBase.invert_cours_devise}



    
> `def invert_cours_devise(self, devise_cible: str) -> float`


Retourne l'inverse du cours de change ex: MAD/USD = 1/(USD/MAD)


###### Args

devise_cible (<code>str</code>): La devise à inverser le cours

###### Returns

<code>float</code>: L'inverse du cours de change / à la devise locale

    
##### Method `invert_montant_devise` {#hexagon.devise.DeviseTableBase.invert_montant_devise}



    
> `def invert_montant_devise(self, montant: float, devise_cible: str) -> float`


Invertit un montant exprimé en devise locale vers une devise cible

**Tip:** 


**La devise locale est déclarée lors de l'instanciation de DeviseTable.**

Args:

- montant (<code>float</code>): Le montant en devise locale (self.code_devise_locale)
- devise_cible (<code>str</code>): code de devise du montant en 3 caractères i.e MAD, USD, EUR ....


###### Returns

<code>float</code>: le montant converti en devise étrangère.

    
### Class `DeviseTableMany` {#hexagon.devise.DeviseTableMany}



> `class DeviseTableMany(ldates: list[datetime.date], dbsession)`


A MutableMapping is a generic container for associating
key/value pairs.

This class provides concrete generic implementations of all
methods except for __getitem__, __setitem__, __delitem__,
__iter__, and __len__.


    
#### Ancestors (in MRO)

* [collections.UserDict](#collections.UserDict)
* [collections.abc.MutableMapping](#collections.abc.MutableMapping)
* [collections.abc.Mapping](#collections.abc.Mapping)
* [collections.abc.Collection](#collections.abc.Collection)
* [collections.abc.Sized](#collections.abc.Sized)
* [collections.abc.Iterable](#collections.abc.Iterable)
* [collections.abc.Container](#collections.abc.Container)






    
#### Methods


    
##### Method `convert_montant_devise` {#hexagon.devise.DeviseTableMany.convert_montant_devise}



    
> `def convert_montant_devise(self, date_marche: datetime.date, montant: float, devise_montant: str) -> float`


Convertit un montant exprimé en devise vers la devise de base de l'objet DeviseTable

Args:

- date_marche (<code>datetime.date</code>): Date de publication du cours de devise par BKAM
- montant (<code>float</code>): Le montant en devise étrangère
- devise_montant (<code>str</code>): code de devise du montant en 3 caractères i.e MAD, USD, EUR ....


###### Returns

<code>float</code>: le montant converti en devise base 'self.code_devise_locale'

###### Raises

<code>[DeviseDateNotFound](#hexagon.devise.DeviseDateNotFound "hexagon.devise.DeviseDateNotFound")</code>: Si date non disponible

    
##### Method `get_cours_devise` {#hexagon.devise.DeviseTableMany.get_cours_devise}



    
> `def get_cours_devise(self, date_marche: datetime.date, devise_cible: str) -> float`


Retourne le cours de conversion de la devise passée en paramètre code ALPHA-3

Args:

- date_marche (<code>datetime.date</code>): Date de publication du cours de devise par BKAM
- devise_cible (<code>str</code>): Code de la devise


###### Returns

<code>float</code>: le cours de conversion de la devise en date du self.date_marche

###### Raises

<code>[DeviseDateNotFound](#hexagon.devise.DeviseDateNotFound "hexagon.devise.DeviseDateNotFound")</code>: Si date non disponible

    
##### Method `get_link_devise` {#hexagon.devise.DeviseTableMany.get_link_devise}



    
> `def get_link_devise(self, date_marche: datetime.date, from_devise: str, to_devise: str) -> float`


Chaine les cours de deux devises étrangères en utilisant comme devise
    étalon la devise locale. ex: MAD

Args:

- date_marche (<code>datetime.date</code>): Date de publication du cours de devise par BKAM
- from_devise (<code>str</code>): Code de la devise source
- to_devise (<code>str</code>): Code de la devise cible


###### Returns

<code>float</code>: Le cours de conversion entre la devise source et la devise cible

###### Raises

<code>[DeviseDateNotFound](#hexagon.devise.DeviseDateNotFound "hexagon.devise.DeviseDateNotFound")</code>: Si date non disponible

    
##### Method `get_unite_devise` {#hexagon.devise.DeviseTableMany.get_unite_devise}



    
> `def get_unite_devise(self, date_marche: datetime.date, devise_cible: str) -> int`


Retourne l'unité de base la devise passée en paramètre i.e: 100 Yen, 100 Couronnes etc....

Args:

- date_marche (<code>datetime.date</code>): Date de publication du cours de devise par BKAM
- devise_cible (<code>str</code>): Devise désirée


###### Returns

<code>int</code>: l'unite de base de la devise en date du self.date_marche

###### Raises

<code>[DeviseDateNotFound](#hexagon.devise.DeviseDateNotFound "hexagon.devise.DeviseDateNotFound")</code>: Si date non disponible

    
##### Method `invert_cours_devise` {#hexagon.devise.DeviseTableMany.invert_cours_devise}



    
> `def invert_cours_devise(self, date_marche: datetime.date, devise_cible: str) -> float`


Retourne l'inverse du cours de change ex: MAD/USD = 1/(USD/MAD)

Args:

- date_marche (<code>datetime.date</code>): Date de publication du cours de devise par BKAM
- devise_cible (<code>str</code>): La devise à inverser le cours


###### Returns

<code>float</code>: L'inverse du cours de change / à la devise locale

###### Raises

<code>[DeviseDateNotFound](#hexagon.devise.DeviseDateNotFound "hexagon.devise.DeviseDateNotFound")</code>: Si date non disponible

    
##### Method `invert_montant_devise` {#hexagon.devise.DeviseTableMany.invert_montant_devise}



    
> `def invert_montant_devise(self, date_marche: datetime.date, montant: float, devise_cible: str) -> float`


Invertit un montant exprimé en devise locale vers une devise cible

**La devise locale est déclarée lors de l'instanciation de DeviseTable.**

Args:

- date_marche (<code>datetime.date</code>): Date de publication du cours de devise par BKAM
- montant (<code>float</code>): Le montant en devise locale (self.code_devise_locale)
- devise_cible (<code>str</code>): code de devise du montant en 3 caractères i.e MAD, USD, EUR ....


###### Returns

<code>float</code>: le montant converti en devise étrangère.

###### Raises

<code>[DeviseDateNotFound](#hexagon.devise.DeviseDateNotFound "hexagon.devise.DeviseDateNotFound")</code>: Si date non disponible



    
# Module `hexagon.etl` {#hexagon.etl}

ETL est le module d'interactions avec **le monde externe**, importe les données depuis Excel
à la BDD Hexagon et offre des fonctions d'adaptation des fichiers ASFIM, MBI et Maroclear

# Fichiers Supportés
- Excel (xlsx):
    - <code>[ExcelOut](#hexagon.etl.ExcelOut "hexagon.etl.ExcelOut")</code>: Classe dédiée à **l'écriture seulement** utilisée pour exporter les objets
        python au format Excel (.xlsx)
    - <code>[ExcelIn](#hexagon.etl.ExcelIn "hexagon.etl.ExcelIn")</code>: Classe dédiée à **la lecture seulement** utilisée pour lire les données 
        de fichiers Excel et les structurer en objets Python (list[dict])

- CSV: Utilise le dialecte Excel avec (delimiter=';' et decimal=',')
    - <code>[write\_csv\_records()](#hexagon.etl.write\_csv\_records "hexagon.etl.write\_csv\_records")</code>: Exporte une collection de Mapping vers un fichier CSV.
        Les valeurs sont localisées avant l'export.
    - <code>[write\_csv\_objects()](#hexagon.etl.write\_csv\_objects "hexagon.etl.write\_csv\_objects")</code>: Exporte une collection d'objets Python vers un fichier CSV.
        Les valeurs sont localisées avant l'export.
    - <code>[read\_csv\_file()](#hexagon.etl.read\_csv\_file "hexagon.etl.read\_csv\_file")</code>: Lis les données depuis le fichier CSV en appliquant les mêms règles dialectiques.

- YAML:
    - <code>[YamlFile](#hexagon.etl.YamlFile "hexagon.etl.YamlFile")</code>: Classe de struct/destructuration des objets Python vers le format YAML.
        impose **CSafeLoder** et **CSafeDumper** avec des paramètres par défaut.
    **Important:** 
    **CSafeLoader** et **CSafeDumper** sont imposés pour des raisons de sécurité.

# Adaptateurs des fichiers

- <code>[transform\_mbi\_file()](#hexagon.etl.transform\_mbi\_file "hexagon.etl.transform\_mbi\_file")</code>: Transforme et adapte Le fichier MBI communiqué
    par BMCE à la structure attendue par Hexagon
- <code>[transform\_asfim\_file()](#hexagon.etl.transform\_asfim\_file "hexagon.etl.transform\_asfim\_file")</code>: Transforme et adapte Le fichier ASFIM (Hebdomadaire) communiqué
    par l'ASFIM à la structure attendue par Hexagon
- <code>[transform\_xml\_file()](#hexagon.etl.transform\_xml\_file "hexagon.etl.transform\_xml\_file")</code>: Transforme et adapte Le fichier Referentiel Titres communiqué
    par Maroclear à la structure attendue par Hexagon

# Importation des données

- <code>[import\_from\_excel()](#hexagon.etl.import\_from\_excel "hexagon.etl.import\_from\_excel")</code>: Importe les données depuis un fichier Excel respectant la structure
    attendue, à la BDD Hexagon.

# Génération du Template Hexagon

- <code>[write\_template()](#hexagon.etl.write\_template "hexagon.etl.write\_template")</code>: Génère le un Template Excel structuré selon la convention Hexagon.


    
## Sub-modules

* [hexagon.etl.csv_util](#hexagon.etl.csv_util)
* [hexagon.etl.data_importer](#hexagon.etl.data_importer)
* [hexagon.etl.excel](#hexagon.etl.excel)
* [hexagon.etl.exceptions](#hexagon.etl.exceptions)
* [hexagon.etl.template](#hexagon.etl.template)
* [hexagon.etl.transform](#hexagon.etl.transform)
* [hexagon.etl.yaml_utils](#hexagon.etl.yaml_utils)



    
## Functions


    
### Function `import_from_excel` {#hexagon.etl.import_from_excel}



    
> `def import_from_excel(filename: str, sheetname=None)`


Importe une feuille/Classeur Excel à la BDD en utilisant Importe Register comme guide.

Args:

- filename (str): Chemin du fichier Excel
- sheetname (None, optional): Feuille spécifique à importer, si None, importe tout le classeur.

    
### Function `read_csv_file` {#hexagon.etl.read_csv_file}



    
> `def read_csv_file(fpath: str, fields: list[str] = None, dialect=hexagon.etl.csv_util.LocalCSVDialect)`


Lis le contenu d'un fichier csv

    >> g = read_csv_file('file.csv', fields=('name', 'mail', 'age'))
    >> next(g) -> OrderedDict({'name':'mero', 'mail':'mero@fakir.ma', 'age':27})

Args:

- fpath (<code>str</code>): chemin du fichier à lire
- fields (<code>list</code>, None): Liste des champs à extraire depuis le fichier.
- dialect (<code>csv.dialect</code>, optional): Dialecte à utiliser pour lire le fichier csv,


###### Yields

<code>OrderedDict</code>
:   Dictionnaire contenant les champs des lignes, if fields



    
### Function `transform_asfim_file` {#hexagon.etl.transform_asfim_file}



    
> `def transform_asfim_file(asfim_path: pathlib.Path, dst_path: pathlib.Path, *, worksheet_index: int = 0, header_row: int = 2, lower_headers: bool = True, constant_memory: bool = True, in_memory: bool = False, export_params: dict = None)`


Transforme le fichier asfim en un classeur Excel de deux feuilles (fonds et indicateurs_fonds)

###### Args

**```src_path```**
:   chemin du fichier source.


**```dst_path```**
:   chemin du fichier de destination.



    
### Function `transform_mbi_file` {#hexagon.etl.transform_mbi_file}



    
> `def transform_mbi_file(src_path: str, dst_path: str, start_col: int = 1, start_row: int = 12, end_col: int = 26, skip_previous_week=True)`


Transforme le fichier MBI en une structure exploitable.


###### Args

**```src_path```** :&ensp;<code>str</code>
:   chemin du fichier MBI


**```dst_path```** :&ensp;<code>str</code>
:   chemin du fichier transformé


**```start_col```** :&ensp;<code>int</code>, optional
:   Colonne de départ pour lire les données


**```start_row```** :&ensp;<code>int</code>, optional
:   Ligne de départ pour lire les données


**```end_col```** :&ensp;<code>int</code>, optional
:   Colonne de fin des données, cette variable permet de limiter
    les aléas liés aux formattages des fichiers Excel.



    
### Function `transform_xml_file` {#hexagon.etl.transform_xml_file}



    
> `def transform_xml_file(inpath, outpath)`


Convertit et traite le nouveau fichier XML vers un format Excel.


###### Args

**```infile```** :&ensp;<code>str</code> or <code>Path</code>
:   Le Chemin du fichier XML.


**```outfile```** :&ensp;<code>str</code> or <code>Path</code>
:   Le Chemin du fichier Excel.



    
### Function `write_csv_objects` {#hexagon.etl.write_csv_objects}



    
> `def write_csv_objects(fpath: str, data: list[object], fields: list[str], header: bool = True, dialect=hexagon.etl.csv_util.LocalCSVDialect)`




    
### Function `write_csv_records` {#hexagon.etl.write_csv_records}



    
> `def write_csv_records(fpath: str, data: list[dict], fields: list[str], header: bool = True, dialect=hexagon.etl.csv_util.LocalCSVDialect)`


Exporte une liste de dictionnaires vers un fichier csv.

Args:

- fpath (<code>str</code>): Chemin du fichier
- data (<code>list</code>[`dict`]): Liste de dictionnaires.
- fields (<code>list</code>[`str`]): Liste des champs à exporter
- header (<code>bool</code>, True): La première ligne contient les champs
- dialect (<code>csv.Dialect</code>, optional): Voir <code>csv.Dialect</code>, default to LocalCSVDialect.
- isodate (<code>bool</code>, False): if True exporte les dates au format "yyyy-mm-dd"

    
### Function `write_template` {#hexagon.etl.write_template}



    
> `def write_template(path)`





    
## Classes


    
### Class `DataImportError` {#hexagon.etl.DataImportError}



> `class DataImportError(...)`


Exception de base pour les erreurs d'importation


    
#### Ancestors (in MRO)

* [hexagon.etl.exceptions.ETLError](#hexagon.etl.exceptions.ETLError)
* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)






    
### Class `ETLError` {#hexagon.etl.ETLError}



> `class ETLError(...)`


Exception de base pour les exceptions du module etl


    
#### Ancestors (in MRO)

* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)


    
#### Descendants

* [hexagon.etl.exceptions.DataImportError](#hexagon.etl.exceptions.DataImportError)
* [hexagon.etl.exceptions.WriterError](#hexagon.etl.exceptions.WriterError)





    
### Class `ExcelIn` {#hexagon.etl.ExcelIn}



> `class ExcelIn(path: str, **read_args)`


Classe pour lire les données depuis Excel.



    
#### Descendants

* [hexagon.etl.transform.transform_asfim.AsfimReader](#hexagon.etl.transform.transform_asfim.AsfimReader)


    
#### Class variables


    
##### Variable `XL` {#hexagon.etl.ExcelIn.XL}

Create a collection of name/value pairs.

Example enumeration:

```python
>>> class Color(Enum):
...     RED = 1
...     BLUE = 2
...     GREEN = 3
```


Access them by:

- attribute access::

```python
>>> Color.RED
<Color.RED: 1>
```


- value lookup:

```python
>>> Color(1)
<Color.RED: 1>
```


- name lookup:

```python
>>> Color['RED']
<Color.RED: 1>
```


Enumerations can be iterated over, and know how many members they have:

```python
>>> len(Color)
3
```


```python
>>> list(Color)
[<Color.RED: 1>, <Color.BLUE: 2>, <Color.GREEN: 3>]
```


Methods can be added to enumerations, and members can have their own
attributes -- see the documentation for details.


    
#### Instance variables


    
##### Variable `sheet_names` {#hexagon.etl.ExcelIn.sheet_names}



    
##### Variable `worksheets` {#hexagon.etl.ExcelIn.worksheets}

A property that redirects to WorkBook.worksheets


###### Returns

<code>list</code>[`openpyxl.Worksheet`]: Liste des Feuilles du classeur



    
#### Methods


    
##### Method `close` {#hexagon.etl.ExcelIn.close}



    
> `def close(self)`




    
##### Method `get_worksheet` {#hexagon.etl.ExcelIn.get_worksheet}



    
> `def get_worksheet(self, sname: str | int)`


Accede aux WorkSheets du WorkBook par index (Zero Based) et nom de la feuille


###### Args

sname (<code>int</code> | <code>str</code>): nom ou l'index de la feuille (Commence par Zéro)

###### Returns

<code>openpyxl.WorkSheet</code>: WorkSheet object, permet de lire les données via cell.value

###### Raises

<code>ValueError</code>: Si le nom est introuvable ou l'indice depasse le nombre de feuilles.

    
##### Method `read_dict` {#hexagon.etl.ExcelIn.read_dict}



    
> `def read_dict(self, sheetname: str, header: bool = True, lower_case: bool = True) -> list[dict]`


Utilise Openpyxl comme engine.

Permet de lire une feuille Excel, les données doivent obligatoirement avoir
des Headers.
Transforme la feuille Excel en Liste de dictionnaires.

Args:

- sheetname (<code>str</code>): Le nom de la feuille.


###### Returns

<code>list</code>[`dict`]: List of records.

    
##### Method `read_list` {#hexagon.etl.ExcelIn.read_list}



    
> `def read_list(self, sheetname: str) -> list[list]`


Utilise Openpyxl comme engine.

Permet de lire une feuille Excel, les données doivent obligatoirement avoir
des Headers.
Transforme la feuille Excel en Liste de dictionnaires.

Args:

- sheetname (<code>str</code>): Le nom de la feuille.


###### Returns

<code>list\[tuple]</code>
:   List of records.



    
### Class `ExcelOut` {#hexagon.etl.ExcelOut}



> `class ExcelOut(path: str = '', constant_memory: bool = False, wbook: xlsxwriter.workbook.Workbook = None, in_memory: bool = False, params: dict = None)`


Classe permettant de generer des fichiers excels a partir d'une liste
de dictionnaires.

La premiere ligne sera reservee au nom des champs des dictionnaires.
Utilise xlsxwriter comme Excel Engine.

Classe pour exporter certaines structures python sous format excel


#### Args

path (<code>str</code>): Chemin du fichier à créer
constant_memory (<code>bool</code>, False): Si vrai passe en mode d'optimization de la RAM
    Inconvénient: L'écriture doit se faire one shot,
    impossible de màj une cellule déjà renseignée.
**```wbook```** :&ensp;<code>xw.Workbook</code>, optional
:   Si un Workbook est passé,
    n'utilise pas le paramètre path et utilise ce Workbook comme objet interne




    
#### Ancestors (in MRO)

* [hexagon.etl.excel.WriterMixin](#hexagon.etl.excel.WriterMixin)



    
#### Class variables


    
##### Variable `XL` {#hexagon.etl.ExcelOut.XL}

Create a collection of name/value pairs.

Example enumeration:

```python
>>> class Color(Enum):
...     RED = 1
...     BLUE = 2
...     GREEN = 3
```


Access them by:

- attribute access::

```python
>>> Color.RED
<Color.RED: 1>
```


- value lookup:

```python
>>> Color(1)
<Color.RED: 1>
```


- name lookup:

```python
>>> Color['RED']
<Color.RED: 1>
```


Enumerations can be iterated over, and know how many members they have:

```python
>>> len(Color)
3
```


```python
>>> list(Color)
[<Color.RED: 1>, <Color.BLUE: 2>, <Color.GREEN: 3>]
```


Methods can be added to enumerations, and members can have their own
attributes -- see the documentation for details.




    
#### Methods


    
##### Method `close` {#hexagon.etl.ExcelOut.close}



    
> `def close(self) -> _io.BytesIO | None`


More file like method for convenience only!

    
##### Method `save` {#hexagon.etl.ExcelOut.save}



    
> `def save(self) -> _io.BytesIO | None`


More file like method for convenience only!

    
##### Method `set_properties` {#hexagon.etl.ExcelOut.set_properties}



    
> `def set_properties(self)`




    
##### Method `write_collection` {#hexagon.etl.ExcelOut.write_collection}



    
> `def write_collection(self, collection: list[object], sheetname: str, row: int = 0, column: int = 0, attr_list: list[str] = None, headers: bool = True)`


Ecrit le contenu d'une collection of Python objects avec ou sans les headers.
fournir 'attr_list' pour controler les attributs à exporter.
Useful for exporting queries objects i.e:

```python
>>> q = DBSession.query(Fonds)
>>> write_collection(q, 'Liste des Fonds', 0, 0, ['code', 'isin', 'categorie'])
```


Exemple plus compliqué.

```python
>>> q = DBSession.query(
    Fonds.code, Fonds.categorie, EtatFonds.vl, EtatFonds.actif_net).where(
    Fonds.id == EtatFonds.id_fonds, Fonds.code == 'CODE_FONDS')
>>> attr_list = q[0].keys()  # Take into account all the requested fields
>>> write_collection(q, 'Liste des Fonds', 0, 0, attr_list)  # ...EEET VOILÀ ;-)
```


Args:

- collection (<code>list</code>): a Collection of Python objects.
- sheetname (<code>str</code>): Nom de la feuille, si inexistante crée une nouvelle.
- row (<code>int</code>, 0): Numéro de la ligne. Zero-indexed.
- column (<code>int</code>, 0): Numéro de la colonne. Zero-indexed.
- attr_list (<code>list</code>[`str`]): Fournir une liste afin de controler les champs à écrire.
- headers (<code>bool</code>, True): Exporte les attributs comme headers

    
##### Method `write_collection_many` {#hexagon.etl.ExcelOut.write_collection_many}



    
> `def write_collection_many(self, mapping_collections: dict[str, tuple[list[object], list[str]]])`




    
##### Method `write_dataframe` {#hexagon.etl.ExcelOut.write_dataframe}



    
> `def write_dataframe(self, df, sheetname: str, xrow: int = 0, ycol: int = 0)`


Better Use a pandas.ExcelWriter
Write a Dataframe into an Excel WorkSheet.
This function offers more flexilbility to handle Dataframes
writing into Excel Files than the default behaviour of to_excel method.

    
##### Method `write_dict` {#hexagon.etl.ExcelOut.write_dict}



    
> `def write_dict(self, dico: dict, sheetname: str, row: int = 0, column: int = 0, key_list: list[str] = None, headers: bool = True, orient=XL.vorient)`


Ecrit le contenu d'un Dict avec ou sans les headers.

fournir 'key_list' pour controler les champs à exporter.

```python
>>> write_dict({'name':'mero', 'age':27}, 'names_sheet', 3, 3), orient=XL.horient)
```


écrit les valeurs à partir de la cellule "D4" dans la feuille 'names_sheet' verticalement.

==> +-------+----------+
    |  name |  mero    |
    |       |          |
    +-------+----------+
    | age   |    27    |
    |       |          |
    +-------+----------+

Args:

- dico (<code>dict</code>): Data in a dict.
- sheetname (<code>str</code>): Nom de la feuille, si inexistante crée une nouvelle.
- row (<code>int</code>, 0): Numéro de la ligne. Zero-indexed.
- column (<code>int</code>, 0): Numéro de la colonne. Zero-indexed.
- key_list (<code>list</code>, None): Fournir une liste afin de controler les champs à écrire.
- headers (<code>bool</code>, True): If False écrit les données sans les 'Keys'.
- orient (<code>XL</code>, optional): Orientation d'écritue => (XL.horient, XL.vorient).

    
##### Method `write_object` {#hexagon.etl.ExcelOut.write_object}



    
> `def write_object(self, obj: object, sheetname: str, row: int, column: int, attr_list: list[str], headers: bool = True, orient=XL.vorient)`


Ecrit le contenu d'un Python object avec ou sans les headers.
fournir 'attr_list' pour controler les attributs à exporter.

Args:

- obj (Any): Python Object, useful for Sqlalchemy models instances.
- sheetname (<code>str</code>): Nom de la feuille, si inexistante crée une nouvelle.
- row (<code>int</code>, 0): Numéro de la ligne. Zero-indexed.
- column (<code>int</code>, 0): Numéro de la colonne. Zero-indexed.
- attr_list (<code>list</code>[`str`]): Fournir une liste afin de controler les champs à écrire.
- headers (<code>bool</code>, True): If False n'écrit que les valeurs de 'dico'
- orient (<code>XL</code>, optional): Orientation d'écritue => (XL.horient, XL.vorient)

    
##### Method `write_records` {#hexagon.etl.ExcelOut.write_records}



    
> `def write_records(self, records: list[dict], sheetname: str, row: int = 0, column: int = 0, key_list: list[str] = None, headers: bool = True)`


Exporte une liste de dictionnaires dans une feuille Excel de nom 'sheetname'.

fournir key_list pour controler les champs à exporter.

Args:

- records (<code>list</code>): Liste de dict.
- sheetname (<code>str</code>): Nom de la feuille, si inexistante crée une nouvelle.
- row (<code>int</code>, 0): Numéro de la ligne. Zero-indexed.
- column (<code>int</code>, 0): Numéro de la colonne. Zero-indexed.
- headers (<code>bool</code>, True): Si True exporte aussi les champs, defaut to True.
- key_list (<code>list</code>[`str`], None): Champs spécifiques, if None utilise dict.keys() du premier élément de la liste.

    
##### Method `write_records_many` {#hexagon.etl.ExcelOut.write_records_many}



    
> `def write_records_many(self, mapping_records: dict[str, list[dict]])`




    
##### Method `write_row` {#hexagon.etl.ExcelOut.write_row}



    
> `def write_row(self, row_data: list[str], sheetname: str, row: int = 0, column: int = 0, row_header: list[str] = None, orient=XL.vorient)`


Ecrit une liste de données selon une orientation

Args:

- row_data (<code>list</code>): données 1D, Ligne, Colonne
- sheetname (<code>str</code>): nom de la feuille, crée une nouvelle si nom introuvable
- row (<code>int</code>, 0): starting row
- column (<code>int</code>, 0): starting col
- row_header (<code>list</code>[`str`], None): Titre de la colonne ou ligne
- orient (<code>XL</code>, optional): Orientation d'écritue => (XL.horient 'Horizontale', XL.vorient 'Verticale')

    
##### Method `write_rows` {#hexagon.etl.ExcelOut.write_rows}



    
> `def write_rows(self, data: list[list], sheetname: str, row: int = 0, column: int = 0, headers: bool = True)`


Ecrit une liste de liste avec comme premiere ligne header True ou false

Args:

- data (<code>list</code>[`list`]): données 2D, tableau, matrice
- sheetname (<code>str</code>): nom de la feuille, crée une nouvelle si nom introuvable
- row (<code>int</code>, 0): starting row
- column (<code>int</code>, 0): starting col
- headers (<code>bool</code>, True): Prend la première ligne comme champs de données

    
##### Method `write_rows_many` {#hexagon.etl.ExcelOut.write_rows_many}



    
> `def write_rows_many(self, mapping_rows: dict[str, list[list]], headers: bool = True)`




    
##### Method `write_title` {#hexagon.etl.ExcelOut.write_title}



    
> `def write_title(self, title: str, sheetname: str, nw_point: tuple[int, int], se_point: tuple[int, int], merge: bool = True)`


Ecrit du texte considéré comme titre, si merge=True, fusionne les cellules entre xrow et xcol.

Args:

- title (<code>str</code>): Le titre à écrire
- sheetname (<code>str</code>): Le nom de la feuille
- nw_point (<code>tuple</code>[`int`, <code>int</code>]): North-West Point, Coordonnées (x, y) du point Haut gauche
- se_point (<code>tuple</code>[`int`, <code>int</code>]): South-East Point, Coordonnées (x, y) du point bas droite
- merge (<code>bool</code>, True): Fusionne les cellules contenant le titre

    
### Class `YamlFile` {#hexagon.etl.YamlFile}



> `class YamlFile(dump_config: dict = None)`






    
#### Descendants

* [hexagon.infrastructure.config.config_file_handler.ConfigFileHandler](#hexagon.infrastructure.config.config_file_handler.ConfigFileHandler)


    
#### Class variables


    
##### Variable `YamlDumper` {#hexagon.etl.YamlFile.YamlDumper}



    
##### Variable `YamlLoader` {#hexagon.etl.YamlFile.YamlLoader}






    
#### Methods


    
##### Method `dumps` {#hexagon.etl.YamlFile.dumps}



    
> `def dumps(self, document: Any) -> str`




    
##### Method `loads` {#hexagon.etl.YamlFile.loads}



    
> `def loads(self, anystr: str) -> Any`




    
##### Method `read_document` {#hexagon.etl.YamlFile.read_document}



    
> `def read_document(self, path: pathlib.Path) -> dict`




    
##### Method `save_document` {#hexagon.etl.YamlFile.save_document}



    
> `def save_document(self, path: pathlib.Path, document: dict) -> bool`


Sauvegarde un document représenté par un dict.

Args:

- document (<code>dict</code>): Document (object) à sauvegarder
- path (<code>Path</code>): Chemin du fichier


###### Returns

<code>bool</code>: True if file is saved



    
# Module `hexagon.etl.csv_util` {#hexagon.etl.csv_util}






    
## Functions


    
### Function `read_csv_file` {#hexagon.etl.csv_util.read_csv_file}



    
> `def read_csv_file(fpath: str, fields: list[str] = None, dialect=hexagon.etl.csv_util.LocalCSVDialect)`


Lis le contenu d'un fichier csv

    >> g = read_csv_file('file.csv', fields=('name', 'mail', 'age'))
    >> next(g) -> OrderedDict({'name':'mero', 'mail':'mero@fakir.ma', 'age':27})

Args:

- fpath (<code>str</code>): chemin du fichier à lire
- fields (<code>list</code>, None): Liste des champs à extraire depuis le fichier.
- dialect (<code>csv.dialect</code>, optional): Dialecte à utiliser pour lire le fichier csv,


###### Yields

<code>OrderedDict</code>
:   Dictionnaire contenant les champs des lignes, if fields



    
### Function `stringify_dict` {#hexagon.etl.csv_util.stringify_dict}



    
> `def stringify_dict(adict: dict) -> dict`


Transforme les valeurs du dict en CdC pour être exploité selon le Locale
Marocain.

Args:

- adict (<code>dict</code>): Dictionnaire input
- isodate (<code>bool</code>, False): if True exporte les dates au format "yyyy-mm-dd"


###### Returns

<code>dict</code>: Nouveau dict avec les valeurs en CdC

    
### Function `stringify_values` {#hexagon.etl.csv_util.stringify_values}



    
> `def stringify_values(alist: list) -> list[str]`


Transforme les valeurs d'une liste en CdC pour export CSV
au locale Marocain

Args:

- alist (<code>list</code>): Liste des valeurs
- isodate (<code>bool</code>, optional): if True exporte les dates au format "yyyy-mm-dd"


###### Returns

<code>list</code>[`str`]: Nouvelle liste avec les valeurs en CdC

    
### Function `write_csv_objects` {#hexagon.etl.csv_util.write_csv_objects}



    
> `def write_csv_objects(fpath: str, data: list[object], fields: list[str], header: bool = True, dialect=hexagon.etl.csv_util.LocalCSVDialect)`




    
### Function `write_csv_records` {#hexagon.etl.csv_util.write_csv_records}



    
> `def write_csv_records(fpath: str, data: list[dict], fields: list[str], header: bool = True, dialect=hexagon.etl.csv_util.LocalCSVDialect)`


Exporte une liste de dictionnaires vers un fichier csv.

Args:

- fpath (<code>str</code>): Chemin du fichier
- data (<code>list</code>[`dict`]): Liste de dictionnaires.
- fields (<code>list</code>[`str`]): Liste des champs à exporter
- header (<code>bool</code>, True): La première ligne contient les champs
- dialect (<code>csv.Dialect</code>, optional): Voir <code>csv.Dialect</code>, default to LocalCSVDialect.
- isodate (<code>bool</code>, False): if True exporte les dates au format "yyyy-mm-dd"




    
# Module `hexagon.etl.data_importer` {#hexagon.etl.data_importer}






    
## Functions


    
### Function `import_from_excel` {#hexagon.etl.data_importer.import_from_excel}



    
> `def import_from_excel(filename: str, sheetname=None)`


Importe une feuille/Classeur Excel à la BDD en utilisant Importe Register comme guide.

Args:

- filename (str): Chemin du fichier Excel
- sheetname (None, optional): Feuille spécifique à importer, si None, importe tout le classeur.




    
# Module `hexagon.etl.excel` {#hexagon.etl.excel}







    
## Classes


    
### Class `ExcelIn` {#hexagon.etl.excel.ExcelIn}



> `class ExcelIn(path: str, **read_args)`


Classe pour lire les données depuis Excel.



    
#### Descendants

* [hexagon.etl.transform.transform_asfim.AsfimReader](#hexagon.etl.transform.transform_asfim.AsfimReader)


    
#### Class variables


    
##### Variable `XL` {#hexagon.etl.excel.ExcelIn.XL}

Create a collection of name/value pairs.

Example enumeration:

```python
>>> class Color(Enum):
...     RED = 1
...     BLUE = 2
...     GREEN = 3
```


Access them by:

- attribute access::

```python
>>> Color.RED
<Color.RED: 1>
```


- value lookup:

```python
>>> Color(1)
<Color.RED: 1>
```


- name lookup:

```python
>>> Color['RED']
<Color.RED: 1>
```


Enumerations can be iterated over, and know how many members they have:

```python
>>> len(Color)
3
```


```python
>>> list(Color)
[<Color.RED: 1>, <Color.BLUE: 2>, <Color.GREEN: 3>]
```


Methods can be added to enumerations, and members can have their own
attributes -- see the documentation for details.


    
#### Instance variables


    
##### Variable `sheet_names` {#hexagon.etl.excel.ExcelIn.sheet_names}



    
##### Variable `worksheets` {#hexagon.etl.excel.ExcelIn.worksheets}

A property that redirects to WorkBook.worksheets


###### Returns

<code>list</code>[`openpyxl.Worksheet`]: Liste des Feuilles du classeur



    
#### Methods


    
##### Method `close` {#hexagon.etl.excel.ExcelIn.close}



    
> `def close(self)`




    
##### Method `get_worksheet` {#hexagon.etl.excel.ExcelIn.get_worksheet}



    
> `def get_worksheet(self, sname: str | int)`


Accede aux WorkSheets du WorkBook par index (Zero Based) et nom de la feuille


###### Args

sname (<code>int</code> | <code>str</code>): nom ou l'index de la feuille (Commence par Zéro)

###### Returns

<code>openpyxl.WorkSheet</code>: WorkSheet object, permet de lire les données via cell.value

###### Raises

<code>ValueError</code>: Si le nom est introuvable ou l'indice depasse le nombre de feuilles.

    
##### Method `read_dict` {#hexagon.etl.excel.ExcelIn.read_dict}



    
> `def read_dict(self, sheetname: str, header: bool = True, lower_case: bool = True) -> list[dict]`


Utilise Openpyxl comme engine.

Permet de lire une feuille Excel, les données doivent obligatoirement avoir
des Headers.
Transforme la feuille Excel en Liste de dictionnaires.

Args:

- sheetname (<code>str</code>): Le nom de la feuille.


###### Returns

<code>list</code>[`dict`]: List of records.

    
##### Method `read_list` {#hexagon.etl.excel.ExcelIn.read_list}



    
> `def read_list(self, sheetname: str) -> list[list]`


Utilise Openpyxl comme engine.

Permet de lire une feuille Excel, les données doivent obligatoirement avoir
des Headers.
Transforme la feuille Excel en Liste de dictionnaires.

Args:

- sheetname (<code>str</code>): Le nom de la feuille.


###### Returns

<code>list\[tuple]</code>
:   List of records.



    
### Class `ExcelOut` {#hexagon.etl.excel.ExcelOut}



> `class ExcelOut(path: str = '', constant_memory: bool = False, wbook: xlsxwriter.workbook.Workbook = None, in_memory: bool = False, params: dict = None)`


Classe permettant de generer des fichiers excels a partir d'une liste
de dictionnaires.

La premiere ligne sera reservee au nom des champs des dictionnaires.
Utilise xlsxwriter comme Excel Engine.

Classe pour exporter certaines structures python sous format excel


#### Args

path (<code>str</code>): Chemin du fichier à créer
constant_memory (<code>bool</code>, False): Si vrai passe en mode d'optimization de la RAM
    Inconvénient: L'écriture doit se faire one shot,
    impossible de màj une cellule déjà renseignée.
**```wbook```** :&ensp;<code>xw.Workbook</code>, optional
:   Si un Workbook est passé,
    n'utilise pas le paramètre path et utilise ce Workbook comme objet interne




    
#### Ancestors (in MRO)

* [hexagon.etl.excel.WriterMixin](#hexagon.etl.excel.WriterMixin)



    
#### Class variables


    
##### Variable `XL` {#hexagon.etl.excel.ExcelOut.XL}

Create a collection of name/value pairs.

Example enumeration:

```python
>>> class Color(Enum):
...     RED = 1
...     BLUE = 2
...     GREEN = 3
```


Access them by:

- attribute access::

```python
>>> Color.RED
<Color.RED: 1>
```


- value lookup:

```python
>>> Color(1)
<Color.RED: 1>
```


- name lookup:

```python
>>> Color['RED']
<Color.RED: 1>
```


Enumerations can be iterated over, and know how many members they have:

```python
>>> len(Color)
3
```


```python
>>> list(Color)
[<Color.RED: 1>, <Color.BLUE: 2>, <Color.GREEN: 3>]
```


Methods can be added to enumerations, and members can have their own
attributes -- see the documentation for details.




    
#### Methods


    
##### Method `close` {#hexagon.etl.excel.ExcelOut.close}



    
> `def close(self) -> _io.BytesIO | None`


More file like method for convenience only!

    
##### Method `save` {#hexagon.etl.excel.ExcelOut.save}



    
> `def save(self) -> _io.BytesIO | None`


More file like method for convenience only!

    
##### Method `set_properties` {#hexagon.etl.excel.ExcelOut.set_properties}



    
> `def set_properties(self)`




    
##### Method `write_collection` {#hexagon.etl.excel.ExcelOut.write_collection}



    
> `def write_collection(self, collection: list[object], sheetname: str, row: int = 0, column: int = 0, attr_list: list[str] = None, headers: bool = True)`


Ecrit le contenu d'une collection of Python objects avec ou sans les headers.
fournir 'attr_list' pour controler les attributs à exporter.
Useful for exporting queries objects i.e:

```python
>>> q = DBSession.query(Fonds)
>>> write_collection(q, 'Liste des Fonds', 0, 0, ['code', 'isin', 'categorie'])
```


Exemple plus compliqué.

```python
>>> q = DBSession.query(
    Fonds.code, Fonds.categorie, EtatFonds.vl, EtatFonds.actif_net).where(
    Fonds.id == EtatFonds.id_fonds, Fonds.code == 'CODE_FONDS')
>>> attr_list = q[0].keys()  # Take into account all the requested fields
>>> write_collection(q, 'Liste des Fonds', 0, 0, attr_list)  # ...EEET VOILÀ ;-)
```


Args:

- collection (<code>list</code>): a Collection of Python objects.
- sheetname (<code>str</code>): Nom de la feuille, si inexistante crée une nouvelle.
- row (<code>int</code>, 0): Numéro de la ligne. Zero-indexed.
- column (<code>int</code>, 0): Numéro de la colonne. Zero-indexed.
- attr_list (<code>list</code>[`str`]): Fournir une liste afin de controler les champs à écrire.
- headers (<code>bool</code>, True): Exporte les attributs comme headers

    
##### Method `write_collection_many` {#hexagon.etl.excel.ExcelOut.write_collection_many}



    
> `def write_collection_many(self, mapping_collections: dict[str, tuple[list[object], list[str]]])`




    
##### Method `write_dataframe` {#hexagon.etl.excel.ExcelOut.write_dataframe}



    
> `def write_dataframe(self, df, sheetname: str, xrow: int = 0, ycol: int = 0)`


Better Use a pandas.ExcelWriter
Write a Dataframe into an Excel WorkSheet.
This function offers more flexilbility to handle Dataframes
writing into Excel Files than the default behaviour of to_excel method.

    
##### Method `write_dict` {#hexagon.etl.excel.ExcelOut.write_dict}



    
> `def write_dict(self, dico: dict, sheetname: str, row: int = 0, column: int = 0, key_list: list[str] = None, headers: bool = True, orient=XL.vorient)`


Ecrit le contenu d'un Dict avec ou sans les headers.

fournir 'key_list' pour controler les champs à exporter.

```python
>>> write_dict({'name':'mero', 'age':27}, 'names_sheet', 3, 3), orient=XL.horient)
```


écrit les valeurs à partir de la cellule "D4" dans la feuille 'names_sheet' verticalement.

==> +-------+----------+
    |  name |  mero    |
    |       |          |
    +-------+----------+
    | age   |    27    |
    |       |          |
    +-------+----------+

Args:

- dico (<code>dict</code>): Data in a dict.
- sheetname (<code>str</code>): Nom de la feuille, si inexistante crée une nouvelle.
- row (<code>int</code>, 0): Numéro de la ligne. Zero-indexed.
- column (<code>int</code>, 0): Numéro de la colonne. Zero-indexed.
- key_list (<code>list</code>, None): Fournir une liste afin de controler les champs à écrire.
- headers (<code>bool</code>, True): If False écrit les données sans les 'Keys'.
- orient (<code>[XL](#hexagon.etl.excel.XL "hexagon.etl.excel.XL")</code>, optional): Orientation d'écritue => (XL.horient, XL.vorient).

    
##### Method `write_object` {#hexagon.etl.excel.ExcelOut.write_object}



    
> `def write_object(self, obj: object, sheetname: str, row: int, column: int, attr_list: list[str], headers: bool = True, orient=XL.vorient)`


Ecrit le contenu d'un Python object avec ou sans les headers.
fournir 'attr_list' pour controler les attributs à exporter.

Args:

- obj (Any): Python Object, useful for Sqlalchemy models instances.
- sheetname (<code>str</code>): Nom de la feuille, si inexistante crée une nouvelle.
- row (<code>int</code>, 0): Numéro de la ligne. Zero-indexed.
- column (<code>int</code>, 0): Numéro de la colonne. Zero-indexed.
- attr_list (<code>list</code>[`str`]): Fournir une liste afin de controler les champs à écrire.
- headers (<code>bool</code>, True): If False n'écrit que les valeurs de 'dico'
- orient (<code>[XL](#hexagon.etl.excel.XL "hexagon.etl.excel.XL")</code>, optional): Orientation d'écritue => (XL.horient, XL.vorient)

    
##### Method `write_records` {#hexagon.etl.excel.ExcelOut.write_records}



    
> `def write_records(self, records: list[dict], sheetname: str, row: int = 0, column: int = 0, key_list: list[str] = None, headers: bool = True)`


Exporte une liste de dictionnaires dans une feuille Excel de nom 'sheetname'.

fournir key_list pour controler les champs à exporter.

Args:

- records (<code>list</code>): Liste de dict.
- sheetname (<code>str</code>): Nom de la feuille, si inexistante crée une nouvelle.
- row (<code>int</code>, 0): Numéro de la ligne. Zero-indexed.
- column (<code>int</code>, 0): Numéro de la colonne. Zero-indexed.
- headers (<code>bool</code>, True): Si True exporte aussi les champs, defaut to True.
- key_list (<code>list</code>[`str`], None): Champs spécifiques, if None utilise dict.keys() du premier élément de la liste.

    
##### Method `write_records_many` {#hexagon.etl.excel.ExcelOut.write_records_many}



    
> `def write_records_many(self, mapping_records: dict[str, list[dict]])`




    
##### Method `write_row` {#hexagon.etl.excel.ExcelOut.write_row}



    
> `def write_row(self, row_data: list[str], sheetname: str, row: int = 0, column: int = 0, row_header: list[str] = None, orient=XL.vorient)`


Ecrit une liste de données selon une orientation

Args:

- row_data (<code>list</code>): données 1D, Ligne, Colonne
- sheetname (<code>str</code>): nom de la feuille, crée une nouvelle si nom introuvable
- row (<code>int</code>, 0): starting row
- column (<code>int</code>, 0): starting col
- row_header (<code>list</code>[`str`], None): Titre de la colonne ou ligne
- orient (<code>[XL](#hexagon.etl.excel.XL "hexagon.etl.excel.XL")</code>, optional): Orientation d'écritue => (XL.horient 'Horizontale', XL.vorient 'Verticale')

    
##### Method `write_rows` {#hexagon.etl.excel.ExcelOut.write_rows}



    
> `def write_rows(self, data: list[list], sheetname: str, row: int = 0, column: int = 0, headers: bool = True)`


Ecrit une liste de liste avec comme premiere ligne header True ou false

Args:

- data (<code>list</code>[`list`]): données 2D, tableau, matrice
- sheetname (<code>str</code>): nom de la feuille, crée une nouvelle si nom introuvable
- row (<code>int</code>, 0): starting row
- column (<code>int</code>, 0): starting col
- headers (<code>bool</code>, True): Prend la première ligne comme champs de données

    
##### Method `write_rows_many` {#hexagon.etl.excel.ExcelOut.write_rows_many}



    
> `def write_rows_many(self, mapping_rows: dict[str, list[list]], headers: bool = True)`




    
##### Method `write_title` {#hexagon.etl.excel.ExcelOut.write_title}



    
> `def write_title(self, title: str, sheetname: str, nw_point: tuple[int, int], se_point: tuple[int, int], merge: bool = True)`


Ecrit du texte considéré comme titre, si merge=True, fusionne les cellules entre xrow et xcol.

Args:

- title (<code>str</code>): Le titre à écrire
- sheetname (<code>str</code>): Le nom de la feuille
- nw_point (<code>tuple</code>[`int`, <code>int</code>]): North-West Point, Coordonnées (x, y) du point Haut gauche
- se_point (<code>tuple</code>[`int`, <code>int</code>]): South-East Point, Coordonnées (x, y) du point bas droite
- merge (<code>bool</code>, True): Fusionne les cellules contenant le titre

    
### Class `XL` {#hexagon.etl.excel.XL}



> `class XL(value, names=None, *, module=None, qualname=None, type=None, start=1, boundary=None)`


Create a collection of name/value pairs.

Example enumeration:

```python
>>> class Color(Enum):
...     RED = 1
...     BLUE = 2
...     GREEN = 3
```


Access them by:

- attribute access::

```python
>>> Color.RED
<Color.RED: 1>
```


- value lookup:

```python
>>> Color(1)
<Color.RED: 1>
```


- name lookup:

```python
>>> Color['RED']
<Color.RED: 1>
```


Enumerations can be iterated over, and know how many members they have:

```python
>>> len(Color)
3
```


```python
>>> list(Color)
[<Color.RED: 1>, <Color.BLUE: 2>, <Color.GREEN: 3>]
```


Methods can be added to enumerations, and members can have their own
attributes -- see the documentation for details.


    
#### Ancestors (in MRO)

* [enum.Enum](#enum.Enum)



    
#### Class variables


    
##### Variable `horient` {#hexagon.etl.excel.XL.horient}



    
##### Variable `openpyxl_read` {#hexagon.etl.excel.XL.openpyxl_read}



    
##### Variable `openpyxl_read_only` {#hexagon.etl.excel.XL.openpyxl_read_only}



    
##### Variable `title_formatting` {#hexagon.etl.excel.XL.title_formatting}



    
##### Variable `vorient` {#hexagon.etl.excel.XL.vorient}



    
##### Variable `xl_cust_properties` {#hexagon.etl.excel.XL.xl_cust_properties}



    
##### Variable `xl_properties` {#hexagon.etl.excel.XL.xl_properties}








    
# Module `hexagon.etl.exceptions` {#hexagon.etl.exceptions}







    
## Classes


    
### Class `DataImportError` {#hexagon.etl.exceptions.DataImportError}



> `class DataImportError(...)`


Exception de base pour les erreurs d'importation


    
#### Ancestors (in MRO)

* [hexagon.etl.exceptions.ETLError](#hexagon.etl.exceptions.ETLError)
* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)






    
### Class `DataOperationError` {#hexagon.etl.exceptions.DataOperationError}



> `class DataOperationError(*args, **kwargs)`


Classe de base des exceptions des operations de données de ce module


    
#### Ancestors (in MRO)

* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)


    
#### Descendants

* [hexagon.etl.exceptions.MappingOperationError](#hexagon.etl.exceptions.MappingOperationError)





    
### Class `ETLError` {#hexagon.etl.exceptions.ETLError}



> `class ETLError(...)`


Exception de base pour les exceptions du module etl


    
#### Ancestors (in MRO)

* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)


    
#### Descendants

* [hexagon.etl.exceptions.DataImportError](#hexagon.etl.exceptions.DataImportError)
* [hexagon.etl.exceptions.WriterError](#hexagon.etl.exceptions.WriterError)





    
### Class `ExcelWriterError` {#hexagon.etl.exceptions.ExcelWriterError}



> `class ExcelWriterError(...)`


Exception d'ExcelWriter


    
#### Ancestors (in MRO)

* [hexagon.etl.exceptions.WriterError](#hexagon.etl.exceptions.WriterError)
* [hexagon.etl.exceptions.ETLError](#hexagon.etl.exceptions.ETLError)
* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)






    
### Class `FieldsNotFound` {#hexagon.etl.exceptions.FieldsNotFound}



> `class FieldsNotFound(*args, fields: Iterable[str] = None, **kwargs)`


Exception pour les champs manquants


    
#### Ancestors (in MRO)

* [hexagon.etl.exceptions.MappingOperationError](#hexagon.etl.exceptions.MappingOperationError)
* [hexagon.etl.exceptions.DataOperationError](#hexagon.etl.exceptions.DataOperationError)
* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)






    
### Class `MappingOperationError` {#hexagon.etl.exceptions.MappingOperationError}



> `class MappingOperationError(*args, **kwargs)`


Classe de base des exceptions des operations de données de ce module


    
#### Ancestors (in MRO)

* [hexagon.etl.exceptions.DataOperationError](#hexagon.etl.exceptions.DataOperationError)
* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)


    
#### Descendants

* [hexagon.etl.exceptions.FieldsNotFound](#hexagon.etl.exceptions.FieldsNotFound)





    
### Class `WriterError` {#hexagon.etl.exceptions.WriterError}



> `class WriterError(...)`


Exception d'acquisition de la data


    
#### Ancestors (in MRO)

* [hexagon.etl.exceptions.ETLError](#hexagon.etl.exceptions.ETLError)
* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)


    
#### Descendants

* [hexagon.etl.exceptions.ExcelWriterError](#hexagon.etl.exceptions.ExcelWriterError)







    
# Module `hexagon.etl.template` {#hexagon.etl.template}






    
## Functions


    
### Function `write_template` {#hexagon.etl.template.write_template}



    
> `def write_template(path)`




    
### Function `write_template_from_list` {#hexagon.etl.template.write_template_from_list}



    
> `def write_template_from_list(path, list_tables)`




    
### Function `write_template_table` {#hexagon.etl.template.write_template_table}



    
> `def write_template_table(path, tablename)`







    
# Module `hexagon.etl.transform` {#hexagon.etl.transform}




    
## Sub-modules

* [hexagon.etl.transform.gisement_mbi](#hexagon.etl.transform.gisement_mbi)
* [hexagon.etl.transform.maroclear_excel](#hexagon.etl.transform.maroclear_excel)
* [hexagon.etl.transform.transform_asfim](#hexagon.etl.transform.transform_asfim)
* [hexagon.etl.transform.transform_mbi](#hexagon.etl.transform.transform_mbi)
* [hexagon.etl.transform.xml_maroclear](#hexagon.etl.transform.xml_maroclear)






    
# Module `hexagon.etl.transform.gisement_mbi` {#hexagon.etl.transform.gisement_mbi}






    
## Functions


    
### Function `plages_strates_mbi` {#hexagon.etl.transform.gisement_mbi.plages_strates_mbi}



    
> `def plages_strates_mbi(wsheet, cell_row: int = 0, cell_col: int = 0)`


détecte une plage continue de données en prenant en considération
les max_row et max_column de la feuille

    
### Function `read_gisement_mbi` {#hexagon.etl.transform.gisement_mbi.read_gisement_mbi}



    
> `def read_gisement_mbi(fpath) -> hexagon.etl.excel.ExcelIn`




    
### Function `split_on_tables` {#hexagon.etl.transform.gisement_mbi.split_on_tables}



    
> `def split_on_tables(excelin: hexagon.etl.excel.ExcelIn, sheet: int = 0) -> list`


Splitte les tables des Strates du MBI en Range de
matrices pour lecture.
Must return 4 list[dict]
La lecture nécessite la date d'inventaire




    
# Module `hexagon.etl.transform.maroclear_excel` {#hexagon.etl.transform.maroclear_excel}






    
## Functions


    
### Function `convert_repertoire_maroclear_old` {#hexagon.etl.transform.maroclear_excel.convert_repertoire_maroclear_old}



    
> `def convert_repertoire_maroclear_old(infile, outfile)`







    
# Module `hexagon.etl.transform.transform_asfim` {#hexagon.etl.transform.transform_asfim}






    
## Functions


    
### Function `batch_transform_asfim_files` {#hexagon.etl.transform.transform_asfim.batch_transform_asfim_files}



    
> `def batch_transform_asfim_files(src_dir, dst_dir, workers=2)`


Transforme plusieurs fichiers ASFIM.

###### Args

**```src_dir```**
:   Le dossier source contenant les fichiers ASFIM.


**```dst_dir```**
:   le dossier cible ou stocker les fichiers Transformés.


**```workers```**
:   default to 2, le nombre de process parallèles à utiliser
    pour la transformation des fichiers, garder le chiffre 2.



    
### Function `transform_asfim_file` {#hexagon.etl.transform.transform_asfim.transform_asfim_file}



    
> `def transform_asfim_file(asfim_path: pathlib.Path, dst_path: pathlib.Path, *, worksheet_index: int = 0, header_row: int = 2, lower_headers: bool = True, constant_memory: bool = True, in_memory: bool = False, export_params: dict = None)`


Transforme le fichier asfim en un classeur Excel de deux feuilles (fonds et indicateurs_fonds)

###### Args

**```src_path```**
:   chemin du fichier source.


**```dst_path```**
:   chemin du fichier de destination.




    
## Classes


    
### Class `AsfimReader` {#hexagon.etl.transform.transform_asfim.AsfimReader}



> `class AsfimReader(path: str, worksheet_index: int = 0, header_row: int = 2, lower_headers: bool = True, **kwargs)`


Classe pour lire les données depuis Excel.


    
#### Ancestors (in MRO)

* [hexagon.etl.excel.ExcelIn](#hexagon.etl.excel.ExcelIn)






    
#### Methods


    
##### Method `build_mapped_headers` {#hexagon.etl.transform.transform_asfim.AsfimReader.build_mapped_headers}



    
> `def build_mapped_headers(self) -> tuple[str]`


Construit les headers mappés par leur champs correspondants


###### Returns

<code>tuple\[str]</code>
:   Tuple des champs des headers.



    
##### Method `calculate_max_column` {#hexagon.etl.transform.transform_asfim.AsfimReader.calculate_max_column}



    
> `def calculate_max_column(self) -> int`


Si une cellule est formatée et sans valeur sa colonne sera renvoyée comme max_column.
Retourne la colonne maximale contenant une valeur et qui délimite le tableau de données.


###### Returns

<code>int</code>
:   la colonne maximale qui cadre le tableau à itérer.



    
##### Method `calculate_max_row` {#hexagon.etl.transform.transform_asfim.AsfimReader.calculate_max_row}



    
> `def calculate_max_row(self) -> int`




    
##### Method `extract_date_from_header` {#hexagon.etl.transform.transform_asfim.AsfimReader.extract_date_from_header}



    
> `def extract_date_from_header(self) -> datetime.date`


Extrait la date de valeur depuis la première ligne (Cellule fusionnée)


###### Returns

<code>datetime.date</code>
:   Date de valeur



    
##### Method `extract_date_from_sheetname` {#hexagon.etl.transform.transform_asfim.AsfimReader.extract_date_from_sheetname}



    
> `def extract_date_from_sheetname(self) -> datetime.date`


Le nom de la feuille est une date.


###### Returns

<code>datetime.date</code>
:   Date de valeur



    
##### Method `format_string_float` {#hexagon.etl.transform.transform_asfim.AsfimReader.format_string_float}



    
> `def format_string_float(self, sfloat: str) -> float`




    
##### Method `iter_raw_records` {#hexagon.etl.transform.transform_asfim.AsfimReader.iter_raw_records}



    
> `def iter_raw_records(self) -> Generator[dict, NoneType, NoneType]`


Construit un List[Dict] du tableau Asfim avec les champs Mappés et dans l'ordre défini
afin de procéder au post-processing et au splitting entre les données du referentiel et dynamiques
* Ajoute la date de marché


###### Yields

<code>Generator\[dict, None, None]</code>
:   Mapping avec mapped_headers comme keys et rows comme values



    
##### Method `process_raw_record` {#hexagon.etl.transform.transform_asfim.AsfimReader.process_raw_record}



    
> `def process_raw_record(self, record: dict) -> tuple[dict, dict]`


Fonction de post processing des records.
Splitte le raw_record en deux dicts, le premier est le referentiel le second est la donnée chronologique


###### Args

**```record```** :&ensp;<code>dict</code>
:   Resultat de iter_raw_records



    
##### Method `transform_to_file` {#hexagon.etl.transform.transform_asfim.AsfimReader.transform_to_file}



    
> `def transform_to_file(self, fpath: pathlib.Path, constant_memory: bool = True, in_memory: bool = False, params: dict = None)`




    
### Class `AsfimReaderError` {#hexagon.etl.transform.transform_asfim.AsfimReaderError}



> `class AsfimReaderError(...)`


Exception de transformation du fichier ASFIM


    
#### Ancestors (in MRO)

* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)








    
# Module `hexagon.etl.transform.transform_mbi` {#hexagon.etl.transform.transform_mbi}

Ce module définit des fonctions pour convertir le fichier Weekly MBI de BMCE
en un fichier exploitable pour la structure de données qu'attend la BDD market.
Les constantes définies concernent les indices des données relatives à chaque
strate du MBI.
RQ: Je me demande toujours pq ils ont adopté le format actuel??




    
## Functions


    
### Function `read_mbi_file` {#hexagon.etl.transform.transform_mbi.read_mbi_file}



    
> `def read_mbi_file(path, start_col=1, start_row=12, end_col=26)`


Lis le fichier MBI selon la structure et le format actuels.
yields:
    list[date, strate, values...]

    
### Function `read_rows` {#hexagon.etl.transform.transform_mbi.read_rows}



    
> `def read_rows(wsheet, start_row: int = 1, start_col: int = 1, end_row: int = None, end_col: int = None)`


Lis une feuille d'une manière paramétrable en définissant les bornes.

###### Args

**```wsheet```**
:   xlsxwriter Worksheet object


**```start_row```**
:   defaults to 1


**```end_col```**
:   defaults to None, si None prend le max depuis wsheet object


returns:
    list[list]: liste des lignes à lire.

    
### Function `transform_mbi_file` {#hexagon.etl.transform.transform_mbi.transform_mbi_file}



    
> `def transform_mbi_file(src_path: str, dst_path: str, start_col: int = 1, start_row: int = 12, end_col: int = 26, skip_previous_week=True)`


Transforme le fichier MBI en une structure exploitable.


###### Args

**```src_path```** :&ensp;<code>str</code>
:   chemin du fichier MBI


**```dst_path```** :&ensp;<code>str</code>
:   chemin du fichier transformé


**```start_col```** :&ensp;<code>int</code>, optional
:   Colonne de départ pour lire les données


**```start_row```** :&ensp;<code>int</code>, optional
:   Ligne de départ pour lire les données


**```end_col```** :&ensp;<code>int</code>, optional
:   Colonne de fin des données, cette variable permet de limiter
    les aléas liés aux formattages des fichiers Excel.



    
### Function `transform_row` {#hexagon.etl.transform.transform_mbi.transform_row}



    
> `def transform_row(row)`


Extrait les bonnes strates avec leur informations
en se repérant avec les indices prédefinis dans le dict "INDICES"
yields:
    list[date, strate, values...]




    
# Module `hexagon.etl.transform.xml_maroclear` {#hexagon.etl.transform.xml_maroclear}






    
## Functions


    
### Function `apply_on_element` {#hexagon.etl.transform.xml_maroclear.apply_on_element}



    
> `def apply_on_element(func) -> <built-in function callable>`




    
### Function `extract_element` {#hexagon.etl.transform.xml_maroclear.extract_element}



    
> `def extract_element(node_element, mapper: dict) -> tuple`


Extrait et transforme un champs du noeud concerné
ex: Debt possède l'attribut 'ISSUEDT' qui est une date en forme 'yyyy-mm-dd HH:MM:SS'
si l'attribut existe dans NODE_MAPPER, la fonction dans la clé 'func' est executée
* La fonction func(node_element) accepte un seul argument: node_element

###### Args

**```node_element```** :&ensp;<code>xml.etree.ElementTree.Element</code>
:   Element XML d'un noeud


**```mapper```** :&ensp;<code>dict</code>
:   {'out_field': 'your_field', 'func'; your_func}



###### Returns

`tuple(key, value): returns a tuple` of `(out_field_name, value=your_func(element.text))`
:   &nbsp;



    
### Function `extract_node` {#hexagon.etl.transform.xml_maroclear.extract_node}



    
> `def extract_node(node) -> dict`


Transforme un noeud en dictionnaire

###### Args

**```node```** :&ensp;<code>xml.etree.ElementTree.Element</code>
:   Noeud XML



###### Returns

<code>dict</code>
:   Champs et valeurs extraits en exécutant la fonction définie dans <code>NODE\_MAPPER</code>



    
### Function `get_dates_coupon` {#hexagon.etl.transform.xml_maroclear.get_dates_coupon}



    
> `def get_dates_coupon(node_element) -> str`




    
### Function `map_forme_detention` {#hexagon.etl.transform.xml_maroclear.map_forme_detention}



    
> `def map_forme_detention(node_element) -> str`




    
### Function `map_frequency` {#hexagon.etl.transform.xml_maroclear.map_frequency}



    
> `def map_frequency(node_element) -> str`




    
### Function `map_type_coupon` {#hexagon.etl.transform.xml_maroclear.map_type_coupon}



    
> `def map_type_coupon(node_element) -> str`




    
### Function `map_type_remboursement` {#hexagon.etl.transform.xml_maroclear.map_type_remboursement}



    
> `def map_type_remboursement(node_element) -> str`




    
### Function `post_process` {#hexagon.etl.transform.xml_maroclear.post_process}



    
> `def post_process(records) -> list`


Do some post processing stuff like extracting code titre from isin, and splitting
Instrument type into classe et categorie


###### Args

**```records```** :&ensp;<code>list\[dict]</code>
:   same dict with more fields



    
### Function `post_process_record` {#hexagon.etl.transform.xml_maroclear.post_process_record}



    
> `def post_process_record(record: dict)`


Insere des champs déduits à partir d'autres champs du dictionnaire.
Ajoute ('code', 'categorie', 'classe', 'forme_juridique')

###### Args

**```record```** :&ensp;<code>dict</code>
:   Dictionnaire avec les champs lus depuis le fichier XML



    
### Function `transform_xml_file` {#hexagon.etl.transform.xml_maroclear.transform_xml_file}



    
> `def transform_xml_file(inpath, outpath)`


Convertit et traite le nouveau fichier XML vers un format Excel.


###### Args

**```infile```** :&ensp;<code>str</code> or <code>Path</code>
:   Le Chemin du fichier XML.


**```outfile```** :&ensp;<code>str</code> or <code>Path</code>
:   Le Chemin du fichier Excel.






    
# Module `hexagon.etl.yaml_utils` {#hexagon.etl.yaml_utils}

Fonctions de manipulation des fichiers en Yaml avec une définition unifiée
des Loaders et readers des fichiers yaml





    
## Classes


    
### Class `YamlFile` {#hexagon.etl.yaml_utils.YamlFile}



> `class YamlFile(dump_config: dict = None)`






    
#### Descendants

* [hexagon.infrastructure.config.config_file_handler.ConfigFileHandler](#hexagon.infrastructure.config.config_file_handler.ConfigFileHandler)


    
#### Class variables


    
##### Variable `YamlDumper` {#hexagon.etl.yaml_utils.YamlFile.YamlDumper}



    
##### Variable `YamlLoader` {#hexagon.etl.yaml_utils.YamlFile.YamlLoader}






    
#### Methods


    
##### Method `dumps` {#hexagon.etl.yaml_utils.YamlFile.dumps}



    
> `def dumps(self, document: Any) -> str`




    
##### Method `loads` {#hexagon.etl.yaml_utils.YamlFile.loads}



    
> `def loads(self, anystr: str) -> Any`




    
##### Method `read_document` {#hexagon.etl.yaml_utils.YamlFile.read_document}



    
> `def read_document(self, path: pathlib.Path) -> dict`




    
##### Method `save_document` {#hexagon.etl.yaml_utils.YamlFile.save_document}



    
> `def save_document(self, path: pathlib.Path, document: dict) -> bool`


Sauvegarde un document représenté par un dict.

Args:

- document (<code>dict</code>): Document (object) à sauvegarder
- path (<code>Path</code>): Chemin du fichier


###### Returns

<code>bool</code>: True if file is saved



    
# Module `hexagon.infrastructure` {#hexagon.infrastructure}




    
## Sub-modules

* [hexagon.infrastructure.config](#hexagon.infrastructure.config)
* [hexagon.infrastructure.db](#hexagon.infrastructure.db)
* [hexagon.infrastructure.exceptions](#hexagon.infrastructure.exceptions)
* [hexagon.infrastructure.query_executor](#hexagon.infrastructure.query_executor)
* [hexagon.infrastructure.text_query_executor](#hexagon.infrastructure.text_query_executor)
* [hexagon.infrastructure.timeseries](#hexagon.infrastructure.timeseries)






    
# Module `hexagon.infrastructure.config` {#hexagon.infrastructure.config}




    
## Sub-modules

* [hexagon.infrastructure.config.config_file_handler](#hexagon.infrastructure.config.config_file_handler)
* [hexagon.infrastructure.config.config_manager](#hexagon.infrastructure.config.config_manager)
* [hexagon.infrastructure.config.domains](#hexagon.infrastructure.config.domains)
* [hexagon.infrastructure.config.exceptions](#hexagon.infrastructure.config.exceptions)






    
# Module `hexagon.infrastructure.config.config_file_handler` {#hexagon.infrastructure.config.config_file_handler}







    
## Classes


    
### Class `ConfigFileHandler` {#hexagon.infrastructure.config.config_file_handler.ConfigFileHandler}



> `class ConfigFileHandler(path: pathlib.Path, config_cls: hexagon.infrastructure.config.domains.baseconfig.BaseConfig, *, dump_config: dict = None, file_init: bool = True)`





    
#### Ancestors (in MRO)

* [hexagon.etl.yaml_utils.YamlFile](#hexagon.etl.yaml_utils.YamlFile)






    
#### Methods


    
##### Method `extract_configuration` {#hexagon.infrastructure.config.config_file_handler.ConfigFileHandler.extract_configuration}



    
> `def extract_configuration(self, config_klass: hexagon.infrastructure.config.domains.baseconfig.BaseConfig = None) -> hexagon.infrastructure.config.domains.baseconfig.BaseConfig`




    
##### Method `init_from_file` {#hexagon.infrastructure.config.config_file_handler.ConfigFileHandler.init_from_file}



    
> `def init_from_file(self)`




    
##### Method `load_document` {#hexagon.infrastructure.config.config_file_handler.ConfigFileHandler.load_document}



    
> `def load_document(self) -> dict`


Charge le document self.document 


###### Returns

<code>BaseConfig</code>
:   Description



###### Raises

<code>ConfigurationError</code>
:   Description



    
##### Method `save_config` {#hexagon.infrastructure.config.config_file_handler.ConfigFileHandler.save_config}



    
> `def save_config(self, configuration: hexagon.infrastructure.config.domains.baseconfig.BaseConfig) -> bool`






    
# Module `hexagon.infrastructure.config.config_manager` {#hexagon.infrastructure.config.config_manager}







    
## Classes


    
### Class `ConfigManager` {#hexagon.infrastructure.config.config_manager.ConfigManager}



> `class ConfigManager(config_registry: dict = None, dump_config: dict = None)`







    
#### Class variables


    
##### Variable `extension` {#hexagon.infrastructure.config.config_manager.ConfigManager.extension}




    
#### Instance variables


    
##### Variable `dirpath` {#hexagon.infrastructure.config.config_manager.ConfigManager.dirpath}

Chamin du dossier de configuration de l'application
fait appel à platformdir.user_config_path


###### Returns

<code>Path</code>
:   Chemin du dossier de configuration



    
##### Variable `registry_keys` {#hexagon.infrastructure.config.config_manager.ConfigManager.registry_keys}

Liste les clés des rubriques déclarées


    
#### Static methods


    
##### `Method clear_cache` {#hexagon.infrastructure.config.config_manager.ConfigManager.clear_cache}



    
> `def clear_cache()`





    
#### Methods


    
##### Method `load_config` {#hexagon.infrastructure.config.config_manager.ConfigManager.load_config}



    
> `def load_config(self, registry_name: str) -> hexagon.infrastructure.config.config_file_handler.ConfigFileHandler`




    
##### Method `referesh_cache` {#hexagon.infrastructure.config.config_manager.ConfigManager.referesh_cache}



    
> `def referesh_cache(self)`




    
### Class `ConfigRegistry` {#hexagon.infrastructure.config.config_manager.ConfigRegistry}



> `class ConfigRegistry(name: str, data: dict)`


A MutableMapping is a generic container for associating
key/value pairs.

This class provides concrete generic implementations of all
methods except for __getitem__, __setitem__, __delitem__,
__iter__, and __len__.


    
#### Ancestors (in MRO)

* [collections.UserDict](#collections.UserDict)
* [collections.abc.MutableMapping](#collections.abc.MutableMapping)
* [collections.abc.Mapping](#collections.abc.Mapping)
* [collections.abc.Collection](#collections.abc.Collection)
* [collections.abc.Sized](#collections.abc.Sized)
* [collections.abc.Iterable](#collections.abc.Iterable)
* [collections.abc.Container](#collections.abc.Container)





    
#### Static methods


    
##### `Method default` {#hexagon.infrastructure.config.config_manager.ConfigRegistry.default}



    
> `def default()`







    
# Module `hexagon.infrastructure.config.domains` {#hexagon.infrastructure.config.domains}




    
## Sub-modules

* [hexagon.infrastructure.config.domains.analytics](#hexagon.infrastructure.config.domains.analytics)
* [hexagon.infrastructure.config.domains.baseconfig](#hexagon.infrastructure.config.domains.baseconfig)
* [hexagon.infrastructure.config.domains.courbe](#hexagon.infrastructure.config.domains.courbe)
* [hexagon.infrastructure.config.domains.default_factories](#hexagon.infrastructure.config.domains.default_factories)
* [hexagon.infrastructure.config.domains.etl](#hexagon.infrastructure.config.domains.etl)
* [hexagon.infrastructure.config.domains.performance](#hexagon.infrastructure.config.domains.performance)
* [hexagon.infrastructure.config.domains.rapports](#hexagon.infrastructure.config.domains.rapports)
* [hexagon.infrastructure.config.domains.reporting](#hexagon.infrastructure.config.domains.reporting)
* [hexagon.infrastructure.config.domains.system](#hexagon.infrastructure.config.domains.system)
* [hexagon.infrastructure.config.domains.veille](#hexagon.infrastructure.config.domains.veille)






    
# Module `hexagon.infrastructure.config.domains.analytics` {#hexagon.infrastructure.config.domains.analytics}







    
## Classes


    
### Class `AnalyticsConfig` {#hexagon.infrastructure.config.domains.analytics.AnalyticsConfig}



> `class AnalyticsConfig(ratios: hexagon.infrastructure.config.domains.analytics.RatiosEngineConfig, performance: hexagon.infrastructure.config.domains.performance.PerformanceConfig, devise: hexagon.infrastructure.config.domains.analytics.DeviseConfig, calendar: hexagon.infrastructure.config.domains.performance.MarketCalendarConfig, echeancier: hexagon.infrastructure.config.domains.analytics.EcheancierConfig = _Nothing.NOTHING)`


Configuration de la partie analytics d'Hexagon.


#### Attributes

- ratios (<code>[RatiosEngineConfig](#hexagon.infrastructure.config.domains.analytics.RatiosEngineConfig "hexagon.infrastructure.config.domains.analytics.RatiosEngineConfig")</code>): Configuration du calcul des Ratios
- performance (<code>PerformanceConfig</code>): Configuration du calcul de la performance. Distributions et variable
- devise (<code>[DeviseConfig](#hexagon.infrastructure.config.domains.analytics.DeviseConfig "hexagon.infrastructure.config.domains.analytics.DeviseConfig")</code>): Configuration de la devise locale
- calendar (<code>MarketCalendarConfig</code>): Configuration du calendrier des dates (indice de référence pour le calendrier)
- echeancier (<code>[EcheancierConfig](#hexagon.infrastructure.config.domains.analytics.EcheancierConfig "hexagon.infrastructure.config.domains.analytics.EcheancierConfig")</code>): Configuration des echeanciers par défaut utilisés pour les rapports des inventaires
Method generated by attrs for class AnalyticsConfig.


    
#### Ancestors (in MRO)

* [hexagon.infrastructure.config.domains.baseconfig.BaseConfig](#hexagon.infrastructure.config.domains.baseconfig.BaseConfig)




    
#### Instance variables


    
##### Variable `calendar` {#hexagon.infrastructure.config.domains.analytics.AnalyticsConfig.calendar}

Return an attribute of instance, which is of type owner.

    
##### Variable `devise` {#hexagon.infrastructure.config.domains.analytics.AnalyticsConfig.devise}

Return an attribute of instance, which is of type owner.

    
##### Variable `echeancier` {#hexagon.infrastructure.config.domains.analytics.AnalyticsConfig.echeancier}

Return an attribute of instance, which is of type owner.

    
##### Variable `performance` {#hexagon.infrastructure.config.domains.analytics.AnalyticsConfig.performance}

Return an attribute of instance, which is of type owner.

    
##### Variable `ratios` {#hexagon.infrastructure.config.domains.analytics.AnalyticsConfig.ratios}

Return an attribute of instance, which is of type owner.


    
#### Static methods


    
##### `Method default` {#hexagon.infrastructure.config.domains.analytics.AnalyticsConfig.default}



    
> `def default()`





    
### Class `DeviseConfig` {#hexagon.infrastructure.config.domains.analytics.DeviseConfig}



> `class DeviseConfig(code_devise_locale: str, unite_devise_locale: int)`


Devsie locale d'Hexagon


#### Attributes

- code_devise_locale (str='MAD'): Code iso de la devise locale
- unite_devise_locale (int=1): L'unite utilisée lors de l'échange de la monnaie. i.e: 100yen pour 1$
Method generated by attrs for class DeviseConfig.


    
#### Ancestors (in MRO)

* [hexagon.infrastructure.config.domains.baseconfig.BaseConfig](#hexagon.infrastructure.config.domains.baseconfig.BaseConfig)




    
#### Instance variables


    
##### Variable `code_devise_locale` {#hexagon.infrastructure.config.domains.analytics.DeviseConfig.code_devise_locale}

Return an attribute of instance, which is of type owner.

    
##### Variable `unite_devise_locale` {#hexagon.infrastructure.config.domains.analytics.DeviseConfig.unite_devise_locale}

Return an attribute of instance, which is of type owner.


    
#### Static methods


    
##### `Method default` {#hexagon.infrastructure.config.domains.analytics.DeviseConfig.default}



    
> `def default()`





    
### Class `EcheancierConfig` {#hexagon.infrastructure.config.domains.analytics.EcheancierConfig}



> `class EcheancierConfig(echeancier_ptf: list[hexagon.infrastructure.config.domains.analytics.EcheancierPeriodeItem] = _Nothing.NOTHING, echeancier_mbi: list[hexagon.infrastructure.config.domains.analytics.EcheancierPeriodeItem] = _Nothing.NOTHING, echancier_defaut: list[hexagon.infrastructure.config.domains.analytics.EcheancierPeriodeItem] = _Nothing.NOTHING, echancier_sensibilite: list[hexagon.infrastructure.config.domains.analytics.EcheancierSensibiliteItem] = _Nothing.NOTHING)`


Classe mère des modèles de configuration.
La particularité des modèles de config les rend difficiles à raisonner
des structures imbriquées qui nécessitent un ONSM (Object Nested Structures mapper)

Method generated by attrs for class EcheancierConfig.


    
#### Ancestors (in MRO)

* [hexagon.infrastructure.config.domains.baseconfig.BaseConfig](#hexagon.infrastructure.config.domains.baseconfig.BaseConfig)




    
#### Instance variables


    
##### Variable `echancier_defaut` {#hexagon.infrastructure.config.domains.analytics.EcheancierConfig.echancier_defaut}

Return an attribute of instance, which is of type owner.

    
##### Variable `echancier_sensibilite` {#hexagon.infrastructure.config.domains.analytics.EcheancierConfig.echancier_sensibilite}

Return an attribute of instance, which is of type owner.

    
##### Variable `echeancier_mbi` {#hexagon.infrastructure.config.domains.analytics.EcheancierConfig.echeancier_mbi}

Return an attribute of instance, which is of type owner.

    
##### Variable `echeancier_ptf` {#hexagon.infrastructure.config.domains.analytics.EcheancierConfig.echeancier_ptf}

Return an attribute of instance, which is of type owner.


    
#### Static methods


    
##### `Method default` {#hexagon.infrastructure.config.domains.analytics.EcheancierConfig.default}



    
> `def default()`





    
### Class `EcheancierPeriodeItem` {#hexagon.infrastructure.config.domains.analytics.EcheancierPeriodeItem}



> `class EcheancierPeriodeItem(order: int, min: dict, max: dict, tagname: str = None)`


Classe mère des modèles de configuration.
La particularité des modèles de config les rend difficiles à raisonner
des structures imbriquées qui nécessitent un ONSM (Object Nested Structures mapper)

Method generated by attrs for class EcheancierPeriodeItem.


    
#### Ancestors (in MRO)

* [hexagon.infrastructure.config.domains.baseconfig.BaseConfig](#hexagon.infrastructure.config.domains.baseconfig.BaseConfig)




    
#### Instance variables


    
##### Variable `max` {#hexagon.infrastructure.config.domains.analytics.EcheancierPeriodeItem.max}

Return an attribute of instance, which is of type owner.

    
##### Variable `min` {#hexagon.infrastructure.config.domains.analytics.EcheancierPeriodeItem.min}

Return an attribute of instance, which is of type owner.

    
##### Variable `order` {#hexagon.infrastructure.config.domains.analytics.EcheancierPeriodeItem.order}

Return an attribute of instance, which is of type owner.

    
##### Variable `tagname` {#hexagon.infrastructure.config.domains.analytics.EcheancierPeriodeItem.tagname}

Return an attribute of instance, which is of type owner.


    
#### Static methods


    
##### `Method from_doc` {#hexagon.infrastructure.config.domains.analytics.EcheancierPeriodeItem.from_doc}



    
> `def from_doc(doc: dict)`





    
#### Methods


    
##### Method `default` {#hexagon.infrastructure.config.domains.analytics.EcheancierPeriodeItem.default}



    
> `def default(cls)`




    
### Class `EcheancierSensibiliteItem` {#hexagon.infrastructure.config.domains.analytics.EcheancierSensibiliteItem}



> `class EcheancierSensibiliteItem(order: int, min: float, max: float, include: str, tagname: str = None)`


Classe mère des modèles de configuration.
La particularité des modèles de config les rend difficiles à raisonner
des structures imbriquées qui nécessitent un ONSM (Object Nested Structures mapper)

Method generated by attrs for class EcheancierSensibiliteItem.


    
#### Ancestors (in MRO)

* [hexagon.infrastructure.config.domains.baseconfig.BaseConfig](#hexagon.infrastructure.config.domains.baseconfig.BaseConfig)




    
#### Instance variables


    
##### Variable `include` {#hexagon.infrastructure.config.domains.analytics.EcheancierSensibiliteItem.include}

Return an attribute of instance, which is of type owner.

    
##### Variable `max` {#hexagon.infrastructure.config.domains.analytics.EcheancierSensibiliteItem.max}

Return an attribute of instance, which is of type owner.

    
##### Variable `min` {#hexagon.infrastructure.config.domains.analytics.EcheancierSensibiliteItem.min}

Return an attribute of instance, which is of type owner.

    
##### Variable `order` {#hexagon.infrastructure.config.domains.analytics.EcheancierSensibiliteItem.order}

Return an attribute of instance, which is of type owner.

    
##### Variable `tagname` {#hexagon.infrastructure.config.domains.analytics.EcheancierSensibiliteItem.tagname}

Return an attribute of instance, which is of type owner.


    
#### Static methods


    
##### `Method from_doc` {#hexagon.infrastructure.config.domains.analytics.EcheancierSensibiliteItem.from_doc}



    
> `def from_doc(doc: dict)`





    
#### Methods


    
##### Method `default` {#hexagon.infrastructure.config.domains.analytics.EcheancierSensibiliteItem.default}



    
> `def default(cls)`




    
### Class `RatiosEngineConfig` {#hexagon.infrastructure.config.domains.analytics.RatiosEngineConfig}



> `class RatiosEngineConfig(periode_performance: str, type_benchmark: str, adjust_stdev: bool, annualiser_performance: bool, var_intervalle_confidence: int, var_coeff_stdev: float, indices_sans_risque: list[str] = _Nothing.NOTHING)`


Configuration du calcul des Ratios de performance.
La configuration est utilisée par défaut pour la génération des rapports OPCVM, des reporting,
des analytics et la fiche performance.


#### Attributes

- periode_performance (str='1:A'): La période de l'historique à prendre en considération. defaut='1:A'
- type_benchmark (str='COM'): le type de benchmark à utiliser. Utilise le benchmark commercial
- adjust_stdev (bool=True): Ajuste le calcul de l'ecart-type par la période (Racine(N-1))
- annualiser_performance (bool=False): Annualise la performance avant de procéder au traitements. Ratio de Sharpe, Ratio d'information
- var_intervalle_confidence (int=95): L'intervalle de confiance pour calculer la VaR. Methode simple qui suppose la normalité.
- var_coeff_stdev (float=1.96): Le coefficient utilisé pour l'intervalle de confidence. N'a pas d'impacte juste pour l'affichage
- indices_sans_risque (list['MONIA', TMP]): Liste des codes des indices à considérer comme indices sans risque pour le calcul du ratio de sharpe.
**Tip:** 
Pour les historiques longs, l'indice sans risque récent sera complété/capitalisé par celui d'après.

Method generated by attrs for class RatiosEngineConfig.


    
#### Ancestors (in MRO)

* [hexagon.infrastructure.config.domains.baseconfig.BaseConfig](#hexagon.infrastructure.config.domains.baseconfig.BaseConfig)




    
#### Instance variables


    
##### Variable `adjust_stdev` {#hexagon.infrastructure.config.domains.analytics.RatiosEngineConfig.adjust_stdev}

Return an attribute of instance, which is of type owner.

    
##### Variable `annualiser_performance` {#hexagon.infrastructure.config.domains.analytics.RatiosEngineConfig.annualiser_performance}

Return an attribute of instance, which is of type owner.

    
##### Variable `indices_sans_risque` {#hexagon.infrastructure.config.domains.analytics.RatiosEngineConfig.indices_sans_risque}

Return an attribute of instance, which is of type owner.

    
##### Variable `periode_performance` {#hexagon.infrastructure.config.domains.analytics.RatiosEngineConfig.periode_performance}

Return an attribute of instance, which is of type owner.

    
##### Variable `type_benchmark` {#hexagon.infrastructure.config.domains.analytics.RatiosEngineConfig.type_benchmark}

Return an attribute of instance, which is of type owner.

    
##### Variable `var_coeff_stdev` {#hexagon.infrastructure.config.domains.analytics.RatiosEngineConfig.var_coeff_stdev}

Return an attribute of instance, which is of type owner.

    
##### Variable `var_intervalle_confidence` {#hexagon.infrastructure.config.domains.analytics.RatiosEngineConfig.var_intervalle_confidence}

Return an attribute of instance, which is of type owner.


    
#### Static methods


    
##### `Method default` {#hexagon.infrastructure.config.domains.analytics.RatiosEngineConfig.default}



    
> `def default()`







    
# Module `hexagon.infrastructure.config.domains.baseconfig` {#hexagon.infrastructure.config.domains.baseconfig}







    
## Classes


    
### Class `BaseConfig` {#hexagon.infrastructure.config.domains.baseconfig.BaseConfig}



> `class BaseConfig()`


Classe mère des modèles de configuration.
La particularité des modèles de config les rend difficiles à raisonner
des structures imbriquées qui nécessitent un ONSM (Object Nested Structures mapper)

Method generated by attrs for class BaseConfig.



    
#### Descendants

* [hexagon.infrastructure.config.domains.analytics.AnalyticsConfig](#hexagon.infrastructure.config.domains.analytics.AnalyticsConfig)
* [hexagon.infrastructure.config.domains.analytics.DeviseConfig](#hexagon.infrastructure.config.domains.analytics.DeviseConfig)
* [hexagon.infrastructure.config.domains.analytics.EcheancierConfig](#hexagon.infrastructure.config.domains.analytics.EcheancierConfig)
* [hexagon.infrastructure.config.domains.analytics.EcheancierPeriodeItem](#hexagon.infrastructure.config.domains.analytics.EcheancierPeriodeItem)
* [hexagon.infrastructure.config.domains.analytics.EcheancierSensibiliteItem](#hexagon.infrastructure.config.domains.analytics.EcheancierSensibiliteItem)
* [hexagon.infrastructure.config.domains.analytics.RatiosEngineConfig](#hexagon.infrastructure.config.domains.analytics.RatiosEngineConfig)
* [hexagon.infrastructure.config.domains.courbe.CourbeConfig](#hexagon.infrastructure.config.domains.courbe.CourbeConfig)
* [hexagon.infrastructure.config.domains.etl.CSVDialectConfig](#hexagon.infrastructure.config.domains.etl.CSVDialectConfig)
* [hexagon.infrastructure.config.domains.etl.ETLConfig](#hexagon.infrastructure.config.domains.etl.ETLConfig)
* [hexagon.infrastructure.config.domains.etl.YamlDumpConfig](#hexagon.infrastructure.config.domains.etl.YamlDumpConfig)
* [hexagon.infrastructure.config.domains.performance.MarketCalendarConfig](#hexagon.infrastructure.config.domains.performance.MarketCalendarConfig)
* [hexagon.infrastructure.config.domains.performance.PerformanceConfig](#hexagon.infrastructure.config.domains.performance.PerformanceConfig)
* [hexagon.infrastructure.config.domains.rapports.RapportsConfig](#hexagon.infrastructure.config.domains.rapports.RapportsConfig)
* [hexagon.infrastructure.config.domains.reporting.PaletteHexCKG](#hexagon.infrastructure.config.domains.reporting.PaletteHexCKG)
* [hexagon.infrastructure.config.domains.reporting.RapportGlobalConfig](#hexagon.infrastructure.config.domains.reporting.RapportGlobalConfig)
* [hexagon.infrastructure.config.domains.reporting.ReportingConfig](#hexagon.infrastructure.config.domains.reporting.ReportingConfig)
* [hexagon.infrastructure.config.domains.reporting.TGEConfig](#hexagon.infrastructure.config.domains.reporting.TGEConfig)
* [hexagon.infrastructure.config.domains.reporting.TGEDataDataMapperConfig](#hexagon.infrastructure.config.domains.reporting.TGEDataDataMapperConfig)
* [hexagon.infrastructure.config.domains.system.APICredentials](#hexagon.infrastructure.config.domains.system.APICredentials)
* [hexagon.infrastructure.config.domains.system.APPInfo](#hexagon.infrastructure.config.domains.system.APPInfo)
* [hexagon.infrastructure.config.domains.system.ApiClientConfig](#hexagon.infrastructure.config.domains.system.ApiClientConfig)
* [hexagon.infrastructure.config.domains.system.BkamApiConfig](#hexagon.infrastructure.config.domains.system.BkamApiConfig)
* [hexagon.infrastructure.config.domains.system.DatabaseURL](#hexagon.infrastructure.config.domains.system.DatabaseURL)
* [hexagon.infrastructure.config.domains.system.HttpxClientConfig](#hexagon.infrastructure.config.domains.system.HttpxClientConfig)
* [hexagon.infrastructure.config.domains.system.ORMEngineConfig](#hexagon.infrastructure.config.domains.system.ORMEngineConfig)
* [hexagon.infrastructure.config.domains.system.SeleniumConfig](#hexagon.infrastructure.config.domains.system.SeleniumConfig)
* [hexagon.infrastructure.config.domains.system.SystemConfig](#hexagon.infrastructure.config.domains.system.SystemConfig)
* [hexagon.infrastructure.config.domains.veille.VeilleConfig](#hexagon.infrastructure.config.domains.veille.VeilleConfig)




    
#### Static methods


    
##### `Method default` {#hexagon.infrastructure.config.domains.baseconfig.BaseConfig.default}



    
> `def default()`







    
# Module `hexagon.infrastructure.config.domains.courbe` {#hexagon.infrastructure.config.domains.courbe}







    
## Classes


    
### Class `CourbeConfig` {#hexagon.infrastructure.config.domains.courbe.CourbeConfig}



> `class CourbeConfig()`


Classe mère des modèles de configuration.
La particularité des modèles de config les rend difficiles à raisonner
des structures imbriquées qui nécessitent un ONSM (Object Nested Structures mapper)

Method generated by attrs for class CourbeConfig.


    
#### Ancestors (in MRO)

* [hexagon.infrastructure.config.domains.baseconfig.BaseConfig](#hexagon.infrastructure.config.domains.baseconfig.BaseConfig)








    
# Module `hexagon.infrastructure.config.domains.default_factories` {#hexagon.infrastructure.config.domains.default_factories}






    
## Functions


    
### Function `default_httpx_client_headers` {#hexagon.infrastructure.config.domains.default_factories.default_httpx_client_headers}



    
> `def default_httpx_client_headers() -> dict`




    
### Function `default_rapport_global_cols_inventaire` {#hexagon.infrastructure.config.domains.default_factories.default_rapport_global_cols_inventaire}



    
> `def default_rapport_global_cols_inventaire() -> list[str]`




    
### Function `default_rapport_global_cols_operations` {#hexagon.infrastructure.config.domains.default_factories.default_rapport_global_cols_operations}



    
> `def default_rapport_global_cols_operations() -> list[str]`




    
### Function `default_tge_categorie_mapper` {#hexagon.infrastructure.config.domains.default_factories.default_tge_categorie_mapper}



    
> `def default_tge_categorie_mapper() -> dict[str, str]`




    
### Function `default_tge_mapper_cols_inventaire_agg` {#hexagon.infrastructure.config.domains.default_factories.default_tge_mapper_cols_inventaire_agg}



    
> `def default_tge_mapper_cols_inventaire_agg() -> list[str]`




    
### Function `default_tge_mapper_cols_operation_periode` {#hexagon.infrastructure.config.domains.default_factories.default_tge_mapper_cols_operation_periode}



    
> `def default_tge_mapper_cols_operation_periode() -> list[str]`




    
### Function `default_tge_mapper_cols_poche_act` {#hexagon.infrastructure.config.domains.default_factories.default_tge_mapper_cols_poche_act}



    
> `def default_tge_mapper_cols_poche_act() -> list[str]`




    
### Function `default_tge_mapper_cols_poche_obl` {#hexagon.infrastructure.config.domains.default_factories.default_tge_mapper_cols_poche_obl}



    
> `def default_tge_mapper_cols_poche_obl() -> list[str]`




    
### Function `default_tge_output_dir` {#hexagon.infrastructure.config.domains.default_factories.default_tge_output_dir}



    
> `def default_tge_output_dir() -> pathlib.Path`




    
### Function `factory_default_echeancier` {#hexagon.infrastructure.config.domains.default_factories.factory_default_echeancier}



    
> `def factory_default_echeancier() -> list`




    
### Function `factory_default_echeancier_mbi` {#hexagon.infrastructure.config.domains.default_factories.factory_default_echeancier_mbi}



    
> `def factory_default_echeancier_mbi() -> list`




    
### Function `factory_default_fonds_hebdo` {#hexagon.infrastructure.config.domains.default_factories.factory_default_fonds_hebdo}



    
> `def factory_default_fonds_hebdo() -> list[str]`




    
### Function `factory_default_periodes` {#hexagon.infrastructure.config.domains.default_factories.factory_default_periodes}



    
> `def factory_default_periodes() -> list[str]`




    
### Function `factory_default_sensibilite` {#hexagon.infrastructure.config.domains.default_factories.factory_default_sensibilite}



    
> `def factory_default_sensibilite() -> list`







    
# Module `hexagon.infrastructure.config.domains.etl` {#hexagon.infrastructure.config.domains.etl}







    
## Classes


    
### Class `CSVDialectConfig` {#hexagon.infrastructure.config.domains.etl.CSVDialectConfig}



> `class CSVDialectConfig(delimiter: str = ';', decimal: str = ',')`


Classe mère des modèles de configuration.
La particularité des modèles de config les rend difficiles à raisonner
des structures imbriquées qui nécessitent un ONSM (Object Nested Structures mapper)

Method generated by attrs for class CSVDialectConfig.


    
#### Ancestors (in MRO)

* [hexagon.infrastructure.config.domains.baseconfig.BaseConfig](#hexagon.infrastructure.config.domains.baseconfig.BaseConfig)




    
#### Instance variables


    
##### Variable `decimal` {#hexagon.infrastructure.config.domains.etl.CSVDialectConfig.decimal}

Return an attribute of instance, which is of type owner.

    
##### Variable `delimiter` {#hexagon.infrastructure.config.domains.etl.CSVDialectConfig.delimiter}

Return an attribute of instance, which is of type owner.


    
#### Static methods


    
##### `Method default` {#hexagon.infrastructure.config.domains.etl.CSVDialectConfig.default}



    
> `def default()`





    
### Class `ETLConfig` {#hexagon.infrastructure.config.domains.etl.ETLConfig}



> `class ETLConfig()`


Classe mère des modèles de configuration.
La particularité des modèles de config les rend difficiles à raisonner
des structures imbriquées qui nécessitent un ONSM (Object Nested Structures mapper)

Method generated by attrs for class ETLConfig.


    
#### Ancestors (in MRO)

* [hexagon.infrastructure.config.domains.baseconfig.BaseConfig](#hexagon.infrastructure.config.domains.baseconfig.BaseConfig)






    
### Class `YamlDumpConfig` {#hexagon.infrastructure.config.domains.etl.YamlDumpConfig}



> `class YamlDumpConfig(default_flow_style: bool = False, indent: int = 2, sort_keys: bool = False, encoding: str = 'utf8')`


Classe mère des modèles de configuration.
La particularité des modèles de config les rend difficiles à raisonner
des structures imbriquées qui nécessitent un ONSM (Object Nested Structures mapper)

Method generated by attrs for class YamlDumpConfig.


    
#### Ancestors (in MRO)

* [hexagon.infrastructure.config.domains.baseconfig.BaseConfig](#hexagon.infrastructure.config.domains.baseconfig.BaseConfig)




    
#### Instance variables


    
##### Variable `default_flow_style` {#hexagon.infrastructure.config.domains.etl.YamlDumpConfig.default_flow_style}

Return an attribute of instance, which is of type owner.

    
##### Variable `encoding` {#hexagon.infrastructure.config.domains.etl.YamlDumpConfig.encoding}

Return an attribute of instance, which is of type owner.

    
##### Variable `indent` {#hexagon.infrastructure.config.domains.etl.YamlDumpConfig.indent}

Return an attribute of instance, which is of type owner.

    
##### Variable `sort_keys` {#hexagon.infrastructure.config.domains.etl.YamlDumpConfig.sort_keys}

Return an attribute of instance, which is of type owner.


    
#### Static methods


    
##### `Method default` {#hexagon.infrastructure.config.domains.etl.YamlDumpConfig.default}



    
> `def default()`







    
# Module `hexagon.infrastructure.config.domains.performance` {#hexagon.infrastructure.config.domains.performance}







    
## Classes


    
### Class `MarketCalendarConfig` {#hexagon.infrastructure.config.domains.performance.MarketCalendarConfig}



> `class MarketCalendarConfig(code_indice_base: str, weekends: tuple = (5, 6))`


Configuration du Calendrier interne d'Hexagon


#### Attributes

- code_indice_base (str='MASI'): Le code de l'indice marché à prendre comme référence. defaut='MASI'
- weekends (tuple=(5, 6)): Les jours weekends de la semaine depuis le Lundi. Samedi=5, dimanche=6, Lundi=0
Method generated by attrs for class MarketCalendarConfig.


    
#### Ancestors (in MRO)

* [hexagon.infrastructure.config.domains.baseconfig.BaseConfig](#hexagon.infrastructure.config.domains.baseconfig.BaseConfig)




    
#### Instance variables


    
##### Variable `code_indice_base` {#hexagon.infrastructure.config.domains.performance.MarketCalendarConfig.code_indice_base}

Return an attribute of instance, which is of type owner.

    
##### Variable `weekends` {#hexagon.infrastructure.config.domains.performance.MarketCalendarConfig.weekends}

Return an attribute of instance, which is of type owner.


    
#### Static methods


    
##### `Method default` {#hexagon.infrastructure.config.domains.performance.MarketCalendarConfig.default}



    
> `def default()`





    
### Class `PerformanceConfig` {#hexagon.infrastructure.config.domains.performance.PerformanceConfig}



> `class PerformanceConfig(inclure_distribution: bool = True, inclure_variable: bool = False, periodes_defaut: list[str] = _Nothing.NOTHING)`


Configuration du module de calcul de la performance utilisée par défaut par Hexagon


#### Attributes

- inclure_distribution (bool=True): Inclure les distributions lors du calcul de la performance des Fonds. defaut=True
- inclure_variable (bool=False): Inclure le variable lors du calcul de la performance des Fonds
- periodes_defaut (list[str]): Liste des périodes par défaut pour le calcul des performances.
    Utilisé par divers rapports internes et les reporting. defaut=['1:S', '1:M', '3:M', '6:M', '1:A', '2:A', '3:A', '5:A', 'YTD']
Method generated by attrs for class PerformanceConfig.


    
#### Ancestors (in MRO)

* [hexagon.infrastructure.config.domains.baseconfig.BaseConfig](#hexagon.infrastructure.config.domains.baseconfig.BaseConfig)




    
#### Instance variables


    
##### Variable `inclure_distribution` {#hexagon.infrastructure.config.domains.performance.PerformanceConfig.inclure_distribution}

Return an attribute of instance, which is of type owner.

    
##### Variable `inclure_variable` {#hexagon.infrastructure.config.domains.performance.PerformanceConfig.inclure_variable}

Return an attribute of instance, which is of type owner.

    
##### Variable `periodes_defaut` {#hexagon.infrastructure.config.domains.performance.PerformanceConfig.periodes_defaut}

Return an attribute of instance, which is of type owner.


    
#### Static methods


    
##### `Method default` {#hexagon.infrastructure.config.domains.performance.PerformanceConfig.default}



    
> `def default()`







    
# Module `hexagon.infrastructure.config.domains.rapports` {#hexagon.infrastructure.config.domains.rapports}







    
## Classes


    
### Class `RapportsConfig` {#hexagon.infrastructure.config.domains.rapports.RapportsConfig}



> `class RapportsConfig()`


Classe mère des modèles de configuration.
La particularité des modèles de config les rend difficiles à raisonner
des structures imbriquées qui nécessitent un ONSM (Object Nested Structures mapper)

Method generated by attrs for class RapportsConfig.


    
#### Ancestors (in MRO)

* [hexagon.infrastructure.config.domains.baseconfig.BaseConfig](#hexagon.infrastructure.config.domains.baseconfig.BaseConfig)








    
# Module `hexagon.infrastructure.config.domains.reporting` {#hexagon.infrastructure.config.domains.reporting}

Modèles de configuration du Template Generator Engine (TGE)





    
## Classes


    
### Class `PaletteHexCKG` {#hexagon.infrastructure.config.domains.reporting.PaletteHexCKG}



> `class PaletteHexCKG(white: str, light_blue: str, dark_blue: str, light_grey: str, dark_grey: str, light_green: str, dark_green: str, orange: str, black: str)`


Classe mère des modèles de configuration.
La particularité des modèles de config les rend difficiles à raisonner
des structures imbriquées qui nécessitent un ONSM (Object Nested Structures mapper)

Method generated by attrs for class PaletteHexCKG.


    
#### Ancestors (in MRO)

* [hexagon.infrastructure.config.domains.baseconfig.BaseConfig](#hexagon.infrastructure.config.domains.baseconfig.BaseConfig)




    
#### Instance variables


    
##### Variable `black` {#hexagon.infrastructure.config.domains.reporting.PaletteHexCKG.black}

Return an attribute of instance, which is of type owner.

    
##### Variable `dark_blue` {#hexagon.infrastructure.config.domains.reporting.PaletteHexCKG.dark_blue}

Return an attribute of instance, which is of type owner.

    
##### Variable `dark_green` {#hexagon.infrastructure.config.domains.reporting.PaletteHexCKG.dark_green}

Return an attribute of instance, which is of type owner.

    
##### Variable `dark_grey` {#hexagon.infrastructure.config.domains.reporting.PaletteHexCKG.dark_grey}

Return an attribute of instance, which is of type owner.

    
##### Variable `light_blue` {#hexagon.infrastructure.config.domains.reporting.PaletteHexCKG.light_blue}

Return an attribute of instance, which is of type owner.

    
##### Variable `light_green` {#hexagon.infrastructure.config.domains.reporting.PaletteHexCKG.light_green}

Return an attribute of instance, which is of type owner.

    
##### Variable `light_grey` {#hexagon.infrastructure.config.domains.reporting.PaletteHexCKG.light_grey}

Return an attribute of instance, which is of type owner.

    
##### Variable `orange` {#hexagon.infrastructure.config.domains.reporting.PaletteHexCKG.orange}

Return an attribute of instance, which is of type owner.

    
##### Variable `white` {#hexagon.infrastructure.config.domains.reporting.PaletteHexCKG.white}

Return an attribute of instance, which is of type owner.


    
#### Static methods


    
##### `Method default` {#hexagon.infrastructure.config.domains.reporting.PaletteHexCKG.default}



    
> `def default()`





    
#### Methods


    
##### Method `to_rgb` {#hexagon.infrastructure.config.domains.reporting.PaletteHexCKG.to_rgb}



    
> `def to_rgb(self, hexcode: str) -> tuple[int, int, int]`


Transforme un code couleur HEX en un tuple RGB.
Split le code en deux et procède à la conversion en tuple de 3 nombres entiers. (R, G, B)


###### Args

**```hexcode```** :&ensp;<code>str</code>
:   Code couleur HEXADECIMAL complet. ex: 'a5a5a5', '70AD47', '000000'



###### Returns

<code>tuple\[int, int, int]</code>
:   Code RGB au format (R, G, B)



    
##### Method `to_rgb_all` {#hexagon.infrastructure.config.domains.reporting.PaletteHexCKG.to_rgb_all}



    
> `def to_rgb_all(self) -> dict[str, tuple[int, int, int]]`


Transforme la strucutre en dict RGB depuis les codes Hexadécimaux de la structure


###### Returns

<code>dict\[str, tuple\[int, int, int]]</code>
:   Description



    
### Class `RapportGlobalConfig` {#hexagon.infrastructure.config.domains.reporting.RapportGlobalConfig}



> `class RapportGlobalConfig(cols_inventaire: list[str], cols_operations: list[str])`


Classe mère des modèles de configuration.
La particularité des modèles de config les rend difficiles à raisonner
des structures imbriquées qui nécessitent un ONSM (Object Nested Structures mapper)

Method generated by attrs for class RapportGlobalConfig.


    
#### Ancestors (in MRO)

* [hexagon.infrastructure.config.domains.baseconfig.BaseConfig](#hexagon.infrastructure.config.domains.baseconfig.BaseConfig)




    
#### Instance variables


    
##### Variable `cols_inventaire` {#hexagon.infrastructure.config.domains.reporting.RapportGlobalConfig.cols_inventaire}

Return an attribute of instance, which is of type owner.

    
##### Variable `cols_operations` {#hexagon.infrastructure.config.domains.reporting.RapportGlobalConfig.cols_operations}

Return an attribute of instance, which is of type owner.


    
#### Static methods


    
##### `Method default` {#hexagon.infrastructure.config.domains.reporting.RapportGlobalConfig.default}



    
> `def default()`





    
### Class `ReportingConfig` {#hexagon.infrastructure.config.domains.reporting.ReportingConfig}



> `class ReportingConfig(tge_engine: hexagon.infrastructure.config.domains.reporting.TGEConfig, tge_data_mapper: hexagon.infrastructure.config.domains.reporting.TGEDataDataMapperConfig, rapport_global: hexagon.infrastructure.config.domains.reporting.RapportGlobalConfig, fonds_hebdo: list[str] = _Nothing.NOTHING, palette_hex: hexagon.infrastructure.config.domains.reporting.PaletteHexCKG = _Nothing.NOTHING)`


Classe mère des modèles de configuration.
La particularité des modèles de config les rend difficiles à raisonner
des structures imbriquées qui nécessitent un ONSM (Object Nested Structures mapper)

Method generated by attrs for class ReportingConfig.


    
#### Ancestors (in MRO)

* [hexagon.infrastructure.config.domains.baseconfig.BaseConfig](#hexagon.infrastructure.config.domains.baseconfig.BaseConfig)




    
#### Instance variables


    
##### Variable `fonds_hebdo` {#hexagon.infrastructure.config.domains.reporting.ReportingConfig.fonds_hebdo}

Return an attribute of instance, which is of type owner.

    
##### Variable `palette_hex` {#hexagon.infrastructure.config.domains.reporting.ReportingConfig.palette_hex}

Return an attribute of instance, which is of type owner.

    
##### Variable `rapport_global` {#hexagon.infrastructure.config.domains.reporting.ReportingConfig.rapport_global}

Return an attribute of instance, which is of type owner.

    
##### Variable `tge_data_mapper` {#hexagon.infrastructure.config.domains.reporting.ReportingConfig.tge_data_mapper}

Return an attribute of instance, which is of type owner.

    
##### Variable `tge_engine` {#hexagon.infrastructure.config.domains.reporting.ReportingConfig.tge_engine}

Return an attribute of instance, which is of type owner.


    
#### Static methods


    
##### `Method default` {#hexagon.infrastructure.config.domains.reporting.ReportingConfig.default}



    
> `def default()`





    
### Class `TGEConfig` {#hexagon.infrastructure.config.domains.reporting.TGEConfig}



> `class TGEConfig(tge_doctype: str, tge_docname: str, tge_periode_defaut: str, output_dir: pathlib.Path = _Nothing.NOTHING, categorie_mapper: dict = _Nothing.NOTHING)`


Classe mère des modèles de configuration.
La particularité des modèles de config les rend difficiles à raisonner
des structures imbriquées qui nécessitent un ONSM (Object Nested Structures mapper)

Method generated by attrs for class TGEConfig.


    
#### Ancestors (in MRO)

* [hexagon.infrastructure.config.domains.baseconfig.BaseConfig](#hexagon.infrastructure.config.domains.baseconfig.BaseConfig)




    
#### Instance variables


    
##### Variable `categorie_mapper` {#hexagon.infrastructure.config.domains.reporting.TGEConfig.categorie_mapper}

Return an attribute of instance, which is of type owner.

    
##### Variable `output_dir` {#hexagon.infrastructure.config.domains.reporting.TGEConfig.output_dir}

Return an attribute of instance, which is of type owner.

    
##### Variable `tge_docname` {#hexagon.infrastructure.config.domains.reporting.TGEConfig.tge_docname}

Return an attribute of instance, which is of type owner.

    
##### Variable `tge_doctype` {#hexagon.infrastructure.config.domains.reporting.TGEConfig.tge_doctype}

Return an attribute of instance, which is of type owner.

    
##### Variable `tge_periode_defaut` {#hexagon.infrastructure.config.domains.reporting.TGEConfig.tge_periode_defaut}

Return an attribute of instance, which is of type owner.


    
#### Static methods


    
##### `Method default` {#hexagon.infrastructure.config.domains.reporting.TGEConfig.default}



    
> `def default()`





    
### Class `TGEDataDataMapperConfig` {#hexagon.infrastructure.config.domains.reporting.TGEDataDataMapperConfig}



> `class TGEDataDataMapperConfig(cols_inventaire_agg: list[str], cols_poche_obl: list[str], cols_poche_act: list[str], cols_operation_periode: list[str])`


Classe mère des modèles de configuration.
La particularité des modèles de config les rend difficiles à raisonner
des structures imbriquées qui nécessitent un ONSM (Object Nested Structures mapper)

Method generated by attrs for class TGEDataDataMapperConfig.


    
#### Ancestors (in MRO)

* [hexagon.infrastructure.config.domains.baseconfig.BaseConfig](#hexagon.infrastructure.config.domains.baseconfig.BaseConfig)




    
#### Instance variables


    
##### Variable `cols_inventaire_agg` {#hexagon.infrastructure.config.domains.reporting.TGEDataDataMapperConfig.cols_inventaire_agg}

Return an attribute of instance, which is of type owner.

    
##### Variable `cols_operation_periode` {#hexagon.infrastructure.config.domains.reporting.TGEDataDataMapperConfig.cols_operation_periode}

Return an attribute of instance, which is of type owner.

    
##### Variable `cols_poche_act` {#hexagon.infrastructure.config.domains.reporting.TGEDataDataMapperConfig.cols_poche_act}

Return an attribute of instance, which is of type owner.

    
##### Variable `cols_poche_obl` {#hexagon.infrastructure.config.domains.reporting.TGEDataDataMapperConfig.cols_poche_obl}

Return an attribute of instance, which is of type owner.


    
#### Static methods


    
##### `Method default` {#hexagon.infrastructure.config.domains.reporting.TGEDataDataMapperConfig.default}



    
> `def default()`







    
# Module `hexagon.infrastructure.config.domains.system` {#hexagon.infrastructure.config.domains.system}







    
## Classes


    
### Class `APICredentials` {#hexagon.infrastructure.config.domains.system.APICredentials}



> `class APICredentials(uname: str = None, mail: str = None, password: str = None, token: str = None)`


Classe de Configuration Pour les credentials des API

Method generated by attrs for class APICredentials.


    
#### Ancestors (in MRO)

* [hexagon.infrastructure.config.domains.baseconfig.BaseConfig](#hexagon.infrastructure.config.domains.baseconfig.BaseConfig)


    
#### Descendants

* [hexagon.infrastructure.config.domains.system._BkamCredentials](#hexagon.infrastructure.config.domains.system._BkamCredentials)



    
#### Instance variables


    
##### Variable `mail` {#hexagon.infrastructure.config.domains.system.APICredentials.mail}

Return an attribute of instance, which is of type owner.

    
##### Variable `password` {#hexagon.infrastructure.config.domains.system.APICredentials.password}

Return an attribute of instance, which is of type owner.

    
##### Variable `token` {#hexagon.infrastructure.config.domains.system.APICredentials.token}

Return an attribute of instance, which is of type owner.

    
##### Variable `uname` {#hexagon.infrastructure.config.domains.system.APICredentials.uname}

Return an attribute of instance, which is of type owner.


    
#### Static methods


    
##### `Method default` {#hexagon.infrastructure.config.domains.system.APICredentials.default}



    
> `def default()`





    
### Class `APPInfo` {#hexagon.infrastructure.config.domains.system.APPInfo}



> `class APPInfo(version: str = '2023.04.1', authors: tuple[str] = ('M.FAKIR',), company: str = 'CDG CAPITAL GESTION')`


Contient par défaut les informations sur l'Application Hexagon

Method generated by attrs for class APPInfo.


    
#### Ancestors (in MRO)

* [hexagon.infrastructure.config.domains.baseconfig.BaseConfig](#hexagon.infrastructure.config.domains.baseconfig.BaseConfig)




    
#### Instance variables


    
##### Variable `authors` {#hexagon.infrastructure.config.domains.system.APPInfo.authors}

Return an attribute of instance, which is of type owner.

    
##### Variable `company` {#hexagon.infrastructure.config.domains.system.APPInfo.company}

Return an attribute of instance, which is of type owner.

    
##### Variable `version` {#hexagon.infrastructure.config.domains.system.APPInfo.version}

Return an attribute of instance, which is of type owner.


    
#### Static methods


    
##### `Method default` {#hexagon.infrastructure.config.domains.system.APPInfo.default}



    
> `def default()`





    
### Class `ApiClientConfig` {#hexagon.infrastructure.config.domains.system.ApiClientConfig}



> `class ApiClientConfig(endpoint: str, apiname: str = 'API-Client : Unnamed', token_headers: dict = _Nothing.NOTHING, time_between_queries: float = 0.1, client_config: hexagon.infrastructure.config.domains.system.HttpxClientConfig = _Nothing.NOTHING)`


classe de configuration httpx pour un client API

Method generated by attrs for class ApiClientConfig.


    
#### Ancestors (in MRO)

* [hexagon.infrastructure.config.domains.baseconfig.BaseConfig](#hexagon.infrastructure.config.domains.baseconfig.BaseConfig)




    
#### Instance variables


    
##### Variable `apiname` {#hexagon.infrastructure.config.domains.system.ApiClientConfig.apiname}

Return an attribute of instance, which is of type owner.

    
##### Variable `client_config` {#hexagon.infrastructure.config.domains.system.ApiClientConfig.client_config}

Return an attribute of instance, which is of type owner.

    
##### Variable `endpoint` {#hexagon.infrastructure.config.domains.system.ApiClientConfig.endpoint}

Return an attribute of instance, which is of type owner.

    
##### Variable `time_between_queries` {#hexagon.infrastructure.config.domains.system.ApiClientConfig.time_between_queries}

Return an attribute of instance, which is of type owner.

    
##### Variable `token_headers` {#hexagon.infrastructure.config.domains.system.ApiClientConfig.token_headers}

Return an attribute of instance, which is of type owner.


    
#### Static methods


    
##### `Method default` {#hexagon.infrastructure.config.domains.system.ApiClientConfig.default}



    
> `def default()`





    
### Class `BkamApiConfig` {#hexagon.infrastructure.config.domains.system.BkamApiConfig}



> `class BkamApiConfig(cours_devise: hexagon.infrastructure.config.domains.system.ApiClientConfig = ApiClientConfig(endpoint='https://api.centralbankofmorocco.ma/cours/Version1/api/CoursVirement', apiname='BKAM COURS DEVISE BBQ', token_headers={'Ocp-Apim-Subscription-Key': 'b7302ebb2e9a444ea6cc80879df65377'}, time_between_queries=46, client_config=HttpxClientConfig(headers={'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/15.6 Safari/605.1.15', 'X-Client-Agent': 'Hexagon Client V 1.0 (httpx)', 'X-HTTPX-VERSION': '0.23.3'}, timeout=10, default_encoding='utf8', ssl_verify=False, ssl_cert=None)), courbe_bam: hexagon.infrastructure.config.domains.system.ApiClientConfig = ApiClientConfig(endpoint='https://api.centralbankofmorocco.ma/mo/Version1/api/CourbeBDT', apiname='BKAM COURBE BDT SECONDAIRE', token_headers={'Ocp-Apim-Subscription-Key': '2bbfe28ae9314f14a5538719dbc68418'}, time_between_queries=46, client_config=HttpxClientConfig(headers={'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/15.6 Safari/605.1.15', 'X-Client-Agent': 'Hexagon Client V 1.0 (httpx)', 'X-HTTPX-VERSION': '0.23.3'}, timeout=10, default_encoding='utf8', ssl_verify=False, ssl_cert=None)))`


Configuration des API BKAM pour les devises et la courbe

Method generated by attrs for class BkamApiConfig.


    
#### Ancestors (in MRO)

* [hexagon.infrastructure.config.domains.baseconfig.BaseConfig](#hexagon.infrastructure.config.domains.baseconfig.BaseConfig)




    
#### Instance variables


    
##### Variable `courbe_bam` {#hexagon.infrastructure.config.domains.system.BkamApiConfig.courbe_bam}

Return an attribute of instance, which is of type owner.

    
##### Variable `cours_devise` {#hexagon.infrastructure.config.domains.system.BkamApiConfig.cours_devise}

Return an attribute of instance, which is of type owner.


    
#### Static methods


    
##### `Method default` {#hexagon.infrastructure.config.domains.system.BkamApiConfig.default}



    
> `def default()`





    
### Class `DatabaseURL` {#hexagon.infrastructure.config.domains.system.DatabaseURL}



> `class DatabaseURL(drivername: str, host: str, port: str, database: str, username: str, password: str)`


Classe de Configuration d'une connexion à une BDD en utilisant sqlalchemy.
similaire à sqlalchemy.engine.URL


#### Attributes

- drivername (str=postgresql+psycopg2): Nom du driver Postgresql. Psycopg2 est utilisé par défaut
- host (str=127.0.0.1): Adresse ip du serveur Postgresql de la Base Hexagon
- port (str=5432): Port découte du serveur Postgresql de la Base Hexagon
- database (str=hexagon): Nom de la base de données. defaut=hexagon
- username (str=hexagon_services): nom de l'utilisateur. defaut=hexagon_services
- password (str): le fameux Mot de passe!
Method generated by attrs for class DatabaseURL.


    
#### Ancestors (in MRO)

* [hexagon.infrastructure.config.domains.baseconfig.BaseConfig](#hexagon.infrastructure.config.domains.baseconfig.BaseConfig)




    
#### Instance variables


    
##### Variable `database` {#hexagon.infrastructure.config.domains.system.DatabaseURL.database}

Return an attribute of instance, which is of type owner.

    
##### Variable `drivername` {#hexagon.infrastructure.config.domains.system.DatabaseURL.drivername}

Return an attribute of instance, which is of type owner.

    
##### Variable `host` {#hexagon.infrastructure.config.domains.system.DatabaseURL.host}

Return an attribute of instance, which is of type owner.

    
##### Variable `password` {#hexagon.infrastructure.config.domains.system.DatabaseURL.password}

Return an attribute of instance, which is of type owner.

    
##### Variable `port` {#hexagon.infrastructure.config.domains.system.DatabaseURL.port}

Return an attribute of instance, which is of type owner.

    
##### Variable `username` {#hexagon.infrastructure.config.domains.system.DatabaseURL.username}

Return an attribute of instance, which is of type owner.


    
#### Static methods


    
##### `Method default` {#hexagon.infrastructure.config.domains.system.DatabaseURL.default}



    
> `def default()`





    
### Class `HttpxClientConfig` {#hexagon.infrastructure.config.domains.system.HttpxClientConfig}



> `class HttpxClientConfig(headers: dict[str, str] = _Nothing.NOTHING, timeout: int = 10, default_encoding: str = 'utf8', ssl_verify: bool = False, ssl_cert: pathlib.Path = None)`


Configuration globale du client httpx

Method generated by attrs for class HttpxClientConfig.


    
#### Ancestors (in MRO)

* [hexagon.infrastructure.config.domains.baseconfig.BaseConfig](#hexagon.infrastructure.config.domains.baseconfig.BaseConfig)




    
#### Instance variables


    
##### Variable `default_encoding` {#hexagon.infrastructure.config.domains.system.HttpxClientConfig.default_encoding}

Return an attribute of instance, which is of type owner.

    
##### Variable `headers` {#hexagon.infrastructure.config.domains.system.HttpxClientConfig.headers}

Return an attribute of instance, which is of type owner.

    
##### Variable `ssl_cert` {#hexagon.infrastructure.config.domains.system.HttpxClientConfig.ssl_cert}

Return an attribute of instance, which is of type owner.

    
##### Variable `ssl_verify` {#hexagon.infrastructure.config.domains.system.HttpxClientConfig.ssl_verify}

Return an attribute of instance, which is of type owner.

    
##### Variable `timeout` {#hexagon.infrastructure.config.domains.system.HttpxClientConfig.timeout}

Return an attribute of instance, which is of type owner.


    
#### Static methods


    
##### `Method default` {#hexagon.infrastructure.config.domains.system.HttpxClientConfig.default}



    
> `def default()`





    
### Class `ORMEngineConfig` {#hexagon.infrastructure.config.domains.system.ORMEngineConfig}



> `class ORMEngineConfig(engine_name: str = 'Hexagon-Core-DevApp', client_encoding: str = 'utf8', isolation_autocommit: bool = True, isolation_level: str = 'AUTOCOMMIT', pool_pre_ping: bool = True, pool_recycle: int = 900, pool_size: int = 10, max_overflow: int = 15, query_cache_size: int = 1250, db_url: hexagon.infrastructure.config.domains.system.DatabaseURL = None)`


Configuration de l'Engine et du driver utilisés pour la connexion à la BDD

Method generated by attrs for class ORMEngineConfig.


    
#### Ancestors (in MRO)

* [hexagon.infrastructure.config.domains.baseconfig.BaseConfig](#hexagon.infrastructure.config.domains.baseconfig.BaseConfig)




    
#### Instance variables


    
##### Variable `client_encoding` {#hexagon.infrastructure.config.domains.system.ORMEngineConfig.client_encoding}

Return an attribute of instance, which is of type owner.

    
##### Variable `db_url` {#hexagon.infrastructure.config.domains.system.ORMEngineConfig.db_url}

Return an attribute of instance, which is of type owner.

    
##### Variable `engine_name` {#hexagon.infrastructure.config.domains.system.ORMEngineConfig.engine_name}

Return an attribute of instance, which is of type owner.

    
##### Variable `isolation_autocommit` {#hexagon.infrastructure.config.domains.system.ORMEngineConfig.isolation_autocommit}

Return an attribute of instance, which is of type owner.

    
##### Variable `isolation_level` {#hexagon.infrastructure.config.domains.system.ORMEngineConfig.isolation_level}

Return an attribute of instance, which is of type owner.

    
##### Variable `max_overflow` {#hexagon.infrastructure.config.domains.system.ORMEngineConfig.max_overflow}

Return an attribute of instance, which is of type owner.

    
##### Variable `pool_pre_ping` {#hexagon.infrastructure.config.domains.system.ORMEngineConfig.pool_pre_ping}

Return an attribute of instance, which is of type owner.

    
##### Variable `pool_recycle` {#hexagon.infrastructure.config.domains.system.ORMEngineConfig.pool_recycle}

Return an attribute of instance, which is of type owner.

    
##### Variable `pool_size` {#hexagon.infrastructure.config.domains.system.ORMEngineConfig.pool_size}

Return an attribute of instance, which is of type owner.

    
##### Variable `query_cache_size` {#hexagon.infrastructure.config.domains.system.ORMEngineConfig.query_cache_size}

Return an attribute of instance, which is of type owner.


    
#### Static methods


    
##### `Method default` {#hexagon.infrastructure.config.domains.system.ORMEngineConfig.default}



    
> `def default(db_url: hexagon.infrastructure.config.domains.system.DatabaseURL = None)`





    
#### Methods


    
##### Method `driver_options` {#hexagon.infrastructure.config.domains.system.ORMEngineConfig.driver_options}



    
> `def driver_options(self) -> dict`


Extrait les options pour le driver: psycopg2


###### Returns

<code>dict</code>
:   Kwargs to driver dialect



    
##### Method `engine_options` {#hexagon.infrastructure.config.domains.system.ORMEngineConfig.engine_options}



    
> `def engine_options(self) -> dict`




    
##### Method `url_dict` {#hexagon.infrastructure.config.domains.system.ORMEngineConfig.url_dict}



    
> `def url_dict(self) -> dict`




    
### Class `SeleniumConfig` {#hexagon.infrastructure.config.domains.system.SeleniumConfig}



> `class SeleniumConfig(driver_path: pathlib.Path, download_dir: pathlib.Path, firefox_app_path: pathlib.Path, driver_name: str = 'GECKO')`


Le scraper d'Hexagon se base sur Gecko de Firefox dépendance forte.

Method generated by attrs for class SeleniumConfig.


    
#### Ancestors (in MRO)

* [hexagon.infrastructure.config.domains.baseconfig.BaseConfig](#hexagon.infrastructure.config.domains.baseconfig.BaseConfig)




    
#### Instance variables


    
##### Variable `download_dir` {#hexagon.infrastructure.config.domains.system.SeleniumConfig.download_dir}

Return an attribute of instance, which is of type owner.

    
##### Variable `driver_name` {#hexagon.infrastructure.config.domains.system.SeleniumConfig.driver_name}

Return an attribute of instance, which is of type owner.

    
##### Variable `driver_path` {#hexagon.infrastructure.config.domains.system.SeleniumConfig.driver_path}

Return an attribute of instance, which is of type owner.

    
##### Variable `firefox_app_path` {#hexagon.infrastructure.config.domains.system.SeleniumConfig.firefox_app_path}

Return an attribute of instance, which is of type owner.


    
#### Static methods


    
##### `Method default` {#hexagon.infrastructure.config.domains.system.SeleniumConfig.default}



    
> `def default()`





    
### Class `SystemConfig` {#hexagon.infrastructure.config.domains.system.SystemConfig}



> `class SystemConfig(hexagon: hexagon.infrastructure.config.domains.system.ORMEngineConfig, manar: hexagon.infrastructure.config.domains.system.ORMEngineConfig, selenium: hexagon.infrastructure.config.domains.system.SeleniumConfig, httpx_client: hexagon.infrastructure.config.domains.system.HttpxClientConfig, bkam_apis: hexagon.infrastructure.config.domains.system.BkamApiConfig)`


Configuration de la partie système d'Hexagon.


#### Attributes

- hexagon (ORMEngineConfig): Configuration de l'engine de connexion à la base Hexagon
- manar (ORMEngineConfig): Configuration de l'engine de connexion à la base Manar
- selenium (SeleniumConfig): Configuration du Driver GECKO pour le scraping
- httpx_client (HttpxClientConfig): Configuration du client httpx pour les requêtes externes (BKAM, Asfim, ...)
- bkam_apis (BkamApiConfig): Credentials et configuration du client httpx pour l'accès aux service APIS de BKAM.
Method generated by attrs for class SystemConfig.


    
#### Ancestors (in MRO)

* [hexagon.infrastructure.config.domains.baseconfig.BaseConfig](#hexagon.infrastructure.config.domains.baseconfig.BaseConfig)




    
#### Instance variables


    
##### Variable `bkam_apis` {#hexagon.infrastructure.config.domains.system.SystemConfig.bkam_apis}

Return an attribute of instance, which is of type owner.

    
##### Variable `hexagon` {#hexagon.infrastructure.config.domains.system.SystemConfig.hexagon}

Return an attribute of instance, which is of type owner.

    
##### Variable `httpx_client` {#hexagon.infrastructure.config.domains.system.SystemConfig.httpx_client}

Return an attribute of instance, which is of type owner.

    
##### Variable `manar` {#hexagon.infrastructure.config.domains.system.SystemConfig.manar}

Return an attribute of instance, which is of type owner.

    
##### Variable `selenium` {#hexagon.infrastructure.config.domains.system.SystemConfig.selenium}

Return an attribute of instance, which is of type owner.


    
#### Static methods


    
##### `Method default` {#hexagon.infrastructure.config.domains.system.SystemConfig.default}



    
> `def default()`







    
# Module `hexagon.infrastructure.config.domains.veille` {#hexagon.infrastructure.config.domains.veille}







    
## Classes


    
### Class `VeilleConfig` {#hexagon.infrastructure.config.domains.veille.VeilleConfig}



> `class VeilleConfig()`


Classe mère des modèles de configuration.
La particularité des modèles de config les rend difficiles à raisonner
des structures imbriquées qui nécessitent un ONSM (Object Nested Structures mapper)

Method generated by attrs for class VeilleConfig.


    
#### Ancestors (in MRO)

* [hexagon.infrastructure.config.domains.baseconfig.BaseConfig](#hexagon.infrastructure.config.domains.baseconfig.BaseConfig)








    
# Module `hexagon.infrastructure.config.exceptions` {#hexagon.infrastructure.config.exceptions}







    
## Classes


    
### Class `ConfigurationError` {#hexagon.infrastructure.config.exceptions.ConfigurationError}



> `class ConfigurationError(...)`


Exception générique du domaine infrastructure.config


    
#### Ancestors (in MRO)

* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)








    
# Module `hexagon.infrastructure.db` {#hexagon.infrastructure.db}

Module de manipulation de la BDD via l'ORM sqlalchemy.
Dans ce module, on définit les modèles de données qui seront utilisés par Hexagon.


    
## Sub-modules

* [hexagon.infrastructure.db.db_access](#hexagon.infrastructure.db.db_access)
* [hexagon.infrastructure.db.db_utils](#hexagon.infrastructure.db.db_utils)
* [hexagon.infrastructure.db.exceptions](#hexagon.infrastructure.db.exceptions)
* [hexagon.infrastructure.db.models](#hexagon.infrastructure.db.models)
* [hexagon.infrastructure.db.models_utils](#hexagon.infrastructure.db.models_utils)






    
# Module `hexagon.infrastructure.db.db_access` {#hexagon.infrastructure.db.db_access}






    
## Functions


    
### Function `create_db_engine` {#hexagon.infrastructure.db.db_access.create_db_engine}



    
> `def create_db_engine(db_url: dict, **kwargs) -> sqlalchemy.engine.base.Engine`


Crée une instance DBEngine de sqlalchemy


###### Args

**```**kwargs```**
:   Dict des paramètres à passer lors de create_engine de sqlalchemy



###### Returns

<code>Engine</code>
:   instance d'Engine de sqlalchemy



    
### Function `create_db_engine_from_config` {#hexagon.infrastructure.db.db_access.create_db_engine_from_config}



    
> `def create_db_engine_from_config(engine_args: dict = None, driver_args: dict = None) -> sqlalchemy.engine.base.Engine`


Crée un Databse Engine depuis le fichier de Configuration


###### Args

**```engine_args```** :&ensp;<code>dict</code>, optional
:   Ajoute les clés supplémentaires aux configurations existantes,
    mais préserve les configurations existantes. Priorité au dimensionnement du concepteur.


**```driver_args```** :&ensp;<code>dict</code>, optional
:   Met à jour et ajoute les clés renseignées. Priorité à l'utilisateur



###### Returns

<code>engine.Engine</code>
:   Sqlalchemy Engine Instance avec la configuration adéquate



###### Raises

<code>ConfigurationError</code>
:   configuration inadéquate au niveau de URL ou Engine Config



    
### Function `reset_session` {#hexagon.infrastructure.db.db_access.reset_session}



    
> `def reset_session(session_factory: sqlalchemy.orm.session.Session) -> bool`


Ferme une session sqlalchemy ou la session initiée si 'session' is None


###### Args

**```session_factory```** :&ensp;<code>Session</code>
:   Session Sqlalchemy



###### Returns

<code>bool</code>
:   True si la cloture de la session est effectuée avec succès, False par défaut






    
# Module `hexagon.infrastructure.db.db_utils` {#hexagon.infrastructure.db.db_utils}






    
## Functions


    
### Function `apply_date_filter` {#hexagon.infrastructure.db.db_utils.apply_date_filter}



    
> `def apply_date_filter(query: sqlalchemy.sql.selectable.Select, model: sqlalchemy.orm.decl_api.DeclarativeMeta, date_field: str, debut: datetime.date = None, fin: datetime.date = None, *, include: str = 'BOTH') -> sqlalchemy.sql.selectable.Select`


Applique un filtre d'intervalle de dates à une requete, l'avantage de cette fonction
est qu'elle fonctionne même en cas de jointures de plusieurs modèles,
vu qu'elle cible le modèle et la query concernée.

include prend les valeurs possibles (passer as keyword parameter):
    * 'BOTH': inclut les deux bornes --> [debut, fin]
    * 'NONE': exclut les deux bornes --> ]debut, fin[
    * 'LEFT': inclut la borne gauche --> [debut, fin[
    * 'RIGHT': inclut la borne droite --> ]debut, fin]


###### Args

**```query```** :&ensp;<code>Select</code>
:   Requete sqlalchemy


**```model```** :&ensp;<code>db.Model</code>
:   Modèle d'Hexagon (db.EtatTitre, db.Courbe, ...)


**```date_field```** :&ensp;<code>str</code>
:   Champs de date du Modèle à appliquer le filtre. ex: ('date_marche' pour db.EtaFonds)


**```debut```** :&ensp;<code>date</code>, optional
:   Date de début de l'intervalle, passer None pour un intervalle ouvert à gauche (left)


**```fin```** :&ensp;<code>date</code>, optional
:   Date de fin de l'intervalle, passer None pour un intervalle ouvert à droite (right)


**```include```** :&ensp;`str, Include=BOTH`
:   Paramètre d'inclusion des dates. 'BOTH'=[a, b], 'NONE'= ]a, b[, 'RIGHT'= ]a, b], 'LEFT'= [a, b[



###### Returns

`Select`
:   Requete sqlalchemy avec le fitre des dates appliquées au modèle.



<code>N.B</code>
:   Le champs et le modèle passés en paramètre doivent être sélectionné



###### Raises

<code>TypeError</code>
:   Si le champs est invalide pour le Modèle (hasattr)


<code>ValueError</code>
:   Si la date de début > date de fin



    
### Function `as_dict` {#hexagon.infrastructure.db.db_utils.as_dict}



    
> `def as_dict(obj: sqlalchemy.orm.decl_api.DeclarativeMeta) -> dict`


For internal use, use the object.data property


###### Args

**```obj```**
:   sqlalchemy Declarative Base subclass Model.
    correspond aux modèles définis et qui héritent de "Base"


returns:
    dict: Transforme un enregistrement (objet sqlalchemy) en dictionnaire

    
### Function `build_key_from_column` {#hexagon.infrastructure.db.db_utils.build_key_from_column}



    
> `def build_key_from_column(field: sqlalchemy.orm.attributes.InstrumentedAttribute, *, sep: str = '.') -> str`


Construit automatique la clé d'une colonne spécifiée dans une requête


###### Args

**```col```** :&ensp;<code>InstrumentedAttribute</code>
:   Attribut d'un modèle sqlalchemy


sep (str, '.''): séparateur de la tablename et du champs

###### Returns

<code>str</code>
:   Clé au format <model.__tablename__>.<champs>



###### Raises

<code>HexagonDbError</code>
:   Si field n'est pas une instance de InstrumentedAttribute



    
### Function `commit_chunked` {#hexagon.infrastructure.db.db_utils.commit_chunked}



    
> `def commit_chunked(session: sqlalchemy.orm.session.Session, records: list[sqlalchemy.orm.decl_api.DeclarativeMeta], chunk: int = 30000, start_step: int = 0)`


Commit une série de données en petits sous-blocks de data


###### Args

**```session```**
:   Sqlalchemy Session Object


**```records```**
:   list[Models]


**```chunk```**
:   taille de la portion à utiliser.


**```start_step```**
:   l'ordre du chunk.



    
### Function `commit_chunked_generator` {#hexagon.infrastructure.db.db_utils.commit_chunked_generator}



    
> `def commit_chunked_generator(session: sqlalchemy.orm.session.Session, records: list[sqlalchemy.orm.decl_api.DeclarativeMeta], chunk: int = 30000, start_step: int = 0)`


Commit une série de données fournie en tant que generateur en petits sous-blocks de data


###### Args

**```session```**
:   Sqlalchemy Session Object


**```records```**
:   list[Models]


**```chunk```**
:   taille de la portion à utiliser.


**```start_step```**
:   l'ordre du chunk.



    
### Function `drop_db` {#hexagon.infrastructure.db.db_utils.drop_db}



    
> `def drop_db(Base: sqlalchemy.orm.decl_api.DeclarativeMeta, engine: sqlalchemy.engine.base.Engine, tables: list[sqlalchemy.orm.decl_api.DeclarativeMeta] = None)`


Drop a database.

    
### Function `filter_table` {#hexagon.infrastructure.db.db_utils.filter_table}



    
> `def filter_table(model: sqlalchemy.orm.decl_api.DeclarativeMeta, field: str, opstring: str, value: object) -> sqlalchemy.sql.selectable.Select`


Construit une requete selon les filtres passés en paramètre.


###### Args

**```session```** :&ensp;<code>Session</code>
:   Session définie par l'utilisateur ex: DBSession


**```model```** :&ensp;<code>DeclarativeBase</code>
:   Classe de la table


**```field```** :&ensp;<code>str</code>
:   champs à filtrer


**```opstring```** :&ensp;<code>str</code>
:   l'Operateur à appliquer: '<', '<=', '>', '>=', '=', '!='


**```value```** :&ensp;<code>Valeur contre le opstring</code>
:   &nbsp;

###### Returns

<code>Select</code>
:   Requête contenant les filtres appliqués.



    
### Function `get_fields` {#hexagon.infrastructure.db.db_utils.get_fields}



    
> `def get_fields(model: sqlalchemy.orm.decl_api.DeclarativeMeta) -> list[str]`



###### Args

**```model```**
:   sqlalchemy Declarative Base subclass Model.
    correspond aux modèles définis et qui héritent de "Base"


returns:
    list: Les champs du modèle tels que définis au niveau de la BDD.

    
### Function `get_fields_with_type` {#hexagon.infrastructure.db.db_utils.get_fields_with_type}



    
> `def get_fields_with_type(model: sqlalchemy.orm.decl_api.DeclarativeMeta) -> dict[str, type]`



###### Args

**```model```**
:   sqlalchemy Declarative Base subclass Model.
    correspond aux modèles définis et qui héritent de "Base"


returns:
    dict[column_name, python_type]: Les champs du model tels que définis au niveau de la BDD.

    
### Function `get_tables_headers` {#hexagon.infrastructure.db.db_utils.get_tables_headers}



    
> `def get_tables_headers(base: sqlalchemy.orm.decl_api.DeclarativeMeta) -> dict[str, list[str]]`


retourne un dictionnaire ayant le nom des tables comme key et
une liste des champs de chaqune d'entre elles.


###### Args

**```Base```**
:   Sqlalchemy Base classe (cette classe contient les informations relatives à la B.D.D)



###### Returns

<code>dict</code>
:   dict[tablename, list[fields]]



    
### Function `labelize_fields` {#hexagon.infrastructure.db.db_utils.labelize_fields}



    
> `def labelize_fields(fields: list[sqlalchemy.orm.attributes.InstrumentedAttribute], *, columns_mapper: dict[sqlalchemy.orm.attributes.InstrumentedAttribute, str] = None) -> Generator[sqlalchemy.orm.attributes.InstrumentedAttribute, NoneType, NoneType]`


Crée un label normalisé aux d'une requete Sqlalchemy selon la norme dot('.') separator
La forme canonique utilisée est:  *label = <__tablename__>.<field>*

Si l'attribut est déjà un Label retourne same instance

si columns_mapper est fourni, utilise le champs forcé


###### Args

**```fields```** :&ensp;<code>list\[InstrumentedAttribute]</code>
:   Liste des attributs des class Models


**```columns_mapper```** :&ensp;<code>dict</code>, optional
:   Mapper des champs pour forcer certains champs
    qui ne sont pas gérés par l'auto-générateur des Labels



###### Yields

<code>Generator\[InstrumentedAttribute]</code>
:   Retourne un générateur pour possibilité de pipelining.



    
### Function `sanitize_model_fields` {#hexagon.infrastructure.db.db_utils.sanitize_model_fields}



    
> `def sanitize_model_fields(model: sqlalchemy.orm.decl_api.DeclarativeMeta, fields: list[str] = None, *, query: bool = True) -> str | list[sqlalchemy.orm.attributes.InstrumentedAttribute]`


Valide et filter les champs propres au modèle considéré selon les champs demandés

* Si un champs est inadéquat => raise AttributeError
* Si query is True: retourne les champs/colonnes/fields as InstrumentedAttributes. If False sous format texte


###### Args

**```model```** :&ensp;<code>db.Model</code>
:   Modèle SA représentant une table dans la BDD.


**```fields```** :&ensp;<code>list, None</code>
:   Liste des champs demandés, Laisser vide pour tous les champs de la table


**```query```** :&ensp;<code>bool, True</code>
:   Contrôle si les champs à retourner sont au format 'str'
    ou InstrumentedAttribute. Laisser True pour un usage direct dans une requête.



###### Returns

<code>list</code>
:   Liste des champs validés et selon l'ordre demandé



###### Raises

<code>AttributeError</code>
:   Si un Champs/attribut n'est pas défini au niveau du modèle.
    permet de catcher les requêtes incorrectes. Philosophy of "All or Nothing".



    
### Function `scaffold_db` {#hexagon.infrastructure.db.db_utils.scaffold_db}



    
> `def scaffold_db(Base: sqlalchemy.orm.decl_api.DeclarativeMeta, engine: sqlalchemy.engine.base.Engine, tables: list[sqlalchemy.orm.decl_api.DeclarativeMeta] = None)`


Crée une Base de donnée en se basant sur l'objet engine et Base

    
### Function `subclasses` {#hexagon.infrastructure.db.db_utils.subclasses}



    
> `def subclasses(base: sqlalchemy.orm.decl_api.DeclarativeMeta) -> list[sqlalchemy.orm.decl_api.DeclarativeMeta]`


Retourne les classes mères dont hérite la classe base


###### Args

**```base```** :&ensp;<code>obj</code>
:   Any class



###### Returns

<code>tuple</code>
:   Les classes Mères de l'objet excluant object.



    
### Function `tablename_model` {#hexagon.infrastructure.db.db_utils.tablename_model}



    
> `def tablename_model(base: sqlalchemy.orm.decl_api.DeclarativeMeta) -> dict[str, sqlalchemy.orm.decl_api.DeclarativeMeta]`


Cree un mapping entre le nom des ttables et les Modèles associés


###### Args

**```base```** :&ensp;<code>DeclarativeBase</code>
:   DesclarativeBase class dont héritent les classes des modèles


de la BDD.

###### Returns

<code>Dict</code>
:   dict[tablename:Model class]






    
# Module `hexagon.infrastructure.db.exceptions` {#hexagon.infrastructure.db.exceptions}







    
## Classes


    
### Class `HexagonDbError` {#hexagon.infrastructure.db.exceptions.HexagonDbError}



> `class HexagonDbError(...)`


Classe mère pour les exceptions de DB Utils


    
#### Ancestors (in MRO)

* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)








    
# Module `hexagon.infrastructure.db.models` {#hexagon.infrastructure.db.models}

Module de définition des modèles sqlalchemy


## Attributes

**```DB_TABLES```** :&ensp;<code>dict</code>
:   Registre des noms des tables avec le modèle associé


**```MODELS_LIST```** :&ensp;<code>list</code>
:   Liste des modèles définis par Hexagon




    
## Sub-modules

* [hexagon.infrastructure.db.models.base_mixin](#hexagon.infrastructure.db.models.base_mixin)
* [hexagon.infrastructure.db.models.clients](#hexagon.infrastructure.db.models.clients)
* [hexagon.infrastructure.db.models.data_items](#hexagon.infrastructure.db.models.data_items)
* [hexagon.infrastructure.db.models.documents](#hexagon.infrastructure.db.models.documents)
* [hexagon.infrastructure.db.models.market](#hexagon.infrastructure.db.models.market)
* [hexagon.infrastructure.db.models.opcvm](#hexagon.infrastructure.db.models.opcvm)
* [hexagon.infrastructure.db.models.reporting](#hexagon.infrastructure.db.models.reporting)
* [hexagon.infrastructure.db.models.views](#hexagon.infrastructure.db.models.views)






    
# Module `hexagon.infrastructure.db.models.base_mixin` {#hexagon.infrastructure.db.models.base_mixin}







    
## Classes


    
### Class `BaseEntityMixin` {#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin}



> `class BaseEntityMixin()`






    
#### Descendants

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)



    
#### Instance variables


    
##### Variable `data` {#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin.data}

Retourne les attributs de l'objet sous forme de dict


###### Returns

<code>dict</code>
:   Keys = attributs et values = objValues




    
#### Static methods


    
##### `Method by_id` {#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin.by_id}



    
> `def by_id(_id: int, dbsession: sqlalchemy.orm.session.Session) -> Self`


Retourne un enregistrement de la BDD avec l'ID fourni


###### Args

**```_id```** :&ensp;<code>int</code>
:   L'ID de l'instance voulue



###### Returns

<code>object</code>
:   Une instance du modèle fils



    
##### `Method columns` {#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin.columns}



    
> `def columns() -> list[str]`


ClassMethod: Retourne les champs du modèle


###### Returns

<code>list</code>
:   Les colonnes du modèle tels que définis



    
##### `Method columns_orm` {#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin.columns_orm}



    
> `def columns_orm() -> list[str]`




    
##### `Method get_all` {#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin.get_all}



    
> `def get_all(*, dbsession: sqlalchemy.orm.session.Session, filter_by: dict = None) -> list[typing.Self]`


Construit une requete SELECT * et retourne le Select Object


###### Returns

<code>sqlalchemy.Select</code>
:   Select Object pointant sur le modèle




    
### Class `DocumentTableMixin` {#hexagon.infrastructure.db.models.base_mixin.DocumentTableMixin}



> `class DocumentTableMixin()`






    
#### Descendants

* [hexagon.infrastructure.db.models.documents.FileDocument](#hexagon.infrastructure.db.models.documents.FileDocument)





    
### Class `HistoriqueTableMixin` {#hexagon.infrastructure.db.models.base_mixin.HistoriqueTableMixin}



> `class HistoriqueTableMixin()`






    
#### Descendants

* [hexagon.infrastructure.db.models.market.CompositionMBI](#hexagon.infrastructure.db.models.market.CompositionMBI)
* [hexagon.infrastructure.db.models.market.CompositionMasi](#hexagon.infrastructure.db.models.market.CompositionMasi)
* [hexagon.infrastructure.db.models.market.Courbe](#hexagon.infrastructure.db.models.market.Courbe)
* [hexagon.infrastructure.db.models.market.EtatFondsMarket](#hexagon.infrastructure.db.models.market.EtatFondsMarket)
* [hexagon.infrastructure.db.models.market.RawMBI](#hexagon.infrastructure.db.models.market.RawMBI)
* [hexagon.infrastructure.db.models.market.RawTMP](#hexagon.infrastructure.db.models.market.RawTMP)
* [hexagon.infrastructure.db.models.opcvm.EtatFonds](#hexagon.infrastructure.db.models.opcvm.EtatFonds)
* [hexagon.infrastructure.db.models.opcvm.EtatTitre](#hexagon.infrastructure.db.models.opcvm.EtatTitre)
* [hexagon.infrastructure.db.models.opcvm.Operation](#hexagon.infrastructure.db.models.opcvm.Operation)
* [hexagon.infrastructure.db.models.opcvm.OperationPassif](#hexagon.infrastructure.db.models.opcvm.OperationPassif)





    
### Class `RawTableMixin` {#hexagon.infrastructure.db.models.base_mixin.RawTableMixin}



> `class RawTableMixin()`






    
#### Descendants

* [hexagon.infrastructure.db.models.market.BenchmarkComposition](#hexagon.infrastructure.db.models.market.BenchmarkComposition)
* [hexagon.infrastructure.db.models.market.SCDFondsMarket](#hexagon.infrastructure.db.models.market.SCDFondsMarket)
* [hexagon.infrastructure.db.models.market.SplitFondsMarket](#hexagon.infrastructure.db.models.market.SplitFondsMarket)
* [hexagon.infrastructure.db.models.opcvm.BenchmarkFonds](#hexagon.infrastructure.db.models.opcvm.BenchmarkFonds)
* [hexagon.infrastructure.db.models.opcvm.Echeancier](#hexagon.infrastructure.db.models.opcvm.Echeancier)
* [hexagon.infrastructure.db.models.opcvm.InventaireFonds](#hexagon.infrastructure.db.models.opcvm.InventaireFonds)





    
### Class `ReferentielTableMixin` {#hexagon.infrastructure.db.models.base_mixin.ReferentielTableMixin}



> `class ReferentielTableMixin()`






    
#### Descendants

* [hexagon.infrastructure.db.models.documents.MimeType](#hexagon.infrastructure.db.models.documents.MimeType)
* [hexagon.infrastructure.db.models.market.Benchmark](#hexagon.infrastructure.db.models.market.Benchmark)
* [hexagon.infrastructure.db.models.market.Devise](#hexagon.infrastructure.db.models.market.Devise)
* [hexagon.infrastructure.db.models.market.Emetteur](#hexagon.infrastructure.db.models.market.Emetteur)
* [hexagon.infrastructure.db.models.market.FondsMarket](#hexagon.infrastructure.db.models.market.FondsMarket)
* [hexagon.infrastructure.db.models.market.Indice](#hexagon.infrastructure.db.models.market.Indice)
* [hexagon.infrastructure.db.models.market.Pays](#hexagon.infrastructure.db.models.market.Pays)
* [hexagon.infrastructure.db.models.market.Secteur](#hexagon.infrastructure.db.models.market.Secteur)
* [hexagon.infrastructure.db.models.market.TitreMarket](#hexagon.infrastructure.db.models.market.TitreMarket)
* [hexagon.infrastructure.db.models.opcvm.Fonds](#hexagon.infrastructure.db.models.opcvm.Fonds)
* [hexagon.infrastructure.db.models.opcvm.PosteOperation](#hexagon.infrastructure.db.models.opcvm.PosteOperation)
* [hexagon.infrastructure.db.models.opcvm.Titre](#hexagon.infrastructure.db.models.opcvm.Titre)





    
### Class `TableType` {#hexagon.infrastructure.db.models.base_mixin.TableType}



> `class TableType(value, names=None, *, module=None, qualname=None, type=None, start=1, boundary=None)`


Create a collection of name/value pairs.

Example enumeration:

```python
>>> class Color(Enum):
...     RED = 1
...     BLUE = 2
...     GREEN = 3
```


Access them by:

- attribute access::

```python
>>> Color.RED
<Color.RED: 1>
```


- value lookup:

```python
>>> Color(1)
<Color.RED: 1>
```


- name lookup:

```python
>>> Color['RED']
<Color.RED: 1>
```


Enumerations can be iterated over, and know how many members they have:

```python
>>> len(Color)
3
```


```python
>>> list(Color)
[<Color.RED: 1>, <Color.BLUE: 2>, <Color.GREEN: 3>]
```


Methods can be added to enumerations, and members can have their own
attributes -- see the documentation for details.


    
#### Ancestors (in MRO)

* [enum.Enum](#enum.Enum)



    
#### Class variables


    
##### Variable `DOCUMENT` {#hexagon.infrastructure.db.models.base_mixin.TableType.DOCUMENT}



    
##### Variable `HISTORIQUE` {#hexagon.infrastructure.db.models.base_mixin.TableType.HISTORIQUE}



    
##### Variable `RAW` {#hexagon.infrastructure.db.models.base_mixin.TableType.RAW}



    
##### Variable `REFERENTIEL` {#hexagon.infrastructure.db.models.base_mixin.TableType.REFERENTIEL}



    
##### Variable `TIMESERIES` {#hexagon.infrastructure.db.models.base_mixin.TableType.TIMESERIES}






    
### Class `TimeSeriesTableMixin` {#hexagon.infrastructure.db.models.base_mixin.TimeSeriesTableMixin}



> `class TimeSeriesTableMixin()`






    
#### Descendants

* [hexagon.infrastructure.db.models.market.DeviseCours](#hexagon.infrastructure.db.models.market.DeviseCours)
* [hexagon.infrastructure.db.models.market.IndiceValue](#hexagon.infrastructure.db.models.market.IndiceValue)







    
# Module `hexagon.infrastructure.db.models.clients` {#hexagon.infrastructure.db.models.clients}







    
## Classes


    
### Class `Client` {#hexagon.infrastructure.db.models.clients.Client}



> `class Client(**kwargs)`


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)




    
#### Instance variables


    
##### Variable `apparente` {#hexagon.infrastructure.db.models.clients.Client.apparente}



    
##### Variable `canal_distribution` {#hexagon.infrastructure.db.models.clients.Client.canal_distribution}



    
##### Variable `code` {#hexagon.infrastructure.db.models.clients.Client.code}



    
##### Variable `concurrentiel` {#hexagon.infrastructure.db.models.clients.Client.concurrentiel}



    
##### Variable `created_at` {#hexagon.infrastructure.db.models.clients.Client.created_at}



    
##### Variable `description` {#hexagon.infrastructure.db.models.clients.Client.description}



    
##### Variable `id` {#hexagon.infrastructure.db.models.clients.Client.id}



    
##### Variable `labels` {#hexagon.infrastructure.db.models.clients.Client.labels}



    
##### Variable `nationalite` {#hexagon.infrastructure.db.models.clients.Client.nationalite}



    
##### Variable `pays_residence` {#hexagon.infrastructure.db.models.clients.Client.pays_residence}



    
##### Variable `type_economique` {#hexagon.infrastructure.db.models.clients.Client.type_economique}





    
### Class `Compte` {#hexagon.infrastructure.db.models.clients.Compte}



> `class Compte(**kwargs)`


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)




    
#### Instance variables


    
##### Variable `banque` {#hexagon.infrastructure.db.models.clients.Compte.banque}



    
##### Variable `client` {#hexagon.infrastructure.db.models.clients.Compte.client}



    
##### Variable `code` {#hexagon.infrastructure.db.models.clients.Compte.code}



    
##### Variable `created_at` {#hexagon.infrastructure.db.models.clients.Compte.created_at}



    
##### Variable `depositaire` {#hexagon.infrastructure.db.models.clients.Compte.depositaire}



    
##### Variable `devise` {#hexagon.infrastructure.db.models.clients.Compte.devise}



    
##### Variable `id` {#hexagon.infrastructure.db.models.clients.Compte.id}



    
##### Variable `id_client` {#hexagon.infrastructure.db.models.clients.Compte.id_client}



    
##### Variable `id_devise` {#hexagon.infrastructure.db.models.clients.Compte.id_devise}



    
##### Variable `methode_cmp` {#hexagon.infrastructure.db.models.clients.Compte.methode_cmp}



    
##### Variable `type_compte` {#hexagon.infrastructure.db.models.clients.Compte.type_compte}





    
### Class `HistoCompte` {#hexagon.infrastructure.db.models.clients.HistoCompte}



> `class HistoCompte(**kwargs)`


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)




    
#### Instance variables


    
##### Variable `cmp` {#hexagon.infrastructure.db.models.clients.HistoCompte.cmp}



    
##### Variable `date_marche` {#hexagon.infrastructure.db.models.clients.HistoCompte.date_marche}



    
##### Variable `date_valeur` {#hexagon.infrastructure.db.models.clients.HistoCompte.date_valeur}



    
##### Variable `id` {#hexagon.infrastructure.db.models.clients.HistoCompte.id}



    
##### Variable `id_compte` {#hexagon.infrastructure.db.models.clients.HistoCompte.id_compte}



    
##### Variable `id_fonds` {#hexagon.infrastructure.db.models.clients.HistoCompte.id_fonds}



    
##### Variable `quantite` {#hexagon.infrastructure.db.models.clients.HistoCompte.quantite}



    
##### Variable `valo_titre` {#hexagon.infrastructure.db.models.clients.HistoCompte.valo_titre}



    
##### Variable `vl` {#hexagon.infrastructure.db.models.clients.HistoCompte.vl}





    
### Class `Label` {#hexagon.infrastructure.db.models.clients.Label}



> `class Label(**kwargs)`


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)




    
#### Instance variables


    
##### Variable `code` {#hexagon.infrastructure.db.models.clients.Label.code}



    
##### Variable `created_at` {#hexagon.infrastructure.db.models.clients.Label.created_at}



    
##### Variable `groupe` {#hexagon.infrastructure.db.models.clients.Label.groupe}



    
##### Variable `id` {#hexagon.infrastructure.db.models.clients.Label.id}



    
##### Variable `id_groupe` {#hexagon.infrastructure.db.models.clients.Label.id_groupe}



    
##### Variable `updated_at` {#hexagon.infrastructure.db.models.clients.Label.updated_at}





    
### Class `LabelGroupe` {#hexagon.infrastructure.db.models.clients.LabelGroupe}



> `class LabelGroupe(**kwargs)`


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)




    
#### Instance variables


    
##### Variable `code` {#hexagon.infrastructure.db.models.clients.LabelGroupe.code}



    
##### Variable `created_at` {#hexagon.infrastructure.db.models.clients.LabelGroupe.created_at}



    
##### Variable `id` {#hexagon.infrastructure.db.models.clients.LabelGroupe.id}



    
##### Variable `id_parent` {#hexagon.infrastructure.db.models.clients.LabelGroupe.id_parent}



    
##### Variable `updated_at` {#hexagon.infrastructure.db.models.clients.LabelGroupe.updated_at}







    
# Module `hexagon.infrastructure.db.models.data_items` {#hexagon.infrastructure.db.models.data_items}







    
## Classes


    
### Class `AppItem` {#hexagon.infrastructure.db.models.data_items.AppItem}



> `class AppItem(**kwargs)`


Table pour stocker des valeurs à utiliser par défaut au niveau de l'Application

'store' offre la possibilité de stocker les valeurs sur 2 niveaux d'hierarchisation
et aussi de stocker des valeurs binaires 'bytes_content', afin de tirer profit du méchanisme de sérialisation
qu'offre Python via (cPickle).

* Pour les Valeurs binaires, le protocole 4 de Pickle sera utilisé.


#### Attributes

**```id```** :&ensp;<code>int</code>
:   L'ID de la ligne, auto-increment.


**```scope```** :&ensp;<code>str</code>
:   Représente le second niveau d'hierarchisation


**```key```** :&ensp;<code>str</code>
:   La clé de la valeur


**```store_content```** :&ensp;<code>HSTORE</code>
:   Dictionnaire cle-valeur en Texte {Keys: Values[str]}


**```byte_content```** :&ensp;<code>BYTEA</code>
:   Permet de stocker un objet Binaire (généralement Pickle)


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)




    
#### Instance variables


    
##### Variable `bytes_content` {#hexagon.infrastructure.db.models.data_items.AppItem.bytes_content}



    
##### Variable `created_at` {#hexagon.infrastructure.db.models.data_items.AppItem.created_at}



    
##### Variable `id` {#hexagon.infrastructure.db.models.data_items.AppItem.id}



    
##### Variable `key` {#hexagon.infrastructure.db.models.data_items.AppItem.key}



    
##### Variable `store` {#hexagon.infrastructure.db.models.data_items.AppItem.store}



    
##### Variable `store_content` {#hexagon.infrastructure.db.models.data_items.AppItem.store_content}



    
##### Variable `updated_at` {#hexagon.infrastructure.db.models.data_items.AppItem.updated_at}




    
#### Static methods


    
##### `Method delete_store_item` {#hexagon.infrastructure.db.models.data_items.AppItem.delete_store_item}



    
> `def delete_store_item(key: str, *, store: str = None, dbsession: sqlalchemy.orm.session.Session)`


Supprime un enregistrement de la Base en passant le "store" et "key"
Le couple (store, key) permet d'identifier un seul Enregistrement


###### Args

**```key```** :&ensp;<code>str</code>
:   Clé d'identification de l'item


**```store```** :&ensp;<code>str</code>, optional
:   Le store pour localiser l'item



###### Returns

<code>[AppItem](#hexagon.infrastructure.db.models.data\_items.AppItem "hexagon.infrastructure.db.models.data\_items.AppItem")</code>
:   L'instance supprimée rertournée par sqlalchemy, l'instance est marquée 'deleted'



###### Raises

<code>ValueError</code>
:   Si l'element à supprimer n'existe pas dans la BDD.



    
##### `Method get_many` {#hexagon.infrastructure.db.models.data_items.AppItem.get_many}



    
> `def get_many(*, store: str = None, dbsession: sqlalchemy.orm.session.Session) -> list`


Helper Function: Retourne tous les items dans un store


###### Args

**```store```** :&ensp;<code>str</code>, default=<code>APP</code>
:   Description



###### Returns

<code>list</code>
:   liste des AppItem du même store



    
##### `Method get_one` {#hexagon.infrastructure.db.models.data_items.AppItem.get_one}



    
> `def get_one(key: str, *, store: str = None, dbsession: sqlalchemy.orm.session.Session)`


Retourne une liste ou un element selon les filtres (store, key)
Si key est fourni, retourne une seule instance


###### Args

**```key```** :&ensp;<code>str</code>
:   Clé d'identification de l'item


**```store```** :&ensp;<code>str</code>, default=<code>APP</code>
:   Le store de l'item



###### Returns

<code>[AppItem](#hexagon.infrastructure.db.models.data\_items.AppItem "hexagon.infrastructure.db.models.data\_items.AppItem")</code>
:   L'item correspondant au tuple (store, key)



    
##### `Method list_keys` {#hexagon.infrastructure.db.models.data_items.AppItem.list_keys}



    
> `def list_keys(*, store: str = None, dbsession: sqlalchemy.orm.session.Session) -> set`


Liste les keys définis dans un store, if None,
retourne tous les keys *Uniques* tout store confondu


###### Args

**```store```** :&ensp;<code>str</code>, default=<code>APP</code>
:   store cible, if None retourne tous les keys uniques définis



###### Returns

<code>set</code>
:   set des keys uniques



    
##### `Method list_stores` {#hexagon.infrastructure.db.models.data_items.AppItem.list_stores}



    
> `def list_stores(dbsession: sqlalchemy.orm.session.Session) -> set`


Liste les stores dans la table app_item


###### Returns

<code>set</code>
:   set[str] des stores définis dans la table



    
##### `Method stringify_dict` {#hexagon.infrastructure.db.models.data_items.AppItem.stringify_dict}



    
> `def stringify_dict(dico: dict) -> dict`


Convertit Les clés et les valeurs en texte pour être exploité par HSTORE


###### Args

**```dico```** :&ensp;<code>dict</code>
:   Dict d'input



###### Returns

<code>dict</code>
:   dict[str: str] Le meme dict avec les valeurs en texte




    
#### Methods


    
##### Method `to_document` {#hexagon.infrastructure.db.models.data_items.AppItem.to_document}



    
> `def to_document(self, *, bytes_content: bool = False) -> dict`


Retourne la ligne sous forme d'un document linéarisé avec le contenu de store_content


###### Args

**```bytes_content```** :&ensp;<code>bool</code>, default=<code>False</code>
:   Si True retourne aussi le champs bytes_content



###### Returns

<code>dict</code>
:   Dictionnaire avec les clés (store, key)



    
##### Method `to_records` {#hexagon.infrastructure.db.models.data_items.AppItem.to_records}



    
> `def to_records(self, *, bytes_content: bool = False) -> list[dict]`




    
### Class `DataItemManager` {#hexagon.infrastructure.db.models.data_items.DataItemManager}



> `class DataItemManager(model, default_store: str = None, *, dbsession: sqlalchemy.orm.session.Session)`





    
#### Ancestors (in MRO)

* [hexagon.infrastructure.query_executor.QueryExecutor](#hexagon.infrastructure.query_executor.QueryExecutor)






    
#### Methods


    
##### Method `get_one` {#hexagon.infrastructure.db.models.data_items.DataItemManager.get_one}



    
> `def get_one(self, key: str, store: str = None)`


Retourne une liste ou un element selon les filtres (store, key)
Si key est fourni, retourne une seule instance


###### Args

**```key```** :&ensp;<code>str</code>
:   Clé d'identification de l'item


**```store```** :&ensp;<code>str</code>, default=<code>APP</code>
:   Le store de l'item



###### Returns

<code>[AppItem](#hexagon.infrastructure.db.models.data\_items.AppItem "hexagon.infrastructure.db.models.data\_items.AppItem")</code>
:   L'item correspondant au tuple (store, key)



    
##### Method `list_store_keys` {#hexagon.infrastructure.db.models.data_items.DataItemManager.list_store_keys}



    
> `def list_store_keys(self, store: str = None) -> list[str]`




    
##### Method `list_stores` {#hexagon.infrastructure.db.models.data_items.DataItemManager.list_stores}



    
> `def list_stores(self) -> set`




    
### Class `MetaFonds` {#hexagon.infrastructure.db.models.data_items.MetaFonds}



> `class MetaFonds(**kwargs)`


Modèle des Meta Données des Fonds


#### Attributes

**```id```** :&ensp;<code>int</code>
:   L'ID du de l'enregistrement


**```id_fonds```** :&ensp;<code>int</code>
:   L'ID du Fonds


**```meta_key```** :&ensp;<code>str</code>
:   Code/Clé *UNIQUE* de qualification de la Meta-Valeur en Majuscules


**```meta_value```** :&ensp;<code>str</code>
:   Valeur de la Meta-Key


**```created_at```** :&ensp;<code>datetime.date</code>
:   Date de création de l'enregistrement


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)




    
#### Instance variables


    
##### Variable `created_at` {#hexagon.infrastructure.db.models.data_items.MetaFonds.created_at}



    
##### Variable `fonds` {#hexagon.infrastructure.db.models.data_items.MetaFonds.fonds}



    
##### Variable `id` {#hexagon.infrastructure.db.models.data_items.MetaFonds.id}



    
##### Variable `id_fonds` {#hexagon.infrastructure.db.models.data_items.MetaFonds.id_fonds}



    
##### Variable `meta_key` {#hexagon.infrastructure.db.models.data_items.MetaFonds.meta_key}



    
##### Variable `meta_value` {#hexagon.infrastructure.db.models.data_items.MetaFonds.meta_value}




    
#### Static methods


    
##### `Method get_fonds_meta` {#hexagon.infrastructure.db.models.data_items.MetaFonds.get_fonds_meta}



    
> `def get_fonds_meta(fonds, meta_key: str, dbsession: sqlalchemy.orm.session.Session)`


Retourne l'enregistrement Meta du Fonds correspondant à la clé 'meta_key'

```python
>>> from hexagon.infrastructure import db
>>> fonds = db.Fonds.by_id(5)
>>> get_fonds_meta(fonds, 'horizon_placement')  # MetaFonds instance
```



###### Args

**```fonds```** :&ensp;<code>db.Fonds</code>
:   Instance du Fonds désiré


**```meta_key```** :&ensp;<code>str</code>
:   Clé de la valeur voulue ex: UNIVERS_INVESTISSEMENT, HORIZON_PLACEMENT



###### Returns

<code>[MetaFonds](#hexagon.infrastructure.db.models.data\_items.MetaFonds "hexagon.infrastructure.db.models.data\_items.MetaFonds")</code>
:   Objet MetaFonds, retrieve the value via obj.meta_value



    
##### `Method list_fonds_meta` {#hexagon.infrastructure.db.models.data_items.MetaFonds.list_fonds_meta}



    
> `def list_fonds_meta(fonds, dbsession: sqlalchemy.orm.session.Session) -> list`


Liste les Meta Keys du Fonds, par ordre alphabétique croissant


###### Args

**```fonds```** :&ensp;<code>db.Fonds</code>
:   Instance du Fonds



###### Returns

<code>list\[[MetaFonds](#hexagon.infrastructure.db.models.data\_items.MetaFonds "hexagon.infrastructure.db.models.data\_items.MetaFonds")]</code>
:   If any, return the whole meta_keys list






    
# Module `hexagon.infrastructure.db.models.documents` {#hexagon.infrastructure.db.models.documents}







    
## Classes


    
### Class `FileDocument` {#hexagon.infrastructure.db.models.documents.FileDocument}



> `class FileDocument(**kwargs)`


Table pour stocker Les fichiers et documents au niveau de Postgresql

  - doctype: représente la classe de document ou son MetaType
i.e: (Template, Reference, Schema, Ressource, Static, IMG)

  - dockey: représente un deuxième niveau de classification i.e:
  (doctype=Template, dockey=SPECIFIQUE)
  (doctype=Template, dockey=GENERATOR)


#### Attributes

**```id```** :&ensp;<code>int</code>
:   id


**```id_mime_type```** :&ensp;<code>int</code>
:   id


**```doctype```** :&ensp;<code>str</code>
:   Type du document (Template, IMG, REFERENCE, ect)


**```dockey```** :&ensp;<code>str</code>
:   Cle du document


**```docname```** :&ensp;<code>str</code>
:   Nom du document, sera utilisé par défaut pour l'exportation avec son extension


**```description```** :&ensp;<code>str</code>
:   Description sommaire du fichier


**```content```** :&ensp;<code>byte</code>
:   Fichier en mode binaire


**```categorie```** :&ensp;<code>str</code>
:   Categorie du Fonds (DIV, ACTIONS, OBL, etc)


**```frequence_vl```** :&ensp;<code>str</code>
:   frequence vl du Fonds H, Q (template only)


**```type_gestion```** :&ensp;<code>str</code>
:   Type de gestion du Fonds D, P (template only)


**```codes_clients```** :&ensp;<code>list\[str]</code>
:   liste des codes des Clients (code_manar)


**```codes_fonds```** :&ensp;<code>list\[str]</code>
:   liste des codes des Fonds (code_manar)


**```mime_type```** :&ensp;<code>[MimeType](#hexagon.infrastructure.db.models.documents.MimeType "hexagon.infrastructure.db.models.documents.MimeType")</code>
:   MimeType Class


created_at (DateTime(tz)): Date de création
updated_at (DateTime(tz)): Date de dernière maj
A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)
* [hexagon.infrastructure.db.models.base_mixin.DocumentTableMixin](#hexagon.infrastructure.db.models.base_mixin.DocumentTableMixin)




    
#### Instance variables


    
##### Variable `categorie` {#hexagon.infrastructure.db.models.documents.FileDocument.categorie}



    
##### Variable `codes_clients` {#hexagon.infrastructure.db.models.documents.FileDocument.codes_clients}



    
##### Variable `codes_fonds` {#hexagon.infrastructure.db.models.documents.FileDocument.codes_fonds}



    
##### Variable `content` {#hexagon.infrastructure.db.models.documents.FileDocument.content}



    
##### Variable `content_enc` {#hexagon.infrastructure.db.models.documents.FileDocument.content_enc}



    
##### Variable `created_at` {#hexagon.infrastructure.db.models.documents.FileDocument.created_at}



    
##### Variable `description` {#hexagon.infrastructure.db.models.documents.FileDocument.description}



    
##### Variable `dockey` {#hexagon.infrastructure.db.models.documents.FileDocument.dockey}



    
##### Variable `docname` {#hexagon.infrastructure.db.models.documents.FileDocument.docname}



    
##### Variable `doctype` {#hexagon.infrastructure.db.models.documents.FileDocument.doctype}



    
##### Variable `id` {#hexagon.infrastructure.db.models.documents.FileDocument.id}



    
##### Variable `id_mime_type` {#hexagon.infrastructure.db.models.documents.FileDocument.id_mime_type}



    
##### Variable `mime_type` {#hexagon.infrastructure.db.models.documents.FileDocument.mime_type}



    
##### Variable `periodicite_reporting` {#hexagon.infrastructure.db.models.documents.FileDocument.periodicite_reporting}



    
##### Variable `periodicite_vl` {#hexagon.infrastructure.db.models.documents.FileDocument.periodicite_vl}



    
##### Variable `type_gestion` {#hexagon.infrastructure.db.models.documents.FileDocument.type_gestion}



    
##### Variable `updated_at` {#hexagon.infrastructure.db.models.documents.FileDocument.updated_at}




    
#### Static methods


    
##### `Method by_ids` {#hexagon.infrastructure.db.models.documents.FileDocument.by_ids}



    
> `def by_ids(liste_ids: list[int] = None, *, dbsession: sqlalchemy.orm.session.Session) -> list[typing.Self]`




    
##### `Method create_tge_template` {#hexagon.infrastructure.db.models.documents.FileDocument.create_tge_template}



    
> `def create_tge_template(docname: str = None, extension: str = None, content_enc: str = None, periodicite_reporting: str = None, periodicite_vl: str = None, *, description: str = None, codes_fonds: list[str] = None, codes_clients: list[str] = None, dbsession: sqlalchemy.orm.session.Session) -> Self`




    
##### `Method create_tge_template_from_file` {#hexagon.infrastructure.db.models.documents.FileDocument.create_tge_template_from_file}



    
> `def create_tge_template_from_file(fpath: str, periodicite_reporting: str, categorie: str = None, *, codes_fonds: list[str] = None, codes_clients: list[str] = None, periodicite_vl: str = None, type_gestion: str = None, dbsession: sqlalchemy.orm.session.Session) -> Self`


Factory method pour créer une instance pour le tge template avec une description auto générée


###### Args

**```fpath```** :&ensp;<code>TYPE</code>
:   Description


**```categorie```** :&ensp;<code>str</code>
:   Description


**```periodicite_reporting```** :&ensp;<code>str</code>
:   Description


**```periodicite_vl```** :&ensp;<code>str</code>, optional
:   Description


**```type_gestion```** :&ensp;<code>str</code>, optional
:   Description



###### Returns

<code>[FileDocument](#hexagon.infrastructure.db.models.documents.FileDocument "hexagon.infrastructure.db.models.documents.FileDocument")</code>
:   Description



###### Raises

<code>ValueError</code>
:   Si extension non valide



    
##### `Method get_document` {#hexagon.infrastructure.db.models.documents.FileDocument.get_document}



    
> `def get_document(doctype: str = None, dockey: str = None, docname: str = None, *, description: str = None, dbsession: sqlalchemy.orm.session.Session) -> list[typing.Self]`


Retourne une liste ou un element selon les filtres (doctype, key)


###### Args

**```doctype```** :&ensp;<code>str</code>
:   Le doctype de l'item


**```key```** :&ensp;<code>str</code>
:   Clé d'identification de l'item



###### Returns

<code>[FileDocument](#hexagon.infrastructure.db.models.documents.FileDocument "hexagon.infrastructure.db.models.documents.FileDocument")</code>
:   L'item correspondant au tuple (doctype, key)



    
##### `Method get_tge_template` {#hexagon.infrastructure.db.models.documents.FileDocument.get_tge_template}



    
> `def get_tge_template(categorie: str, periodicite_reporting: str, docname: str = None, periodicite_vl: str = None, *, type_gestion: str = None, dbsession: sqlalchemy.orm.session.Session) -> list[typing.Self]`




    
##### `Method list_dockey` {#hexagon.infrastructure.db.models.documents.FileDocument.list_dockey}



    
> `def list_dockey(*, doctype: str = None, dbsession: sqlalchemy.orm.session.Session) -> list[str]`


Liste les clés des dimensions (doctype, , key)


###### Args

**```doctype```** :&ensp;<code>str</code>, default=<code>None</code>
:   doctype cible, if None retourne tous les keys uniques définis



###### Returns

<code>set</code>
:   set des keys uniques



    
##### `Method list_doctype` {#hexagon.infrastructure.db.models.documents.FileDocument.list_doctype}



    
> `def list_doctype(dbsession: sqlalchemy.orm.session.Session) -> list[str]`




    
##### `Method of_doctype` {#hexagon.infrastructure.db.models.documents.FileDocument.of_doctype}



    
> `def of_doctype(doctype: str, dbsession: sqlalchemy.orm.session.Session) -> list[typing.Self]`




    
##### `Method of_fonds_all` {#hexagon.infrastructure.db.models.documents.FileDocument.of_fonds_all}



    
> `def of_fonds_all(codes_fonds: list[str] = None, *, dbsession: sqlalchemy.orm.session.Session) -> list[typing.Self]`




    
##### `Method of_fonds_any` {#hexagon.infrastructure.db.models.documents.FileDocument.of_fonds_any}



    
> `def of_fonds_any(codes_fonds: list[str] = None, *, dbsession: sqlalchemy.orm.session.Session) -> list[typing.Self]`





    
#### Methods


    
##### Method `save_to_excel` {#hexagon.infrastructure.db.models.documents.FileDocument.save_to_excel}



    
> `def save_to_excel(self, dirpath: pathlib.Path = PosixPath('.'), fname: str = None, metadata: bool = True)`




    
##### Method `save_to_file` {#hexagon.infrastructure.db.models.documents.FileDocument.save_to_file}



    
> `def save_to_file(self, dirpath: pathlib.Path = PosixPath('.'), fname: str = None, extension: str = None)`




    
##### Method `update_content_from_file` {#hexagon.infrastructure.db.models.documents.FileDocument.update_content_from_file}



    
> `def update_content_from_file(self, fpath: str)`


Lis le contenu d'un fichier en binaire et l'insère dans self.content


###### Args

**```fpath```** :&ensp;<code>str</code>
:   Chemin du fichier



###### Raises

<code>ValueError</code>
:   Si le chemin fourni n'est pas valide



    
### Class `MimeType` {#hexagon.infrastructure.db.models.documents.MimeType}



> `class MimeType(**kwargs)`


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)
* [hexagon.infrastructure.db.models.base_mixin.ReferentielTableMixin](#hexagon.infrastructure.db.models.base_mixin.ReferentielTableMixin)




    
#### Instance variables


    
##### Variable `code` {#hexagon.infrastructure.db.models.documents.MimeType.code}



    
##### Variable `created_at` {#hexagon.infrastructure.db.models.documents.MimeType.created_at}



    
##### Variable `description` {#hexagon.infrastructure.db.models.documents.MimeType.description}



    
##### Variable `extension` {#hexagon.infrastructure.db.models.documents.MimeType.extension}



    
##### Variable `id` {#hexagon.infrastructure.db.models.documents.MimeType.id}



    
##### Variable `mime` {#hexagon.infrastructure.db.models.documents.MimeType.mime}



    
##### Variable `secondary_extensions` {#hexagon.infrastructure.db.models.documents.MimeType.secondary_extensions}




    
#### Static methods


    
##### `Method any_secondary_extension` {#hexagon.infrastructure.db.models.documents.MimeType.any_secondary_extension}



    
> `def any_secondary_extension(extension: str, dbsession: sqlalchemy.orm.session.Session) -> Self`




    
##### `Method by_code` {#hexagon.infrastructure.db.models.documents.MimeType.by_code}



    
> `def by_code(code: str, dbsession: sqlalchemy.orm.session.Session) -> Self`




    
##### `Method by_extension` {#hexagon.infrastructure.db.models.documents.MimeType.by_extension}



    
> `def by_extension(extension: str, dbsession: sqlalchemy.orm.session.Session) -> Self`







    
# Module `hexagon.infrastructure.db.models.market` {#hexagon.infrastructure.db.models.market}







    
## Classes


    
### Class `Benchmark` {#hexagon.infrastructure.db.models.market.Benchmark}



> `class Benchmark(**kwargs)`


Modèle du Benchmark


#### Attributes

**```id```** :&ensp;<code>int</code>
:   ID de l'enregistrement du Benchmark


**```code```** :&ensp;<code>str</code>
:   Code *UNIQUE* du Benchmark


**```description```** :&ensp;<code>str</code>
:   Description courte du Benchmark


**```base_initialisation```** :&ensp;<code>float</code>
:   Base d'initialisation du benchmark


**```spread```** :&ensp;<code>float</code>
:   Le spread à ajouter lors du calcul de la perf


periodicite_spread (str='A'): Périodicité de calcul du spread
**```composition```** :&ensp;<code>[BenchmarkComposition](#hexagon.infrastructure.db.models.market.BenchmarkComposition "hexagon.infrastructure.db.models.market.BenchmarkComposition")</code>
:   Relation de la composition du Benchmark
    Même si le bench est Simple, il est nécessaire de l'assicier avec l'indice correspondant


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)
* [hexagon.infrastructure.db.models.base_mixin.ReferentielTableMixin](#hexagon.infrastructure.db.models.base_mixin.ReferentielTableMixin)




    
#### Instance variables


    
##### Variable `base_initialisation` {#hexagon.infrastructure.db.models.market.Benchmark.base_initialisation}



    
##### Variable `code` {#hexagon.infrastructure.db.models.market.Benchmark.code}



    
##### Variable `composition` {#hexagon.infrastructure.db.models.market.Benchmark.composition}



    
##### Variable `composition_indice_dico` {#hexagon.infrastructure.db.models.market.Benchmark.composition_indice_dico}

Construit un dict où chaque clé contient le code de l'indice.


###### Returns

<code>dict</code>
:   Dictionnaire indexé par Indice.code



    
##### Variable `created_at` {#hexagon.infrastructure.db.models.market.Benchmark.created_at}



    
##### Variable `description` {#hexagon.infrastructure.db.models.market.Benchmark.description}



    
##### Variable `id` {#hexagon.infrastructure.db.models.market.Benchmark.id}



    
##### Variable `periodicite_spread` {#hexagon.infrastructure.db.models.market.Benchmark.periodicite_spread}



    
##### Variable `spread` {#hexagon.infrastructure.db.models.market.Benchmark.spread}




    
#### Static methods


    
##### `Method by_code` {#hexagon.infrastructure.db.models.market.Benchmark.by_code}



    
> `def by_code(code_bench: str, dbsession: sqlalchemy.orm.session.Session)`


Fetch un Benchmark par 'code'


###### Args

**```code_bench```** :&ensp;<code>str</code>
:   Code naturel *UNIQUE* du Benchmark



###### Returns

<code>[Benchmark](#hexagon.infrastructure.db.models.market.Benchmark "hexagon.infrastructure.db.models.market.Benchmark")</code>
:   Enregistrement depuis la BDD




    
### Class `BenchmarkComposition` {#hexagon.infrastructure.db.models.market.BenchmarkComposition}



> `class BenchmarkComposition(**kwargs)`


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)
* [hexagon.infrastructure.db.models.base_mixin.RawTableMixin](#hexagon.infrastructure.db.models.base_mixin.RawTableMixin)




    
#### Instance variables


    
##### Variable `benchmark` {#hexagon.infrastructure.db.models.market.BenchmarkComposition.benchmark}



    
##### Variable `created_at` {#hexagon.infrastructure.db.models.market.BenchmarkComposition.created_at}



    
##### Variable `id` {#hexagon.infrastructure.db.models.market.BenchmarkComposition.id}



    
##### Variable `id_benchmark` {#hexagon.infrastructure.db.models.market.BenchmarkComposition.id_benchmark}



    
##### Variable `id_indice` {#hexagon.infrastructure.db.models.market.BenchmarkComposition.id_indice}



    
##### Variable `indice` {#hexagon.infrastructure.db.models.market.BenchmarkComposition.indice}



    
##### Variable `poids` {#hexagon.infrastructure.db.models.market.BenchmarkComposition.poids}



    
##### Variable `spread_indice` {#hexagon.infrastructure.db.models.market.BenchmarkComposition.spread_indice}





    
### Class `CompositionMBI` {#hexagon.infrastructure.db.models.market.CompositionMBI}



> `class CompositionMBI(**kwargs)`


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)
* [hexagon.infrastructure.db.models.base_mixin.HistoriqueTableMixin](#hexagon.infrastructure.db.models.base_mixin.HistoriqueTableMixin)




    
#### Instance variables


    
##### Variable `code_strate` {#hexagon.infrastructure.db.models.market.CompositionMBI.code_strate}



    
##### Variable `code_titre` {#hexagon.infrastructure.db.models.market.CompositionMBI.code_titre}



    
##### Variable `cours` {#hexagon.infrastructure.db.models.market.CompositionMBI.cours}



    
##### Variable `cours_ligne` {#hexagon.infrastructure.db.models.market.CompositionMBI.cours_ligne}



    
##### Variable `date_marche` {#hexagon.infrastructure.db.models.market.CompositionMBI.date_marche}



    
##### Variable `gisement` {#hexagon.infrastructure.db.models.market.CompositionMBI.gisement}



    
##### Variable `id` {#hexagon.infrastructure.db.models.market.CompositionMBI.id}



    
##### Variable `nominal` {#hexagon.infrastructure.db.models.market.CompositionMBI.nominal}



    
##### Variable `poids_strate` {#hexagon.infrastructure.db.models.market.CompositionMBI.poids_strate}



    
##### Variable `quantite` {#hexagon.infrastructure.db.models.market.CompositionMBI.quantite}



    
##### Variable `quantite_totale` {#hexagon.infrastructure.db.models.market.CompositionMBI.quantite_totale}




    
#### Static methods


    
##### `Method by_date` {#hexagon.infrastructure.db.models.market.CompositionMBI.by_date}



    
> `def by_date(date_marche: datetime.date, dbsession: sqlalchemy.orm.session.Session) -> list[typing.Self]`


Retourne la composition de toutes les strates à une date donnée

    
##### `Method by_strate` {#hexagon.infrastructure.db.models.market.CompositionMBI.by_strate}



    
> `def by_strate(code_strate: str, date_marche: datetime.date, dbsession: sqlalchemy.orm.session.Session) -> list[typing.Self]`


Retourne la composition titre de la strate


    
### Class `CompositionMasi` {#hexagon.infrastructure.db.models.market.CompositionMasi}



> `class CompositionMasi(**kwargs)`


Composition de l'indice du MASI, par titre.


#### Attributes

**```capi_flottante```** :&ensp;<code>float</code>
:   Capitalisation de la valeur


**```code_titre```** :&ensp;<code>str</code>
:   Code ISIN *MA000xxxx* du Titre


**```cours```** :&ensp;<code>float</code>
:   Description


**```date_marche```** :&ensp;<code>TYPE</code>
:   Description


**```facteur_flottant```** :&ensp;<code>TYPE</code>
:   Description


**```facteur_plafonnement```** :&ensp;<code>TYPE</code>
:   Description


**```id```** :&ensp;<code>TYPE</code>
:   Description


**```poids```** :&ensp;<code>TYPE</code>
:   Description


**```total_titres```** :&ensp;<code>TYPE</code>
:   Description


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)
* [hexagon.infrastructure.db.models.base_mixin.HistoriqueTableMixin](#hexagon.infrastructure.db.models.base_mixin.HistoriqueTableMixin)




    
#### Instance variables


    
##### Variable `capi_flottante` {#hexagon.infrastructure.db.models.market.CompositionMasi.capi_flottante}



    
##### Variable `code_titre` {#hexagon.infrastructure.db.models.market.CompositionMasi.code_titre}



    
##### Variable `cours` {#hexagon.infrastructure.db.models.market.CompositionMasi.cours}



    
##### Variable `date_marche` {#hexagon.infrastructure.db.models.market.CompositionMasi.date_marche}



    
##### Variable `facteur_flottant` {#hexagon.infrastructure.db.models.market.CompositionMasi.facteur_flottant}



    
##### Variable `facteur_plafonnement` {#hexagon.infrastructure.db.models.market.CompositionMasi.facteur_plafonnement}



    
##### Variable `id` {#hexagon.infrastructure.db.models.market.CompositionMasi.id}



    
##### Variable `poids` {#hexagon.infrastructure.db.models.market.CompositionMasi.poids}



    
##### Variable `total_titres` {#hexagon.infrastructure.db.models.market.CompositionMasi.total_titres}




    
#### Static methods


    
##### `Method by_date` {#hexagon.infrastructure.db.models.market.CompositionMasi.by_date}



    
> `def by_date(date_marche: datetime.date, dbsession: sqlalchemy.orm.session.Session) -> list[typing.Self]`




    
##### `Method by_titre` {#hexagon.infrastructure.db.models.market.CompositionMasi.by_titre}



    
> `def by_titre(code_titre: str, debut: datetime.date = None, fin: datetime.date = None, *, dbsession: sqlalchemy.orm.session.Session) -> Self`


Retourne la série de valeurs du titre entre deux dates.


###### Args

**```code_titre```** :&ensp;<code>str</code>
:   le code isin du titre 'MA000...'


**```debut```** :&ensp;<code>datetime.date, None</code>
:   date de début incluse


**```fin```** :&ensp;<code>datetime.date, None</code>
:   date de fin incluse




    
### Class `Courbe` {#hexagon.infrastructure.db.models.market.Courbe}



> `class Courbe(**kwargs)`


La courbe des taux telle que publiée par BKAM


#### Attributes

**```id```** :&ensp;<code>int</code>
:   L'ID de l'enregistrement


**```date_marche```** :&ensp;<code>date</code>
:   La date de valeur de la courbe/date de valorisation des titres


**```date_echeance```** :&ensp;<code>date</code>
:   La date d'échéance de la ligne de la courbe


**```transactions```** :&ensp;<code>float</code>
:   Les transactions sur la ligne en Millions de MAD


**```taux```** :&ensp;<code>float</code>
:   Le taux sur la courbe


**```date_valeur```** :&ensp;<code>date</code>
:   La date de valeur de la ligne BDT


**```date_transaction```** :&ensp;<code>date</code>
:   La date de transaction de la courbe généralement J-1


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)
* [hexagon.infrastructure.db.models.base_mixin.HistoriqueTableMixin](#hexagon.infrastructure.db.models.base_mixin.HistoriqueTableMixin)




    
#### Instance variables


    
##### Variable `date_echeance` {#hexagon.infrastructure.db.models.market.Courbe.date_echeance}



    
##### Variable `date_marche` {#hexagon.infrastructure.db.models.market.Courbe.date_marche}



    
##### Variable `date_transaction` {#hexagon.infrastructure.db.models.market.Courbe.date_transaction}



    
##### Variable `date_valeur` {#hexagon.infrastructure.db.models.market.Courbe.date_valeur}



    
##### Variable `id` {#hexagon.infrastructure.db.models.market.Courbe.id}



    
##### Variable `maturite` {#hexagon.infrastructure.db.models.market.Courbe.maturite}

Property pour calculer la maturite résiduelle d'un point Courbe


###### Returns

<code>int</code>
:   La difference entre date_echeance et date_valeur en jours



###### Raises

<code>ValueError</code>
:   si date_valeur ou date_echeance manquante



    
##### Variable `taux` {#hexagon.infrastructure.db.models.market.Courbe.taux}



    
##### Variable `transactions` {#hexagon.infrastructure.db.models.market.Courbe.transactions}




    
#### Static methods


    
##### `Method at_date_marche` {#hexagon.infrastructure.db.models.market.Courbe.at_date_marche}



    
> `def at_date_marche(date_marche: datetime.date, dbsession: sqlalchemy.orm.session.Session) -> list[typing.Self]`




    
##### `Method at_date_transaction` {#hexagon.infrastructure.db.models.market.Courbe.at_date_transaction}



    
> `def at_date_transaction(date_transaction: datetime.date, dbsession: sqlalchemy.orm.session.Session) -> list[typing.Self]`





    
### Class `Devise` {#hexagon.infrastructure.db.models.market.Devise}



> `class Devise(**kwargs)`


Modèle des Devise, Les codes adhèrent au standard Mondial


#### Attributes

**```id```** :&ensp;<code>int</code>
:   L'ID de la Devise


**```code```** :&ensp;<code>str</code>
:   Code *UNIQUE* de la devise


**```description```** :&ensp;<code>str</code>
:   Description de la devise


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)
* [hexagon.infrastructure.db.models.base_mixin.ReferentielTableMixin](#hexagon.infrastructure.db.models.base_mixin.ReferentielTableMixin)




    
#### Instance variables


    
##### Variable `code` {#hexagon.infrastructure.db.models.market.Devise.code}



    
##### Variable `created_at` {#hexagon.infrastructure.db.models.market.Devise.created_at}



    
##### Variable `description` {#hexagon.infrastructure.db.models.market.Devise.description}



    
##### Variable `id` {#hexagon.infrastructure.db.models.market.Devise.id}




    
#### Static methods


    
##### `Method by_code` {#hexagon.infrastructure.db.models.market.Devise.by_code}



    
> `def by_code(code_devise: str, dbsession: sqlalchemy.orm.session.Session)`


Cherche une devise par son code


###### Args

**```code_devise```** :&ensp;<code>str</code>
:   Code de la devise



###### Returns

<code>[Devise](#hexagon.infrastructure.db.models.market.Devise "hexagon.infrastructure.db.models.market.Devise")</code>
:   Instance Devise or None if not Found!




    
### Class `DeviseCours` {#hexagon.infrastructure.db.models.market.DeviseCours}



> `class DeviseCours(**kwargs)`


Stocke les cours de devise selon une structure qui facilite la conversion,
qui prend en charge les différences des unités minimales de conversion i.e: YEN, SEK
et qui permet de stocker les cours pour plusieurs cours de base ou plusieurs pays. Awesome!

Cependant La définition de la devise de base ou devise par défaut est gérée
au niveau du fichier de configuration d'Hexagon et qui peut être changée at RUN Time.

Ce qui ouvre la possibilité de générer un reporting où l'inventaire est valaorisé selon
une devise autre que le 'MAD' i.e: prospect d'un investisseur étranger.


#### Attributes

**```id```** :&ensp;<code>int</code>
:   L'ID du cours de change


**```date_marche```** :&ensp;<code>date</code>
:   Date de marché / Date de valeur


**```id_devise```** :&ensp;<code>int</code>
:   L'ID de la devise étrangère


**```unite_devise```** :&ensp;<code>int</code>
:   Unité de la devise étrangère


**```id_devise_locale```** :&ensp;<code>int</code>
:   L'ID de la devise de base ou devise Locale


**```unite_devise_locale```** :&ensp;<code>int</code>
:   Unité de la devise de base


**```cours```** :&ensp;<code>float</code>
:   Cours de conversion de 'devise' vers 'devise_base'



**```devise```** :&ensp;<code>[Devise](#hexagon.infrastructure.db.models.market.Devise "hexagon.infrastructure.db.models.market.Devise")</code>
:   *REL* Devise étrangère


**```devise_base```** :&ensp;<code>[Devise](#hexagon.infrastructure.db.models.market.Devise "hexagon.infrastructure.db.models.market.Devise")</code>
:   *REL* Devise de base


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)
* [hexagon.infrastructure.db.models.base_mixin.TimeSeriesTableMixin](#hexagon.infrastructure.db.models.base_mixin.TimeSeriesTableMixin)




    
#### Instance variables


    
##### Variable `cours` {#hexagon.infrastructure.db.models.market.DeviseCours.cours}



    
##### Variable `date_marche` {#hexagon.infrastructure.db.models.market.DeviseCours.date_marche}



    
##### Variable `devise` {#hexagon.infrastructure.db.models.market.DeviseCours.devise}



    
##### Variable `devise_base` {#hexagon.infrastructure.db.models.market.DeviseCours.devise_base}



    
##### Variable `id` {#hexagon.infrastructure.db.models.market.DeviseCours.id}



    
##### Variable `id_devise` {#hexagon.infrastructure.db.models.market.DeviseCours.id_devise}



    
##### Variable `id_devise_locale` {#hexagon.infrastructure.db.models.market.DeviseCours.id_devise_locale}



    
##### Variable `unite_devise` {#hexagon.infrastructure.db.models.market.DeviseCours.unite_devise}



    
##### Variable `unite_devise_locale` {#hexagon.infrastructure.db.models.market.DeviseCours.unite_devise_locale}




    
#### Static methods


    
##### `Method by_date` {#hexagon.infrastructure.db.models.market.DeviseCours.by_date}



    
> `def by_date(date_marche: datetime.date, dbsession: sqlalchemy.orm.session.Session) -> list`


Retourne les cours de devise à la date passée en paramètres


###### Args

**```date_marche```** :&ensp;<code>date</code>
:   date de valeur / Date marché



###### Returns

<code>list\[[DeviseCours](#hexagon.infrastructure.db.models.market.DeviseCours "hexagon.infrastructure.db.models.market.DeviseCours")]</code>
:   Liste des cours de devise à la date (date_marche)




    
### Class `Emetteur` {#hexagon.infrastructure.db.models.market.Emetteur}



> `class Emetteur(**kwargs)`


Modèle pour les Emetteurs du Marché


#### Attributes

**```id```** :&ensp;<code>int</code>
:   L'ID de l'émetteur


**```id_parent```** :&ensp;<code>int</code>
:   L'ID de l'emetteur parent, permet de lier les filiales


**```id_pays```** :&ensp;<code>int</code>
:   L'ID du Pays de l'emetteur


**```id_secteur```** :&ensp;<code>int</code>
:   L'ID du Secteur de l'emetteur


**```code```** :&ensp;<code>str</code>
:   Code *UNIQUE* de l'Emetteur


**```description```** :&ensp;<code>str</code>
:   Description de l'émetteur


**```isin```** :&ensp;<code>str</code>
:   Code ISIN de l'emetteur


**```rating```** :&ensp;<code>str</code>
:   Rating interne ou externe


**```secteur```** :&ensp;<code>[Secteur](#hexagon.infrastructure.db.models.market.Secteur "hexagon.infrastructure.db.models.market.Secteur")</code>
:   (relationship) Le Secteur de l'emetteur


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)
* [hexagon.infrastructure.db.models.base_mixin.ReferentielTableMixin](#hexagon.infrastructure.db.models.base_mixin.ReferentielTableMixin)




    
#### Instance variables


    
##### Variable `childs` {#hexagon.infrastructure.db.models.market.Emetteur.childs}



    
##### Variable `code` {#hexagon.infrastructure.db.models.market.Emetteur.code}



    
##### Variable `created_at` {#hexagon.infrastructure.db.models.market.Emetteur.created_at}



    
##### Variable `description` {#hexagon.infrastructure.db.models.market.Emetteur.description}



    
##### Variable `has_children` {#hexagon.infrastructure.db.models.market.Emetteur.has_children}



    
##### Variable `id` {#hexagon.infrastructure.db.models.market.Emetteur.id}



    
##### Variable `id_parent` {#hexagon.infrastructure.db.models.market.Emetteur.id_parent}



    
##### Variable `id_pays` {#hexagon.infrastructure.db.models.market.Emetteur.id_pays}



    
##### Variable `id_secteur` {#hexagon.infrastructure.db.models.market.Emetteur.id_secteur}



    
##### Variable `isin` {#hexagon.infrastructure.db.models.market.Emetteur.isin}



    
##### Variable `pays` {#hexagon.infrastructure.db.models.market.Emetteur.pays}



    
##### Variable `rating` {#hexagon.infrastructure.db.models.market.Emetteur.rating}



    
##### Variable `secteur` {#hexagon.infrastructure.db.models.market.Emetteur.secteur}




    
#### Static methods


    
##### `Method by_code` {#hexagon.infrastructure.db.models.market.Emetteur.by_code}



    
> `def by_code(code_emetteur: str, dbsession: sqlalchemy.orm.session.Session) -> Self`


Cherche un Emetteur par son code


###### Args

**```code_emetteur```** :&ensp;<code>str</code>
:   Le code de l'emetteur



###### Returns

<code>[Emetteur](#hexagon.infrastructure.db.models.market.Emetteur "hexagon.infrastructure.db.models.market.Emetteur")</code>
:   Instance Emetteur ou None if NotFound!



    
##### `Method by_id` {#hexagon.infrastructure.db.models.market.Emetteur.by_id}



    
> `def by_id(id_emetteur: int, dbsession: sqlalchemy.orm.session.Session) -> Self`


Cherche un Emetteur par son ID


###### Args

**```id_emetteur```** :&ensp;<code>str</code>
:   L'ID de l'emetteur



###### Returns

<code>[Emetteur](#hexagon.infrastructure.db.models.market.Emetteur "hexagon.infrastructure.db.models.market.Emetteur")</code>
:   Instance Emetteur ou None if NotFound!



    
##### `Method by_isin` {#hexagon.infrastructure.db.models.market.Emetteur.by_isin}



    
> `def by_isin(isin_emetteur: str, dbsession: sqlalchemy.orm.session.Session) -> Self`


Cherche un Emetteur par son code ISIN


###### Args

**```isin_emetteur```** :&ensp;<code>str</code>
:   Le code de l'emetteur



###### Returns

<code>[Emetteur](#hexagon.infrastructure.db.models.market.Emetteur "hexagon.infrastructure.db.models.market.Emetteur")</code>
:   Instance Emetteur ou None if NotFound!




    
### Class `EtatFondsMarket` {#hexagon.infrastructure.db.models.market.EtatFondsMarket}



> `class EtatFondsMarket(**kwargs)`


Historique des Fonds Marché publié par l'ASFIM


#### Attributes

**```id```** :&ensp;<code>int</code>
:   L'ID de l'enregistrement


**```id_fonds```** :&ensp;<code>int</code>
:   L'ID du Fonds concerné


**```actif_net```** :&ensp;<code>float</code>
:   L'actif Net du Fonds


**```date_marche```** :&ensp;<code>date</code>
:   Date de valeur


**```vl```** :&ensp;<code>float</code>
:   La Valeur Liquidative du Fonds


**```perf_1s```** :&ensp;<code>float</code>
:   Performance 1 Semaine


**```perf_1m```** :&ensp;<code>float</code>
:   Performance 1 Mois


**```perf_3m```** :&ensp;<code>float</code>
:   Performance 3 Mois


**```perf_6m```** :&ensp;<code>float</code>
:   Performance 6 Mois


**```perf_1a```** :&ensp;<code>float</code>
:   Performance 1 An


**```perf_2a```** :&ensp;<code>float</code>
:   Performance 2 Ans


**```perf_3a```** :&ensp;<code>float</code>
:   Performance 3 Ans


**```perf_5a```** :&ensp;<code>float</code>
:   Performance 5 Ans


**```perf_ytd```** :&ensp;<code>float</code>
:   Performance YTD


**```fonds```** :&ensp;<code>[FondsMarket](#hexagon.infrastructure.db.models.market.FondsMarket "hexagon.infrastructure.db.models.market.FondsMarket")</code>
:   Le Fonds concerné


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)
* [hexagon.infrastructure.db.models.base_mixin.HistoriqueTableMixin](#hexagon.infrastructure.db.models.base_mixin.HistoriqueTableMixin)




    
#### Instance variables


    
##### Variable `actif_net` {#hexagon.infrastructure.db.models.market.EtatFondsMarket.actif_net}



    
##### Variable `date_marche` {#hexagon.infrastructure.db.models.market.EtatFondsMarket.date_marche}



    
##### Variable `fonds` {#hexagon.infrastructure.db.models.market.EtatFondsMarket.fonds}



    
##### Variable `id` {#hexagon.infrastructure.db.models.market.EtatFondsMarket.id}



    
##### Variable `id_fonds` {#hexagon.infrastructure.db.models.market.EtatFondsMarket.id_fonds}



    
##### Variable `perf_1a` {#hexagon.infrastructure.db.models.market.EtatFondsMarket.perf_1a}



    
##### Variable `perf_1m` {#hexagon.infrastructure.db.models.market.EtatFondsMarket.perf_1m}



    
##### Variable `perf_1s` {#hexagon.infrastructure.db.models.market.EtatFondsMarket.perf_1s}



    
##### Variable `perf_2a` {#hexagon.infrastructure.db.models.market.EtatFondsMarket.perf_2a}



    
##### Variable `perf_3a` {#hexagon.infrastructure.db.models.market.EtatFondsMarket.perf_3a}



    
##### Variable `perf_3m` {#hexagon.infrastructure.db.models.market.EtatFondsMarket.perf_3m}



    
##### Variable `perf_5a` {#hexagon.infrastructure.db.models.market.EtatFondsMarket.perf_5a}



    
##### Variable `perf_6m` {#hexagon.infrastructure.db.models.market.EtatFondsMarket.perf_6m}



    
##### Variable `perf_ytd` {#hexagon.infrastructure.db.models.market.EtatFondsMarket.perf_ytd}



    
##### Variable `vl` {#hexagon.infrastructure.db.models.market.EtatFondsMarket.vl}





    
### Class `FondsMarket` {#hexagon.infrastructure.db.models.market.FondsMarket}



> `class FondsMarket(**kwargs)`


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)
* [hexagon.infrastructure.db.models.base_mixin.ReferentielTableMixin](#hexagon.infrastructure.db.models.base_mixin.ReferentielTableMixin)




    
#### Instance variables


    
##### Variable `affectation_resultat` {#hexagon.infrastructure.db.models.market.FondsMarket.affectation_resultat}



    
##### Variable `categorie` {#hexagon.infrastructure.db.models.market.FondsMarket.categorie}



    
##### Variable `code_fonds` {#hexagon.infrastructure.db.models.market.FondsMarket.code_fonds}



    
##### Variable `code_isin` {#hexagon.infrastructure.db.models.market.FondsMarket.code_isin}



    
##### Variable `created_at` {#hexagon.infrastructure.db.models.market.FondsMarket.created_at}



    
##### Variable `depositaire` {#hexagon.infrastructure.db.models.market.FondsMarket.depositaire}



    
##### Variable `description` {#hexagon.infrastructure.db.models.market.FondsMarket.description}



    
##### Variable `etat` {#hexagon.infrastructure.db.models.market.FondsMarket.etat}



    
##### Variable `forme_juridique` {#hexagon.infrastructure.db.models.market.FondsMarket.forme_juridique}



    
##### Variable `frais_gestion` {#hexagon.infrastructure.db.models.market.FondsMarket.frais_gestion}



    
##### Variable `frais_rachat` {#hexagon.infrastructure.db.models.market.FondsMarket.frais_rachat}



    
##### Variable `frais_souscription` {#hexagon.infrastructure.db.models.market.FondsMarket.frais_souscription}



    
##### Variable `id` {#hexagon.infrastructure.db.models.market.FondsMarket.id}



    
##### Variable `indice_benchmark` {#hexagon.infrastructure.db.models.market.FondsMarket.indice_benchmark}



    
##### Variable `periodicite` {#hexagon.infrastructure.db.models.market.FondsMarket.periodicite}



    
##### Variable `promoteur` {#hexagon.infrastructure.db.models.market.FondsMarket.promoteur}



    
##### Variable `reseau_placeur` {#hexagon.infrastructure.db.models.market.FondsMarket.reseau_placeur}



    
##### Variable `sdg` {#hexagon.infrastructure.db.models.market.FondsMarket.sdg}



    
##### Variable `type_gestion` {#hexagon.infrastructure.db.models.market.FondsMarket.type_gestion}




    
#### Static methods


    
##### `Method by_code_fonds` {#hexagon.infrastructure.db.models.market.FondsMarket.by_code_fonds}



    
> `def by_code_fonds(code_fonds: str, dbsession: sqlalchemy.orm.session.Session) -> Self`


Retourne une instance 'FondsMarket' depuis la BDD en passant code_fonds
Le code Fonds est celui de 4 chiffres.

    
##### `Method by_code_isin` {#hexagon.infrastructure.db.models.market.FondsMarket.by_code_isin}



    
> `def by_code_isin(code_isin: str, dbsession: sqlalchemy.orm.session.Session) -> Self`


Retourne une instance 'FondsMarket' depuis la BDD en passant code_isin


    
### Class `Indice` {#hexagon.infrastructure.db.models.market.Indice}



> `class Indice(**kwargs)`


Modèle des Indices


#### Attributes

**```code```** :&ensp;<code>str</code>
:   Code Unique de l'indice


**```description```** :&ensp;<code>str</code>
:   Descriptoin courte de l'indice


**```marche```** :&ensp;<code>str</code>
:   Le marché selon notre vision dans IN (ACT, OBL)


**```place_cotation```** :&ensp;<code>str</code>
:   Description


**```date_creation```** :&ensp;<code>datetime.date</code>
:   Date de création


**```date_reference```** :&ensp;<code>datetime.date</code>
:   Date de référence


**```valeur_base```** :&ensp;<code>float</code>
:   Valeur de base de l'indice ex: 100 ou 1000


**```comment```** :&ensp;<code>str</code>
:   Commentaire sur l'indice, généralement le descriptif long publiquement publié


**```ticker```** :&ensp;<code>str</code>
:   Mnémonique


**```id_devise```** :&ensp;<code>int</code>
:   ID de la Devise de publication de l'indice


**```id_pays```** :&ensp;<code>int</code>
:   ID du Pays de cotation de l'indice


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)
* [hexagon.infrastructure.db.models.base_mixin.ReferentielTableMixin](#hexagon.infrastructure.db.models.base_mixin.ReferentielTableMixin)




    
#### Instance variables


    
##### Variable `code` {#hexagon.infrastructure.db.models.market.Indice.code}



    
##### Variable `comment` {#hexagon.infrastructure.db.models.market.Indice.comment}



    
##### Variable `created_at` {#hexagon.infrastructure.db.models.market.Indice.created_at}



    
##### Variable `date_creation` {#hexagon.infrastructure.db.models.market.Indice.date_creation}



    
##### Variable `date_reference` {#hexagon.infrastructure.db.models.market.Indice.date_reference}



    
##### Variable `description` {#hexagon.infrastructure.db.models.market.Indice.description}



    
##### Variable `devise` {#hexagon.infrastructure.db.models.market.Indice.devise}



    
##### Variable `etat` {#hexagon.infrastructure.db.models.market.Indice.etat}



    
##### Variable `id` {#hexagon.infrastructure.db.models.market.Indice.id}



    
##### Variable `id_devise` {#hexagon.infrastructure.db.models.market.Indice.id_devise}



    
##### Variable `id_pays` {#hexagon.infrastructure.db.models.market.Indice.id_pays}



    
##### Variable `marche` {#hexagon.infrastructure.db.models.market.Indice.marche}



    
##### Variable `pays` {#hexagon.infrastructure.db.models.market.Indice.pays}



    
##### Variable `place_cotation` {#hexagon.infrastructure.db.models.market.Indice.place_cotation}



    
##### Variable `ticker` {#hexagon.infrastructure.db.models.market.Indice.ticker}



    
##### Variable `valeur_base` {#hexagon.infrastructure.db.models.market.Indice.valeur_base}




    
#### Static methods


    
##### `Method by_code` {#hexagon.infrastructure.db.models.market.Indice.by_code}



    
> `def by_code(code: str, dbsession: sqlalchemy.orm.session.Session) -> Self`





    
### Class `IndiceValue` {#hexagon.infrastructure.db.models.market.IndiceValue}



> `class IndiceValue(**kwargs)`


Table générique pour stocker tout type d'indice.
Dans notre cas on utilisera cette table pour stocker et manipuler
les indices "actions" à savoir le MASI, MADEX, CFG25 et CFG25 flottant.
Chaque indice est identifiable par son ticker -> code.
Les "codes" sont stockés en Upper Case


#### Attributes

**```id```** :&ensp;<code>int</code>
:   ID de l'enregistrement


**```id_indice```** :&ensp;<code>int</code>
:   ID de l'indice Marché


**```date_marche```** :&ensp;<code>datetime.date</code>
:   Date de valeur


**```value```** :&ensp;<code>float</code>
:   Valeur de l'indice


**```indice```** :&ensp;<code>[Indice](#hexagon.infrastructure.db.models.market.Indice "hexagon.infrastructure.db.models.market.Indice")</code>
:   Relation Indice


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)
* [hexagon.infrastructure.db.models.base_mixin.TimeSeriesTableMixin](#hexagon.infrastructure.db.models.base_mixin.TimeSeriesTableMixin)




    
#### Instance variables


    
##### Variable `date_marche` {#hexagon.infrastructure.db.models.market.IndiceValue.date_marche}



    
##### Variable `id` {#hexagon.infrastructure.db.models.market.IndiceValue.id}



    
##### Variable `id_indice` {#hexagon.infrastructure.db.models.market.IndiceValue.id_indice}



    
##### Variable `indice` {#hexagon.infrastructure.db.models.market.IndiceValue.indice}



    
##### Variable `value` {#hexagon.infrastructure.db.models.market.IndiceValue.value}





    
### Class `Pays` {#hexagon.infrastructure.db.models.market.Pays}



> `class Pays(**kwargs)`


Représente les Pays selon la norme ISO 3361-2
avec une ségmentation par region et sub-region.


#### Attributes

**```id```** :&ensp;<code>int</code>
:   L'ID du Pays


**```alpha2```** :&ensp;<code>str</code>
:   Code alpha-2 du Pays *UNIQUE*


**```alpha3```** :&ensp;<code>str</code>
:   Code alpha-3 du Pays *UNIQUE*


**```code```** :&ensp;<code>str</code>
:   Code numérique su Pays


**```description```** :&ensp;<code>str</code>
:   Descriptoin du Pays (Anglais)


**```iso_version```** :&ensp;<code>str</code>
:   Version de la norme ISO


**```region```** :&ensp;<code>str</code>
:   Région du Pays


**```region_code```** :&ensp;<code>str</code>
:   Code de la Région


**```sub_region```** :&ensp;<code>str</code>
:   Sous-Région


**```sub_region_code```** :&ensp;<code>str</code>
:   Code de la Sous-Région


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)
* [hexagon.infrastructure.db.models.base_mixin.ReferentielTableMixin](#hexagon.infrastructure.db.models.base_mixin.ReferentielTableMixin)




    
#### Instance variables


    
##### Variable `alpha2` {#hexagon.infrastructure.db.models.market.Pays.alpha2}



    
##### Variable `alpha3` {#hexagon.infrastructure.db.models.market.Pays.alpha3}



    
##### Variable `code` {#hexagon.infrastructure.db.models.market.Pays.code}



    
##### Variable `created_at` {#hexagon.infrastructure.db.models.market.Pays.created_at}



    
##### Variable `description` {#hexagon.infrastructure.db.models.market.Pays.description}



    
##### Variable `id` {#hexagon.infrastructure.db.models.market.Pays.id}



    
##### Variable `iso_version` {#hexagon.infrastructure.db.models.market.Pays.iso_version}



    
##### Variable `region` {#hexagon.infrastructure.db.models.market.Pays.region}



    
##### Variable `region_code` {#hexagon.infrastructure.db.models.market.Pays.region_code}



    
##### Variable `sub_region` {#hexagon.infrastructure.db.models.market.Pays.sub_region}



    
##### Variable `sub_region_code` {#hexagon.infrastructure.db.models.market.Pays.sub_region_code}




    
#### Static methods


    
##### `Method by_alpha2` {#hexagon.infrastructure.db.models.market.Pays.by_alpha2}



    
> `def by_alpha2(alpha2: str, dbsession: sqlalchemy.orm.session.Session) -> Self`


Cherche un Pays par son code ALPHA-2


###### Args

**```alpha2```** :&ensp;<code>str</code>
:   Code ALPHA-2 du Pays selon la norme ISO 3361-2



###### Returns

<code>[Pays](#hexagon.infrastructure.db.models.market.Pays "hexagon.infrastructure.db.models.market.Pays")</code>
:   Instance Pays ou None if NotFound!



    
##### `Method by_alpha3` {#hexagon.infrastructure.db.models.market.Pays.by_alpha3}



    
> `def by_alpha3(alpha3: str, dbsession: sqlalchemy.orm.session.Session) -> Self`


Cherche un Pays par son code ALPHA-3


###### Args

**```alpha3```** :&ensp;<code>str</code>
:   Code ALPHA-3 du Pays selon la norme ISO 3361-2



###### Returns

<code>[Pays](#hexagon.infrastructure.db.models.market.Pays "hexagon.infrastructure.db.models.market.Pays")</code>
:   Instance Pays ou None if NotFound!




    
### Class `RawMBI` {#hexagon.infrastructure.db.models.market.RawMBI}



> `class RawMBI(**kwargs)`


Table intermédiaire pour stocker les valeurs publiées par BMCE


#### Attributes

**```id```** :&ensp;<code>int</code>
:   L'ID de l'enregistrement


**```date_marche```** :&ensp;<code>date</code>
:   Date de valeur


**```code_strate```** :&ensp;<code>str</code>
:   Code de la strate (MBI, MBICT, MBIMT, MBIMLT, MBILT)


**```value```** :&ensp;<code>float</code>
:   La valeur de l'indice de la strate


**```duration```** :&ensp;<code>float</code>
:   La duration pondérée de la Strate


**```sensibilite```** :&ensp;<code>float</code>
:   La sensibilité pondérée


**```ytm```** :&ensp;<code>ytm</code>
:   ytm pondéré de la Strate


**```coupon```** :&ensp;<code>float</code>
:   le taux coupon de la strate


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)
* [hexagon.infrastructure.db.models.base_mixin.HistoriqueTableMixin](#hexagon.infrastructure.db.models.base_mixin.HistoriqueTableMixin)




    
#### Instance variables


    
##### Variable `code_strate` {#hexagon.infrastructure.db.models.market.RawMBI.code_strate}



    
##### Variable `coupon` {#hexagon.infrastructure.db.models.market.RawMBI.coupon}



    
##### Variable `date_marche` {#hexagon.infrastructure.db.models.market.RawMBI.date_marche}



    
##### Variable `duration` {#hexagon.infrastructure.db.models.market.RawMBI.duration}



    
##### Variable `id` {#hexagon.infrastructure.db.models.market.RawMBI.id}



    
##### Variable `sensibilite` {#hexagon.infrastructure.db.models.market.RawMBI.sensibilite}



    
##### Variable `value` {#hexagon.infrastructure.db.models.market.RawMBI.value}



    
##### Variable `ytm` {#hexagon.infrastructure.db.models.market.RawMBI.ytm}




    
#### Static methods


    
##### `Method at_date` {#hexagon.infrastructure.db.models.market.RawMBI.at_date}



    
> `def at_date(date_marche: datetime.date, dbsession: sqlalchemy.orm.session.Session) -> list[typing.Self]`





    
### Class `RawTMP` {#hexagon.infrastructure.db.models.market.RawTMP}



> `class RawTMP(**kwargs)`


Table intermédiaire pour stocker les taux publiés pour le TMP chez BAM


#### Attributes

**```id```** :&ensp;<code>int</code>
:   L'ID de l'Enregistrement


**```date_marche```** :&ensp;<code>date</code>
:   date de  valeur


**```code```** :&ensp;<code>str</code>
:   Code de l'indice TMP


**```taux_value```** :&ensp;<code>float</code>
:   le taux publié par la BAM


**```value```** :&ensp;<code>float</code>
:   La valeur du Taux


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)
* [hexagon.infrastructure.db.models.base_mixin.HistoriqueTableMixin](#hexagon.infrastructure.db.models.base_mixin.HistoriqueTableMixin)




    
#### Instance variables


    
##### Variable `code` {#hexagon.infrastructure.db.models.market.RawTMP.code}



    
##### Variable `date_marche` {#hexagon.infrastructure.db.models.market.RawTMP.date_marche}



    
##### Variable `id` {#hexagon.infrastructure.db.models.market.RawTMP.id}



    
##### Variable `taux_value` {#hexagon.infrastructure.db.models.market.RawTMP.taux_value}



    
##### Variable `value` {#hexagon.infrastructure.db.models.market.RawTMP.value}




    
#### Static methods


    
##### `Method at_date` {#hexagon.infrastructure.db.models.market.RawTMP.at_date}



    
> `def at_date(date_marche: datetime.date, dbsession: sqlalchemy.orm.session.Session) -> list[typing.Self]`





    
### Class `SCDFondsMarket` {#hexagon.infrastructure.db.models.market.SCDFondsMarket}



> `class SCDFondsMarket(**kwargs)`


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)
* [hexagon.infrastructure.db.models.base_mixin.RawTableMixin](#hexagon.infrastructure.db.models.base_mixin.RawTableMixin)




    
#### Instance variables


    
##### Variable `attribut` {#hexagon.infrastructure.db.models.market.SCDFondsMarket.attribut}



    
##### Variable `created_at` {#hexagon.infrastructure.db.models.market.SCDFondsMarket.created_at}



    
##### Variable `date_effet` {#hexagon.infrastructure.db.models.market.SCDFondsMarket.date_effet}



    
##### Variable `fonds` {#hexagon.infrastructure.db.models.market.SCDFondsMarket.fonds}



    
##### Variable `id` {#hexagon.infrastructure.db.models.market.SCDFondsMarket.id}



    
##### Variable `id_fonds` {#hexagon.infrastructure.db.models.market.SCDFondsMarket.id_fonds}



    
##### Variable `new_value` {#hexagon.infrastructure.db.models.market.SCDFondsMarket.new_value}



    
##### Variable `old_value` {#hexagon.infrastructure.db.models.market.SCDFondsMarket.old_value}





    
### Class `Secteur` {#hexagon.infrastructure.db.models.market.Secteur}



> `class Secteur(**kwargs)`


Modèle pour les secteurs du marché


#### Attributes

**```id```** :&ensp;<code>int</code>
:   L'ID du secteur


**```id_parent```** :&ensp;<code>int</code>
:   L'ID du secteur parent, permet d'hierarchiser les secteurs


**```code```** :&ensp;<code>str</code>
:   Code *UNIQUE* du Secteur


**```description```** :&ensp;<code>str</code>
:   Description du secteur


**```domaine```** :&ensp;<code>str</code>
:   Domaine du Secteur, par defaut FINANCE, permet une segmentation horizontale de la data


**```emetteurs```** :&ensp;<code>list\[[Emetteur](#hexagon.infrastructure.db.models.market.Emetteur "hexagon.infrastructure.db.models.market.Emetteur")]</code>
:   Liste des Emetteurs appartenant au secteur


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)
* [hexagon.infrastructure.db.models.base_mixin.ReferentielTableMixin](#hexagon.infrastructure.db.models.base_mixin.ReferentielTableMixin)




    
#### Instance variables


    
##### Variable `childs` {#hexagon.infrastructure.db.models.market.Secteur.childs}



    
##### Variable `code` {#hexagon.infrastructure.db.models.market.Secteur.code}



    
##### Variable `created_at` {#hexagon.infrastructure.db.models.market.Secteur.created_at}



    
##### Variable `description` {#hexagon.infrastructure.db.models.market.Secteur.description}



    
##### Variable `domaine` {#hexagon.infrastructure.db.models.market.Secteur.domaine}



    
##### Variable `emetteurs` {#hexagon.infrastructure.db.models.market.Secteur.emetteurs}



    
##### Variable `has_children` {#hexagon.infrastructure.db.models.market.Secteur.has_children}



    
##### Variable `id` {#hexagon.infrastructure.db.models.market.Secteur.id}



    
##### Variable `id_parent` {#hexagon.infrastructure.db.models.market.Secteur.id_parent}




    
#### Static methods


    
##### `Method by_code` {#hexagon.infrastructure.db.models.market.Secteur.by_code}



    
> `def by_code(code_secteur: str, dbsession: sqlalchemy.orm.session.Session) -> Self`


Cherche un Secteur par son code


###### Args

**```code_secteur```** :&ensp;<code>str</code>
:   Code du Secteur désiré



###### Returns

<code>[Secteur](#hexagon.infrastructure.db.models.market.Secteur "hexagon.infrastructure.db.models.market.Secteur")</code>
:   Instance Secteur ou None if NotFound!



    
##### `Method by_id` {#hexagon.infrastructure.db.models.market.Secteur.by_id}



    
> `def by_id(id_secteur: int, dbsession: sqlalchemy.orm.session.Session) -> Self`


Cherche un Secteur par son ID


###### Args

**```id_secteur```** :&ensp;<code>str</code>
:   L'ID du secteur



###### Returns

<code>[Secteur](#hexagon.infrastructure.db.models.market.Secteur "hexagon.infrastructure.db.models.market.Secteur")</code>
:   Instance Secteur ou None if NotFound!




    
#### Methods


    
##### Method `dump` {#hexagon.infrastructure.db.models.market.Secteur.dump}



    
> `def dump(self) -> str`




    
### Class `SplitFondsMarket` {#hexagon.infrastructure.db.models.market.SplitFondsMarket}



> `class SplitFondsMarket(**kwargs)`


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)
* [hexagon.infrastructure.db.models.base_mixin.RawTableMixin](#hexagon.infrastructure.db.models.base_mixin.RawTableMixin)




    
#### Instance variables


    
##### Variable `created_at` {#hexagon.infrastructure.db.models.market.SplitFondsMarket.created_at}



    
##### Variable `date_split` {#hexagon.infrastructure.db.models.market.SplitFondsMarket.date_split}



    
##### Variable `facteur_split` {#hexagon.infrastructure.db.models.market.SplitFondsMarket.facteur_split}



    
##### Variable `fonds` {#hexagon.infrastructure.db.models.market.SplitFondsMarket.fonds}



    
##### Variable `id` {#hexagon.infrastructure.db.models.market.SplitFondsMarket.id}



    
##### Variable `id_fonds` {#hexagon.infrastructure.db.models.market.SplitFondsMarket.id_fonds}



    
##### Variable `tuple_split` {#hexagon.infrastructure.db.models.market.SplitFondsMarket.tuple_split}

Retourne le facteur Split en forme de Tuple (Numerateur, Denominateur)



    
### Class `TitreMarket` {#hexagon.infrastructure.db.models.market.TitreMarket}



> `class TitreMarket(**kwargs)`


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)
* [hexagon.infrastructure.db.models.base_mixin.ReferentielTableMixin](#hexagon.infrastructure.db.models.base_mixin.ReferentielTableMixin)




    
#### Instance variables


    
##### Variable `categorie` {#hexagon.infrastructure.db.models.market.TitreMarket.categorie}



    
##### Variable `classe` {#hexagon.infrastructure.db.models.market.TitreMarket.classe}



    
##### Variable `code` {#hexagon.infrastructure.db.models.market.TitreMarket.code}



    
##### Variable `created_at` {#hexagon.infrastructure.db.models.market.TitreMarket.created_at}



    
##### Variable `description` {#hexagon.infrastructure.db.models.market.TitreMarket.description}



    
##### Variable `description_centralisateur` {#hexagon.infrastructure.db.models.market.TitreMarket.description_centralisateur}



    
##### Variable `description_emetteur` {#hexagon.infrastructure.db.models.market.TitreMarket.description_emetteur}



    
##### Variable `echeance` {#hexagon.infrastructure.db.models.market.TitreMarket.echeance}



    
##### Variable `emission` {#hexagon.infrastructure.db.models.market.TitreMarket.emission}



    
##### Variable `forme_detention` {#hexagon.infrastructure.db.models.market.TitreMarket.forme_detention}



    
##### Variable `id` {#hexagon.infrastructure.db.models.market.TitreMarket.id}



    
##### Variable `id_devise` {#hexagon.infrastructure.db.models.market.TitreMarket.id_devise}



    
##### Variable `id_emetteur` {#hexagon.infrastructure.db.models.market.TitreMarket.id_emetteur}



    
##### Variable `id_pays` {#hexagon.infrastructure.db.models.market.TitreMarket.id_pays}



    
##### Variable `isin` {#hexagon.infrastructure.db.models.market.TitreMarket.isin}



    
##### Variable `isin_centralisateur` {#hexagon.infrastructure.db.models.market.TitreMarket.isin_centralisateur}



    
##### Variable `isin_emetteur` {#hexagon.infrastructure.db.models.market.TitreMarket.isin_emetteur}



    
##### Variable `jouissance` {#hexagon.infrastructure.db.models.market.TitreMarket.jouissance}



    
##### Variable `nominal` {#hexagon.infrastructure.db.models.market.TitreMarket.nominal}



    
##### Variable `periodicite_coupon` {#hexagon.infrastructure.db.models.market.TitreMarket.periodicite_coupon}



    
##### Variable `qt_emise` {#hexagon.infrastructure.db.models.market.TitreMarket.qt_emise}



    
##### Variable `spread` {#hexagon.infrastructure.db.models.market.TitreMarket.spread}



    
##### Variable `taux_facial` {#hexagon.infrastructure.db.models.market.TitreMarket.taux_facial}



    
##### Variable `ticker` {#hexagon.infrastructure.db.models.market.TitreMarket.ticker}



    
##### Variable `type_coupon` {#hexagon.infrastructure.db.models.market.TitreMarket.type_coupon}







    
# Module `hexagon.infrastructure.db.models.opcvm` {#hexagon.infrastructure.db.models.opcvm}







    
## Classes


    
### Class `BenchmarkFonds` {#hexagon.infrastructure.db.models.opcvm.BenchmarkFonds}



> `class BenchmarkFonds(**kwargs)`


Associe les Benchmark avec le Fonds
Table conçue pour permettre le chainage des benchmark pour un Fonds Donné
tout en gardant l'historique des changements


#### Attributes

**```id```** :&ensp;<code>int</code>
:   L'ID de l'enregistrement


**```id_benchmark```** :&ensp;<code>int</code>
:   L'ID du Benchmark


**```id_fonds```** :&ensp;<code>int</code>
:   L'ID du Fonds


**```date_effet```** :&ensp;<code>date</code>
:   La date d'application du Benchmark


**```type_benchmark```** :&ensp;<code>str</code>
:   permet d'associer 2 benchmarks à un Fonds, defaut = 'COM'


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)
* [hexagon.infrastructure.db.models.base_mixin.RawTableMixin](#hexagon.infrastructure.db.models.base_mixin.RawTableMixin)




    
#### Instance variables


    
##### Variable `benchmark` {#hexagon.infrastructure.db.models.opcvm.BenchmarkFonds.benchmark}



    
##### Variable `date_effet` {#hexagon.infrastructure.db.models.opcvm.BenchmarkFonds.date_effet}



    
##### Variable `fonds` {#hexagon.infrastructure.db.models.opcvm.BenchmarkFonds.fonds}



    
##### Variable `id` {#hexagon.infrastructure.db.models.opcvm.BenchmarkFonds.id}



    
##### Variable `id_benchmark` {#hexagon.infrastructure.db.models.opcvm.BenchmarkFonds.id_benchmark}



    
##### Variable `id_fonds` {#hexagon.infrastructure.db.models.opcvm.BenchmarkFonds.id_fonds}



    
##### Variable `type_benchmark` {#hexagon.infrastructure.db.models.opcvm.BenchmarkFonds.type_benchmark}




    
#### Static methods


    
##### `Method by_fonds` {#hexagon.infrastructure.db.models.opcvm.BenchmarkFonds.by_fonds}



    
> `def by_fonds(id_fonds: int, *, type_benchmark: str = 'COM', dbsession: sqlalchemy.orm.session.Session) -> list[typing.Self]`





    
### Class `Echeancier` {#hexagon.infrastructure.db.models.opcvm.Echeancier}



> `class Echeancier(**kwargs)`


Modèle de données de l'échéancier


#### Attributes

**```id```** :&ensp;<code>int</code>
:   L'ID de l'enregistrement


**```id_titre```** :&ensp;<code>int</code>
:   L'ID du Titre


**```date_coupon```** :&ensp;<code>date</code>
:   Date de tombée du Coupon


**```amortissement```** :&ensp;<code>float</code>
:   Amortissement du capital (Dans le cas des titres amortissables)


**```coupon```** :&ensp;<code>float</code>
:   Valeur du Coupon en MAD


**```capital```** :&ensp;<code>float</code>
:   Capital Restant = Nominal dans le cas de l'INFINE


**```taux_facial```** :&ensp;<code>float</code>
:   Le taux facial du titre, taux du coupon.


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)
* [hexagon.infrastructure.db.models.base_mixin.RawTableMixin](#hexagon.infrastructure.db.models.base_mixin.RawTableMixin)




    
#### Instance variables


    
##### Variable `amortissement` {#hexagon.infrastructure.db.models.opcvm.Echeancier.amortissement}



    
##### Variable `capital` {#hexagon.infrastructure.db.models.opcvm.Echeancier.capital}



    
##### Variable `coupon` {#hexagon.infrastructure.db.models.opcvm.Echeancier.coupon}



    
##### Variable `date_coupon` {#hexagon.infrastructure.db.models.opcvm.Echeancier.date_coupon}



    
##### Variable `id` {#hexagon.infrastructure.db.models.opcvm.Echeancier.id}



    
##### Variable `id_titre` {#hexagon.infrastructure.db.models.opcvm.Echeancier.id_titre}



    
##### Variable `taux_facial` {#hexagon.infrastructure.db.models.opcvm.Echeancier.taux_facial}





    
### Class `EtatFonds` {#hexagon.infrastructure.db.models.opcvm.EtatFonds}



> `class EtatFonds(**kwargs)`


Modèle de données de l'état du Fonds


#### Attributes

**```id```** :&ensp;<code>int</code>
:   L'ID de l'enregistrement


**```id_fonds```** :&ensp;<code>int</code>
:   L'ID du Fonds


**```date_marche```** :&ensp;<code>datetime.date</code>
:   Date de valeur


**```vl```** :&ensp;<code>float</code>
:   VL du Fonds


**```actif_net```** :&ensp;<code>float</code>
:   Actif Net Avant S/R


**```actif_net_ap_sr```** :&ensp;<code>float</code>
:   Actif Net Après S/R


**```actif_brut```** :&ensp;<code>float</code>
:   Actif Brut Avant S/R


**```nombre_parts```** :&ensp;<code>float</code>
:   Nombre Parts Avant S/R


**```nombre_parts_ap_sr```** :&ensp;<code>float</code>
:   Nombre Parts Après S/R


**```banque```** :&ensp;<code>float</code>
:   Banque


**```agios```** :&ensp;<code>float</code>
:   Agios provisionnés sur la période


**```sensibilite```** :&ensp;<code>float</code>
:   Sensibilite du Fonds (pour la complétude des données du Fonds)


**```duration```** :&ensp;<code>float</code>
:   Duration du Fonds (pour la complétude des données du Fonds)


**```fdg```** :&ensp;<code>float</code>
:   Les FDG déduits du Fonds


**```provision_agios```** :&ensp;<code>float</code>
:   Provision Agios


**```provision_fdg```** :&ensp;<code>float</code>
:   Provision FDG


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)
* [hexagon.infrastructure.db.models.base_mixin.HistoriqueTableMixin](#hexagon.infrastructure.db.models.base_mixin.HistoriqueTableMixin)




    
#### Instance variables


    
##### Variable `actif_brut` {#hexagon.infrastructure.db.models.opcvm.EtatFonds.actif_brut}



    
##### Variable `actif_net` {#hexagon.infrastructure.db.models.opcvm.EtatFonds.actif_net}



    
##### Variable `actif_net_ap_sr` {#hexagon.infrastructure.db.models.opcvm.EtatFonds.actif_net_ap_sr}



    
##### Variable `agios` {#hexagon.infrastructure.db.models.opcvm.EtatFonds.agios}



    
##### Variable `banque` {#hexagon.infrastructure.db.models.opcvm.EtatFonds.banque}



    
##### Variable `date_marche` {#hexagon.infrastructure.db.models.opcvm.EtatFonds.date_marche}



    
##### Variable `duration` {#hexagon.infrastructure.db.models.opcvm.EtatFonds.duration}



    
##### Variable `fdg` {#hexagon.infrastructure.db.models.opcvm.EtatFonds.fdg}



    
##### Variable `fonds` {#hexagon.infrastructure.db.models.opcvm.EtatFonds.fonds}



    
##### Variable `id` {#hexagon.infrastructure.db.models.opcvm.EtatFonds.id}



    
##### Variable `id_fonds` {#hexagon.infrastructure.db.models.opcvm.EtatFonds.id_fonds}



    
##### Variable `nombre_parts` {#hexagon.infrastructure.db.models.opcvm.EtatFonds.nombre_parts}



    
##### Variable `nombre_parts_ap_sr` {#hexagon.infrastructure.db.models.opcvm.EtatFonds.nombre_parts_ap_sr}



    
##### Variable `provision_agios` {#hexagon.infrastructure.db.models.opcvm.EtatFonds.provision_agios}



    
##### Variable `provision_fdg` {#hexagon.infrastructure.db.models.opcvm.EtatFonds.provision_fdg}



    
##### Variable `sensibilite` {#hexagon.infrastructure.db.models.opcvm.EtatFonds.sensibilite}



    
##### Variable `vl` {#hexagon.infrastructure.db.models.opcvm.EtatFonds.vl}




    
#### Static methods


    
##### `Method at_date` {#hexagon.infrastructure.db.models.opcvm.EtatFonds.at_date}



    
> `def at_date(date_marche: datetime.date, *, code_fonds: str = None, dbsession: sqlalchemy.orm.session.Session) -> list[typing.Self]`


Retourne depuis la BDD l'ensemble des Fonds valorisés à une date donnée
Méthode conçue principalement pour l'inspection des données.


    
### Class `EtatTitre` {#hexagon.infrastructure.db.models.opcvm.EtatTitre}



> `class EtatTitre(**kwargs)`


Modèle de persistence des valeurs des titres à une date donnée

* Unique(date_marche, id_titre) *


#### Attributes

**```id```** :&ensp;<code>int</code>
:   L'ID de l'enregistrement au niveau de la BDD


**```id_titre```** :&ensp;<code>int</code>
:   L'ID du Titre


**```date_marche```** :&ensp;<code>date</code>
:   La date de valeur de la valorisation


**```date_reference```** :&ensp;<code>date</code>
:   same thing que date marche, sauf pour les titres valorisés à des fréquences différentes. ex: Trimestriellement


**```taux_courbe```** :&ensp;<code>float</code>
:   Le taux sur la courbe correspondant à la maturité résiduelle


**```taux_valorisation```** :&ensp;<code>float</code>
:   Le coupon couru d'un titre obligataire


**```spread_valorisation```** :&ensp;<code>float</code>
:   Le spread de valorisation d'un titre obligataire


**```coupon_couru```** :&ensp;<code>float</code>
:   Le coupon couru d'un titre obligataire


**```cours```** :&ensp;<code>float</code>
:   Le cours de valorisation à la date 'date_marche'


**```sensibilite```** :&ensp;<code>float</code>
:   La sensibilite d'un titre obligataire


**```convexite```** :&ensp;<code>float</code>
:   La convexite d'un titre obligataire


**```duration```** :&ensp;<code>float</code>
:   La duration d'un titre obligataire


**```devise_valorisation```** :&ensp;<code>str</code>
:   Devise en alpha 3


**```maturite_residuelle```** :&ensp;<code>int</code>
:   Maturité residuelle en jours



**```titre```** :&ensp;<code>[Titre](#hexagon.infrastructure.db.models.opcvm.Titre "hexagon.infrastructure.db.models.opcvm.Titre")</code>
:   Instance du titre associé à la valeur


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)
* [hexagon.infrastructure.db.models.base_mixin.HistoriqueTableMixin](#hexagon.infrastructure.db.models.base_mixin.HistoriqueTableMixin)




    
#### Instance variables


    
##### Variable `convexite` {#hexagon.infrastructure.db.models.opcvm.EtatTitre.convexite}



    
##### Variable `coupon_couru` {#hexagon.infrastructure.db.models.opcvm.EtatTitre.coupon_couru}



    
##### Variable `cours` {#hexagon.infrastructure.db.models.opcvm.EtatTitre.cours}



    
##### Variable `date_marche` {#hexagon.infrastructure.db.models.opcvm.EtatTitre.date_marche}



    
##### Variable `date_reference` {#hexagon.infrastructure.db.models.opcvm.EtatTitre.date_reference}



    
##### Variable `devise_valorisation` {#hexagon.infrastructure.db.models.opcvm.EtatTitre.devise_valorisation}



    
##### Variable `duration` {#hexagon.infrastructure.db.models.opcvm.EtatTitre.duration}



    
##### Variable `id` {#hexagon.infrastructure.db.models.opcvm.EtatTitre.id}



    
##### Variable `id_titre` {#hexagon.infrastructure.db.models.opcvm.EtatTitre.id_titre}



    
##### Variable `maturite_residuelle` {#hexagon.infrastructure.db.models.opcvm.EtatTitre.maturite_residuelle}



    
##### Variable `sensibilite` {#hexagon.infrastructure.db.models.opcvm.EtatTitre.sensibilite}



    
##### Variable `spread_valorisation` {#hexagon.infrastructure.db.models.opcvm.EtatTitre.spread_valorisation}



    
##### Variable `taux_courbe` {#hexagon.infrastructure.db.models.opcvm.EtatTitre.taux_courbe}



    
##### Variable `taux_valorisation` {#hexagon.infrastructure.db.models.opcvm.EtatTitre.taux_valorisation}



    
##### Variable `titre` {#hexagon.infrastructure.db.models.opcvm.EtatTitre.titre}




    
#### Static methods


    
##### `Method by_date` {#hexagon.infrastructure.db.models.opcvm.EtatTitre.by_date}



    
> `def by_date(date_marche: datetime.date, dbsession: sqlalchemy.orm.session.Session) -> list[typing.Self]`


Retourne depuis la BDD l'ensemble des titres valorisés à une date
Méthode conçue principalement pour l'inspection des données.


    
### Class `Fonds` {#hexagon.infrastructure.db.models.opcvm.Fonds}



> `class Fonds(**kwargs)`


Modèle de donnée des Fonds


#### Attributes

**```id```** :&ensp;<code>int</code>
:   L'ID du Fonds


**```code```** :&ensp;<code>str</code>
:   Code du Fonds (MANAR Like)


**```code_isin```** :&ensp;<code>str</code>
:   Code ISIN du Fonds


**```description```** :&ensp;<code>str</code>
:   Description du Fonds


**```categorie```** :&ensp;<code>str</code>
:   categorie du Fonds (OMLT, ACT, ....)


**```periodicite```** :&ensp;<code>str</code>
:   periodicite du Fonds (Hebdo / Quotidien)


**```forme_juridique```** :&ensp;<code>str</code>
:   FCP / SICAV


**```type_gestion```** :&ensp;<code>str</code>
:   'D' pour dédié et 'P' pour Grand Publique


**```date_creation```** :&ensp;<code>datetime.date</code>
:   Date de création du Fonds


**```date_premiere_vl```** :&ensp;<code>datetime.date</code>
:   Date de première VL


**```date_premiere_souscription```** :&ensp;<code>datetime.date</code>
:   Date de première souscription


**```taux_fdg```** :&ensp;<code>float</code>
:   FDG de la SDG 'TTC'


**```taux_depositaire```** :&ensp;<code>float</code>
:   frais depositaire 'TTC'


**```taux_ammc```** :&ensp;<code>float</code>
:   frais AMMC 'TTC'


**```affectation_resultat```** :&ensp;<code>str</code>
:   'CAPI' - 'MIXTE' - 'DISTR'


**```code_sesam```** :&ensp;<code>str</code>
:   Code de déclaration du Fonds sur la plateforme SESAM


**```code_agrement```** :&ensp;<code>str</code>
:   Code de l'agrément du Fonds


**```promoteur```** :&ensp;<code>str</code>
:   Le promoteur du Fonds (dans le cas d'un Fonds Dédié)


**```created_at```** :&ensp;<code>datetime.datetime</code>
:   Date de création de l'enregistrement


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)
* [hexagon.infrastructure.db.models.base_mixin.ReferentielTableMixin](#hexagon.infrastructure.db.models.base_mixin.ReferentielTableMixin)




    
#### Instance variables


    
##### Variable `affectation_resultat` {#hexagon.infrastructure.db.models.opcvm.Fonds.affectation_resultat}



    
##### Variable `categorie` {#hexagon.infrastructure.db.models.opcvm.Fonds.categorie}



    
##### Variable `code` {#hexagon.infrastructure.db.models.opcvm.Fonds.code}



    
##### Variable `code_agrement` {#hexagon.infrastructure.db.models.opcvm.Fonds.code_agrement}



    
##### Variable `code_isin` {#hexagon.infrastructure.db.models.opcvm.Fonds.code_isin}



    
##### Variable `code_sesam` {#hexagon.infrastructure.db.models.opcvm.Fonds.code_sesam}



    
##### Variable `created_at` {#hexagon.infrastructure.db.models.opcvm.Fonds.created_at}



    
##### Variable `date_creation` {#hexagon.infrastructure.db.models.opcvm.Fonds.date_creation}



    
##### Variable `date_premiere_souscription` {#hexagon.infrastructure.db.models.opcvm.Fonds.date_premiere_souscription}



    
##### Variable `date_premiere_vl` {#hexagon.infrastructure.db.models.opcvm.Fonds.date_premiere_vl}



    
##### Variable `description` {#hexagon.infrastructure.db.models.opcvm.Fonds.description}



    
##### Variable `etat` {#hexagon.infrastructure.db.models.opcvm.Fonds.etat}



    
##### Variable `forme_juridique` {#hexagon.infrastructure.db.models.opcvm.Fonds.forme_juridique}



    
##### Variable `id` {#hexagon.infrastructure.db.models.opcvm.Fonds.id}



    
##### Variable `periodicite` {#hexagon.infrastructure.db.models.opcvm.Fonds.periodicite}



    
##### Variable `promoteur` {#hexagon.infrastructure.db.models.opcvm.Fonds.promoteur}



    
##### Variable `stock` {#hexagon.infrastructure.db.models.opcvm.Fonds.stock}



    
##### Variable `taux_ammc` {#hexagon.infrastructure.db.models.opcvm.Fonds.taux_ammc}



    
##### Variable `taux_depositaire` {#hexagon.infrastructure.db.models.opcvm.Fonds.taux_depositaire}



    
##### Variable `taux_fdg` {#hexagon.infrastructure.db.models.opcvm.Fonds.taux_fdg}



    
##### Variable `type_gestion` {#hexagon.infrastructure.db.models.opcvm.Fonds.type_gestion}




    
#### Static methods


    
##### `Method by_categorie` {#hexagon.infrastructure.db.models.opcvm.Fonds.by_categorie}



    
> `def by_categorie(categorie: str, dbsession: sqlalchemy.orm.session.Session) -> list[typing.Self]`


Cherche les Fonds par leur catégorie


###### Args

**```categorie```** :&ensp;<code>str</code>
:   La catégorie des Fonds



###### Returns

<code>list\[[Fonds](#hexagon.infrastructure.db.models.opcvm.Fonds "hexagon.infrastructure.db.models.opcvm.Fonds")]</code>
:   Si pas trouvé returns []



    
##### `Method by_code` {#hexagon.infrastructure.db.models.opcvm.Fonds.by_code}



    
> `def by_code(code: str, dbsession: sqlalchemy.orm.session.Session) -> Self`


Cherche un Fonds par son code *unique*


###### Args

**```code```** :&ensp;<code>str</code>
:   Le code du Fonds ex: FCP_OPP



###### Returns

<code>[Fonds](#hexagon.infrastructure.db.models.opcvm.Fonds "hexagon.infrastructure.db.models.opcvm.Fonds")</code>
:   Si pas trouvé returns None



    
##### `Method by_code_isin` {#hexagon.infrastructure.db.models.opcvm.Fonds.by_code_isin}



    
> `def by_code_isin(isin: str, dbsession: sqlalchemy.orm.session.Session) -> Self`


Cherche un Fonds par le code ISIN


###### Args

**```isin```** :&ensp;<code>str</code>
:   Le code ISIN, ex: MA0001234567



###### Returns

<code>[Fonds](#hexagon.infrastructure.db.models.opcvm.Fonds "hexagon.infrastructure.db.models.opcvm.Fonds")</code>
:   Si pas trouvé returns None



    
##### `Method by_periodicite` {#hexagon.infrastructure.db.models.opcvm.Fonds.by_periodicite}



    
> `def by_periodicite(periodicite: str, dbsession: sqlalchemy.orm.session.Session) -> list[typing.Self]`




    
##### `Method by_promoteur` {#hexagon.infrastructure.db.models.opcvm.Fonds.by_promoteur}



    
> `def by_promoteur(*, promoteur: str = None, dbsession: sqlalchemy.orm.session.Session) -> list[typing.Self]`


Cherche les Fonds par leur promoteur


###### Args

**```promoteur```** :&ensp;<code>str</code>
:   Le promoteur des Fonds, if None, retourne les Fonds sans promoteur



###### Returns

<code>list\[[Fonds](#hexagon.infrastructure.db.models.opcvm.Fonds "hexagon.infrastructure.db.models.opcvm.Fonds")]</code>
:   Si pas trouvé returns []



    
##### `Method by_type_gestion` {#hexagon.infrastructure.db.models.opcvm.Fonds.by_type_gestion}



    
> `def by_type_gestion(type_gestion: str, dbsession: sqlalchemy.orm.session.Session) -> list[typing.Self]`


Cherche les Fonds par leur promoteur


###### Args

**```promoteur```** :&ensp;<code>str</code>
:   Le promoteur des Fonds, if None, retourne les Fonds sans promoteur



###### Returns

<code>list\[[Fonds](#hexagon.infrastructure.db.models.opcvm.Fonds "hexagon.infrastructure.db.models.opcvm.Fonds")]</code>
:   Si pas trouvé returns []




    
### Class `InventaireFonds` {#hexagon.infrastructure.db.models.opcvm.InventaireFonds}



> `class InventaireFonds(**kwargs)`


Table associative des Titres et des Fonds par date et par quantité


#### Attributes

**```id```** :&ensp;<code>int</code>
:   L'ID de l'enregistrement


**```id_fonds```** :&ensp;<code>int</code>
:   L'ID du FONDS


**```id_titre```** :&ensp;<code>int</code>
:   L'ID du TITRE


**```date_marche```** :&ensp;<code>date</code>
:   date de marché ou date d'inventaire


**```quantite```** :&ensp;<code>float</code>
:   Quantité du titre dans l'inventaire du Fonds



**```titre```** :&ensp;<code>[Titre](#hexagon.infrastructure.db.models.opcvm.Titre "hexagon.infrastructure.db.models.opcvm.Titre")</code>
:   Instance du titre associé à l'inventaire'


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)
* [hexagon.infrastructure.db.models.base_mixin.RawTableMixin](#hexagon.infrastructure.db.models.base_mixin.RawTableMixin)




    
#### Instance variables


    
##### Variable `date_marche` {#hexagon.infrastructure.db.models.opcvm.InventaireFonds.date_marche}



    
##### Variable `fonds` {#hexagon.infrastructure.db.models.opcvm.InventaireFonds.fonds}



    
##### Variable `id` {#hexagon.infrastructure.db.models.opcvm.InventaireFonds.id}



    
##### Variable `id_fonds` {#hexagon.infrastructure.db.models.opcvm.InventaireFonds.id_fonds}



    
##### Variable `id_titre` {#hexagon.infrastructure.db.models.opcvm.InventaireFonds.id_titre}



    
##### Variable `pdr_brut` {#hexagon.infrastructure.db.models.opcvm.InventaireFonds.pdr_brut}



    
##### Variable `pdr_net` {#hexagon.infrastructure.db.models.opcvm.InventaireFonds.pdr_net}



    
##### Variable `quantite` {#hexagon.infrastructure.db.models.opcvm.InventaireFonds.quantite}



    
##### Variable `titre` {#hexagon.infrastructure.db.models.opcvm.InventaireFonds.titre}




    
#### Static methods


    
##### `Method at_date` {#hexagon.infrastructure.db.models.opcvm.InventaireFonds.at_date}



    
> `def at_date(date_marche: datetime.date, *, code_fonds: str = None, dbsession: sqlalchemy.orm.session.Session) -> list[typing.Self]`


Retourne depuis la BDD l'inventaire disponible à une date donnée
Méthode conçue principalement pour faciliter l'accès à l'information et
aussi l'inspection des données.


    
### Class `Operation` {#hexagon.infrastructure.db.models.opcvm.Operation}



> `class Operation(**kwargs)`


Modèle de donnée des Opérations


#### Attributes

**```id```** :&ensp;<code>int</code>
:   L'ID de l'opération (Identique au numéro d'OP de MANAR)


**```id_fonds```** :&ensp;<code>int</code>
:   L'ID du Fonds


**```id_titre```** :&ensp;<code>int</code>
:   L'ID du Titre


**```poste_operation```** :&ensp;<code>str</code>
:   Poste de l'opération 'Foreign Key'


**```date_operation```** :&ensp;<code>date</code>
:   Date de l'opération


**```contrepartie```** :&ensp;<code>str</code>
:   Contrepartie de l'opération


**```coupon_couru_unitaire```** :&ensp;<code>float</code>
:   Coupon couru du sous-jacent (dans le cas des titres obligataires)


**```quantite```** :&ensp;<code>float</code>
:   Quantité des titres de l'opération


**```cours_unitaire```** :&ensp;<code>float</code>
:   Cours unitaire du sous-jacent


**```cours_operation_brut```** :&ensp;<code>float</code>
:   Montant brut Global de l'opération


**```cours_operation_net```** :&ensp;<code>float</code>
:   Montant net Global de l'opération


**```date_fin```** :&ensp;<code>date</code>
:   Date de fin de l'opération


**```date_rl```** :&ensp;<code>date</code>
:   Date de R/L de l'opération


**```depositaire```** :&ensp;<code>str</code>
:   Dépositaire de l'opération


**```devise_reglement```** :&ensp;<code>str</code>
:   Devise règlement de l'opération


**```duree```** :&ensp;<code>int</code>
:   Nombre de jours de l'opération (cas des opération temporaires)


**```frais_operation```** :&ensp;<code>float</code>
:   Frais de l'opération


**```interets```** :&ensp;<code>float</code>
:   Interets de l'opération (REPO/RREPO)


**```intermediaire```** :&ensp;<code>str</code>
:   Intermédiaire de l'opération (selon l'opération)


**```pmv_brut```** :&ensp;<code>float</code>
:   PMV brut des frais d'opération (lors de la vente)


**```pmv_net```** :&ensp;<code>float</code>
:   PMV net des frais d'opération (lors de la vente)


**```ppc```** :&ensp;<code>float</code>
:   Prix Pied de coupon (Clean Price)


**```spread_operation```** :&ensp;<code>float</code>
:   Spread de l'opération (Différence entre le taux de valo selon la courbe et le taux de l'opération/négoce)


**```taux_negoce```** :&ensp;<code>float</code>
:   Taux de négociation (Taux d'Achat/Vente)


**```taux_placement```** :&ensp;<code>float</code>
:   Taux de placement (REPO/RREPO)



**```titre```** :&ensp;<code>[Titre](#hexagon.infrastructure.db.models.opcvm.Titre "hexagon.infrastructure.db.models.opcvm.Titre")</code>
:   Instance du Titre de l'opération *relationship*


**```fonds```** :&ensp;<code>[Fonds](#hexagon.infrastructure.db.models.opcvm.Fonds "hexagon.infrastructure.db.models.opcvm.Fonds")</code>
:   Instance du Fonds de l'opération *relationship*


**```poste```** :&ensp;<code>[PosteOperation](#hexagon.infrastructure.db.models.opcvm.PosteOperation "hexagon.infrastructure.db.models.opcvm.PosteOperation")</code>
:   Instance du Poste Operation de l'opération *relationship*


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)
* [hexagon.infrastructure.db.models.base_mixin.HistoriqueTableMixin](#hexagon.infrastructure.db.models.base_mixin.HistoriqueTableMixin)




    
#### Instance variables


    
##### Variable `contrepartie` {#hexagon.infrastructure.db.models.opcvm.Operation.contrepartie}



    
##### Variable `coupon_couru_unitaire` {#hexagon.infrastructure.db.models.opcvm.Operation.coupon_couru_unitaire}



    
##### Variable `cours_devise` {#hexagon.infrastructure.db.models.opcvm.Operation.cours_devise}



    
##### Variable `cours_operation_brut` {#hexagon.infrastructure.db.models.opcvm.Operation.cours_operation_brut}



    
##### Variable `cours_operation_net` {#hexagon.infrastructure.db.models.opcvm.Operation.cours_operation_net}



    
##### Variable `cours_unitaire` {#hexagon.infrastructure.db.models.opcvm.Operation.cours_unitaire}



    
##### Variable `date_fin` {#hexagon.infrastructure.db.models.opcvm.Operation.date_fin}



    
##### Variable `date_operation` {#hexagon.infrastructure.db.models.opcvm.Operation.date_operation}



    
##### Variable `date_rl` {#hexagon.infrastructure.db.models.opcvm.Operation.date_rl}



    
##### Variable `depositaire` {#hexagon.infrastructure.db.models.opcvm.Operation.depositaire}



    
##### Variable `devise_reglement` {#hexagon.infrastructure.db.models.opcvm.Operation.devise_reglement}



    
##### Variable `duree` {#hexagon.infrastructure.db.models.opcvm.Operation.duree}



    
##### Variable `fonds` {#hexagon.infrastructure.db.models.opcvm.Operation.fonds}



    
##### Variable `frais_operation` {#hexagon.infrastructure.db.models.opcvm.Operation.frais_operation}



    
##### Variable `id` {#hexagon.infrastructure.db.models.opcvm.Operation.id}



    
##### Variable `id_fonds` {#hexagon.infrastructure.db.models.opcvm.Operation.id_fonds}



    
##### Variable `id_titre` {#hexagon.infrastructure.db.models.opcvm.Operation.id_titre}



    
##### Variable `interets` {#hexagon.infrastructure.db.models.opcvm.Operation.interets}



    
##### Variable `intermediaire` {#hexagon.infrastructure.db.models.opcvm.Operation.intermediaire}



    
##### Variable `pmv_brut` {#hexagon.infrastructure.db.models.opcvm.Operation.pmv_brut}



    
##### Variable `pmv_net` {#hexagon.infrastructure.db.models.opcvm.Operation.pmv_net}



    
##### Variable `poste` {#hexagon.infrastructure.db.models.opcvm.Operation.poste}



    
##### Variable `poste_operation` {#hexagon.infrastructure.db.models.opcvm.Operation.poste_operation}



    
##### Variable `ppc` {#hexagon.infrastructure.db.models.opcvm.Operation.ppc}



    
##### Variable `quantite` {#hexagon.infrastructure.db.models.opcvm.Operation.quantite}



    
##### Variable `spread_operation` {#hexagon.infrastructure.db.models.opcvm.Operation.spread_operation}



    
##### Variable `taux_negoce` {#hexagon.infrastructure.db.models.opcvm.Operation.taux_negoce}



    
##### Variable `taux_placement` {#hexagon.infrastructure.db.models.opcvm.Operation.taux_placement}



    
##### Variable `titre` {#hexagon.infrastructure.db.models.opcvm.Operation.titre}




    
#### Static methods


    
##### `Method at_date` {#hexagon.infrastructure.db.models.opcvm.Operation.at_date}



    
> `def at_date(date_operation: datetime.date, *, code_fonds: str = None, dbsession: sqlalchemy.orm.session.Session) -> list[typing.Self]`





    
### Class `OperationPassif` {#hexagon.infrastructure.db.models.opcvm.OperationPassif}



> `class OperationPassif(**kwargs)`


Les opérations du passif concernent les S/R et les distributions des Fonds


#### Attributes

**```id```** :&ensp;<code>int</code>
:   L'ID de l'enregistrement


**```id_fonds```** :&ensp;<code>int</code>
:   L'ID du Fonds


**```poste_operation```** :&ensp;<code>str</code>
:   Poste de l'opération


**```date_operation```** :&ensp;<code>date</code>
:   Date de l'opération


**```description```** :&ensp;<code>str</code>
:   Libellé de l'opération


**```devise_reglement```** :&ensp;<code>str</code>
:   Devise du règlement


**```montant_operation```** :&ensp;<code>float</code>
:   Montant de l'opération


**```tiers```** :&ensp;<code>str</code>
:   Le Tiers/Contrepartie de l'opération


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)
* [hexagon.infrastructure.db.models.base_mixin.HistoriqueTableMixin](#hexagon.infrastructure.db.models.base_mixin.HistoriqueTableMixin)




    
#### Instance variables


    
##### Variable `created_at` {#hexagon.infrastructure.db.models.opcvm.OperationPassif.created_at}



    
##### Variable `date_operation` {#hexagon.infrastructure.db.models.opcvm.OperationPassif.date_operation}



    
##### Variable `description` {#hexagon.infrastructure.db.models.opcvm.OperationPassif.description}



    
##### Variable `devise_reglement` {#hexagon.infrastructure.db.models.opcvm.OperationPassif.devise_reglement}



    
##### Variable `fonds` {#hexagon.infrastructure.db.models.opcvm.OperationPassif.fonds}



    
##### Variable `id` {#hexagon.infrastructure.db.models.opcvm.OperationPassif.id}



    
##### Variable `id_fonds` {#hexagon.infrastructure.db.models.opcvm.OperationPassif.id_fonds}



    
##### Variable `montant_operation` {#hexagon.infrastructure.db.models.opcvm.OperationPassif.montant_operation}



    
##### Variable `poste` {#hexagon.infrastructure.db.models.opcvm.OperationPassif.poste}



    
##### Variable `poste_operation` {#hexagon.infrastructure.db.models.opcvm.OperationPassif.poste_operation}



    
##### Variable `tiers` {#hexagon.infrastructure.db.models.opcvm.OperationPassif.tiers}




    
#### Static methods


    
##### `Method by_date` {#hexagon.infrastructure.db.models.opcvm.OperationPassif.by_date}



    
> `def by_date(date_operation: datetime.date, dbsession: sqlalchemy.orm.session.Session) -> list[typing.Self]`


Cherche les opérations à une date données


###### Args

**```date_marche```** :&ensp;<code>date</code>
:   Date d'opération



###### Returns

<code>list</code>
:   Liste des Opérations à la date 'date_marche'




    
### Class `PosteOperation` {#hexagon.infrastructure.db.models.opcvm.PosteOperation}



> `class PosteOperation(**kwargs)`


Table des Postes Opérations pour les tables operation et operation_passif


#### Attributes

**```code```** :&ensp;<code>str</code>
:   Code *UNIQUE* du poste de l'opération


**```classe_poste```** :&ensp;<code>str</code>
:   Classe du Poste (permet de grouper des Postes)


**```description```** :&ensp;<code>str</code>
:   Description du Poste


**```marche```** :&ensp;<code>str</code>
:   Marché du poste opération (pour les ops Marché)


**```precision```** :&ensp;<code>int</code>
:   Nombre de chiffres significatifs (Importé depuis MANAR pour des besoins internes)


**```sens_passif```** :&ensp;<code>int</code>
:   Sens d'impact (-1, 1, 0) de l'opération sur le passif d'un Fonds


**```sens_stock```** :&ensp;<code>int</code>
:   Sens d'impact (-1, 1, 0) de l'opération sur le stock/inventaire d'un Fonds



**```sens_impact```** :&ensp;<code>int</code>
:   Si 1 impact positif, Si -1 impact Négatif, Si 0 aucun impact


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)
* [hexagon.infrastructure.db.models.base_mixin.ReferentielTableMixin](#hexagon.infrastructure.db.models.base_mixin.ReferentielTableMixin)




    
#### Instance variables


    
##### Variable `classe_poste` {#hexagon.infrastructure.db.models.opcvm.PosteOperation.classe_poste}



    
##### Variable `code` {#hexagon.infrastructure.db.models.opcvm.PosteOperation.code}



    
##### Variable `description` {#hexagon.infrastructure.db.models.opcvm.PosteOperation.description}



    
##### Variable `marche` {#hexagon.infrastructure.db.models.opcvm.PosteOperation.marche}



    
##### Variable `precision` {#hexagon.infrastructure.db.models.opcvm.PosteOperation.precision}



    
##### Variable `sens_passif` {#hexagon.infrastructure.db.models.opcvm.PosteOperation.sens_passif}



    
##### Variable `sens_stock` {#hexagon.infrastructure.db.models.opcvm.PosteOperation.sens_stock}




    
#### Static methods


    
##### `Method by_code` {#hexagon.infrastructure.db.models.opcvm.PosteOperation.by_code}



    
> `def by_code(poste_operation: str, dbsession: sqlalchemy.orm.session.Session) -> Self`


Cherche un Poste opération par code *Clé Primaire*


###### Args

**```poste_operation```** :&ensp;<code>str</code>
:   Poste Opération



###### Returns

<code>[PosteOperation](#hexagon.infrastructure.db.models.opcvm.PosteOperation "hexagon.infrastructure.db.models.opcvm.PosteOperation")</code>
:   Instance PosteOperation




    
### Class `Titre` {#hexagon.infrastructure.db.models.opcvm.Titre}



> `class Titre(**kwargs)`


Modèle de donnée des titres


#### Attributes

**```id```** :&ensp;<code>int</code>
:   L'ID du Titre dans la BDD. *UNIQUE*


**```id_devise```** :&ensp;<code>int</code>
:   ID de la devise du titre


**```id_emetteur```** :&ensp;<code>int</code>
:   ID de l'émetteur du titre


**```id_pays```** :&ensp;<code>int</code>
:   ID du Pays d'émission du Titre


**```code```** :&ensp;<code>str</code>
:   code *UNIQUE* 6 chiffres, déduit depuis le code ISIN


**```isin```** :&ensp;<code>str</code>
:   Code *UNIQUE* ISIN du Titre, MAOOO2013449


**```description```** :&ensp;<code>str</code>
:   Description ou libelle du titre


**```nominal```** :&ensp;<code>float</code>
:   Le nominal du Titre


**```taux_facial```** :&ensp;<code>float</code>
:   Le taux Facial dans le cas des titres Obligs


**```emission```** :&ensp;<code>date</code>
:   Date d'émission du Titre


**```jouissance```** :&ensp;<code>date</code>
:   Date de jouissance du Titre


**```echeance```** :&ensp;<code>date</code>
:   Date d'échéancedu Titre


**```revision```** :&ensp;<code>date</code>
:   Date de révision du Titre, Doit être revisable


**```categorie```** :&ensp;<code>str</code>
:   Categorie du titre ou sous-classe


**```classe```** :&ensp;<code>str</code>
:   Classe du titre


**```cote```** :&ensp;<code>bool</code>
:   Si le titre est Coté, le cas des titres ACTIONS


**```garanti```** :&ensp;<code>bool</code>
:   Si le titre est garanti, cas des Offices (ONCF, ONDA, FEC)


**```methode_valo```** :&ensp;<code>str</code>
:   Voir la doc du Pricer


**```type_coupon```** :&ensp;<code>str</code>
:   Fix-Revisable


**```periodicite```** :&ensp;<code>str</code>
:   Periodicite du Titre. IN ('A', 'S', T', 'M')


**```qt_emise```** :&ensp;<code>float</code>
:   Quantité émise du titre


**```spread```** :&ensp;<code>float</code>
:   Le spread à l'émission


**```ticker```** :&ensp;<code>str</code>
:   Mnémonique pour les titres Actions


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)
* [hexagon.infrastructure.db.models.base_mixin.ReferentielTableMixin](#hexagon.infrastructure.db.models.base_mixin.ReferentielTableMixin)




    
#### Instance variables


    
##### Variable `categorie` {#hexagon.infrastructure.db.models.opcvm.Titre.categorie}



    
##### Variable `classe` {#hexagon.infrastructure.db.models.opcvm.Titre.classe}



    
##### Variable `code` {#hexagon.infrastructure.db.models.opcvm.Titre.code}



    
##### Variable `cote` {#hexagon.infrastructure.db.models.opcvm.Titre.cote}



    
##### Variable `created_at` {#hexagon.infrastructure.db.models.opcvm.Titre.created_at}



    
##### Variable `description` {#hexagon.infrastructure.db.models.opcvm.Titre.description}



    
##### Variable `devise` {#hexagon.infrastructure.db.models.opcvm.Titre.devise}



    
##### Variable `echeance` {#hexagon.infrastructure.db.models.opcvm.Titre.echeance}



    
##### Variable `echeancier` {#hexagon.infrastructure.db.models.opcvm.Titre.echeancier}



    
##### Variable `emetteur` {#hexagon.infrastructure.db.models.opcvm.Titre.emetteur}



    
##### Variable `emission` {#hexagon.infrastructure.db.models.opcvm.Titre.emission}



    
##### Variable `etat` {#hexagon.infrastructure.db.models.opcvm.Titre.etat}



    
##### Variable `garanti` {#hexagon.infrastructure.db.models.opcvm.Titre.garanti}



    
##### Variable `id` {#hexagon.infrastructure.db.models.opcvm.Titre.id}



    
##### Variable `id_devise` {#hexagon.infrastructure.db.models.opcvm.Titre.id_devise}



    
##### Variable `id_emetteur` {#hexagon.infrastructure.db.models.opcvm.Titre.id_emetteur}



    
##### Variable `id_pays` {#hexagon.infrastructure.db.models.opcvm.Titre.id_pays}



    
##### Variable `isin` {#hexagon.infrastructure.db.models.opcvm.Titre.isin}



    
##### Variable `jouissance` {#hexagon.infrastructure.db.models.opcvm.Titre.jouissance}



    
##### Variable `maturite_initiale` {#hexagon.infrastructure.db.models.opcvm.Titre.maturite_initiale}

Retourne la maturite initiale du titre en jours
Si aucune des dates d'échéance ou d'émission n'est présente,
une exception (ValueError) est générée.


###### Returns

<code>int</code>
:   Maturité initiale en jours



###### Raises

<code>ValueError</code>
:   Si date d'emisison ou d'échéance non définie



    
##### Variable `methode_valo` {#hexagon.infrastructure.db.models.opcvm.Titre.methode_valo}



    
##### Variable `mode_remboursement` {#hexagon.infrastructure.db.models.opcvm.Titre.mode_remboursement}



    
##### Variable `nominal` {#hexagon.infrastructure.db.models.opcvm.Titre.nominal}



    
##### Variable `pays` {#hexagon.infrastructure.db.models.opcvm.Titre.pays}



    
##### Variable `periodicite` {#hexagon.infrastructure.db.models.opcvm.Titre.periodicite}



    
##### Variable `place_cotation` {#hexagon.infrastructure.db.models.opcvm.Titre.place_cotation}



    
##### Variable `qt_emise` {#hexagon.infrastructure.db.models.opcvm.Titre.qt_emise}



    
##### Variable `rang` {#hexagon.infrastructure.db.models.opcvm.Titre.rang}



    
##### Variable `revision` {#hexagon.infrastructure.db.models.opcvm.Titre.revision}



    
##### Variable `spread` {#hexagon.infrastructure.db.models.opcvm.Titre.spread}



    
##### Variable `taux_facial` {#hexagon.infrastructure.db.models.opcvm.Titre.taux_facial}



    
##### Variable `ticker` {#hexagon.infrastructure.db.models.opcvm.Titre.ticker}



    
##### Variable `type_coupon` {#hexagon.infrastructure.db.models.opcvm.Titre.type_coupon}




    
#### Static methods


    
##### `Method by_categorie` {#hexagon.infrastructure.db.models.opcvm.Titre.by_categorie}



    
> `def by_categorie(categorie: str, dbsession: sqlalchemy.orm.session.Session) -> list[typing.Self]`


Cherche les Titres par leur catégorie


###### Args

**```categorie```** :&ensp;<code>str</code>
:   La catégorie des Titres



###### Returns

<code>list\[[Titre](#hexagon.infrastructure.db.models.opcvm.Titre "hexagon.infrastructure.db.models.opcvm.Titre")]</code>
:   Si pas trouvé returns list()



    
##### `Method by_classe` {#hexagon.infrastructure.db.models.opcvm.Titre.by_classe}



    
> `def by_classe(classe: str, dbsession: sqlalchemy.orm.session.Session) -> list[typing.Self]`


Cherche les Titres par leur classe


###### Args

**```classe```** :&ensp;<code>str</code>
:   La classe des Titres



###### Returns

<code>list\[[Titre](#hexagon.infrastructure.db.models.opcvm.Titre "hexagon.infrastructure.db.models.opcvm.Titre")]</code>
:   Si pas trouvé returns list()



    
##### `Method by_code` {#hexagon.infrastructure.db.models.opcvm.Titre.by_code}



    
> `def by_code(code: str, dbsession: sqlalchemy.orm.session.Session) -> Self`


Cherche un Titre par son code


###### Args

**```code```** :&ensp;<code>str</code>
:   Le code du titre



###### Returns

<code>[Titre](#hexagon.infrastructure.db.models.opcvm.Titre "hexagon.infrastructure.db.models.opcvm.Titre")</code>
:   Si pas trouvé returns None



    
##### `Method by_isin` {#hexagon.infrastructure.db.models.opcvm.Titre.by_isin}



    
> `def by_isin(isin: str, dbsession: sqlalchemy.orm.session.Session) -> Self`


Cherche un Titre par le code ISIN


###### Args

**```isin```** :&ensp;<code>str</code>
:   Le code ISIN, ex: MA0001234567



###### Returns

<code>[Titre](#hexagon.infrastructure.db.models.opcvm.Titre "hexagon.infrastructure.db.models.opcvm.Titre")</code>
:   Si pas trouvé returns None




    
#### Methods


    
##### Method `maturite_residuelle` {#hexagon.infrastructure.db.models.opcvm.Titre.maturite_residuelle}



    
> `def maturite_residuelle(self, date_marche: datetime.date) -> int`


Calcule la maturite residuelle d'un titre pour une date de valeur
Si aucune des dates d'échéance et de révision n'est présente,
une exception (ValueError) est générée.


###### Args

**```date_marche```** :&ensp;<code>datetime.date</code>
:   Date de valeur



###### Returns

<code>int</code>
:   maturité résiduelle en jours



###### Raises

<code>ValueError</code>
:   Si Echeance et révision manquantes





    
# Module `hexagon.infrastructure.db.models.reporting` {#hexagon.infrastructure.db.models.reporting}







    
## Classes


    
### Class `ReportingItem` {#hexagon.infrastructure.db.models.reporting.ReportingItem}



> `class ReportingItem(**kwargs)`


Table pour stocker des données à utiliser par le générateur de reporting (DataMapper)

'store' offre la possibilité de stocker les valeurs sur 2 niveaux d'hierarchisation
et aussi de stocker des valeurs binaires 'bytes_content', afin de tirer profit du méchanisme de sérialisation
qu'offre Python via (cPickle).

* Pour les Valeurs binaires, le protocole 4 de Pickle sera utilisé.


#### Attributes

**```id```** :&ensp;<code>int</code>
:   L'ID de la ligne, auto-increment.


**```scope```** :&ensp;<code>str</code>
:   Représente le second niveau d'hierarchisation


**```key```** :&ensp;<code>str</code>
:   La clé de la valeur


**```store_content```** :&ensp;<code>HSTORE</code>
:   Dictionnaire cle-valeur en Texte {Keys: Values[str]}


**```byte_content```** :&ensp;<code>BYTEA</code>
:   Permet de stocker un objet Binaire (généralement Pickle)


A simple constructor that allows initialization from kwargs.

Sets attributes on the constructed instance using the names and
values in <code>kwargs</code>.

Only keys that are present as
attributes of the instance's class are allowed. These could be,
for example, any mapped columns or relationships.


    
#### Ancestors (in MRO)

* [sqlalchemy.orm.decl_api.Base](#sqlalchemy.orm.decl_api.Base)
* [hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin](#hexagon.infrastructure.db.models.base_mixin.BaseEntityMixin)




    
#### Instance variables


    
##### Variable `bytes_content` {#hexagon.infrastructure.db.models.reporting.ReportingItem.bytes_content}



    
##### Variable `created_at` {#hexagon.infrastructure.db.models.reporting.ReportingItem.created_at}



    
##### Variable `id` {#hexagon.infrastructure.db.models.reporting.ReportingItem.id}



    
##### Variable `key` {#hexagon.infrastructure.db.models.reporting.ReportingItem.key}



    
##### Variable `store` {#hexagon.infrastructure.db.models.reporting.ReportingItem.store}



    
##### Variable `store_content` {#hexagon.infrastructure.db.models.reporting.ReportingItem.store_content}



    
##### Variable `updated_at` {#hexagon.infrastructure.db.models.reporting.ReportingItem.updated_at}




    
#### Static methods


    
##### `Method delete_store_item` {#hexagon.infrastructure.db.models.reporting.ReportingItem.delete_store_item}



    
> `def delete_store_item(key: str, store: str, dbsession: sqlalchemy.orm.session.Session)`


Supprime un enregistrement de la Base en passant le "store" et "key"
Le couple (store, key) permet d'identifier un seul Enregistrement


###### Args

**```key```** :&ensp;<code>str</code>
:   Clé d'identification de l'item


**```store```** :&ensp;<code>str</code>
:   Le store pour localiser l'item



###### Returns

<code>[ReportingItem](#hexagon.infrastructure.db.models.reporting.ReportingItem "hexagon.infrastructure.db.models.reporting.ReportingItem")</code>
:   L'instance supprimée rertournée par sqlalchemy, l'instance est marquée 'deleted'



###### Raises

<code>ValueError</code>
:   Si l'element à supprimer n'existe pas dans la BDD.



    
##### `Method get_many` {#hexagon.infrastructure.db.models.reporting.ReportingItem.get_many}



    
> `def get_many(store: str, dbsession: sqlalchemy.orm.session.Session) -> list`


Helper Function: Retourne tous les items dans un store


###### Args

**```store```** :&ensp;<code>str</code>
:   Description



###### Returns

<code>list</code>
:   liste des ReportingItem du même store



    
##### `Method get_one` {#hexagon.infrastructure.db.models.reporting.ReportingItem.get_one}



    
> `def get_one(key: str, store: str, dbsession: sqlalchemy.orm.session.Session)`


Retourne une liste ou un element selon les filtres (store, key)
Si key est fourni, retourne une seule instance


###### Args

**```key```** :&ensp;<code>str</code>
:   Clé d'identification de l'item


**```store```** :&ensp;<code>str</code>
:   Le store de l'item



###### Returns

<code>[ReportingItem](#hexagon.infrastructure.db.models.reporting.ReportingItem "hexagon.infrastructure.db.models.reporting.ReportingItem")</code>
:   L'item correspondant au tuple (store, key)



    
##### `Method list_keys` {#hexagon.infrastructure.db.models.reporting.ReportingItem.list_keys}



    
> `def list_keys(*, store: str = None, dbsession: sqlalchemy.orm.session.Session) -> set`


Liste les keys définis dans un store, if None,
retourne tous les keys *Uniques* tout store confondu


###### Args

**```store```** :&ensp;<code>str</code>, default=<code>None</code>
:   store cible, if None retourne tous les keys uniques définis



###### Returns

<code>set</code>
:   set des keys uniques



    
##### `Method list_stores` {#hexagon.infrastructure.db.models.reporting.ReportingItem.list_stores}



    
> `def list_stores(dbsession: sqlalchemy.orm.session.Session) -> set`


Liste les stores dans la table app_item


###### Returns

<code>set</code>
:   set[str] des stores définis dans la table



    
##### `Method stringify_dict` {#hexagon.infrastructure.db.models.reporting.ReportingItem.stringify_dict}



    
> `def stringify_dict(dico: dict) -> dict`


Convertit Les clés et les valeurs en texte pour être exploité par HSTORE


###### Args

**```dico```** :&ensp;<code>dict</code>
:   Dict d'input



###### Returns

<code>dict</code>
:   dict[str: str] Le meme dict avec les valeurs en texte




    
#### Methods


    
##### Method `to_document` {#hexagon.infrastructure.db.models.reporting.ReportingItem.to_document}



    
> `def to_document(self, *, bytes_content: bool = False) -> dict`


Retourne la ligne sous forme d'un document linéarisé avec le contenu de store_content


###### Args

**```bytes_content```** :&ensp;<code>bool</code>, default=<code>False</code>
:   Si True retourne aussi le champs bytes_content



###### Returns

<code>dict</code>
:   Dictionnaire avec les clés (store, key)



    
##### Method `to_records` {#hexagon.infrastructure.db.models.reporting.ReportingItem.to_records}



    
> `def to_records(self, *, bytes_content: bool = False) -> list`






    
# Module `hexagon.infrastructure.db.models.views` {#hexagon.infrastructure.db.models.views}






    
## Functions


    
### Function `get_view` {#hexagon.infrastructure.db.models.views.get_view}



    
> `def get_view(engine: sqlalchemy.engine.base.Engine, view_name: str = None) -> sqlalchemy.sql.schema.Table`




    
### Function `list_vues` {#hexagon.infrastructure.db.models.views.list_vues}



    
> `def list_vues(metadata: sqlalchemy.sql.schema.MetaData, prefix: str = None) -> list[sqlalchemy.sql.schema.Table]`




    
### Function `reflect_tables` {#hexagon.infrastructure.db.models.views.reflect_tables}



    
> `def reflect_tables(engine: sqlalchemy.engine.base.Engine) -> sqlalchemy.sql.schema.MetaData`





    
## Classes


    
### Class `ViewLoaderError` {#hexagon.infrastructure.db.models.views.ViewLoaderError}



> `class ViewLoaderError(...)`


Exception du Loader des Vues


    
#### Ancestors (in MRO)

* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)








    
# Module `hexagon.infrastructure.db.models_utils` {#hexagon.infrastructure.db.models_utils}

Module de sélection, validation des Champs des Modèles.
Dorénavant sqlalchemy est le système deFacto de construction des requêtes.
A lot of functions must be written.
L'idée est de centraliser les fonctions de construction des requêtes
et de définir une interface standarde de consolidation des données et d'auto-génération des Champs
Via les labels.




    
## Functions


    
### Function `fields_model_select` {#hexagon.infrastructure.db.models_utils.fields_model_select}



    
> `def fields_model_select(model: sqlalchemy.orm.decl_api.DeclarativeMeta, fields: list[str]) -> operator.attrgetter`




    
### Function `fields_model_validate` {#hexagon.infrastructure.db.models_utils.fields_model_validate}



    
> `def fields_model_validate(model: sqlalchemy.orm.decl_api.DeclarativeMeta, fields: list[str]) -> list[str]`


Valide les champs d'un modèle et retourne la même liste
Si tous les champs demandés sont valides


###### Args

**```model```** :&ensp;<code>BaseEntity</code>
:   Modèle Hexagon instance de db.BaseEntity


**```fields```** :&ensp;<code>list\[str]</code>
:   Liste des champs à valider



###### Returns

<code>list\[str]</code>
:   Liste des champs si Valide



###### Raises

<code>AttributeError</code>
:   Si des champs sont erronés






    
# Module `hexagon.infrastructure.exceptions` {#hexagon.infrastructure.exceptions}







    
## Classes


    
### Class `InfrastructureException` {#hexagon.infrastructure.exceptions.InfrastructureException}



> `class InfrastructureException(...)`


Classe de base pour le domaine de l'infrastructure


    
#### Ancestors (in MRO)

* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)


    
#### Descendants

* [hexagon.infrastructure.exceptions.QueryExecutorError](#hexagon.infrastructure.exceptions.QueryExecutorError)
* [hexagon.infrastructure.exceptions.TSError](#hexagon.infrastructure.exceptions.TSError)
* [hexagon.infrastructure.exceptions.TSRepositoryError](#hexagon.infrastructure.exceptions.TSRepositoryError)





    
### Class `QueryExecutorError` {#hexagon.infrastructure.exceptions.QueryExecutorError}



> `class QueryExecutorError(...)`


Exception pour L'exécution d'une requête.


    
#### Ancestors (in MRO)

* [hexagon.infrastructure.exceptions.InfrastructureException](#hexagon.infrastructure.exceptions.InfrastructureException)
* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)


    
#### Descendants

* [hexagon.infrastructure.exceptions.TextQueryExecutorError](#hexagon.infrastructure.exceptions.TextQueryExecutorError)





    
### Class `TSError` {#hexagon.infrastructure.exceptions.TSError}



> `class TSError(...)`


Exception pour le module de timeseries.py


    
#### Ancestors (in MRO)

* [hexagon.infrastructure.exceptions.InfrastructureException](#hexagon.infrastructure.exceptions.InfrastructureException)
* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)






    
### Class `TSRepositoryError` {#hexagon.infrastructure.exceptions.TSRepositoryError}



> `class TSRepositoryError(...)`


Exception pour le RepositoryBase pour les TimeSeries (Courbe, Mbi, Indice, etc, ...)


    
#### Ancestors (in MRO)

* [hexagon.infrastructure.exceptions.InfrastructureException](#hexagon.infrastructure.exceptions.InfrastructureException)
* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)






    
### Class `TextQueryExecutorError` {#hexagon.infrastructure.exceptions.TextQueryExecutorError}



> `class TextQueryExecutorError(...)`


Exception pour les exécutions textuelles via TextQueryExecutor


    
#### Ancestors (in MRO)

* [hexagon.infrastructure.exceptions.QueryExecutorError](#hexagon.infrastructure.exceptions.QueryExecutorError)
* [hexagon.infrastructure.exceptions.InfrastructureException](#hexagon.infrastructure.exceptions.InfrastructureException)
* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)








    
# Module `hexagon.infrastructure.query_executor` {#hexagon.infrastructure.query_executor}







    
## Classes


    
### Class `QueryExecutor` {#hexagon.infrastructure.query_executor.QueryExecutor}



> `class QueryExecutor(dbsession: sqlalchemy.orm.session.Session, logger: logging.Logger = None)`






    
#### Descendants

* [hexagon.calendrier.dao.CalendarDao](#hexagon.calendrier.dao.CalendarDao)
* [hexagon.core.repository.CoreEntityRepository](#hexagon.core.repository.CoreEntityRepository)
* [hexagon.infrastructure.db.models.data_items.DataItemManager](#hexagon.infrastructure.db.models.data_items.DataItemManager)
* [hexagon.infrastructure.tsrepository_base.TSRepositoryBase](#hexagon.infrastructure.tsrepository_base.TSRepositoryBase)
* [hexagon.performance.repository.OperationPassifRepository](#hexagon.performance.repository.OperationPassifRepository)
* [hexagon.performance.repository.PerformanceRepository](#hexagon.performance.repository.PerformanceRepository)
* [hexagon.repository.loader_inventaire.InventaireLoader](#hexagon.repository.loader_inventaire.InventaireLoader)
* [hexagon.repository.loader_operation.OperationLoader](#hexagon.repository.loader_operation.OperationLoader)
* [hexagon.repository.referentiel.ReferentielRepository](#hexagon.repository.referentiel.ReferentielRepository)
* [hexagon.repository.store.RepoBenchmark](#hexagon.repository.store.RepoBenchmark)
* [hexagon.repository.store.RepoFonds](#hexagon.repository.store.RepoFonds)
* [hexagon.repository.store.RepoIndice](#hexagon.repository.store.RepoIndice)
* [hexagon.repository.store.RepoOperation](#hexagon.repository.store.RepoOperation)
* [hexagon.repository.store.RepoPassif](#hexagon.repository.store.RepoPassif)
* [hexagon.repository.store.RepoTitre](#hexagon.repository.store.RepoTitre)




    
#### Static methods


    
##### `Method apply_date_filter` {#hexagon.infrastructure.query_executor.QueryExecutor.apply_date_filter}



    
> `def apply_date_filter(query: sqlalchemy.sql.selectable.Select, model, date_field: sqlalchemy.orm.attributes.InstrumentedAttribute | str, debut: datetime.date = None, fin: datetime.date = None, *, include: hexagon.utils.enumerations.Include | str = BOTH) -> sqlalchemy.sql.selectable.Select`


Applique un filtre d'intervalle de dates à une requete, l'avantage de cette fonction
est qu'elle fonctionne même en cas de jointures de plusieurs modèles,
vu qu'elle cible le modèle et la query concernée.

include prend les valeurs possibles (passer as keyword parameter):
    * 'BOTH': inclut les deux bornes --> [debut, fin]
    * 'NONE': exclut les deux bornes --> ]debut, fin[
    * 'LEFT': inclut la borne gauche --> [debut, fin[
    * 'RIGHT': inclut la borne droite --> ]debut, fin]


###### Args

**```query```** :&ensp;<code>Select</code>
:   Requete sqlalchemy


**```table```** :&ensp;<code>db.Model</code>
:   Modèle d'Hexagon (db.EtatTitre, db.Courbe, ...)


**```date_field```** :&ensp;<code>str</code>
:   Champs de date du Modèle à appliquer le filtre. ex: ('date_marche' pour db.EtaFonds)


**```debut```** :&ensp;<code>date</code>, optional
:   Date de début de l'intervalle, passer None pour un intervalle ouvert à gauche (left)


**```fin```** :&ensp;<code>date</code>, optional
:   Date de fin de l'intervalle, passer None pour un intervalle ouvert à droite (right)


**```include```** :&ensp;<code>str</code>, optional
:   Paramètre d'inclusion des dates. 'BOTH'=[a, b], 'NONE'= ]a, b[, 'RIGHT'= ]a, b], 'LEFT'= [a, b[



###### Returns

`Select`
:   Requete sqlalchemy avec le fitre des dates appliquées au modèle.



<code>N.B</code>
:   Le champs et le modèle passés en paramètre doivent être sélectionné



###### Raises

<code>TypeError</code>
:   Si le champs est invalide pour le Modèle (hasattr)


<code>ValueError</code>
:   Si la date de début > date de fin



    
##### `Method build_date_filter` {#hexagon.infrastructure.query_executor.QueryExecutor.build_date_filter}



    
> `def build_date_filter(orm_attr: sqlalchemy.orm.attributes.InstrumentedAttribute, debut: datetime.date = None, fin: datetime.date = None, include: hexagon.utils.enumerations.Include | str = BOTH) -> list[sqlalchemy.sql.elements.BinaryExpression]`




    
##### `Method labelize_select` {#hexagon.infrastructure.query_executor.QueryExecutor.labelize_select}



    
> `def labelize_select(query: sqlalchemy.sql.selectable.Select, label_mapper: dict[str, str]) -> sqlalchemy.sql.selectable.Select`




    
##### `Method orm_build_attrs_getter` {#hexagon.infrastructure.query_executor.QueryExecutor.orm_build_attrs_getter}



    
> `def orm_build_attrs_getter(orm_class: sqlalchemy.orm.decl_api.DeclarativeMeta, fields: collections.abc.Iterable[str]) -> operator.attrgetter`


Construit un attrgetter des instances ou Rows retournés par Sqlalchemy.
Permet d'extraire les données dans l'ordre demandé.

La fonction vérifie et valide les champs avant de construire le *attrgetter*


###### Args

**```orm_class```** :&ensp;<code>DeclarativeMeta</code>
:   Classe du modèle ORM


**```fields```** :&ensp;<code>Iterable\[str]</code>
:   Liste des champs désirés (case sensitive)



###### Returns

<code>op.attrgetter</code>
:   Fonction d'accès des champs validés selon l'ordre fourni.



    
##### `Method orm_select_instrumented_attributes` {#hexagon.infrastructure.query_executor.QueryExecutor.orm_select_instrumented_attributes}



    
> `def orm_select_instrumented_attributes(orm_class: sqlalchemy.orm.decl_api.DeclarativeMeta, fields: collections.abc.Iterable[str]) -> list[sqlalchemy.orm.attributes.InstrumentedAttribute]`


Construit une liste de selectables pour la classe ORM fournie.
Les selectables sont des Instrumented comme défini par sqlalchemy.


###### Args

**```orm_class```** :&ensp;<code>DeclarativeMeta</code>
:   Classe du modèle ORM


**```fields```** :&ensp;<code>Iterable\[str]</code>
:   Liste des champs désirés (case sensitive)



###### Returns

<code>list\[InstrumentedAttribute]</code>
:   Liste des attributs du modèle ORM



    
##### `Method orm_validate_fields` {#hexagon.infrastructure.query_executor.QueryExecutor.orm_validate_fields}



    
> `def orm_validate_fields(orm_class: sqlalchemy.orm.decl_api.DeclarativeMeta, fields: collections.abc.Iterable[str]) -> list[str]`


Retourne la même liste dans l'ordre fourni si tous les champs sont valides et appartiennent à la classe ORM


###### Args

**```orm_class```** :&ensp;<code>Declarative Class</code>
:   ORM model of the associated table


**```fields```** :&ensp;<code>list\[str]</code>
:   Liste des champs à valider



###### Returns

<code>list\[str]</code>
:   Liste des champs validés, l'ordre est préservé.



###### Raises

<code>AttributeError</code>
:   Si certains champs ne figurent pas au niveau du modèle.




    
#### Methods


    
##### Method `execute_delete` {#hexagon.infrastructure.query_executor.QueryExecutor.execute_delete}



    
> `def execute_delete(self, query: sqlalchemy.sql.dml.Delete) -> int`




    
##### Method `execute_insert` {#hexagon.infrastructure.query_executor.QueryExecutor.execute_insert}



    
> `def execute_insert(self, query: sqlalchemy.sql.dml.Insert) -> int`




    
##### Method `execute_query` {#hexagon.infrastructure.query_executor.QueryExecutor.execute_query}



    
> `def execute_query(self, query: sqlalchemy.sql.selectable.Select | sqlalchemy.sql.dml.Insert | sqlalchemy.sql.dml.Delete | sqlalchemy.sql.dml.Update, *, as_mapping: bool = False, as_scalar: bool = False, scalar_one: bool = False, as_result_set: bool = False) -> list[sqlalchemy.engine.row.Row | sqlalchemy.engine.row.RowMapping]`




    
##### Method `execute_select` {#hexagon.infrastructure.query_executor.QueryExecutor.execute_select}



    
> `def execute_select(self, query: sqlalchemy.sql.selectable.Select, *, as_mapping: bool = False, as_scalar: bool = False, scalar_one: bool = False, as_result_set: bool = False) -> list[sqlalchemy.engine.row.Row | sqlalchemy.engine.row.RowMapping]`


Execute un selectable en mode context manager (mode isolé) via la session self.dbsession

selon les args passés la requête sera exécutée différemment.
N.B: si as_scalar=True: retourne un CursorResult et la requête n'est pas exécutée en mode
ctxt-manager pour permettre l'exploitation des méthodes de CursorResult.


###### Args

**```query```** :&ensp;<code>Select</code>
:   Any Select object


**```as_mapping```** :&ensp;<code>bool, False</code>
:   Retourne list[RowMapping] comme résultat


**```as_scalar```** :&ensp;<code>bool, False</code>
:   Execute scalars() à la requête utilse pour db.DeclarativeModels


**```scalar_one```** :&ensp;<code>bool, False</code>
:   If one result value is expected, return result.first() i.e (func.count)


**```as_result_set```** :&ensp;<code>bool, False</code>
:   Retourne le CursorResult ou ChunckedIrerator selon la structure du select



###### Returns

`list[Row| RowMapping]: liste des Row objects ou RowMapping ou CursorResult`
:   &nbsp;

###### Raises

<code>QueryExecutorError</code>
:   if The query is not a selectable



    
##### Method `execute_update` {#hexagon.infrastructure.query_executor.QueryExecutor.execute_update}



    
> `def execute_update(self, query: sqlalchemy.sql.dml.Update) -> int`


Function Summary


###### Args

**```query```** :&ensp;<code>Update</code>
:   Description



###### Returns

<code>int</code>
:   Description



###### Raises

<code>QueryExecutorError</code>
:   Description





    
# Module `hexagon.infrastructure.text_query_executor` {#hexagon.infrastructure.text_query_executor}







    
## Classes


    
### Class `TextQueryExecutor` {#hexagon.infrastructure.text_query_executor.TextQueryExecutor}



> `class TextQueryExecutor(logger: logging.Logger = None, app_name: str = 'Hexagon-TextQueryExecutor')`


Cette classe servira de base pour l'exécution des Requêtes SQL textuelles (PLAIN SQL)

Afin d'assurer une séparation des responsabilités, la classe doit gérer la création
du DB Engine et des sessions qui en découlent

L'exécution est Read Only et ne doit en aucun cas exécuter les commandes suivantes:
    - INSERT, UPDATE et DELETE
    - CREATE, ALTER et DROP


#### Attributes

**```dbsession```** :&ensp;<code>session</code>
:   Session d'execution


**```logger```** :&ensp;<code>logging.logger</code>
:   Log handler pour l'exécution des requêtes









    
#### Methods


    
##### Method `execute_plain_query` {#hexagon.infrastructure.text_query_executor.TextQueryExecutor.execute_plain_query}



    
> `def execute_plain_query(self, plain_query: str, as_mapping: bool = False, as_result_set: bool = False) -> list[sqlalchemy.engine.row.Row | sqlalchemy.engine.row.RowMapping]`




    
##### Method `execute_text_clause` {#hexagon.infrastructure.text_query_executor.TextQueryExecutor.execute_text_clause}



    
> `def execute_text_clause(self, text_query: sqlalchemy.sql.elements.TextClause, as_mapping: bool = False, as_result_set: bool = False) -> list[sqlalchemy.engine.row.Row | sqlalchemy.engine.row.RowMapping]`




    
##### Method `get_session` {#hexagon.infrastructure.text_query_executor.TextQueryExecutor.get_session}



    
> `def get_session(self) -> sqlalchemy.orm.session.Session`






    
# Module `hexagon.infrastructure.timeseries` {#hexagon.infrastructure.timeseries}







    
## Classes


    
### Class `TSDeviseQuery` {#hexagon.infrastructure.timeseries.TSDeviseQuery}



> `class TSDeviseQuery(dbsession: sqlalchemy.orm.session.Session, *, ts_values: list[str] = None)`


Function Summary


#### Attributes

**```dbsession```** :&ensp;<code>TYPE</code>
:   Description


Function Summary


#### Args

**```dbsession```** :&ensp;<code>Session</code>
:   Description


**```ts_values```** :&ensp;<code>list\[str]</code>, optional
:   Description




    
#### Ancestors (in MRO)

* [hexagon.infrastructure.timeseries.TSModel](#hexagon.infrastructure.timeseries.TSModel)
* [hexagon.infrastructure.timeseries.TSQueryCore](#hexagon.infrastructure.timeseries.TSQueryCore)






    
#### Methods


    
##### Method `dates_historique` {#hexagon.infrastructure.timeseries.TSDeviseQuery.dates_historique}



    
> `def dates_historique(self, devise: hexagon.infrastructure.db.models.market.Devise, debut: datetime.date = None, fin: datetime.date = None, as_mapping: bool = False) -> list[datetime.date]`


Function Summary


###### Args

**```devise```** :&ensp;<code>db.Devise</code>
:   Description



###### Returns

<code>list\[date]</code>
:   Description



    
##### Method `historique` {#hexagon.infrastructure.timeseries.TSDeviseQuery.historique}



    
> `def historique(self, devises: list[hexagon.infrastructure.db.models.market.Devise], *, debut: datetime.date = None, fin: datetime.date = None, date_list: list[datetime.date] = None, ts_values: list[str] = None, as_mapping: bool = False) -> list[tuple[datetime.date, float]]`


Function Summary


###### Args

**```devise```** :&ensp;<code>list\[db.Devise]</code>
:   Description


**```debut```** :&ensp;<code>date</code>, optional
:   Description


**```fin```** :&ensp;<code>date</code>, optional
:   Description


**```date_list```** :&ensp;<code>list\[date]</code>, optional
:   Description


**```ts_values```** :&ensp;<code>list\[str]</code>, optional
:   Description



###### Returns

<code>list\[tuple\[date, float]]</code>
:   Description



    
##### Method `serie_variation` {#hexagon.infrastructure.timeseries.TSDeviseQuery.serie_variation}



    
> `def serie_variation(self, devises: list[hexagon.infrastructure.db.models.market.Devise], *, debut: datetime.date = None, fin: datetime.date = None, date_list: list[datetime.date] = None, ts_values: list[str] = None, with_values: bool = True, as_mapping: bool = False) -> list[sqlalchemy.engine.row.Row]`




    
### Class `TSFondsMarketQuery` {#hexagon.infrastructure.timeseries.TSFondsMarketQuery}



> `class TSFondsMarketQuery(dbsession: sqlalchemy.orm.session.Session, *, ts_values: list[str] = None)`


Function Summary


#### Attributes

**```dbsession```** :&ensp;<code>TYPE</code>
:   Description


Function Summary


#### Args

**```dbsession```** :&ensp;<code>Session</code>
:   Description


**```ts_values```** :&ensp;<code>list\[str]</code>, optional
:   Description




    
#### Ancestors (in MRO)

* [hexagon.infrastructure.timeseries.TSModel](#hexagon.infrastructure.timeseries.TSModel)
* [hexagon.infrastructure.timeseries.TSQueryCore](#hexagon.infrastructure.timeseries.TSQueryCore)






    
#### Methods


    
##### Method `dates_historique` {#hexagon.infrastructure.timeseries.TSFondsMarketQuery.dates_historique}



    
> `def dates_historique(self, fonds: hexagon.infrastructure.db.models.market.FondsMarket, debut: datetime.date = None, fin: datetime.date = None, as_mapping: bool = False) -> list[datetime.date]`


Function Summary


###### Args

**```fonds```** :&ensp;<code>db.FondsMarket</code>
:   Description



###### Returns

<code>list\[date]</code>
:   Description



    
##### Method `historique` {#hexagon.infrastructure.timeseries.TSFondsMarketQuery.historique}



    
> `def historique(self, fonds: list[hexagon.infrastructure.db.models.market.FondsMarket], *, debut: datetime.date = None, fin: datetime.date = None, date_list: list[datetime.date] = None, ts_values: list[str] = None, as_mapping: bool = False) -> list[tuple[datetime.date, float]]`


Function Summary


###### Args

**```fonds```** :&ensp;<code>list\[db.FondsMarket]</code>
:   Description


**```debut```** :&ensp;<code>date</code>, optional
:   Description


**```fin```** :&ensp;<code>date</code>, optional
:   Description


**```date_list```** :&ensp;<code>list\[date]</code>, optional
:   Description


**```ts_values```** :&ensp;<code>list\[str]</code>, optional
:   Description



###### Returns

<code>list\[tuple\[date, float]]</code>
:   Description



    
##### Method `serie_variation` {#hexagon.infrastructure.timeseries.TSFondsMarketQuery.serie_variation}



    
> `def serie_variation(self, fonds: list[hexagon.infrastructure.db.models.market.FondsMarket], *, debut: datetime.date = None, fin: datetime.date = None, date_list: list[datetime.date] = None, ts_values: list[str] = None, with_values: bool = True, as_mapping: bool = False) -> list[sqlalchemy.engine.row.Row]`




    
### Class `TSFondsQuery` {#hexagon.infrastructure.timeseries.TSFondsQuery}



> `class TSFondsQuery(dbsession: sqlalchemy.orm.session.Session, *, ts_values: list[str] = None)`


Joue le rôle d'un Factory Inferentiel des modèles des Séries chronologiques et
    aussi le rôle d'un executeur parametrique des requêtes (select) construites,
    l'exécution utilise le context manager de SQLA 2.0


#### Attributes

**```dbsession```** :&ensp;<code>Session</code>
:   instance de Session


**```ts_table```** :&ensp;<code>Table</code>
:   Modèle de Série chronologique -"SC"- (avec le champs 'date_marche')



#### Args

**```ts_table```** :&ensp;<code>Table</code>
:   Classe SQLA du modèle qui représente la série chronologique.
    La table qui contient les observations


**```ts_rid```** :&ensp;<code>str</code>
:   nom de la colonne du RowID


**```ts_fk_id```** :&ensp;<code>str</code>
:   nom la colonne du Foreign Key de l'entité observée


**```ts_date```** :&ensp;<code>str</code>
:   nom de la colonne de la date chronologique


**```ts_values```** :&ensp;<code>list\[str]</code>, optional
:   Liste des noms des colonnes des mesures de l'observable




    
#### Ancestors (in MRO)

* [hexagon.infrastructure.timeseries.TSModel](#hexagon.infrastructure.timeseries.TSModel)
* [hexagon.infrastructure.timeseries.TSQueryCore](#hexagon.infrastructure.timeseries.TSQueryCore)






    
#### Methods


    
##### Method `dates_historique` {#hexagon.infrastructure.timeseries.TSFondsQuery.dates_historique}



    
> `def dates_historique(self, fonds: hexagon.infrastructure.db.models.opcvm.Fonds = None, debut: datetime.date = None, fin: datetime.date = None, as_mapping: bool = False) -> list[datetime.date]`




    
##### Method `historique` {#hexagon.infrastructure.timeseries.TSFondsQuery.historique}



    
> `def historique(self, fonds: list[hexagon.infrastructure.db.models.opcvm.Fonds], *, debut: datetime.date = None, fin: datetime.date = None, date_list: list[datetime.date] = None, ts_values: list[str] = None, as_mapping: bool = False, with_ids: bool = True) -> list[sqlalchemy.engine.row.Row]`


Function Summary


###### Args

**```fonds```** :&ensp;<code>list\[db.Fonds]</code>
:   Description


**```debut```** :&ensp;<code>date</code>, optional
:   Description


**```fin```** :&ensp;<code>date</code>, optional
:   Description


**```date_list```** :&ensp;<code>list\[date]</code>, optional
:   Description


**```ts_values```** :&ensp;<code>list\[str]</code>, optional
:   Description


**```as_mapping```** :&ensp;<code>bool</code>, optional
:   Description



###### Returns

<code>list\[Row]</code>
:   Description



    
##### Method `serie_variation` {#hexagon.infrastructure.timeseries.TSFondsQuery.serie_variation}



    
> `def serie_variation(self, fonds: list[hexagon.infrastructure.db.models.opcvm.Fonds], *, debut: datetime.date = None, fin: datetime.date = None, date_list: list[datetime.date] = None, ts_values: list[str] = None, with_values: bool = True, as_mapping: bool = False) -> list[sqlalchemy.engine.row.Row]`




    
### Class `TSIndiceQuery` {#hexagon.infrastructure.timeseries.TSIndiceQuery}



> `class TSIndiceQuery(dbsession: sqlalchemy.orm.session.Session, *, ts_values: list[str] = None)`


Function Summary


#### Attributes

**```dbsession```** :&ensp;<code>TYPE</code>
:   Description


Function Summary


#### Args

**```dbsession```** :&ensp;<code>Session</code>
:   Description


**```ts_values```** :&ensp;<code>list\[str]</code>, optional
:   Description




    
#### Ancestors (in MRO)

* [hexagon.infrastructure.timeseries.TSModel](#hexagon.infrastructure.timeseries.TSModel)
* [hexagon.infrastructure.timeseries.TSQueryCore](#hexagon.infrastructure.timeseries.TSQueryCore)






    
#### Methods


    
##### Method `dates_historique` {#hexagon.infrastructure.timeseries.TSIndiceQuery.dates_historique}



    
> `def dates_historique(self, indice: hexagon.infrastructure.db.models.market.Indice, debut: datetime.date = None, fin: datetime.date = None, as_mapping: bool = False) -> list[datetime.date]`


Function Summary


###### Args

**```indice```** :&ensp;<code>db.Indice</code>
:   Description



###### Returns

<code>list\[date]</code>
:   Description



    
##### Method `historique` {#hexagon.infrastructure.timeseries.TSIndiceQuery.historique}



    
> `def historique(self, indices: list[hexagon.infrastructure.db.models.market.Indice], *, debut: datetime.date = None, fin: datetime.date = None, date_list: list[datetime.date] = None, ts_values: list[str] = None, as_mapping: bool = False) -> list[tuple[datetime.date, float]]`


Function Summary


###### Args

**```indice```** :&ensp;<code>list\[db.Indice]</code>
:   Description


**```debut```** :&ensp;<code>date</code>, optional
:   Description


**```fin```** :&ensp;<code>date</code>, optional
:   Description


**```date_list```** :&ensp;<code>list\[date]</code>, optional
:   Description


**```ts_values```** :&ensp;<code>list\[str]</code>, optional
:   Description



###### Returns

<code>list\[tuple\[date, float]]</code>
:   Description



    
##### Method `serie_variation` {#hexagon.infrastructure.timeseries.TSIndiceQuery.serie_variation}



    
> `def serie_variation(self, indices: list[hexagon.infrastructure.db.models.market.Indice], *, debut: datetime.date = None, fin: datetime.date = None, date_list: list[datetime.date] = None, ts_values: list[str] = None, with_values: bool = True, as_mapping: bool = False) -> list[sqlalchemy.engine.row.Row]`




    
### Class `TSModel` {#hexagon.infrastructure.timeseries.TSModel}



> `class TSModel(dbsession: sqlalchemy.orm.session.Session, ts_table: sqlalchemy.sql.schema.Table, ts_values: list[str] = None)`


Joue le rôle d'un Factory Inferentiel des modèles des Séries chronologiques et
    aussi le rôle d'un executeur parametrique des requêtes (select) construites,
    l'exécution utilise le context manager de SQLA 2.0


#### Attributes

**```dbsession```** :&ensp;<code>Session</code>
:   instance de Session


**```ts_table```** :&ensp;<code>Table</code>
:   Modèle de Série chronologique -"SC"- (avec le champs 'date_marche')



#### Args

**```ts_table```** :&ensp;<code>Table</code>
:   Classe SQLA du modèle qui représente la série chronologique.
    La table qui contient les observations


**```ts_rid```** :&ensp;<code>str</code>
:   nom de la colonne du RowID


**```ts_fk_id```** :&ensp;<code>str</code>
:   nom la colonne du Foreign Key de l'entité observée


**```ts_date```** :&ensp;<code>str</code>
:   nom de la colonne de la date chronologique


**```ts_values```** :&ensp;<code>list\[str]</code>, optional
:   Liste des noms des colonnes des mesures de l'observable




    
#### Ancestors (in MRO)

* [hexagon.infrastructure.timeseries.TSQueryCore](#hexagon.infrastructure.timeseries.TSQueryCore)


    
#### Descendants

* [hexagon.infrastructure.timeseries.TSDeviseQuery](#hexagon.infrastructure.timeseries.TSDeviseQuery)
* [hexagon.infrastructure.timeseries.TSFondsMarketQuery](#hexagon.infrastructure.timeseries.TSFondsMarketQuery)
* [hexagon.infrastructure.timeseries.TSFondsQuery](#hexagon.infrastructure.timeseries.TSFondsQuery)
* [hexagon.infrastructure.timeseries.TSIndiceQuery](#hexagon.infrastructure.timeseries.TSIndiceQuery)
* [hexagon.infrastructure.timeseries.TSTitreQuery](#hexagon.infrastructure.timeseries.TSTitreQuery)


    
#### Class variables


    
##### Variable `DATE_FIELD` {#hexagon.infrastructure.timeseries.TSModel.DATE_FIELD}





    
#### Static methods


    
##### `Method infer_fields` {#hexagon.infrastructure.timeseries.TSModel.infer_fields}



    
> `def infer_fields(ts_table: sqlalchemy.sql.schema.Table) -> dict`


Construit le dict des arguments de TSQuery à partir du modèle de SC passé


###### Args

**```ts_table```** :&ensp;<code>Table</code>
:   Modèle de série chronologique EtatFonds, EtatTitre, ....



###### Returns

<code>dict\[str, str]</code>
:   Arguments




    
#### Methods


    
##### Method `execute_query` {#hexagon.infrastructure.timeseries.TSModel.execute_query}



    
> `def execute_query(self, query: sqlalchemy.sql.selectable.Select, as_mapping: bool = True) -> list[sqlalchemy.engine.row.Row | sqlalchemy.engine.row.RowMapping]`




    
### Class `TSQueryCore` {#hexagon.infrastructure.timeseries.TSQueryCore}



> `class TSQueryCore(ts_table: sqlalchemy.sql.schema.Table, ts_rid: str, ts_fk_id: str, ts_date: str, ts_values: list[str] = None)`


Classe générique pour construire des requêtes sur sqlalchemy core adaptées
aux séries chronologiques d'un ou plusieurs observable(s) d'une entité donnée.

Cette classe makes assumption que les observations sont au format long. (Une date par ligne)


#### Attributes

**```ts_table```** :&ensp;<code>Table</code>
:   Classe SQLA du modèle qui représente la série chronologique.
    La table qui contient les observations


**```ts_rid```** :&ensp;<code>Column</code>
:   RowID de l'observable


**```ts_fk_id```** :&ensp;<code>Column</code>
:   Foreign ID du de l'entité observée (Fonds, Titre, Indice, Devise, ...)


**```ts_date```** :&ensp;<code>Column</code>
:   L'attribut de date pris en considération (date_marche)


**```ts_values```** :&ensp;<code>Column</code>
:   Champs des mesures observées



#### Args

**```ts_table```** :&ensp;<code>Table</code>
:   Classe SQLA du modèle qui représente la série chronologique.
    La table qui contient les observations


**```ts_rid```** :&ensp;<code>str</code>
:   nom de la colonne du RowID


**```ts_fk_id```** :&ensp;<code>str</code>
:   nom la colonne du Foreign Key de l'entité observée


**```ts_date```** :&ensp;<code>str</code>
:   nom de la colonne de la date chronologique


**```ts_values```** :&ensp;<code>list\[str]</code>, optional
:   Liste des noms des colonnes des mesures de l'observable





    
#### Descendants

* [hexagon.infrastructure.timeseries.TSModel](#hexagon.infrastructure.timeseries.TSModel)





    
#### Methods


    
##### Method `query_all_dates` {#hexagon.infrastructure.timeseries.TSQueryCore.query_all_dates}



    
> `def query_all_dates(self, *, debut: datetime.date = None, fin: datetime.date = None) -> sqlalchemy.sql.selectable.Select`


Construit la requête pour retourner toutes les dates DISCTINCTES
de toutes les mesures de tous les observables (ts_fk_id)

** Extract with scalars()


###### Args

**```debut```** :&ensp;<code>date</code>, optional
:   Description


**```fin```** :&ensp;<code>date</code>, optional
:   Description



###### Returns

<code>Select</code>
:   Requête pointant sur tousles observables à filtrer par le champs "model.ts_fk_id“



###### Raises

<code>TSError</code>
:   Description



    
##### Method `query_ts_values` {#hexagon.infrastructure.timeseries.TSQueryCore.query_ts_values}



    
> `def query_ts_values(self, *, debut: datetime.date = None, fin: datetime.date = None, date_list: list[datetime.date] = None, ts_values: list[str] = None, filter_fk_id: int | list[int] = None, with_ids: bool = True) -> sqlalchemy.sql.selectable.Select`


Construit une requete pour les mesures 'self.ts_values' de l'observable entre deux dates [debut, fin] incluses
    ou pour une liste de dates.
L'ordre est croissant par défaut.
La liste des mesures souhaitées est par défaut: self.ts_values

** Si la liste des dates est fournie, les paramètres (debut, fin) seront ignorés


###### Args

**```debut```** :&ensp;<code>date, None</code>
:   Date de début


**```fin```** :&ensp;<code>date, None</code>
:   date de fin


**```date_list```** :&ensp;<code>list\[date], None</code>
:   Liste des dates


**```ts_values```** :&ensp;<code>list\[str], None</code>
:   Si fournie, la liste renseignée
    sera prise en considération au lieu de self.ts_values



###### Returns

<code>Select</code>
:   Requête générée selon les paramètres. Select conform to the SQLA 2.0 API.



###### Raises

`TSError:`
:   - Si aucun argument n'est fourni
    - Si les dates debut et fin sont inversées



    
##### Method `query_ts_variation` {#hexagon.infrastructure.timeseries.TSQueryCore.query_ts_variation}



    
> `def query_ts_variation(self, *, debut: datetime.date = None, fin: datetime.date = None, date_list: list[datetime.date] = None, ts_values: list[str] = None, with_values: bool = True, filter_fk_id: int | list[int] = None) -> sqlalchemy.sql.selectable.Select`


Construit une requete qui calcule les variations arithmétiques des mesures 'self.ts_values' de l'observable
    'filter_fk_id' entre deux dates debut et fin.
    Quand une liste de dates est fournie, la requete construite est un 'CompoundSelect' qui représente l'union
    des requêtes (pairwise|2à2) de 2 dates consécutives dans la liste.
L'ordre est croissant par défaut.
La liste des mesures souhaitées est par défaut: self.ts_values

** Si la liste des dates est fournie, les paramètres (debut, fin) seront ignorés


###### Args

**```debut```** :&ensp;<code>date, None</code>
:   Date de début


**```fin```** :&ensp;<code>date, None</code>
:   date de fin


**```date_list```** :&ensp;<code>list\[date], None</code>
:   Liste des dates


**```ts_values```** :&ensp;<code>list\[str], None</code>
:   Si fournie, la liste renseignée
    sera prise en considération au lieu de self.ts_values


**```with_values```** :&ensp;<code>bool, True</code>
:   Si False ne retourne que les variations des mesures 2 à 2 (pairwise) sans leurs valeurs respectives


filter_fk_id (int| list[int]=None): id ou Liste des ids de l'observable souhaité,
    injecte la clause dans chaque sous requête

###### Returns

<code>Select</code>
:   Requête générée selon les paramètres. Select conform to the SQLA 2.0 API.
    ** Si date début et fin retourne un select normal
    ** Si date_list renseignée contruit l'union des sous requêtes 2à2|pairwise



###### Raises

`TSError:`
:   - Si aucun argument n'est fourni
    - Si les dates debut et fin sont inversées
    - Si le type de filter_fk_id n'est pas supporté. Doit etre 'int'



    
### Class `TSTitreQuery` {#hexagon.infrastructure.timeseries.TSTitreQuery}



> `class TSTitreQuery(dbsession: sqlalchemy.orm.session.Session, *, ts_values: list[str] = None)`


Joue le rôle d'un Factory Inferentiel des modèles des Séries chronologiques et
    aussi le rôle d'un executeur parametrique des requêtes (select) construites,
    l'exécution utilise le context manager de SQLA 2.0


#### Attributes

**```dbsession```** :&ensp;<code>Session</code>
:   instance de Session


**```ts_table```** :&ensp;<code>Table</code>
:   Modèle de Série chronologique -"SC"- (avec le champs 'date_marche')



#### Args

**```ts_table```** :&ensp;<code>Table</code>
:   Classe SQLA du modèle qui représente la série chronologique.
    La table qui contient les observations


**```ts_rid```** :&ensp;<code>str</code>
:   nom de la colonne du RowID


**```ts_fk_id```** :&ensp;<code>str</code>
:   nom la colonne du Foreign Key de l'entité observée


**```ts_date```** :&ensp;<code>str</code>
:   nom de la colonne de la date chronologique


**```ts_values```** :&ensp;<code>list\[str]</code>, optional
:   Liste des noms des colonnes des mesures de l'observable




    
#### Ancestors (in MRO)

* [hexagon.infrastructure.timeseries.TSModel](#hexagon.infrastructure.timeseries.TSModel)
* [hexagon.infrastructure.timeseries.TSQueryCore](#hexagon.infrastructure.timeseries.TSQueryCore)






    
#### Methods


    
##### Method `dates_historique` {#hexagon.infrastructure.timeseries.TSTitreQuery.dates_historique}



    
> `def dates_historique(self, titre: hexagon.infrastructure.db.models.opcvm.Titre, debut: datetime.date = None, fin: datetime.date = None, as_mapping: bool = False) -> list[datetime.date]`


Function Summary


###### Args

**```titre```** :&ensp;<code>db.Titre</code>
:   Description



###### Returns

<code>list\[date]</code>
:   Description



    
##### Method `historique` {#hexagon.infrastructure.timeseries.TSTitreQuery.historique}



    
> `def historique(self, titres: list[hexagon.infrastructure.db.models.opcvm.Titre], *, debut: datetime.date = None, fin: datetime.date = None, date_list: list[datetime.date] = None, ts_values: list[str] = None, as_mapping: bool = False) -> list[tuple[datetime.date, float]]`


Function Summary


###### Args

**```titre```** :&ensp;<code>list\[db.Titre]</code>
:   Description


**```debut```** :&ensp;<code>date</code>, optional
:   Description


**```fin```** :&ensp;<code>date</code>, optional
:   Description


**```date_list```** :&ensp;<code>list\[date]</code>, optional
:   Description


**```ts_values```** :&ensp;<code>list\[str]</code>, optional
:   Description



###### Returns

<code>list\[tuple\[date, float]]</code>
:   Description



    
##### Method `serie_variation` {#hexagon.infrastructure.timeseries.TSTitreQuery.serie_variation}



    
> `def serie_variation(self, titres: list[hexagon.infrastructure.db.models.opcvm.Titre], *, debut: datetime.date = None, fin: datetime.date = None, date_list: list[datetime.date] = None, ts_values: list[str] = None, with_values: bool = True, as_mapping: bool = False) -> list[sqlalchemy.engine.row.Row]`






    
# Module `hexagon.performance` {#hexagon.performance}




    
## Sub-modules

* [hexagon.performance.aggregates](#hexagon.performance.aggregates)
* [hexagon.performance.benchmark](#hexagon.performance.benchmark)
* [hexagon.performance.benchmark-old](#hexagon.performance.benchmark-old)
* [hexagon.performance.calendar_fonds](#hexagon.performance.calendar_fonds)
* [hexagon.performance.comparator](#hexagon.performance.comparator)
* [hexagon.performance.datastructures](#hexagon.performance.datastructures)
* [hexagon.performance.devise](#hexagon.performance.devise)
* [hexagon.performance.enumerations](#hexagon.performance.enumerations)
* [hexagon.performance.exceptions](#hexagon.performance.exceptions)
* [hexagon.performance.fonds](#hexagon.performance.fonds)
* [hexagon.performance.indice](#hexagon.performance.indice)
* [hexagon.performance.repository](#hexagon.performance.repository)






    
# Module `hexagon.performance.aggregates` {#hexagon.performance.aggregates}







    
## Classes


    
### Class `FlatDelta` {#hexagon.performance.aggregates.FlatDelta}



> `class FlatDelta(date_marche: datetime.date, date_reference: datetime.date, periode: str, compare: str, avec: str, performance_compare: float, performance_avec: float, ecart: float)`


FlatDelta(date_marche: datetime.date, date_reference: datetime.date, periode: str, compare: str, avec: str, performance_compare: float, performance_avec: float, ecart: float)





    
#### Instance variables


    
##### Variable `avec` {#hexagon.performance.aggregates.FlatDelta.avec}

Return an attribute of instance, which is of type owner.

    
##### Variable `compare` {#hexagon.performance.aggregates.FlatDelta.compare}

Return an attribute of instance, which is of type owner.

    
##### Variable `date_marche` {#hexagon.performance.aggregates.FlatDelta.date_marche}

Return an attribute of instance, which is of type owner.

    
##### Variable `date_reference` {#hexagon.performance.aggregates.FlatDelta.date_reference}

Return an attribute of instance, which is of type owner.

    
##### Variable `ecart` {#hexagon.performance.aggregates.FlatDelta.ecart}

Return an attribute of instance, which is of type owner.

    
##### Variable `performance_avec` {#hexagon.performance.aggregates.FlatDelta.performance_avec}

Return an attribute of instance, which is of type owner.

    
##### Variable `performance_compare` {#hexagon.performance.aggregates.FlatDelta.performance_compare}

Return an attribute of instance, which is of type owner.

    
##### Variable `periode` {#hexagon.performance.aggregates.FlatDelta.periode}

Return an attribute of instance, which is of type owner.



    
### Class `PerformanceCollection` {#hexagon.performance.aggregates.PerformanceCollection}



> `class PerformanceCollection(ref_id: int | str, freq: hexagon.calendrier.enumerations.FrequenceCalendar = None, periode: str = None, collection: Iterable[hexagon.performance.aggregates.PerformanceSpot | hexagon.performance.aggregates.PerformanceValue] = <factory>, base_point: float = 100)`


PerformanceCollection(ref_id: int | str, freq: hexagon.calendrier.enumerations.FrequenceCalendar = None, periode: str = None, collection: Iterable[hexagon.performance.aggregates.PerformanceSpot | hexagon.performance.aggregates.PerformanceValue] = <factory>, base_point: float = 100)



    
#### Descendants

* [hexagon.performance.aggregates.PerformanceDeltaCollection](#hexagon.performance.aggregates.PerformanceDeltaCollection)


    
#### Class variables


    
##### Variable `base_point` {#hexagon.performance.aggregates.PerformanceCollection.base_point}



    
##### Variable `freq` {#hexagon.performance.aggregates.PerformanceCollection.freq}



    
##### Variable `periode` {#hexagon.performance.aggregates.PerformanceCollection.periode}




    
#### Instance variables


    
##### Variable `debut` {#hexagon.performance.aggregates.PerformanceCollection.debut}

Calcule la date de début de la collection et la stocke sous _debut.
set _debut to None pour triggerer le recalcul


###### Returns

<code>date</code>: Date de début

    
##### Variable `fin` {#hexagon.performance.aggregates.PerformanceCollection.fin}

Calcule la date de fin de la collection et la stocke sous _fin.
set _fin to None pour triggerer le recalcul


###### Returns

<code>date</code>: Date de fin



    
#### Methods


    
##### Method `performance_cumulee` {#hexagon.performance.aggregates.PerformanceCollection.performance_cumulee}



    
> `def performance_cumulee(self, *, as_base_point: bool = False) -> hexagon.performance.aggregates.PerformanceSpot`


Retourne la performance capitalisée de la série.


###### Returns

<code>[PerformanceSpot](#hexagon.performance.aggregates.PerformanceSpot "hexagon.performance.aggregates.PerformanceSpot")</code>: Performance capitalisée de la série

    
##### Method `performance_trend` {#hexagon.performance.aggregates.PerformanceCollection.performance_trend}



    
> `def performance_trend(self, *, base_point: float = None) -> list[hexagon.performance.aggregates.PerformanceValue]`




    
##### Method `pop` {#hexagon.performance.aggregates.PerformanceCollection.pop}



    
> `def pop(self) -> hexagon.performance.aggregates.PerformanceValue`




    
##### Method `to_dict` {#hexagon.performance.aggregates.PerformanceCollection.to_dict}



    
> `def to_dict(self, *, recurse=False) -> dict`




    
### Class `PerformanceDelta` {#hexagon.performance.aggregates.PerformanceDelta}



> `class PerformanceDelta(date_marche: datetime.date, date_reference: datetime.date, periode: str = None, compare: hexagon.core.entities.CoreEntity = None, avec: hexagon.core.entities.CoreEntity = None, performance_compare: hexagon.performance.aggregates.PerformanceSpot = None, performance_avec: hexagon.performance.aggregates.PerformanceSpot = None, uuid: uuid.UUID = <factory>)`


PerformanceDelta(date_marche: datetime.date, date_reference: datetime.date, periode: str = None, compare: hexagon.core.entities.CoreEntity = None, avec: hexagon.core.entities.CoreEntity = None, performance_compare: hexagon.performance.aggregates.PerformanceSpot = None, performance_avec: hexagon.performance.aggregates.PerformanceSpot = None, uuid: uuid.UUID = <factory>)


    
#### Ancestors (in MRO)

* [hexagon.calendrier.aggregates.CalendarPeriode](#hexagon.calendrier.aggregates.CalendarPeriode)




    
#### Instance variables


    
##### Variable `avec` {#hexagon.performance.aggregates.PerformanceDelta.avec}

Return an attribute of instance, which is of type owner.

    
##### Variable `compare` {#hexagon.performance.aggregates.PerformanceDelta.compare}

Return an attribute of instance, which is of type owner.

    
##### Variable `ecart_performance` {#hexagon.performance.aggregates.PerformanceDelta.ecart_performance}



    
##### Variable `performance_avec` {#hexagon.performance.aggregates.PerformanceDelta.performance_avec}

Return an attribute of instance, which is of type owner.

    
##### Variable `performance_compare` {#hexagon.performance.aggregates.PerformanceDelta.performance_compare}

Return an attribute of instance, which is of type owner.

    
##### Variable `uuid` {#hexagon.performance.aggregates.PerformanceDelta.uuid}

Return an attribute of instance, which is of type owner.



    
#### Methods


    
##### Method `data_performance` {#hexagon.performance.aggregates.PerformanceDelta.data_performance}



    
> `def data_performance(self, entity_attr: str = 'code', *, annualiser: bool = False) -> tuple`




    
##### Method `data_point` {#hexagon.performance.aggregates.PerformanceDelta.data_point}



    
> `def data_point(self, entity_attr: str = 'code', *, base_point: float = 100) -> tuple`




    
##### Method `flatten` {#hexagon.performance.aggregates.PerformanceDelta.flatten}



    
> `def flatten(self, entity_attr: str = 'code', *, annualiser: bool = False) -> hexagon.performance.aggregates.FlatDelta`




    
### Class `PerformanceDeltaCollection` {#hexagon.performance.aggregates.PerformanceDeltaCollection}



> `class PerformanceDeltaCollection(freq: hexagon.calendrier.enumerations.FrequenceCalendar = None, periode: str = None, collection: Iterable[hexagon.performance.aggregates.PerformanceDelta] = <factory>, base_point: float = 100, compare: hexagon.core.entities.CoreEntity = None, avec: hexagon.core.entities.CoreEntity = None)`


PerformanceDeltaCollection(freq: hexagon.calendrier.enumerations.FrequenceCalendar = None, periode: str = None, collection: Iterable[hexagon.performance.aggregates.PerformanceDelta] = <factory>, base_point: float = 100, compare: hexagon.core.entities.CoreEntity = None, avec: hexagon.core.entities.CoreEntity = None)


    
#### Ancestors (in MRO)

* [hexagon.performance.aggregates.PerformanceCollection](#hexagon.performance.aggregates.PerformanceCollection)



    
#### Class variables


    
##### Variable `avec` {#hexagon.performance.aggregates.PerformanceDeltaCollection.avec}



    
##### Variable `compare` {#hexagon.performance.aggregates.PerformanceDeltaCollection.compare}



    
##### Variable `ref_id` {#hexagon.performance.aggregates.PerformanceDeltaCollection.ref_id}






    
#### Methods


    
##### Method `performance_cumulee` {#hexagon.performance.aggregates.PerformanceDeltaCollection.performance_cumulee}



    
> `def performance_cumulee(self, entity_attr: str = 'code', *, as_base_point: bool = False) -> hexagon.performance.aggregates.FlatDelta`


Retourne la performance capitalisée de la série.


###### Returns

<code>[FlatDelta](#hexagon.performance.aggregates.FlatDelta "hexagon.performance.aggregates.FlatDelta")</code>: Performance capitalisée de la comparaison des performances

    
##### Method `performance_trend` {#hexagon.performance.aggregates.PerformanceDeltaCollection.performance_trend}



    
> `def performance_trend(self, entity_attr: str = 'code', *, base_point: float = None) -> list[hexagon.performance.aggregates.FlatDelta]`




    
### Class `PerformanceSpot` {#hexagon.performance.aggregates.PerformanceSpot}



> `class PerformanceSpot(date_marche: datetime.date, date_reference: datetime.date, periode: str = None, performance: hexagon.utils.stringify.Percent = 0, ref_id: int = -1)`


PerformanceSpot(date_marche: datetime.date, date_reference: datetime.date, periode: str = None, performance: hexagon.utils.stringify.Percent = 0, ref_id: int = -1)


    
#### Ancestors (in MRO)

* [hexagon.calendrier.aggregates.CalendarPeriode](#hexagon.calendrier.aggregates.CalendarPeriode)




    
#### Instance variables


    
##### Variable `performance` {#hexagon.performance.aggregates.PerformanceSpot.performance}

Return an attribute of instance, which is of type owner.

    
##### Variable `ref_id` {#hexagon.performance.aggregates.PerformanceSpot.ref_id}

Return an attribute of instance, which is of type owner.



    
#### Methods


    
##### Method `annualiser_performance` {#hexagon.performance.aggregates.PerformanceSpot.annualiser_performance}



    
> `def annualiser_performance(self, date_anniversaire: datetime.date) -> hexagon.utils.stringify.Percent`


Annualise la performance à la date d'anniversaire passée,
L'annualisation se fait selon le mode de calcul de la performance (Arithmetique ou logarithmique)

Args:

- date_anniversaire (<code>date</code>): date d'annualisation


###### Returns

<code>float</code>: performance annualisée

    
##### Method `data_performance` {#hexagon.performance.aggregates.PerformanceSpot.data_performance}



    
> `def data_performance(self, *, annualiser: bool = False) -> tuple`




    
##### Method `data_point` {#hexagon.performance.aggregates.PerformanceSpot.data_point}



    
> `def data_point(self, *, base_point: float = 100) -> tuple`




    
### Class `PerformanceValue` {#hexagon.performance.aggregates.PerformanceValue}



> `class PerformanceValue(date_marche: datetime.date, date_reference: datetime.date, periode: str = None, valeur_marche: float = 1, valeur_reference: float = 1, ref_id: int = -1, poids: float = 1)`


PerformanceValue(date_marche: datetime.date, date_reference: datetime.date, periode: str = None, valeur_marche: float = 1, valeur_reference: float = 1, ref_id: int = -1, poids: float = 1)


    
#### Ancestors (in MRO)

* [hexagon.calendrier.aggregates.CalendarPeriode](#hexagon.calendrier.aggregates.CalendarPeriode)




    
#### Instance variables


    
##### Variable `performance` {#hexagon.performance.aggregates.PerformanceValue.performance}

Property qui calcule la performance selon la méthode définie


###### Returns

<code>float</code>: Variation en % val_marche/val_ref - 1

###### Raises

<code>ValueError</code>: Types inadéquats ou ZeroDivision

    
##### Variable `poids` {#hexagon.performance.aggregates.PerformanceValue.poids}

Return an attribute of instance, which is of type owner.

    
##### Variable `ref_id` {#hexagon.performance.aggregates.PerformanceValue.ref_id}

Return an attribute of instance, which is of type owner.

    
##### Variable `valeur_marche` {#hexagon.performance.aggregates.PerformanceValue.valeur_marche}

Return an attribute of instance, which is of type owner.

    
##### Variable `valeur_reference` {#hexagon.performance.aggregates.PerformanceValue.valeur_reference}

Return an attribute of instance, which is of type owner.



    
#### Methods


    
##### Method `annualiser_performance` {#hexagon.performance.aggregates.PerformanceValue.annualiser_performance}



    
> `def annualiser_performance(self, date_anniversaire: datetime.date) -> hexagon.utils.stringify.Percent`


Annualise la performance à la date d'anniversaire passée,
L'annualisation se fait selon le mode de calcul de la performance (Arithmetique ou logarithmique)

Args:

- date_anniversaire (<code>date</code>): Date d'annualisation


###### Returns

<code>float</code>: Performance Annualisée

    
##### Method `data_performance` {#hexagon.performance.aggregates.PerformanceValue.data_performance}



    
> `def data_performance(self, *, annualiser: bool = False) -> tuple`




    
##### Method `data_point` {#hexagon.performance.aggregates.PerformanceValue.data_point}



    
> `def data_point(self, *, base_point: float = 100) -> tuple`




    
##### Method `to_performance_spot` {#hexagon.performance.aggregates.PerformanceValue.to_performance_spot}



    
> `def to_performance_spot(self) -> hexagon.performance.aggregates.PerformanceSpot`






    
# Module `hexagon.performance.benchmark` {#hexagon.performance.benchmark}







    
## Classes


    
### Class `BenchmarkPerformance` {#hexagon.performance.benchmark.BenchmarkPerformance}



> `class BenchmarkPerformance(benchmark: hexagon.performance.datastructures.PFBenchmark, dbsession: sqlalchemy.orm.session.Session, calendar: hexagon.calendrier.market.MarketCalendar = None, mode_calendar: hexagon.calendrier.enumerations.ModeCalendar = VL, type_benchmark: str = 'COM')`


Un benchmark représente un indice de référence qui change de composition au fil du temps
Par faute de conception ahead of time, ce concept est lié au Fonds. En d'autres termes
nous aurons un indice de référence d'un Fonds.

Attributes:

- benchmark (<code>PFBenchmark</code>): Instance de Benchmark du domaine Performance(PFBenchmark)
- calendar (<code>MarketCalendar</code>): MarketCalendar utilisé
- dbsession (<code>Session</code>): Session de la BDD





    
#### Instance variables


    
##### Variable `pf_entity` {#hexagon.performance.benchmark.BenchmarkPerformance.pf_entity}





    
#### Methods


    
##### Method `current_benchmark` {#hexagon.performance.benchmark.BenchmarkPerformance.current_benchmark}



    
> `def current_benchmark(self, date_obs: datetime.date = None)`


Retourne le benchmark effectif à la date fournie

Args:

- date_obs (datetime.date, optional): If none, retourne le dernier Benchmark dans l'historique.


###### Returns

<code>FondsBenchmark</code>: Instance PFIndiceComposite, pour acceder:

    au fonds utiliser obj.fonds
    au bench utiliser obj.benchmark
    date_effet utiliser obj.date_effet

    
##### Method `histo_benchmark` {#hexagon.performance.benchmark.BenchmarkPerformance.histo_benchmark}



    
> `def histo_benchmark(self, debut: datetime.date, fin: datetime.date)`




    
##### Method `indicateurs_composite_taux` {#hexagon.performance.benchmark.BenchmarkPerformance.indicateurs_composite_taux}



    
> `def indicateurs_composite_taux(self, debut, fin) -> list`


Calcule les indicateurs Taux d'un Bench Obligataire

    
##### Method `performance` {#hexagon.performance.benchmark.BenchmarkPerformance.performance}



    
> `def performance(self, date_marche: datetime.date, date_reference: datetime.date, periode: str = None) -> hexagon.performance.aggregates.PerformanceSpot`




    
##### Method `performance_by_freq` {#hexagon.performance.benchmark.BenchmarkPerformance.performance_by_freq}



    
> `def performance_by_freq(self, debut: datetime.date, fin: datetime.date, freq='H', periode=None) -> hexagon.performance.aggregates.PerformanceCollection`




    
##### Method `performance_periode` {#hexagon.performance.benchmark.BenchmarkPerformance.performance_periode}



    
> `def performance_periode(self, date_marche: datetime.date, periode: str) -> hexagon.performance.aggregates.PerformanceSpot`


Calcule la performance du Benchmark pour une période donnée à la date marché.

periode est une convention "<Periode>: <Periodicite>" exemple: 3:M (pour 3 mois)

Args:

- date_marche (datetime.date): date de valeur
- periode (str): période de performance


###### Returns

<code>PerformanceSpot</code>
:   PerformanceSpot sans valeur_reference et valeur_marche



    
##### Method `performance_periode_range` {#hexagon.performance.benchmark.BenchmarkPerformance.performance_periode_range}



    
> `def performance_periode_range(self, date_marche: datetime.date, periode: str, freq='H') -> hexagon.performance.aggregates.PerformanceCollection`






    
# Module `hexagon.performance.benchmark-old` {#hexagon.performance.benchmark-old}







    
## Classes


    
### Class `BenchmarkPerformance` {#hexagon.performance.benchmark-old.BenchmarkPerformance}



> `class BenchmarkPerformance(fonds: hexagon.performance.datastructures.PFFonds, dbsession: sqlalchemy.orm.session.Session, calendar: hexagon.calendrier.market.MarketCalendar = None, mode_calendar: hexagon.calendrier.enumerations.ModeCalendar = VL, type_benchmark: str = 'COM')`








    
#### Instance variables


    
##### Variable `pf_entity` {#hexagon.performance.benchmark-old.BenchmarkPerformance.pf_entity}





    
#### Methods


    
##### Method `current_benchmark` {#hexagon.performance.benchmark-old.BenchmarkPerformance.current_benchmark}



    
> `def current_benchmark(self, date_obs: datetime.date = None)`


Retourne le benchmark effectif à la date fournie


###### Args

**```date_obs```** :&ensp;<code>datetime.date</code>, optional
:   If none, retourne le dernier Benchmark dans l'historique.



###### Returns

<code>FondsBenchmark</code>
:   Instance PFIndiceComposite, pour acceder:
    au fonds utiliser obj.fonds
    au bench utiliser obj.benchmark
    date_effet utiliser obj.date_effet



    
##### Method `histo_benchmark` {#hexagon.performance.benchmark-old.BenchmarkPerformance.histo_benchmark}



    
> `def histo_benchmark(self, debut: datetime.date, fin: datetime.date)`




    
##### Method `indicateurs_composite_taux` {#hexagon.performance.benchmark-old.BenchmarkPerformance.indicateurs_composite_taux}



    
> `def indicateurs_composite_taux(self, debut, fin) -> list`


Calcule les indicateurs Taux d'un Bench Obligataire

    
##### Method `performance` {#hexagon.performance.benchmark-old.BenchmarkPerformance.performance}



    
> `def performance(self, date_marche: datetime.date, date_reference: datetime.date, periode: str = None) -> hexagon.performance.aggregates.PerformanceSpot`




    
##### Method `performance_by_freq` {#hexagon.performance.benchmark-old.BenchmarkPerformance.performance_by_freq}



    
> `def performance_by_freq(self, debut: datetime.date, fin: datetime.date, freq='H', periode=None) -> hexagon.performance.aggregates.PerformanceCollection`




    
##### Method `performance_periode` {#hexagon.performance.benchmark-old.BenchmarkPerformance.performance_periode}



    
> `def performance_periode(self, date_marche: datetime.date, periode: str) -> hexagon.performance.aggregates.PerformanceSpot`


Calcule la performance du Benchmark pour une période donnée à la date marché.

periode est une convention "<Periode>: <Periodicite>" exemple: 3:M (pour 3 mois)


###### Args

**```date_marche```** :&ensp;<code>datetime.date</code>
:   date de valeur


**```periode```** :&ensp;<code>str</code>
:   période de performance



###### Returns

<code>PerformanceSpot</code>
:   PerformanceSpot sans valeur_reference et valeur_marche



    
##### Method `performance_periode_range` {#hexagon.performance.benchmark-old.BenchmarkPerformance.performance_periode_range}



    
> `def performance_periode_range(self, date_marche: datetime.date, periode: str, freq='H') -> hexagon.performance.aggregates.PerformanceCollection`






    
# Module `hexagon.performance.calendar_fonds` {#hexagon.performance.calendar_fonds}







    
## Classes


    
### Class `CalendarFonds` {#hexagon.performance.calendar_fonds.CalendarFonds}



> `class CalendarFonds(fonds: hexagon.performance.datastructures.PFFonds | int, dbsession: sqlalchemy.orm.session.Session, mode_calendar: hexagon.calendrier.enumerations.ModeCalendar = VL)`


Génere les dates de référence a utiliser pour les dates de performance financières.

Args:

- daily_dates (List): Liste de dates passée en paramètre qui servira comme réference pour le calcul des dates hebdomadaires.


    
#### Ancestors (in MRO)

* [hexagon.calendrier.market.MarketCalendar](#hexagon.calendrier.market.MarketCalendar)



    
#### Class variables


    
##### Variable `periode_fonds_mapper` {#hexagon.performance.calendar_fonds.CalendarFonds.periode_fonds_mapper}






    
#### Methods


    
##### Method `date_reference` {#hexagon.performance.calendar_fonds.CalendarFonds.date_reference}



    
> `def date_reference(self, date_marche: datetime.date, periode: str) -> hexagon.calendrier.aggregates.CalendarPeriode`


Retourne la date de réference associée à la date de valeur, la période et la périodicité de l'instrument
afin de calculer la performance selon le calendrier de l'ASFIM.

Args:

- date_marche (<code>date</code>): date de valeur de l'évaluation de la performance
- periode (<code>str</code>): Période décrite selon le tableau des périodes supportées


###### Returns

<code>CalendarPeriode</code>: Les dates date_reference et date_marche

###### Raises

<code>ValueError</code>: Si le mode date est erroné ou Date introuvable.



    
# Module `hexagon.performance.comparator` {#hexagon.performance.comparator}







    
## Classes


    
### Class `ComparatorFondsBenchmark` {#hexagon.performance.comparator.ComparatorFondsBenchmark}



> `class ComparatorFondsBenchmark(fonds: hexagon.performance.datastructures.PFFonds, benchmark: hexagon.performance.datastructures.PFBenchmark, dbsession: sqlalchemy.orm.session.Session, mode_calendar: hexagon.calendrier.enumerations.ModeCalendar = VL, adjust_distrib: bool = True, adjust_variable: bool = False)`


Classse interface pour implémenter les interfaces publiques de calcul de la performance


    
#### Ancestors (in MRO)

* [hexagon.performance.comparator.ComparatorPerformance](#hexagon.performance.comparator.ComparatorPerformance)
* [hexagon.performance.datastructures.IPerformanceEngine](#hexagon.performance.datastructures.IPerformanceEngine)






    
### Class `ComparatorFondsFonds` {#hexagon.performance.comparator.ComparatorFondsFonds}



> `class ComparatorFondsFonds(fonds: hexagon.performance.datastructures.PFFonds, other: hexagon.performance.datastructures.PFFonds, dbsession: sqlalchemy.orm.session.Session, mode_calendar: hexagon.calendrier.enumerations.ModeCalendar = VL, adjust_distrib: bool = True, adjust_variable: bool = False)`


Classse interface pour implémenter les interfaces publiques de calcul de la performance


    
#### Ancestors (in MRO)

* [hexagon.performance.comparator.ComparatorPerformance](#hexagon.performance.comparator.ComparatorPerformance)
* [hexagon.performance.datastructures.IPerformanceEngine](#hexagon.performance.datastructures.IPerformanceEngine)





    
#### Static methods


    
##### `Method from_uids` {#hexagon.performance.comparator.ComparatorFondsFonds.from_uids}



    
> `def from_uids(fonds_uid: int, other_uid: int, dbsession: sqlalchemy.orm.session.Session, mode_calendar: hexagon.calendrier.enumerations.ModeCalendar = VL, adjust_distrib: bool = True, adjust_variable: bool = False) -> Self`





    
### Class `ComparatorFondsIndice` {#hexagon.performance.comparator.ComparatorFondsIndice}



> `class ComparatorFondsIndice(fonds: hexagon.performance.datastructures.PFFonds, indice: hexagon.performance.datastructures.PFIndice, dbsession: sqlalchemy.orm.session.Session, mode_calendar: hexagon.calendrier.enumerations.ModeCalendar = VL, adjust_distrib: bool = True, adjust_variable: bool = False)`


Classse interface pour implémenter les interfaces publiques de calcul de la performance


    
#### Ancestors (in MRO)

* [hexagon.performance.comparator.ComparatorPerformance](#hexagon.performance.comparator.ComparatorPerformance)
* [hexagon.performance.datastructures.IPerformanceEngine](#hexagon.performance.datastructures.IPerformanceEngine)





    
#### Static methods


    
##### `Method from_uids` {#hexagon.performance.comparator.ComparatorFondsIndice.from_uids}



    
> `def from_uids(fonds_uid: int, indice_uid: int, dbsession: sqlalchemy.orm.session.Session, mode_calendar: hexagon.calendrier.enumerations.ModeCalendar = VL, adjust_distrib: bool = True, adjust_variable: bool = False) -> Self`





    
### Class `ComparatorFondsIndiceComposite` {#hexagon.performance.comparator.ComparatorFondsIndiceComposite}



> `class ComparatorFondsIndiceComposite(fonds: hexagon.performance.datastructures.PFFonds, icomposite: hexagon.performance.datastructures.PFIndiceComposite, dbsession: sqlalchemy.orm.session.Session, mode_calendar: hexagon.calendrier.enumerations.ModeCalendar = VL, adjust_distrib: bool = True, adjust_variable: bool = False)`


Classse interface pour implémenter les interfaces publiques de calcul de la performance


    
#### Ancestors (in MRO)

* [hexagon.performance.comparator.ComparatorPerformance](#hexagon.performance.comparator.ComparatorPerformance)
* [hexagon.performance.datastructures.IPerformanceEngine](#hexagon.performance.datastructures.IPerformanceEngine)





    
#### Static methods


    
##### `Method from_uids` {#hexagon.performance.comparator.ComparatorFondsIndiceComposite.from_uids}



    
> `def from_uids(fonds_uid: int, icomposite_uid: int, dbsession: sqlalchemy.orm.session.Session, mode_calendar: hexagon.calendrier.enumerations.ModeCalendar = VL, adjust_distrib: bool = True, adjust_variable: bool = False) -> Self`





    
### Class `ComparatorPerformance` {#hexagon.performance.comparator.ComparatorPerformance}



> `class ComparatorPerformance(base_pengine: hexagon.performance.datastructures.IPerformanceEngine, other_pengine: hexagon.performance.datastructures.IPerformanceEngine, dbsession: sqlalchemy.orm.session.Session, mode_calendar=VL)`


Classse interface pour implémenter les interfaces publiques de calcul de la performance


    
#### Ancestors (in MRO)

* [hexagon.performance.datastructures.IPerformanceEngine](#hexagon.performance.datastructures.IPerformanceEngine)


    
#### Descendants

* [hexagon.performance.comparator.ComparatorFondsBenchmark](#hexagon.performance.comparator.ComparatorFondsBenchmark)
* [hexagon.performance.comparator.ComparatorFondsFonds](#hexagon.performance.comparator.ComparatorFondsFonds)
* [hexagon.performance.comparator.ComparatorFondsIndice](#hexagon.performance.comparator.ComparatorFondsIndice)
* [hexagon.performance.comparator.ComparatorFondsIndiceComposite](#hexagon.performance.comparator.ComparatorFondsIndiceComposite)



    
#### Instance variables


    
##### Variable `mode_calendar` {#hexagon.performance.comparator.ComparatorPerformance.mode_calendar}





    
#### Methods


    
##### Method `fiche_performance` {#hexagon.performance.comparator.ComparatorPerformance.fiche_performance}



    
> `def fiche_performance(self, date_marche: datetime.date, periodes: list[str] = None) -> list[hexagon.performance.aggregates.PerformanceDelta]`


Génère une fiche comparative des performances selon les périodes du jargon financier
à cela s'ajoute des périodes précises à savoir:

- depuis_creation: Depuis la date de création du Fonds
- depuis_premiere_souscription: Depuis la date de première souscription du Fonds
- exercice_assurance: Depuis le dernier 3e trimestre de l'année précedente. (Pour les Assurances)

Args:

- periods (<code>list</code>, None): Les périodes à générer. defaut=(1:S, 1:M, 3:M, 6:M, 1:A, 2:A, 3:A, 5:A, YTD)

returns:
    <code>list</code>[`PerformanceDelta`]: Liste des performances.

    
##### Method `performance` {#hexagon.performance.comparator.ComparatorPerformance.performance}



    
> `def performance(self, date_marche: datetime.date, date_reference: datetime.date, periode: str = None) -> hexagon.performance.aggregates.PerformanceDelta`




    
##### Method `performance_by_freq` {#hexagon.performance.comparator.ComparatorPerformance.performance_by_freq}



    
> `def performance_by_freq(self, debut: datetime.date, fin: datetime.date, freq: hexagon.calendrier.enumerations.FrequenceCalendar = 'H', periode: str = None) -> hexagon.performance.aggregates.PerformanceDeltaCollection`




    
##### Method `performance_periode` {#hexagon.performance.comparator.ComparatorPerformance.performance_periode}



    
> `def performance_periode(self, date_marche: datetime.date, periode: str) -> hexagon.performance.aggregates.PerformanceDelta`




    
##### Method `performance_periode_range` {#hexagon.performance.comparator.ComparatorPerformance.performance_periode_range}



    
> `def performance_periode_range(self, date_marche: datetime.date, periode: str, freq: hexagon.calendrier.enumerations.FrequenceCalendar = 'H') -> hexagon.performance.aggregates.PerformanceDeltaCollection`




    
##### Method `performance_point_by_freq` {#hexagon.performance.comparator.ComparatorPerformance.performance_point_by_freq}



    
> `def performance_point_by_freq(self, debut: datetime.date, fin: datetime.date, freq: hexagon.calendrier.enumerations.FrequenceCalendar = 'H', base: int = 100, periode: str = None) -> hexagon.performance.aggregates.PerformanceDeltaCollection`




    
##### Method `performance_point_periode` {#hexagon.performance.comparator.ComparatorPerformance.performance_point_periode}



    
> `def performance_point_periode(self, date_marche: datetime.date, periode: str, freq: hexagon.calendrier.enumerations.FrequenceCalendar = 'H', base: int = 100) -> hexagon.performance.aggregates.PerformanceDeltaCollection`






    
# Module `hexagon.performance.datastructures` {#hexagon.performance.datastructures}







    
## Classes


    
### Class `FondsCashOut` {#hexagon.performance.datastructures.FondsCashOut}



> `class FondsCashOut(uid: int = -1, code: str = None, description: str = None, date_operation: datetime.date = None, montant_operation: float = None, nombre_parts: float = None, vl_operation: float = None, date_vl_operation: datetime.date = None, devise_reglement: str = None, fonds_uid: int = -1)`


Method generated by attrs for class FondsCashOut.


    
#### Ancestors (in MRO)

* [hexagon.core.entities.CoreEntity](#hexagon.core.entities.CoreEntity)


    
#### Descendants

* [hexagon.performance.datastructures.FondsDistribution](#hexagon.performance.datastructures.FondsDistribution)
* [hexagon.performance.datastructures.FondsVariable](#hexagon.performance.datastructures.FondsVariable)



    
#### Instance variables


    
##### Variable `date_operation` {#hexagon.performance.datastructures.FondsCashOut.date_operation}

Return an attribute of instance, which is of type owner.

    
##### Variable `date_vl_operation` {#hexagon.performance.datastructures.FondsCashOut.date_vl_operation}

Return an attribute of instance, which is of type owner.

    
##### Variable `devise_reglement` {#hexagon.performance.datastructures.FondsCashOut.devise_reglement}

Return an attribute of instance, which is of type owner.

    
##### Variable `diff_vl` {#hexagon.performance.datastructures.FondsCashOut.diff_vl}

Différence de VL de l'opération

    
##### Variable `fonds_uid` {#hexagon.performance.datastructures.FondsCashOut.fonds_uid}

Return an attribute of instance, which is of type owner.

    
##### Variable `montant_operation` {#hexagon.performance.datastructures.FondsCashOut.montant_operation}

Return an attribute of instance, which is of type owner.

    
##### Variable `nombre_parts` {#hexagon.performance.datastructures.FondsCashOut.nombre_parts}

Return an attribute of instance, which is of type owner.

    
##### Variable `vl_operation` {#hexagon.performance.datastructures.FondsCashOut.vl_operation}

Return an attribute of instance, which is of type owner.


    
#### Static methods


    
##### `Method struct_query` {#hexagon.performance.datastructures.FondsCashOut.struct_query}



    
> `def struct_query()`





    
### Class `FondsDistribution` {#hexagon.performance.datastructures.FondsDistribution}



> `class FondsDistribution(uid: int = -1, code: str = None, description: str = None, date_operation: datetime.date = None, montant_operation: float = None, nombre_parts: float = None, vl_operation: float = None, date_vl_operation: datetime.date = None, devise_reglement: str = None, fonds_uid: int = -1)`


Method generated by attrs for class FondsDistribution.


    
#### Ancestors (in MRO)

* [hexagon.performance.datastructures.FondsCashOut](#hexagon.performance.datastructures.FondsCashOut)
* [hexagon.core.entities.CoreEntity](#hexagon.core.entities.CoreEntity)





    
#### Static methods


    
##### `Method of_fonds_uid` {#hexagon.performance.datastructures.FondsDistribution.of_fonds_uid}



    
> `def of_fonds_uid(fonds_uid: int, dbsession, debut: datetime.date = None, fin: datetime.date = None, include: str = 'BOTH') -> list[typing.Self]`





    
### Class `FondsVariable` {#hexagon.performance.datastructures.FondsVariable}



> `class FondsVariable(uid: int = -1, code: str = None, description: str = None, date_operation: datetime.date = None, montant_operation: float = None, nombre_parts: float = None, vl_operation: float = None, date_vl_operation: datetime.date = None, devise_reglement: str = None, fonds_uid: int = -1)`


Method generated by attrs for class FondsVariable.


    
#### Ancestors (in MRO)

* [hexagon.performance.datastructures.FondsCashOut](#hexagon.performance.datastructures.FondsCashOut)
* [hexagon.core.entities.CoreEntity](#hexagon.core.entities.CoreEntity)





    
#### Static methods


    
##### `Method of_fonds_uid` {#hexagon.performance.datastructures.FondsVariable.of_fonds_uid}



    
> `def of_fonds_uid(fonds_uid: int, dbsession, debut: datetime.date = None, fin: datetime.date = None, include: str = 'BOTH') -> list[typing.Self]`





    
### Class `IPerformanceEngine` {#hexagon.performance.datastructures.IPerformanceEngine}



> `class IPerformanceEngine()`


Classse interface pour implémenter les interfaces publiques de calcul de la performance



    
#### Descendants

* [hexagon.performance.comparator.ComparatorPerformance](#hexagon.performance.comparator.ComparatorPerformance)
* [hexagon.performance.devise.DevisePerformance](#hexagon.performance.devise.DevisePerformance)
* [hexagon.performance.fonds.FondsPerformance](#hexagon.performance.fonds.FondsPerformance)
* [hexagon.performance.indice.IndiceCompositePerformance](#hexagon.performance.indice.IndiceCompositePerformance)
* [hexagon.performance.indice.IndicePerformance](#hexagon.performance.indice.IndicePerformance)





    
#### Methods


    
##### Method `performance` {#hexagon.performance.datastructures.IPerformanceEngine.performance}



    
> `def performance(self, date_marche: datetime.date, date_reference: datetime.date)`




    
##### Method `performance_by_freq` {#hexagon.performance.datastructures.IPerformanceEngine.performance_by_freq}



    
> `def performance_by_freq(self, debut: datetime.date, fin: datetime.date, freq: hexagon.calendrier.enumerations.FrequenceCalendar)`




    
##### Method `performance_periode` {#hexagon.performance.datastructures.IPerformanceEngine.performance_periode}



    
> `def performance_periode(self, date_marche: datetime.date, periode: str)`




    
##### Method `performance_periode_range` {#hexagon.performance.datastructures.IPerformanceEngine.performance_periode_range}



    
> `def performance_periode_range(self, date_marche: datetime.date, periode: str, freq: hexagon.calendrier.enumerations.FrequenceCalendar)`




    
### Class `PFBenchmark` {#hexagon.performance.datastructures.PFBenchmark}



> `class PFBenchmark(uid: int = -1, code: str = None, description: str = None, fonds: hexagon.core.entities.Cfonds = _Nothing.NOTHING, histo_changements: list[hexagon.performance.datastructures.PFIndiceComposite] = _Nothing.NOTHING)`


Method generated by attrs for class PFBenchmark.


    
#### Ancestors (in MRO)

* [hexagon.core.entities.Cbenchmark](#hexagon.core.entities.Cbenchmark)
* [hexagon.core.entities.CoreEntity](#hexagon.core.entities.CoreEntity)




    
#### Instance variables


    
##### Variable `histo_changements` {#hexagon.performance.datastructures.PFBenchmark.histo_changements}

Return an attribute of instance, which is of type owner.


    
#### Static methods


    
##### `Method of_fonds` {#hexagon.performance.datastructures.PFBenchmark.of_fonds}



    
> `def of_fonds(fonds: hexagon.performance.datastructures.PFFonds, dbsession: sqlalchemy.orm.session.Session, type_benchmark: str = 'COM') -> Self`




    
##### `Method of_fonds_id` {#hexagon.performance.datastructures.PFBenchmark.of_fonds_id}



    
> `def of_fonds_id(id_fonds: int, dbsession: sqlalchemy.orm.session.Session, type_benchmark: str = 'COM') -> Self`




    
##### `Method struct_query` {#hexagon.performance.datastructures.PFBenchmark.struct_query}



    
> `def struct_query() -> sqlalchemy.sql.selectable.Select`





    
### Class `PFDEviseCours` {#hexagon.performance.datastructures.PFDEviseCours}



> `class PFDEviseCours(uid: int = -1, code: str = None, description: str = None, date_marche: datetime.date = None, cours: float = 1, code_devise_locale: str = 'MAD')`


Method generated by attrs for class PFDEviseCours.


    
#### Ancestors (in MRO)

* [hexagon.performance.datastructures.PFDevise](#hexagon.performance.datastructures.PFDevise)
* [hexagon.core.entities.Cdevise](#hexagon.core.entities.Cdevise)
* [hexagon.core.entities.CoreEntity](#hexagon.core.entities.CoreEntity)




    
#### Instance variables


    
##### Variable `code_devise_locale` {#hexagon.performance.datastructures.PFDEviseCours.code_devise_locale}

Return an attribute of instance, which is of type owner.

    
##### Variable `cours` {#hexagon.performance.datastructures.PFDEviseCours.cours}

Return an attribute of instance, which is of type owner.

    
##### Variable `date_marche` {#hexagon.performance.datastructures.PFDEviseCours.date_marche}

Return an attribute of instance, which is of type owner.


    
#### Static methods


    
##### `Method at_date` {#hexagon.performance.datastructures.PFDEviseCours.at_date}



    
> `def at_date(date_marche: datetime.date, dbsession: sqlalchemy.orm.session.Session) -> list[typing.Self]`




    
##### `Method of_code` {#hexagon.performance.datastructures.PFDEviseCours.of_code}



    
> `def of_code(code: str, *, debut=None, fin=None, include: str = 'BOTH', dbsession: sqlalchemy.orm.session.Session) -> list[typing.Self]`




    
##### `Method struct_query` {#hexagon.performance.datastructures.PFDEviseCours.struct_query}



    
> `def struct_query() -> sqlalchemy.sql.selectable.Select`





    
### Class `PFDevise` {#hexagon.performance.datastructures.PFDevise}



> `class PFDevise(uid: int = -1, code: str = None, description: str = None)`


Method generated by attrs for class PFDevise.


    
#### Ancestors (in MRO)

* [hexagon.core.entities.Cdevise](#hexagon.core.entities.Cdevise)
* [hexagon.core.entities.CoreEntity](#hexagon.core.entities.CoreEntity)


    
#### Descendants

* [hexagon.performance.datastructures.PFDEviseCours](#hexagon.performance.datastructures.PFDEviseCours)




    
#### Static methods


    
##### `Method by_code` {#hexagon.performance.datastructures.PFDevise.by_code}



    
> `def by_code(code: str, dbsession: sqlalchemy.orm.session.Session) -> Self`




    
##### `Method by_uid` {#hexagon.performance.datastructures.PFDevise.by_uid}



    
> `def by_uid(uid: int, dbsession: sqlalchemy.orm.session.Session) -> Self`




    
##### `Method devise_locale` {#hexagon.performance.datastructures.PFDevise.devise_locale}



    
> `def devise_locale(dbsession: sqlalchemy.orm.session.Session)`




    
##### `Method load_all` {#hexagon.performance.datastructures.PFDevise.load_all}



    
> `def load_all(dbsession: sqlalchemy.orm.session.Session, filter_by: dict = None) -> list[typing.Self]`




    
##### `Method struct_query` {#hexagon.performance.datastructures.PFDevise.struct_query}



    
> `def struct_query() -> sqlalchemy.sql.selectable.Select`





    
### Class `PFEntity` {#hexagon.performance.datastructures.PFEntity}



> `class PFEntity(uid: int = -1, code: str = None, description: str = None)`


Method generated by attrs for class PFEntity.


    
#### Ancestors (in MRO)

* [hexagon.core.entities.CoreEntity](#hexagon.core.entities.CoreEntity)






    
### Class `PFFonds` {#hexagon.performance.datastructures.PFFonds}



> `class PFFonds(uid: int = -1, code: str = None, description: str = None, code_isin: str = None, categorie: hexagon.core.enumerations.CategorieFonds = OPCVM, periodicite: hexagon.core.enumerations.PeriodiciteVL = H, affectation_resultat: hexagon.core.enumerations.AffectationResultat = MIXTE, type_gestion: hexagon.core.enumerations.TypeGestion = ND, forme_juridique: hexagon.core.enumerations.FormeJuridique = FCP, date_creation: datetime.date = None, date_premiere_vl: datetime.date = None, date_premiere_souscription: datetime.date = None, poids_pct: int = 100, distributions: list[hexagon.performance.datastructures.FondsDistribution] = _Nothing.NOTHING, variables: list[hexagon.performance.datastructures.FondsVariable] = _Nothing.NOTHING)`


Method generated by attrs for class PFFonds.


    
#### Ancestors (in MRO)

* [hexagon.core.entities.Cfonds](#hexagon.core.entities.Cfonds)
* [hexagon.core.entities.CoreEntity](#hexagon.core.entities.CoreEntity)




    
#### Instance variables


    
##### Variable `date_debut_performance` {#hexagon.performance.datastructures.PFFonds.date_debut_performance}

Retourne la date de début de calcul de la performance
Si date_premiere_vl est nulle, retourne date_creation


###### Returns

<code>date</code>
:   Date de debut de calcul de la performance



    
##### Variable `distributions` {#hexagon.performance.datastructures.PFFonds.distributions}

Return an attribute of instance, which is of type owner.

    
##### Variable `is_distribuant` {#hexagon.performance.datastructures.PFFonds.is_distribuant}



    
##### Variable `mode_calendar` {#hexagon.performance.datastructures.PFFonds.mode_calendar}

Retourne le ModeCalendar selon la périodicite du Fonds

    
##### Variable `poids` {#hexagon.performance.datastructures.PFFonds.poids}



    
##### Variable `poids_pct` {#hexagon.performance.datastructures.PFFonds.poids_pct}

Return an attribute of instance, which is of type owner.

    
##### Variable `variables` {#hexagon.performance.datastructures.PFFonds.variables}

Return an attribute of instance, which is of type owner.


    
#### Static methods


    
##### `Method by_code` {#hexagon.performance.datastructures.PFFonds.by_code}



    
> `def by_code(code: str, dbsession: sqlalchemy.orm.session.Session) -> Self`




    
##### `Method by_uid` {#hexagon.performance.datastructures.PFFonds.by_uid}



    
> `def by_uid(uid: int, dbsession: sqlalchemy.orm.session.Session) -> Self`




    
##### `Method load_all` {#hexagon.performance.datastructures.PFFonds.load_all}



    
> `def load_all(dbsession: sqlalchemy.orm.session.Session, filter_by: dict = None) -> list[typing.Self]`




    
##### `Method struct_query` {#hexagon.performance.datastructures.PFFonds.struct_query}



    
> `def struct_query() -> sqlalchemy.sql.selectable.Select`





    
### Class `PFIndice` {#hexagon.performance.datastructures.PFIndice}



> `class PFIndice(uid: int = -1, code: str = None, description: str = None, devise: str = None, poids_pct: int = 100)`


Method generated by attrs for class PFIndice.


    
#### Ancestors (in MRO)

* [hexagon.core.entities.Cindice](#hexagon.core.entities.Cindice)
* [hexagon.core.entities.CoreEntity](#hexagon.core.entities.CoreEntity)




    
#### Instance variables


    
##### Variable `poids` {#hexagon.performance.datastructures.PFIndice.poids}



    
##### Variable `poids_pct` {#hexagon.performance.datastructures.PFIndice.poids_pct}

Return an attribute of instance, which is of type owner.


    
#### Static methods


    
##### `Method by_code` {#hexagon.performance.datastructures.PFIndice.by_code}



    
> `def by_code(code: str, dbsession: sqlalchemy.orm.session.Session) -> Self`




    
##### `Method by_uid` {#hexagon.performance.datastructures.PFIndice.by_uid}



    
> `def by_uid(uid: int, dbsession: sqlalchemy.orm.session.Session) -> Self`




    
##### `Method load_all` {#hexagon.performance.datastructures.PFIndice.load_all}



    
> `def load_all(dbsession: sqlalchemy.orm.session.Session, filter_by: dict = None) -> list[typing.Self]`




    
##### `Method struct_query` {#hexagon.performance.datastructures.PFIndice.struct_query}



    
> `def struct_query() -> sqlalchemy.sql.selectable.Select`





    
### Class `PFIndiceComposite` {#hexagon.performance.datastructures.PFIndiceComposite}



> `class PFIndiceComposite(uid: int = -1, code: str = None, description: str = None, composition: list[hexagon.core.entities.Cindice] = _Nothing.NOTHING, uname: str = 'composite:indice', date_effet: datetime.date = None, date_fin: datetime.date = None)`


Method generated by attrs for class PFIndiceComposite.


    
#### Ancestors (in MRO)

* [hexagon.core.entities.CindiceComposite](#hexagon.core.entities.CindiceComposite)
* [hexagon.core.entities.CoreEntity](#hexagon.core.entities.CoreEntity)




    
#### Instance variables


    
##### Variable `date_effet` {#hexagon.performance.datastructures.PFIndiceComposite.date_effet}

Return an attribute of instance, which is of type owner.

    
##### Variable `date_fin` {#hexagon.performance.datastructures.PFIndiceComposite.date_fin}

Return an attribute of instance, which is of type owner.

    
##### Variable `intervalle_dates` {#hexagon.performance.datastructures.PFIndiceComposite.intervalle_dates}

Retourne l'intervalle si défini de début et de fin de l'indice composite
Property pour les Fonds qui changent de Benchmark au fil du temps.


###### Returns

<code>tuple</code>[`date`, <code>date</code>]: (date_effet, date_fin)

    
##### Variable `is_composite` {#hexagon.performance.datastructures.PFIndiceComposite.is_composite}



    
##### Variable `is_empty` {#hexagon.performance.datastructures.PFIndiceComposite.is_empty}



    
##### Variable `is_simple` {#hexagon.performance.datastructures.PFIndiceComposite.is_simple}



    
##### Variable `uname` {#hexagon.performance.datastructures.PFIndiceComposite.uname}

Return an attribute of instance, which is of type owner.


    
#### Static methods


    
##### `Method by_code` {#hexagon.performance.datastructures.PFIndiceComposite.by_code}



    
> `def by_code(code: str, dbsession: sqlalchemy.orm.session.Session) -> Self`




    
##### `Method by_uid` {#hexagon.performance.datastructures.PFIndiceComposite.by_uid}



    
> `def by_uid(uid: int, dbsession: sqlalchemy.orm.session.Session) -> Self`




    
##### `Method load_all` {#hexagon.performance.datastructures.PFIndiceComposite.load_all}



    
> `def load_all(dbsession: sqlalchemy.orm.session.Session, filter_by: dict = None) -> list[typing.Self]`




    
##### `Method struct_query` {#hexagon.performance.datastructures.PFIndiceComposite.struct_query}



    
> `def struct_query() -> sqlalchemy.sql.selectable.Select`







    
# Module `hexagon.performance.devise` {#hexagon.performance.devise}







    
## Classes


    
### Class `DevisePerformance` {#hexagon.performance.devise.DevisePerformance}



> `class DevisePerformance(devise: hexagon.performance.datastructures.PFDevise, *, calendar: hexagon.calendrier.market.MarketCalendar = None, dbsession: sqlalchemy.orm.session.Session)`


Classse interface pour implémenter les interfaces publiques de calcul de la performance


    
#### Ancestors (in MRO)

* [hexagon.performance.datastructures.IPerformanceEngine](#hexagon.performance.datastructures.IPerformanceEngine)


    
#### Descendants

* [hexagon.performance.devise.DevisePerformanceAMMC](#hexagon.performance.devise.DevisePerformanceAMMC)





    
#### Methods


    
##### Method `decale_date` {#hexagon.performance.devise.DevisePerformance.decale_date}



    
> `def decale_date(self, date: datetime.date) -> datetime.date`


Fonction utilitaire pour exploiter DevisePerformance

Args:

- date (<code>datetime.date</code>): Date considérée comme date marché ou date de valeur


###### Returns

<code>datetime.date</code>: Date décalée d'un Jour selon l'AMMC

    
##### Method `performance` {#hexagon.performance.devise.DevisePerformance.performance}



    
> `def performance(self, date_marche: datetime.date, date_reference: datetime.date, periode: str = None) -> hexagon.performance.aggregates.PerformanceValue`


Retourne un objet PerformanceValue paramétré avec les attributs fournis
La performance est accessible via la property performance.


###### Returns

<code>PerformanceValue</code>: Voir hexagon.performance.base

    
##### Method `performance_by_freq` {#hexagon.performance.devise.DevisePerformance.performance_by_freq}



    
> `def performance_by_freq(self, debut: datetime.date, fin: datetime.date, freq: hexagon.calendrier.enumerations.FrequenceCalendar | str = 'H', periode: str = None) -> list[hexagon.performance.aggregates.PerformanceValue]`


Retourne une serie de performances entre 2 dates incluses selon une fréquence.

Les fréquences permises sont:

- H: Hebdomadaire
- Q: Quotidienne
- M: Mensuelle
- T: Trimestrielle
- S: Semestrielle
- A: Annuelle

Args:

- debut (<code>datetime.date</code>): date de début
- fin (<code>datetime.date</code>): date de fin
- freq (<code>str</code>, H): default to H


###### Returns

<code>list</code>[`PerformanceValue`]: Liste d'instances PerformanceValue

    
##### Method `performance_periode` {#hexagon.performance.devise.DevisePerformance.performance_periode}



    
> `def performance_periode(self, date_marche: datetime.date, periode: str) -> hexagon.performance.aggregates.PerformanceValue`


Retourne la performance de l'indice pour la période fournie

Args:

- date_marche (<code>datetime.date</code>): Date de marché évaluation de la performance
- periode (<code>str</code>): période définie par 'Nombre:Periode' ex '3:M', '2:A'


###### Returns

<code>PerformanceValue</code>: Objet qui calcule la perf selon le mode demandé

    
##### Method `performance_periode_range` {#hexagon.performance.devise.DevisePerformance.performance_periode_range}



    
> `def performance_periode_range(self, date_marche: datetime.date, periode: str, freq='H') -> hexagon.performance.aggregates.PerformanceCollection`




    
### Class `DevisePerformanceAMMC` {#hexagon.performance.devise.DevisePerformanceAMMC}



> `class DevisePerformanceAMMC(devise: hexagon.performance.datastructures.PFDevise, *, calendar: hexagon.calendrier.market.MarketCalendar = None, dbsession: sqlalchemy.orm.session.Session)`


Classse interface pour implémenter les interfaces publiques de calcul de la performance


    
#### Ancestors (in MRO)

* [hexagon.performance.devise.DevisePerformance](#hexagon.performance.devise.DevisePerformance)
* [hexagon.performance.datastructures.IPerformanceEngine](#hexagon.performance.datastructures.IPerformanceEngine)








    
# Module `hexagon.performance.enumerations` {#hexagon.performance.enumerations}







    
## Classes


    
### Class `BasePeriodes` {#hexagon.performance.enumerations.BasePeriodes}



> `class BasePeriodes(value, names=None, *, module=None, qualname=None, type=None, start=1, boundary=None)`


Enum where members are also (and must be) ints


    
#### Ancestors (in MRO)

* [enum.IntEnum](#enum.IntEnum)
* [builtins.int](#builtins.int)
* [enum.ReprEnum](#enum.ReprEnum)
* [enum.Enum](#enum.Enum)



    
#### Class variables


    
##### Variable `BASEA` {#hexagon.performance.enumerations.BasePeriodes.BASEA}



    
##### Variable `BASEH` {#hexagon.performance.enumerations.BasePeriodes.BASEH}



    
##### Variable `BASEJO` {#hexagon.performance.enumerations.BasePeriodes.BASEJO}



    
##### Variable `BASEM` {#hexagon.performance.enumerations.BasePeriodes.BASEM}






    
### Class `TypeFraction` {#hexagon.performance.enumerations.TypeFraction}



> `class TypeFraction(value, names=None, *, module=None, qualname=None, type=None, start=1, boundary=None)`


Enum where members are also (and must be) strings


    
#### Ancestors (in MRO)

* [enum.StrEnum](#enum.StrEnum)
* [builtins.str](#builtins.str)
* [enum.ReprEnum](#enum.ReprEnum)
* [enum.Enum](#enum.Enum)



    
#### Class variables


    
##### Variable `ARITHMETIQUE` {#hexagon.performance.enumerations.TypeFraction.ARITHMETIQUE}



    
##### Variable `GEOMETRIQUE` {#hexagon.performance.enumerations.TypeFraction.GEOMETRIQUE}








    
# Module `hexagon.performance.exceptions` {#hexagon.performance.exceptions}







    
## Classes


    
### Class `PerformanceDateNotFound` {#hexagon.performance.exceptions.PerformanceDateNotFound}



> `class PerformanceDateNotFound(...)`


Valeur non disponible à une date demandée pour le calcul de la performance


    
#### Ancestors (in MRO)

* [hexagon.performance.exceptions.PerformanceError](#hexagon.performance.exceptions.PerformanceError)
* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)






    
### Class `PerformanceError` {#hexagon.performance.exceptions.PerformanceError}



> `class PerformanceError(...)`


Base Class for Performance related calculus


    
#### Ancestors (in MRO)

* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)


    
#### Descendants

* [hexagon.performance.exceptions.PerformanceDateNotFound](#hexagon.performance.exceptions.PerformanceDateNotFound)
* [hexagon.performance.exceptions.PerformanceRepositryError](#hexagon.performance.exceptions.PerformanceRepositryError)





    
### Class `PerformanceRepositryError` {#hexagon.performance.exceptions.PerformanceRepositryError}



> `class PerformanceRepositryError(...)`


Exception générique du package Performance pour le module du repository


    
#### Ancestors (in MRO)

* [hexagon.performance.exceptions.PerformanceError](#hexagon.performance.exceptions.PerformanceError)
* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)








    
# Module `hexagon.performance.fonds` {#hexagon.performance.fonds}

Méthodes pour construire les performances des OPCVM gérés.





    
## Classes


    
### Class `FondsPerformance` {#hexagon.performance.fonds.FondsPerformance}



> `class FondsPerformance(fonds: hexagon.performance.datastructures.PFFonds | int, dbsession: sqlalchemy.orm.session.Session, mode_calendar: hexagon.calendrier.enumerations.ModeCalendar = VL, *, adjust_distrib: bool = True, adjust_variable: bool = False, calendar: hexagon.performance.calendar_fonds.CalendarFonds = None)`


Classse interface pour implémenter les interfaces publiques de calcul de la performance


    
#### Ancestors (in MRO)

* [hexagon.performance.datastructures.IPerformanceEngine](#hexagon.performance.datastructures.IPerformanceEngine)




    
#### Instance variables


    
##### Variable `pf_entity` {#hexagon.performance.fonds.FondsPerformance.pf_entity}




    
#### Static methods


    
##### `Method from_repository` {#hexagon.performance.fonds.FondsPerformance.from_repository}



    
> `def from_repository(fonds_uid: int | str, dbsession: sqlalchemy.orm.session.Session, mode_calendar: hexagon.calendrier.enumerations.ModeCalendar = VL, *, adjust_distrib: bool = True, adjust_variable: bool = False, calendar: hexagon.performance.calendar_fonds.CalendarFonds = None) -> Self`





    
#### Methods


    
##### Method `performance` {#hexagon.performance.fonds.FondsPerformance.performance}



    
> `def performance(self, date_marche: datetime.date, date_reference: datetime.date, periode: str = None) -> hexagon.performance.aggregates.PerformanceSpot`


Retourne un objet PerformanceValue paramétré avec les attributs fournis
La performance est accessible via la property performance.

Args:

- date_marche (<code>datetime.date</code>): date de fin
- date_reference (<code>datetime.date</code>): date de début
- periode (<code>str</code>, None): Période de ces dates permet de tagger un intervalle de dates


###### Returns

<code>PerformanceSpot</code>: Voir hexagon.performance.base

    
##### Method `performance_by_freq` {#hexagon.performance.fonds.FondsPerformance.performance_by_freq}



    
> `def performance_by_freq(self, debut: datetime.date, fin: datetime.date, freq: str | hexagon.calendrier.enumerations.FrequenceCalendar = None, periode: str = None) -> hexagon.performance.aggregates.PerformanceCollection`


Retourne une serie de performances entre 2 dates incluses selon une fréquence.

Les fréquences permises sont:

- H: Hebdomadaire
- Q: Quotidienne
- M: Mensuelle
- T: Trimestrielle
- S: Semestrielle
- A: Annuelle

Args:

- debut (<code>datetime.date</code>): date de début
- fin (<code>datetime.date</code>): date de fin
- freq (<code>str</code>, <code>VL</code>): si non renseignée prends la périodicité du Fonds


###### Returns

<code>PerformanceCollection</code>: Liste des performances avec les dates et périodes

    
##### Method `performance_by_freq_unadjusted` {#hexagon.performance.fonds.FondsPerformance.performance_by_freq_unadjusted}



    
> `def performance_by_freq_unadjusted(self, debut: datetime.date, fin: datetime.date, freq: str | hexagon.calendrier.enumerations.FrequenceCalendar = None, periode: str = None)`




    
##### Method `performance_periode` {#hexagon.performance.fonds.FondsPerformance.performance_periode}



    
> `def performance_periode(self, date_marche: datetime.date, periode: str) -> hexagon.performance.aggregates.PerformanceSpot`


Calcule la performance du Fonds sur une période donnée, définie selon le jargon ASFIM et spécification Hexagon.

L'ASFIM définit les périodes selon la fréquence VL des OPCVM (Hebdomadaire, Quotidien)
Spécification Hexagon: Hexagon apporte une nomenclature des périodictés selon la forme suivante <Nombre:PasFréquence>.
Avec PasFrequence un des choix: (J: Jours, S: Semaines, M: Mois, A: Années)

Args:

- date_marche (<code>datetime.date</code>): Date de marché/valeur
- periode (<code>str</code>): Période voulue selon le format hexagon: 3:A, 2:M, 6:M, 1:S, 1:J, 10:S (10 semaines), ...


###### Returns

<code>PerformanceSpot</code>: Performance object avec date_marche, date_reference et performance calculée

    
##### Method `performance_periode_range` {#hexagon.performance.fonds.FondsPerformance.performance_periode_range}



    
> `def performance_periode_range(self, date_marche: datetime.date, periode: str, freq: str | hexagon.calendrier.enumerations.FrequenceCalendar = 'H') -> hexagon.performance.aggregates.PerformanceCollection`






    
# Module `hexagon.performance.indice` {#hexagon.performance.indice}

Méthodes pour construire les performances des indices du marché.





    
## Classes


    
### Class `IndiceCompositePerformance` {#hexagon.performance.indice.IndiceCompositePerformance}



> `class IndiceCompositePerformance(indice_composite: hexagon.performance.datastructures.PFIndiceComposite, dbsession: sqlalchemy.orm.session.Session, mode_calendar: hexagon.calendrier.enumerations.ModeCalendar = H, calendar: hexagon.calendrier.market.MarketCalendar = None)`


Classse interface pour implémenter les interfaces publiques de calcul de la performance


    
#### Ancestors (in MRO)

* [hexagon.performance.datastructures.IPerformanceEngine](#hexagon.performance.datastructures.IPerformanceEngine)




    
#### Instance variables


    
##### Variable `pf_entity` {#hexagon.performance.indice.IndiceCompositePerformance.pf_entity}




    
#### Static methods


    
##### `Method clear_cache` {#hexagon.performance.indice.IndiceCompositePerformance.clear_cache}



    
> `def clear_cache()`





    
#### Methods


    
##### Method `performance` {#hexagon.performance.indice.IndiceCompositePerformance.performance}



    
> `def performance(self, date_marche: datetime.date, date_reference: datetime.date, periode: str = None) -> hexagon.performance.aggregates.PerformanceSpot`


Retourne un objet PerformanceSpot paramétré avec les attributs fournis
La performance est accessible via la property performance.


###### Returns

<code>PerformanceSpot</code>: Voir hexagon.performance.base.PerformanceSpot

    
##### Method `performance_by_freq` {#hexagon.performance.indice.IndiceCompositePerformance.performance_by_freq}



    
> `def performance_by_freq(self, debut: datetime.date, fin: datetime.date, freq: hexagon.calendrier.enumerations.FrequenceCalendar | str = 'H', periode: str = None) -> hexagon.performance.aggregates.PerformanceCollection`


Retourne une serie de performances entre 2 dates incluses selon une fréquence.

Les fréquences permises sont:

- H: Hebdomadaire
- Q: Quotidienne
- M: Mensuelle
- T: Trimestrielle
- S: Semestrielle
- A: Annuelle

Args:

- debut (<code>datetime.date</code>): date de début
- fin (<code>datetime.date</code>): date de fin
- freq (str, optional): default to H


###### Returns

<code>PerformanceCollection</code>: Liste d'instances PerformanceValue

    
##### Method `performance_periode` {#hexagon.performance.indice.IndiceCompositePerformance.performance_periode}



    
> `def performance_periode(self, date_marche: datetime.date, periode: str) -> hexagon.performance.aggregates.PerformanceSpot`


Retourne la performance de l'indice pour la période fournie

Args:

- date_marche (<code>datetime.date</code>): Date de marché évaluation de la performance
- periode (<code>str</code>): période définie par 'Nombre:Periode' ex '3:M', '2:A'


###### Returns

<code>PerformanceValue</code>: Objet qui calcule la perf selon le mode demandé

    
##### Method `performance_periode_range` {#hexagon.performance.indice.IndiceCompositePerformance.performance_periode_range}



    
> `def performance_periode_range(self, date_marche: datetime.date, periode: str, freq: hexagon.calendrier.enumerations.FrequenceCalendar | str = 'H') -> hexagon.performance.aggregates.PerformanceCollection`




    
### Class `IndicePerformance` {#hexagon.performance.indice.IndicePerformance}



> `class IndicePerformance(indice: hexagon.performance.datastructures.PFIndice, dbsession: sqlalchemy.orm.session.Session, mode_calendar: hexagon.calendrier.enumerations.ModeCalendar | str = H, *, calendar: hexagon.calendrier.market.MarketCalendar = None)`


Classse interface pour implémenter les interfaces publiques de calcul de la performance


    
#### Ancestors (in MRO)

* [hexagon.performance.datastructures.IPerformanceEngine](#hexagon.performance.datastructures.IPerformanceEngine)




    
#### Instance variables


    
##### Variable `pf_entity` {#hexagon.performance.indice.IndicePerformance.pf_entity}




    
#### Static methods


    
##### `Method from_init` {#hexagon.performance.indice.IndicePerformance.from_init}



    
> `def from_init(indice: hexagon.performance.datastructures.PFIndice, *, calendar: hexagon.calendrier.market.MarketCalendar = None, mode_calendar: hexagon.calendrier.enumerations.ModeCalendar | str = H, dbsession: sqlalchemy.orm.session.Session)`





    
#### Methods


    
##### Method `clear_cache` {#hexagon.performance.indice.IndicePerformance.clear_cache}



    
> `def clear_cache()`




    
##### Method `performance` {#hexagon.performance.indice.IndicePerformance.performance}



    
> `def performance(self, date_marche: datetime.date, date_reference: datetime.date, periode: str = None) -> hexagon.performance.aggregates.PerformanceValue`


Retourne un objet PerformanceValue paramétré avec les attributs fournis
La performance est accessible via la property performance.


###### Returns

<code>PerformanceValue</code>: Voir hexagon.performance.base

    
##### Method `performance_by_freq` {#hexagon.performance.indice.IndicePerformance.performance_by_freq}



    
> `def performance_by_freq(self, debut: datetime.date, fin: datetime.date, freq: hexagon.calendrier.enumerations.FrequenceCalendar | str = 'H', periode: str = None) -> hexagon.performance.aggregates.PerformanceCollection`


Retourne une serie de performances entre 2 dates incluses selon une fréquence.

Les fréquences permises sont:

- H: Hebdomadaire
- Q: Quotidienne
- M: Mensuelle
- T: Trimestrielle
- S: Semestrielle
- A: Annuelle

Args:

- debut (<code>datetime.date</code>): date de début
- fin (<code>datetime.date</code>): date de fin
- freq (<code>str</code>, H): default to H


###### Returns

<code>PerformanceCollection</code>: Liste d'instances PerformanceValue

    
##### Method `performance_periode` {#hexagon.performance.indice.IndicePerformance.performance_periode}



    
> `def performance_periode(self, date_marche: datetime.date, periode: str) -> hexagon.performance.aggregates.PerformanceValue`


Retourne la performance de l'indice pour la période fournie

Args:

- date_marche (<code>datetime.date</code>): Date de marché évaluation de la performance
- periode (<code>str</code>): période définie par 'Nombre:Periode' ex '3:M', '2:A'


###### Returns

<code>PerformanceValue</code>: Objet qui calcule la perf selon le mode demandé

    
##### Method `performance_periode_range` {#hexagon.performance.indice.IndicePerformance.performance_periode_range}



    
> `def performance_periode_range(self, date_marche: datetime.date, periode: str, freq: hexagon.calendrier.enumerations.FrequenceCalendar | str = 'H') -> hexagon.performance.aggregates.PerformanceCollection`






    
# Module `hexagon.performance.repository` {#hexagon.performance.repository}

Premier test de l'approche du REpository Pattern par layers (Onion layers)





    
## Classes


    
### Class `OperationPassifRepository` {#hexagon.performance.repository.OperationPassifRepository}



> `class OperationPassifRepository(dbsession: sqlalchemy.orm.session.Session)`





    
#### Ancestors (in MRO)

* [hexagon.infrastructure.query_executor.QueryExecutor](#hexagon.infrastructure.query_executor.QueryExecutor)






    
#### Methods


    
##### Method `build_fonds_pf` {#hexagon.performance.repository.OperationPassifRepository.build_fonds_pf}



    
> `def build_fonds_pf(self, fonds_uid: str | int | hexagon.performance.datastructures.PFFonds, variable: bool = False) -> hexagon.performance.datastructures.PFFonds`




    
##### Method `build_operation_vl_mapping` {#hexagon.performance.repository.OperationPassifRepository.build_operation_vl_mapping}



    
> `def build_operation_vl_mapping(self, fonds_uid: int, poste_operation: str, mode_calendar: hexagon.calendrier.enumerations.ModeCalendar, debut=None, fin=None, include=BOTH)`




    
##### Method `load_dates_operation` {#hexagon.performance.repository.OperationPassifRepository.load_dates_operation}



    
> `def load_dates_operation(self, fonds_uid: int, poste_operation: str, debut=None, fin=None, include=BOTH)`




    
### Class `PerformanceRepository` {#hexagon.performance.repository.PerformanceRepository}



> `class PerformanceRepository(dbsession: sqlalchemy.orm.session.Session)`





    
#### Ancestors (in MRO)

* [hexagon.infrastructure.query_executor.QueryExecutor](#hexagon.infrastructure.query_executor.QueryExecutor)






    
#### Methods


    
##### Method `load_dates_devise` {#hexagon.performance.repository.PerformanceRepository.load_dates_devise}



    
> `def load_dates_devise(self, devise: hexagon.performance.datastructures.PFDevise, debut: datetime.date = None, fin: datetime.date = None, include: hexagon.utils.enumerations.Include | str = 'BOTH') -> list[datetime.date]`




    
##### Method `load_dates_fonds` {#hexagon.performance.repository.PerformanceRepository.load_dates_fonds}



    
> `def load_dates_fonds(self, fonds: hexagon.performance.datastructures.PFFonds, debut: datetime.date = None, fin: datetime.date = None, include: hexagon.utils.enumerations.Include | str = 'BOTH') -> list[datetime.date]`




    
##### Method `load_dates_indice` {#hexagon.performance.repository.PerformanceRepository.load_dates_indice}



    
> `def load_dates_indice(self, indice: hexagon.performance.datastructures.PFIndice, debut: datetime.date = None, fin: datetime.date = None, include: hexagon.utils.enumerations.Include | str = 'BOTH') -> list[datetime.date]`




    
##### Method `load_etat_devise_values` {#hexagon.performance.repository.PerformanceRepository.load_etat_devise_values}



    
> `def load_etat_devise_values(self, devise: hexagon.performance.datastructures.PFDevise, dates_list: collections.abc.Iterable[datetime.date], debut: datetime.date = None, fin: datetime.date = None, include: hexagon.utils.enumerations.Include | str = 'BOTH') -> list[sqlalchemy.engine.row.Row[datetime.date, float]]`




    
##### Method `load_etat_fonds_values` {#hexagon.performance.repository.PerformanceRepository.load_etat_fonds_values}



    
> `def load_etat_fonds_values(self, fonds: hexagon.performance.datastructures.PFFonds, dates_list: collections.abc.Iterable[datetime.date], debut: datetime.date = None, fin: datetime.date = None, include: hexagon.utils.enumerations.Include | str = 'BOTH') -> list[sqlalchemy.engine.row.Row[datetime.date, float]]`




    
##### Method `load_indice_values` {#hexagon.performance.repository.PerformanceRepository.load_indice_values}



    
> `def load_indice_values(self, indice: hexagon.performance.datastructures.PFIndice, dates_list: collections.abc.Iterable[datetime.date], debut: datetime.date = None, fin: datetime.date = None, include: hexagon.utils.enumerations.Include | str = 'BOTH') -> list[sqlalchemy.engine.row.Row[datetime.date, float]]`




    
##### Method `load_performance_value_devise` {#hexagon.performance.repository.PerformanceRepository.load_performance_value_devise}



    
> `def load_performance_value_devise(self, devise: hexagon.performance.datastructures.PFDevise, date_marche: datetime.date, date_reference: datetime.date, periode: str = None) -> hexagon.performance.aggregates.PerformanceValue`




    
##### Method `load_performance_value_fonds` {#hexagon.performance.repository.PerformanceRepository.load_performance_value_fonds}



    
> `def load_performance_value_fonds(self, fonds: hexagon.performance.datastructures.PFFonds, date_marche: datetime.date, date_reference: datetime.date, periode: str = None) -> hexagon.performance.aggregates.PerformanceValue`




    
##### Method `load_performance_value_indice` {#hexagon.performance.repository.PerformanceRepository.load_performance_value_indice}



    
> `def load_performance_value_indice(self, indice: hexagon.performance.datastructures.PFIndice, date_marche: datetime.date, date_reference: datetime.date, periode: str = None) -> hexagon.performance.aggregates.PerformanceValue`






    
# Module `hexagon.repository` {#hexagon.repository}

Package de Repository wannabe. Mais efficace pour l'instant

Repository connait un changement radical maintenant il est plus judicieux de diluer
le concept au niveau des domaines <Nobody wants your data!>

# The dawn of Repository Pattern in Hexagon

Avec le temps, il est devenu apparent qu’une modélisation orientée "Repository Pattern" s’avère nécessaire.

Nous adopterons le schéma suivant:

## Repository Infrastructure (R.I):

Ce repository est le plus abstrait et aura comme responsabilité la communication avec les systèmes de persistance
tels que les SGBD, Filesystem, Cloud Storage ou Memory (à la Sqlite).

Le R.I doit être en mesure de faire la séparation entre les différents types de requêtes SELECT, INSERT, UPDATE, DELETE.

Ce repository doit offrir les interfaces publiques suivantes:

- execute_select: fonction qui execute les requêtes de selection/lecture
- execute_insert: fonction qui execute les requêtes/commandes d’insertion/création
- execute_update: fonction qui execute les requêtes/commandes de mise à jour
- execute_delete: fonction qui execute les requêtes/commandes de suppression des enregistrements
- execute_query: fonction générique de dispatch aux autres méthodes

Ces méthodes seront transverses à l’ensemble des implémentations concrètes qui seront utilisées au niveau des différents Domaines ou Modules.

## Selector Repository Domain (S.R.D):

Ce type de repository (S.R.D) sera responsable de construire les requêtes de base pour la création des Entités et des Value Objects du Domaine/Module en question.

Aussi, nous mettrons les filtres par défaut à ce niveau là.

Par exemple: 
    
    une table qui contient des Users avec un flag booléen "is_actif" sera toujours "True"
    si le Domaine/Module ne traite que les Users "Actifs"

Il est préférable d’avoir le mot 'select_' pour les méthodes du S.R.D

Ce repository sera très lourd dans la mesure où il devra gérer l’ensemble des paramètres candidats pour les différents scénarios.

## Repository Domain (R.D):

Enfin on arrive au repository tel que défini par le concept du D.D.D
Ce type de repository sera responsable de charger les instances des Entités/ValueObjects telles que définies par le Domain/Module.

Les méthodes seront explicitement nommées en se conformant au **"Jargon"** du Domaine Métier

tout en conveyant un sens unique sans ambiguïté quant à sa fonction.

A ce niveau là, nous serons peut être temptés par le DRY mais ce sera une mauvaise idée sur le long terme.

L’alternative sera d’écrire autant de méthodes et de fonctions que des nuances des concepts présentés par le Domaine.


## Exemple


load_new_users, load_updated_users: Dans ce cas d’étude, seulement le champs utilisé comme filtre va différer mais nous devons écrire ces Fonctions.
Le DRY est déjà inclus dans le S.R.D qui offrira le paramètre adéquat.

## Schema conceptuel:

L'implémentation adoptée pour Hexagon est basée/adaptée sur les possibilités qu'offre Python et spécialement sqlalchemy
C'est très concret pour l'instant, l'idée est de valider le design et de tester son scaling sur le temps.
C'est le bon moment vu que toute manière nous étions obligés de refactorer la couche d'accès à un système de persistance.

La route expérimentale (dans un environnement maitrisé a été favorisée dans ce chantier)

Le design Actuel est d'exploiter les inner functions de sqlalchemy afin de pouvoir monter un système sur 3 couches conceptuelles du "**Repository Pattern**"

#### Points Importants:

Cette implémentation prend comme acquis certaines fonctionnalités ou propriétés de sqlalchemy, la liste ci-dessous explique le pourquoi de la chose:

 - Possibilité de générer des requêtes en **PUR SQL** et selon le dialect choisi de la BDD (Postgres, Sqlite, Oracle, ...)
 - Possibilité de définir des requêtes indépendemment de la session ou de l'Engine
 - Possibilité de définir des schémas AD-Hoc à partir seulement d'un MetaData
 - La propriété du context manager lors de l'execution des requêtes
 - La propriété de composabilité interne de l'objet Query que ça soit un Select ou Update, etc
 - La propriété de modifier/construire l'objet query en mode **pipeline**
 - Une abondance de hooks de contrôle du flux d'exécution des requêtes via **l'Event Systèm** offert
 - Un design et un niveau de maintenance avangardiste par rapport à l'ensemble des ORM de tous langages de programmation confondus
 - Une suite de tests solide
 - Unit of Work pattern par session

#### Représentation sommaire (UML):

```mermaid
classDiagram
    RepositoryInfrastructure<|-- SelectorRepositoryDomain
    SelectorRepositoryDomain <|-- RepositoryDomain
    class RepositoryInfrastructure{
      +Session dbsession
      +Logger logger
      +str name = "R.I"
      +execute_select(query: Select)
      +execute_insert(query: Insert)
      +execute_update(query: Update)
      +execute_delete(query: Delete)
      +execute_query(query: Any)
    }
    class SelectorRepositoryDomain{
      +Session dbsession
      +Logger logger
      +str name = "S.R.D"
      +select_alion()
    }
    class RepositoryDomain{
      +Session dbsession
      +Logger logger
      +str name = "R.D"
      +load_lion_of_atlas()
    }
```


    
## Sub-modules

* [hexagon.repository.exceptions](#hexagon.repository.exceptions)
* [hexagon.repository.loader_inventaire](#hexagon.repository.loader_inventaire)
* [hexagon.repository.loader_operation](#hexagon.repository.loader_operation)
* [hexagon.repository.referentiel](#hexagon.repository.referentiel)
* [hexagon.repository.store](#hexagon.repository.store)






    
# Module `hexagon.repository.exceptions` {#hexagon.repository.exceptions}







    
## Classes


    
### Class `CotationDateError` {#hexagon.repository.exceptions.CotationDateError}



> `class CotationDateError(...)`


Common base class for all non-exit exceptions.


    
#### Ancestors (in MRO)

* [hexagon.repository.exceptions.CotationError](#hexagon.repository.exceptions.CotationError)
* [hexagon.repository.exceptions.RepositoryError](#hexagon.repository.exceptions.RepositoryError)
* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)






    
### Class `CotationError` {#hexagon.repository.exceptions.CotationError}



> `class CotationError(...)`


Common base class for all non-exit exceptions.


    
#### Ancestors (in MRO)

* [hexagon.repository.exceptions.RepositoryError](#hexagon.repository.exceptions.RepositoryError)
* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)


    
#### Descendants

* [hexagon.repository.exceptions.CotationDateError](#hexagon.repository.exceptions.CotationDateError)





    
### Class `ReferentielError` {#hexagon.repository.exceptions.ReferentielError}



> `class ReferentielError(...)`


Common base class for all non-exit exceptions.


    
#### Ancestors (in MRO)

* [hexagon.repository.exceptions.StoreError](#hexagon.repository.exceptions.StoreError)
* [hexagon.repository.exceptions.RepositoryError](#hexagon.repository.exceptions.RepositoryError)
* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)






    
### Class `RepositoryError` {#hexagon.repository.exceptions.RepositoryError}



> `class RepositoryError(...)`


Common base class for all non-exit exceptions.


    
#### Ancestors (in MRO)

* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)


    
#### Descendants

* [hexagon.repository.exceptions.CotationError](#hexagon.repository.exceptions.CotationError)
* [hexagon.repository.exceptions.StoreError](#hexagon.repository.exceptions.StoreError)





    
### Class `StoreError` {#hexagon.repository.exceptions.StoreError}



> `class StoreError(...)`


Common base class for all non-exit exceptions.


    
#### Ancestors (in MRO)

* [hexagon.repository.exceptions.RepositoryError](#hexagon.repository.exceptions.RepositoryError)
* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)


    
#### Descendants

* [hexagon.repository.exceptions.ReferentielError](#hexagon.repository.exceptions.ReferentielError)
* [hexagon.repository.referentiel.ReferentielError](#hexagon.repository.referentiel.ReferentielError)







    
# Module `hexagon.repository.loader_inventaire` {#hexagon.repository.loader_inventaire}







    
## Classes


    
### Class `InventaireLoader` {#hexagon.repository.loader_inventaire.InventaireLoader}



> `class InventaireLoader(fonds: hexagon.infrastructure.db.models.opcvm.Fonds, session: sqlalchemy.orm.session.Session, *, labelize: bool = True)`





    
#### Ancestors (in MRO)

* [hexagon.infrastructure.query_executor.QueryExecutor](#hexagon.infrastructure.query_executor.QueryExecutor)






    
#### Methods


    
##### Method `load_date` {#hexagon.repository.loader_inventaire.InventaireLoader.load_date}



    
> `def load_date(self, date_marche: datetime.date, *, titre_classe: list[str] = None, titre_categorie: list[str] = None) -> list[sqlalchemy.engine.row.Row]`


Charge l'inventaire à une date donnée.


###### Args

**```date_marche```** :&ensp;<code>date</code>
:   Description



    
##### Method `load_date_list` {#hexagon.repository.loader_inventaire.InventaireLoader.load_date_list}



    
> `def load_date_list(self, date_list: list[datetime.date], *, titre_classe: list[str] = None, titre_categorie: list[str] = None) -> list[sqlalchemy.engine.row.Row]`




    
##### Method `load_date_range` {#hexagon.repository.loader_inventaire.InventaireLoader.load_date_range}



    
> `def load_date_range(self, debut: datetime.date, fin: datetime.date, *, titre_classe: list[str] = None, titre_categorie: list[str] = None) -> list[sqlalchemy.engine.row.Row]`


Function Summary


###### Args

**```debut```** :&ensp;<code>date</code>
:   date de début, incluse


**```fin```** :&ensp;<code>date</code>
:   date de fin, incluse



###### Returns

<code>list\[Row]</code>
:   Collection de Rows resultat de la requete.



    
##### Method `qselect_inventaire` {#hexagon.repository.loader_inventaire.InventaireLoader.qselect_inventaire}



    
> `def qselect_inventaire(self, date_marche: datetime.date | list[datetime.date], date_reference: datetime.date = None, *, titre_classe: list[str] = None, titre_categorie: list[str] = None)`


Vue d'inventaire d'un Fonds passé en paramètres


###### Returns

<code>Select</code>
:   Select object Pour des filtres ultérieurs





    
# Module `hexagon.repository.loader_operation` {#hexagon.repository.loader_operation}







    
## Classes


    
### Class `OperationLoader` {#hexagon.repository.loader_operation.OperationLoader}



> `class OperationLoader(fonds: hexagon.infrastructure.db.models.opcvm.Fonds, dbsession: sqlalchemy.orm.session.Session, *, labelize: bool = True)`


Classe regroupant les requêtes Business Logic pour l'inventaire OPCVM.


#### Attributes

**```fonds```** :&ensp;<code>db.Fonds</code>
:   Instance du Fonds concerné


include (str='RIGHT'): Inclut la borne droite de l'intervalle du temps (debut, fin) pour l'historique
**```labelize```** :&ensp;`bool=True`
:   Labelise automatiquement les champs selon la forme canonique


**```OP_FIELDS```** :&ensp;<code>list\[InstrumentedAttribute]</code>
:   Description



#### Inherits

session (Session): Session associée au loader


    
#### Ancestors (in MRO)

* [hexagon.infrastructure.query_executor.QueryExecutor](#hexagon.infrastructure.query_executor.QueryExecutor)



    
#### Class variables


    
##### Variable `OP_FIELDS` {#hexagon.repository.loader_operation.OperationLoader.OP_FIELDS}






    
#### Methods


    
##### Method `historique` {#hexagon.repository.loader_operation.OperationLoader.historique}



    
> `def historique(self, debut: datetime.date, fin: datetime.date, *, poste_operation: list[str] = None) -> list[sqlalchemy.engine.row.Row]`


Retourne l'historique de l'ensemble des opérations de l'OPCVM présentes dans la BDD
filtrer par poste_operation


###### Args

**```debut```** :&ensp;<code>date</code>
:   Date de début (exclue)


**```fin```** :&ensp;<code>date</code>
:   Date de fin (incluse)


**```poste_operation```** :&ensp;<code>list\[str]</code>, optional
:   Listes des postes des opérations voulues



###### Returns

<code>list\[Row]</code>
:   Liste des opérations [Row]



    
##### Method `operation_differee` {#hexagon.repository.loader_operation.OperationLoader.operation_differee}



    
> `def operation_differee(self, date_marche: datetime.date, *, poste_operation: list[str] = None) -> list[sqlalchemy.engine.row.Row]`


Charge les Opérations différées RL > +1


###### Args

**```date_marche```** :&ensp;<code>date</code>
:   Date de marché/valeur


**```poste_operation```** :&ensp;<code>list\[str]</code>, optional
:   Liste des Postes des opérations pour filtre



###### Returns

<code>list\[Row]</code>
:   Liste des opérations [Row]



    
##### Method `operation_encours` {#hexagon.repository.loader_operation.OperationLoader.operation_encours}



    
> `def operation_encours(self, date_marche: datetime.date, *, poste_operation: list[str] = None) -> list[sqlalchemy.engine.row.Row]`


Retourne la liste des opérations encours à une date donnée


###### Args

**```date_marche```** :&ensp;<code>date</code>
:   Date de marché/valeur


**```poste_operation```** :&ensp;<code>list\[str]</code>, optional
:   Liste des Postes des opérations pour filtre



###### Returns

<code>list\[Row]</code>
:   Liste des opérations [Row]



    
##### Method `transactions` {#hexagon.repository.loader_operation.OperationLoader.transactions}



    
> `def transactions(self, debut: datetime.date, fin: datetime.date, *, poste_operation: list[str] = None) -> list[sqlalchemy.engine.row.Row]`


Retourne les opérations ayant ACHAT et VENTE dans leurs descriptifs respectifs.


###### Args

**```debut```** :&ensp;<code>date</code>
:   Date de début


**```fin```** :&ensp;<code>date</code>
:   Date de fin



###### Returns

<code>list\[Row]</code>
:   Liste des Opérations fermes et temporaires entre 2 dates.





    
# Module `hexagon.repository.referentiel` {#hexagon.repository.referentiel}

Classe Simple de Mapping des tables referentiels par son id, code ou description, ...





    
## Classes


    
### Class `ReferentielBenchmark` {#hexagon.repository.referentiel.ReferentielBenchmark}



> `class ReferentielBenchmark(dbsession, pkey: str = 'id')`


Repository des tables reférentiel, implémente une seule méthode d'extraction des données
du referentiel avec une limite applicative sur les filtres et la structure.


#### Args

**```dbsession```** :&ensp;<code>Session</code>
:   Session Sqlalchemy


**```db_model```** :&ensp;<code>db.BaseEntity</code>
:   Class Model


**```pkey```** :&ensp;<code>str</code>, optional
:   Primary key attribute



#### Raises

<code>[ReferentielError](#hexagon.repository.referentiel.ReferentielError "hexagon.repository.referentiel.ReferentielError")</code>
:   Si la table n'est pas une Table reférentiel


`Si le pkey n'est pas un attribut valide`
:   &nbsp;


`Si le modèle n'est pas un modèle défini au niveau d'Hexagon`
:   &nbsp;




    
#### Ancestors (in MRO)

* [hexagon.repository.referentiel.ReferentielRepository](#hexagon.repository.referentiel.ReferentielRepository)
* [hexagon.infrastructure.query_executor.QueryExecutor](#hexagon.infrastructure.query_executor.QueryExecutor)






    
### Class `ReferentielDevise` {#hexagon.repository.referentiel.ReferentielDevise}



> `class ReferentielDevise(dbsession, pkey: str = 'id')`


Repository des tables reférentiel, implémente une seule méthode d'extraction des données
du referentiel avec une limite applicative sur les filtres et la structure.


#### Args

**```dbsession```** :&ensp;<code>Session</code>
:   Session Sqlalchemy


**```db_model```** :&ensp;<code>db.BaseEntity</code>
:   Class Model


**```pkey```** :&ensp;<code>str</code>, optional
:   Primary key attribute



#### Raises

<code>[ReferentielError](#hexagon.repository.referentiel.ReferentielError "hexagon.repository.referentiel.ReferentielError")</code>
:   Si la table n'est pas une Table reférentiel


`Si le pkey n'est pas un attribut valide`
:   &nbsp;


`Si le modèle n'est pas un modèle défini au niveau d'Hexagon`
:   &nbsp;




    
#### Ancestors (in MRO)

* [hexagon.repository.referentiel.ReferentielRepository](#hexagon.repository.referentiel.ReferentielRepository)
* [hexagon.infrastructure.query_executor.QueryExecutor](#hexagon.infrastructure.query_executor.QueryExecutor)






    
### Class `ReferentielEmetteur` {#hexagon.repository.referentiel.ReferentielEmetteur}



> `class ReferentielEmetteur(dbsession, pkey: str = 'id')`


Repository des tables reférentiel, implémente une seule méthode d'extraction des données
du referentiel avec une limite applicative sur les filtres et la structure.


#### Args

**```dbsession```** :&ensp;<code>Session</code>
:   Session Sqlalchemy


**```db_model```** :&ensp;<code>db.BaseEntity</code>
:   Class Model


**```pkey```** :&ensp;<code>str</code>, optional
:   Primary key attribute



#### Raises

<code>[ReferentielError](#hexagon.repository.referentiel.ReferentielError "hexagon.repository.referentiel.ReferentielError")</code>
:   Si la table n'est pas une Table reférentiel


`Si le pkey n'est pas un attribut valide`
:   &nbsp;


`Si le modèle n'est pas un modèle défini au niveau d'Hexagon`
:   &nbsp;




    
#### Ancestors (in MRO)

* [hexagon.repository.referentiel.ReferentielRepository](#hexagon.repository.referentiel.ReferentielRepository)
* [hexagon.infrastructure.query_executor.QueryExecutor](#hexagon.infrastructure.query_executor.QueryExecutor)






    
### Class `ReferentielError` {#hexagon.repository.referentiel.ReferentielError}



> `class ReferentielError(...)`


Common base class for all non-exit exceptions.


    
#### Ancestors (in MRO)

* [hexagon.repository.exceptions.StoreError](#hexagon.repository.exceptions.StoreError)
* [hexagon.repository.exceptions.RepositoryError](#hexagon.repository.exceptions.RepositoryError)
* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)






    
### Class `ReferentielFonds` {#hexagon.repository.referentiel.ReferentielFonds}



> `class ReferentielFonds(dbsession, pkey: str = 'id')`


Repository des tables reférentiel, implémente une seule méthode d'extraction des données
du referentiel avec une limite applicative sur les filtres et la structure.


#### Args

**```dbsession```** :&ensp;<code>Session</code>
:   Session Sqlalchemy


**```db_model```** :&ensp;<code>db.BaseEntity</code>
:   Class Model


**```pkey```** :&ensp;<code>str</code>, optional
:   Primary key attribute



#### Raises

<code>[ReferentielError](#hexagon.repository.referentiel.ReferentielError "hexagon.repository.referentiel.ReferentielError")</code>
:   Si la table n'est pas une Table reférentiel


`Si le pkey n'est pas un attribut valide`
:   &nbsp;


`Si le modèle n'est pas un modèle défini au niveau d'Hexagon`
:   &nbsp;




    
#### Ancestors (in MRO)

* [hexagon.repository.referentiel.ReferentielRepository](#hexagon.repository.referentiel.ReferentielRepository)
* [hexagon.infrastructure.query_executor.QueryExecutor](#hexagon.infrastructure.query_executor.QueryExecutor)






    
### Class `ReferentielFondsMarket` {#hexagon.repository.referentiel.ReferentielFondsMarket}



> `class ReferentielFondsMarket(dbsession, pkey: str = 'id')`


Repository des tables reférentiel, implémente une seule méthode d'extraction des données
du referentiel avec une limite applicative sur les filtres et la structure.


#### Args

**```dbsession```** :&ensp;<code>Session</code>
:   Session Sqlalchemy


**```db_model```** :&ensp;<code>db.BaseEntity</code>
:   Class Model


**```pkey```** :&ensp;<code>str</code>, optional
:   Primary key attribute



#### Raises

<code>[ReferentielError](#hexagon.repository.referentiel.ReferentielError "hexagon.repository.referentiel.ReferentielError")</code>
:   Si la table n'est pas une Table reférentiel


`Si le pkey n'est pas un attribut valide`
:   &nbsp;


`Si le modèle n'est pas un modèle défini au niveau d'Hexagon`
:   &nbsp;




    
#### Ancestors (in MRO)

* [hexagon.repository.referentiel.ReferentielRepository](#hexagon.repository.referentiel.ReferentielRepository)
* [hexagon.infrastructure.query_executor.QueryExecutor](#hexagon.infrastructure.query_executor.QueryExecutor)






    
### Class `ReferentielIndices` {#hexagon.repository.referentiel.ReferentielIndices}



> `class ReferentielIndices(dbsession, pkey: str = 'id')`


Repository des tables reférentiel, implémente une seule méthode d'extraction des données
du referentiel avec une limite applicative sur les filtres et la structure.


#### Args

**```dbsession```** :&ensp;<code>Session</code>
:   Session Sqlalchemy


**```db_model```** :&ensp;<code>db.BaseEntity</code>
:   Class Model


**```pkey```** :&ensp;<code>str</code>, optional
:   Primary key attribute



#### Raises

<code>[ReferentielError](#hexagon.repository.referentiel.ReferentielError "hexagon.repository.referentiel.ReferentielError")</code>
:   Si la table n'est pas une Table reférentiel


`Si le pkey n'est pas un attribut valide`
:   &nbsp;


`Si le modèle n'est pas un modèle défini au niveau d'Hexagon`
:   &nbsp;




    
#### Ancestors (in MRO)

* [hexagon.repository.referentiel.ReferentielRepository](#hexagon.repository.referentiel.ReferentielRepository)
* [hexagon.infrastructure.query_executor.QueryExecutor](#hexagon.infrastructure.query_executor.QueryExecutor)






    
### Class `ReferentielPays` {#hexagon.repository.referentiel.ReferentielPays}



> `class ReferentielPays(dbsession, pkey: str = 'id')`


Repository des tables reférentiel, implémente une seule méthode d'extraction des données
du referentiel avec une limite applicative sur les filtres et la structure.


#### Args

**```dbsession```** :&ensp;<code>Session</code>
:   Session Sqlalchemy


**```db_model```** :&ensp;<code>db.BaseEntity</code>
:   Class Model


**```pkey```** :&ensp;<code>str</code>, optional
:   Primary key attribute



#### Raises

<code>[ReferentielError](#hexagon.repository.referentiel.ReferentielError "hexagon.repository.referentiel.ReferentielError")</code>
:   Si la table n'est pas une Table reférentiel


`Si le pkey n'est pas un attribut valide`
:   &nbsp;


`Si le modèle n'est pas un modèle défini au niveau d'Hexagon`
:   &nbsp;




    
#### Ancestors (in MRO)

* [hexagon.repository.referentiel.ReferentielRepository](#hexagon.repository.referentiel.ReferentielRepository)
* [hexagon.infrastructure.query_executor.QueryExecutor](#hexagon.infrastructure.query_executor.QueryExecutor)






    
### Class `ReferentielRepository` {#hexagon.repository.referentiel.ReferentielRepository}



> `class ReferentielRepository(dbsession: sqlalchemy.orm.session.Session, db_model: sqlalchemy.orm.decl_api.Base, pkey: str = 'id')`


Repository des tables reférentiel, implémente une seule méthode d'extraction des données
du referentiel avec une limite applicative sur les filtres et la structure.


#### Args

**```dbsession```** :&ensp;<code>Session</code>
:   Session Sqlalchemy


**```db_model```** :&ensp;<code>db.BaseEntity</code>
:   Class Model


**```pkey```** :&ensp;<code>str</code>, optional
:   Primary key attribute



#### Raises

<code>[ReferentielError](#hexagon.repository.referentiel.ReferentielError "hexagon.repository.referentiel.ReferentielError")</code>
:   Si la table n'est pas une Table reférentiel


`Si le pkey n'est pas un attribut valide`
:   &nbsp;


`Si le modèle n'est pas un modèle défini au niveau d'Hexagon`
:   &nbsp;




    
#### Ancestors (in MRO)

* [hexagon.infrastructure.query_executor.QueryExecutor](#hexagon.infrastructure.query_executor.QueryExecutor)


    
#### Descendants

* [hexagon.repository.referentiel.ReferentielBenchmark](#hexagon.repository.referentiel.ReferentielBenchmark)
* [hexagon.repository.referentiel.ReferentielDevise](#hexagon.repository.referentiel.ReferentielDevise)
* [hexagon.repository.referentiel.ReferentielEmetteur](#hexagon.repository.referentiel.ReferentielEmetteur)
* [hexagon.repository.referentiel.ReferentielFonds](#hexagon.repository.referentiel.ReferentielFonds)
* [hexagon.repository.referentiel.ReferentielFondsMarket](#hexagon.repository.referentiel.ReferentielFondsMarket)
* [hexagon.repository.referentiel.ReferentielIndices](#hexagon.repository.referentiel.ReferentielIndices)
* [hexagon.repository.referentiel.ReferentielPays](#hexagon.repository.referentiel.ReferentielPays)
* [hexagon.repository.referentiel.ReferentielSecteur](#hexagon.repository.referentiel.ReferentielSecteur)
* [hexagon.repository.referentiel.ReferentielTitre](#hexagon.repository.referentiel.ReferentielTitre)
* [hexagon.repository.referentiel.ReferentielTitreMarket](#hexagon.repository.referentiel.ReferentielTitreMarket)





    
#### Methods


    
##### Method `filter_get` {#hexagon.repository.referentiel.ReferentielRepository.filter_get}



    
> `def filter_get(self, fields: list[str] = None, filter_by: dict = None, as_mapping=False, as_result_set=False, as_query=False) -> list[sqlalchemy.engine.row.Row] | sqlalchemy.engine.cursor.CursorResult`


Extrait les données du réferentiel selon les filtres fournis et selon la structure
demandée via fields.


###### Args

**```fields```** :&ensp;<code>list\[str]</code>, optional
:   Liste des Champs/Attributs requêtés


**```filter_by```** :&ensp;<code>dict</code>, optional
:   Les attributs et valeurs àutiliser pour filter (voir filter_by de sqlalchemy)


**```as_mapping```** :&ensp;<code>bool, False</code>
:   Retourne le ResultSet as MappingRow.


**```as_result_set```** :&ensp;<code>bool, False</code>
:   Retourne le CursorResult de la requête.


**```as_query```** :&ensp;<code>bool, False</code>
:   Retourne La requête conostruite sans execution.



###### Returns

`list[Row] | CursorResult: Le resultat de la requête sera une liste de Rows`
:   &nbsp;

###### Raises

<code>[ReferentielError](#hexagon.repository.referentiel.ReferentielError "hexagon.repository.referentiel.ReferentielError")</code>
:   Si les attributs ne sont pas valides



    
### Class `ReferentielSecteur` {#hexagon.repository.referentiel.ReferentielSecteur}



> `class ReferentielSecteur(dbsession, pkey: str = 'id')`


Repository des tables reférentiel, implémente une seule méthode d'extraction des données
du referentiel avec une limite applicative sur les filtres et la structure.


#### Args

**```dbsession```** :&ensp;<code>Session</code>
:   Session Sqlalchemy


**```db_model```** :&ensp;<code>db.BaseEntity</code>
:   Class Model


**```pkey```** :&ensp;<code>str</code>, optional
:   Primary key attribute



#### Raises

<code>[ReferentielError](#hexagon.repository.referentiel.ReferentielError "hexagon.repository.referentiel.ReferentielError")</code>
:   Si la table n'est pas une Table reférentiel


`Si le pkey n'est pas un attribut valide`
:   &nbsp;


`Si le modèle n'est pas un modèle défini au niveau d'Hexagon`
:   &nbsp;




    
#### Ancestors (in MRO)

* [hexagon.repository.referentiel.ReferentielRepository](#hexagon.repository.referentiel.ReferentielRepository)
* [hexagon.infrastructure.query_executor.QueryExecutor](#hexagon.infrastructure.query_executor.QueryExecutor)






    
### Class `ReferentielTitre` {#hexagon.repository.referentiel.ReferentielTitre}



> `class ReferentielTitre(dbsession, pkey: str = 'id')`


Repository des tables reférentiel, implémente une seule méthode d'extraction des données
du referentiel avec une limite applicative sur les filtres et la structure.


#### Args

**```dbsession```** :&ensp;<code>Session</code>
:   Session Sqlalchemy


**```db_model```** :&ensp;<code>db.BaseEntity</code>
:   Class Model


**```pkey```** :&ensp;<code>str</code>, optional
:   Primary key attribute



#### Raises

<code>[ReferentielError](#hexagon.repository.referentiel.ReferentielError "hexagon.repository.referentiel.ReferentielError")</code>
:   Si la table n'est pas une Table reférentiel


`Si le pkey n'est pas un attribut valide`
:   &nbsp;


`Si le modèle n'est pas un modèle défini au niveau d'Hexagon`
:   &nbsp;




    
#### Ancestors (in MRO)

* [hexagon.repository.referentiel.ReferentielRepository](#hexagon.repository.referentiel.ReferentielRepository)
* [hexagon.infrastructure.query_executor.QueryExecutor](#hexagon.infrastructure.query_executor.QueryExecutor)






    
### Class `ReferentielTitreMarket` {#hexagon.repository.referentiel.ReferentielTitreMarket}



> `class ReferentielTitreMarket(dbsession, pkey: str = 'id')`


Repository des tables reférentiel, implémente une seule méthode d'extraction des données
du referentiel avec une limite applicative sur les filtres et la structure.


#### Args

**```dbsession```** :&ensp;<code>Session</code>
:   Session Sqlalchemy


**```db_model```** :&ensp;<code>db.BaseEntity</code>
:   Class Model


**```pkey```** :&ensp;<code>str</code>, optional
:   Primary key attribute



#### Raises

<code>[ReferentielError](#hexagon.repository.referentiel.ReferentielError "hexagon.repository.referentiel.ReferentielError")</code>
:   Si la table n'est pas une Table reférentiel


`Si le pkey n'est pas un attribut valide`
:   &nbsp;


`Si le modèle n'est pas un modèle défini au niveau d'Hexagon`
:   &nbsp;




    
#### Ancestors (in MRO)

* [hexagon.repository.referentiel.ReferentielRepository](#hexagon.repository.referentiel.ReferentielRepository)
* [hexagon.infrastructure.query_executor.QueryExecutor](#hexagon.infrastructure.query_executor.QueryExecutor)








    
# Module `hexagon.repository.store` {#hexagon.repository.store}







    
## Classes


    
### Class `RepoBenchmark` {#hexagon.repository.store.RepoBenchmark}



> `class RepoBenchmark(dbsession: sqlalchemy.orm.session.Session, logger: logging.Logger = None)`





    
#### Ancestors (in MRO)

* [hexagon.infrastructure.query_executor.QueryExecutor](#hexagon.infrastructure.query_executor.QueryExecutor)



    
#### Class variables


    
##### Variable `model` {#hexagon.repository.store.RepoBenchmark.model}

Modèle du Benchmark


###### Attributes

**```id```** :&ensp;<code>int</code>
:   ID de l'enregistrement du Benchmark


**```code```** :&ensp;<code>str</code>
:   Code *UNIQUE* du Benchmark


**```description```** :&ensp;<code>str</code>
:   Description courte du Benchmark


**```base_initialisation```** :&ensp;<code>float</code>
:   Base d'initialisation du benchmark


**```spread```** :&ensp;<code>float</code>
:   Le spread à ajouter lors du calcul de la perf


periodicite_spread (str='A'): Périodicité de calcul du spread
**```composition```** :&ensp;<code>BenchmarkComposition</code>
:   Relation de la composition du Benchmark
    Même si le bench est Simple, il est nécessaire de l'assicier avec l'indice correspondant






    
#### Methods


    
##### Method `indicateurs_taux` {#hexagon.repository.store.RepoBenchmark.indicateurs_taux}



    
> `def indicateurs_taux(self, benchmark: hexagon.infrastructure.db.models.market.Benchmark, debut: datetime.date = None, fin: datetime.date = None, include: str = 'BOTH') -> list[dict]`


calcule les indicateurs d'un benchmark composite ou simple

* Si le bench est composé de deux poches oblig, retourne
  les indicateurs pondérés par leurs poids repsectifs.

* Si le bench est composé d'une poche action et d'une poche taux, retourne
  les indicateurs pondérés seulement par le poids de la poche taux.


###### Args

**```benchmark```** :&ensp;<code>db.benchmark</code>
:   Instance de <code>db.Benchmark</code>


**```debut```** :&ensp;<code>None</code>, optional
:   date de début


**```fin```** :&ensp;<code>None</code>, optional
:   date de fin



###### Returns

<code>list\[dict]</code>
:   Les memes indicateurs taux de RawMBI traités selon les cas ci-dessus



    
### Class `RepoFonds` {#hexagon.repository.store.RepoFonds}



> `class RepoFonds(dbsession: sqlalchemy.orm.session.Session, logger: logging.Logger = None)`





    
#### Ancestors (in MRO)

* [hexagon.infrastructure.query_executor.QueryExecutor](#hexagon.infrastructure.query_executor.QueryExecutor)



    
#### Class variables


    
##### Variable `histo_model` {#hexagon.repository.store.RepoFonds.histo_model}

Modèle de données de l'état du Fonds


###### Attributes

**```id```** :&ensp;<code>int</code>
:   L'ID de l'enregistrement


**```id_fonds```** :&ensp;<code>int</code>
:   L'ID du Fonds


**```date_marche```** :&ensp;<code>datetime.date</code>
:   Date de valeur


**```vl```** :&ensp;<code>float</code>
:   VL du Fonds


**```actif_net```** :&ensp;<code>float</code>
:   Actif Net Avant S/R


**```actif_net_ap_sr```** :&ensp;<code>float</code>
:   Actif Net Après S/R


**```actif_brut```** :&ensp;<code>float</code>
:   Actif Brut Avant S/R


**```nombre_parts```** :&ensp;<code>float</code>
:   Nombre Parts Avant S/R


**```nombre_parts_ap_sr```** :&ensp;<code>float</code>
:   Nombre Parts Après S/R


**```banque```** :&ensp;<code>float</code>
:   Banque


**```agios```** :&ensp;<code>float</code>
:   Agios provisionnés sur la période


**```sensibilite```** :&ensp;<code>float</code>
:   Sensibilite du Fonds (pour la complétude des données du Fonds)


**```duration```** :&ensp;<code>float</code>
:   Duration du Fonds (pour la complétude des données du Fonds)


**```fdg```** :&ensp;<code>float</code>
:   Les FDG déduits du Fonds


**```provision_agios```** :&ensp;<code>float</code>
:   Provision Agios


**```provision_fdg```** :&ensp;<code>float</code>
:   Provision FDG



    
##### Variable `model` {#hexagon.repository.store.RepoFonds.model}

Modèle de donnée des Fonds


###### Attributes

**```id```** :&ensp;<code>int</code>
:   L'ID du Fonds


**```code```** :&ensp;<code>str</code>
:   Code du Fonds (MANAR Like)


**```code_isin```** :&ensp;<code>str</code>
:   Code ISIN du Fonds


**```description```** :&ensp;<code>str</code>
:   Description du Fonds


**```categorie```** :&ensp;<code>str</code>
:   categorie du Fonds (OMLT, ACT, ....)


**```periodicite```** :&ensp;<code>str</code>
:   periodicite du Fonds (Hebdo / Quotidien)


**```forme_juridique```** :&ensp;<code>str</code>
:   FCP / SICAV


**```type_gestion```** :&ensp;<code>str</code>
:   'D' pour dédié et 'P' pour Grand Publique


**```date_creation```** :&ensp;<code>datetime.date</code>
:   Date de création du Fonds


**```date_premiere_vl```** :&ensp;<code>datetime.date</code>
:   Date de première VL


**```date_premiere_souscription```** :&ensp;<code>datetime.date</code>
:   Date de première souscription


**```taux_fdg```** :&ensp;<code>float</code>
:   FDG de la SDG 'TTC'


**```taux_depositaire```** :&ensp;<code>float</code>
:   frais depositaire 'TTC'


**```taux_ammc```** :&ensp;<code>float</code>
:   frais AMMC 'TTC'


**```affectation_resultat```** :&ensp;<code>str</code>
:   'CAPI' - 'MIXTE' - 'DISTR'


**```code_sesam```** :&ensp;<code>str</code>
:   Code de déclaration du Fonds sur la plateforme SESAM


**```code_agrement```** :&ensp;<code>str</code>
:   Code de l'agrément du Fonds


**```promoteur```** :&ensp;<code>str</code>
:   Le promoteur du Fonds (dans le cas d'un Fonds Dédié)


**```created_at```** :&ensp;<code>datetime.datetime</code>
:   Date de création de l'enregistrement






    
#### Methods


    
##### Method `histo_operation_passif` {#hexagon.repository.store.RepoFonds.histo_operation_passif}



    
> `def histo_operation_passif(self, code_fonds: list[str] = None, debut: datetime.date = None, fin: datetime.date = None, poste_distribution: str = 'DISTRIB-', include: str = 'BOTH') -> list`


Function Summary


###### Args

**```code_fonds```** :&ensp;<code>list</code>, optional
:   Description


**```debut```** :&ensp;<code>None</code>, optional
:   Description


**```fin```** :&ensp;<code>None</code>, optional
:   Description


**```poste_distribution```** :&ensp;<code>TYPE</code>, optional
:   Description



###### Returns

<code>list</code>
:   Description



    
##### Method `histo_sensidur_an` {#hexagon.repository.store.RepoFonds.histo_sensidur_an}



    
> `def histo_sensidur_an(self, fonds: hexagon.infrastructure.db.models.opcvm.Fonds, debut: datetime.date, fin: datetime.date) -> list[dict]`




    
##### Method `historique` {#hexagon.repository.store.RepoFonds.historique}



    
> `def historique(self, code_fonds: list[str] = None, debut: datetime.date = None, fin: datetime.date = None, fonds_fields: list[str] = None, etat_fonds_fields: list[str] = None, freq: str = 'H', include: str = 'BOTH') -> list`


Construit un historique des données des Fonds cette fonction permet aussi de demander
    les données à une fréquence donnée afin de faciliter son exploitation.

    Si freq=debut=fin: date=None; Retourne tout l'historique avec toutes les dimensions des Fonds et de leurs historiques.

    Fournir une liste des Fonds pour restreindre le périmètre.

    La fonction permet de retourner les données de la BDD liées aux Fonds à savoir:

    * vl: Vl des Fonds sur un intervalle donné
    * actif_net: Actif_net du Fonds
    * nombre_parts: Nombre de parts du Fonds
    * ETC, Voir le Schema de la BDD


###### Args

**```code_fonds```** :&ensp;<code>list</code>, optional
:   Liste des codes des Fonds


**```debut```** :&ensp;<code>None</code>, optional
:   Date de début de l'historique, *inclus*


**```fin```** :&ensp;<code>None</code>, optional
:   Date de fin de l'historique, *inclus*


**```etat_fonds_fields```** :&ensp;<code>list</code>, optional
:   Liste des champs/colonnes des EtatFonds demandés


**```fonds_fields```** :&ensp;<code>list</code>, optional
:   Liste des champs/dimensions des Fonds demandés


**```freq```** :&ensp;<code>str</code>, optional
:   Fréquence de l'historique (H: Hebdo, Q: Quotidien)
    freq annule la date de Début



###### Returns

<code>list</code>
:   list[KeyedTuple] avec les champs demandés et fonds.code si non fourni



###### Raises

<code>ValueError</code>
:   Si un champs fourni ne figure pas au niveau du modèle de données dudit modèle



    
### Class `RepoIndice` {#hexagon.repository.store.RepoIndice}



> `class RepoIndice(dbsession: sqlalchemy.orm.session.Session, logger: logging.Logger = None)`





    
#### Ancestors (in MRO)

* [hexagon.infrastructure.query_executor.QueryExecutor](#hexagon.infrastructure.query_executor.QueryExecutor)



    
#### Class variables


    
##### Variable `model` {#hexagon.repository.store.RepoIndice.model}

Modèle des Indices


###### Attributes

**```code```** :&ensp;<code>str</code>
:   Code Unique de l'indice


**```description```** :&ensp;<code>str</code>
:   Descriptoin courte de l'indice


**```marche```** :&ensp;<code>str</code>
:   Le marché selon notre vision dans IN (ACT, OBL)


**```place_cotation```** :&ensp;<code>str</code>
:   Description


**```date_creation```** :&ensp;<code>datetime.date</code>
:   Date de création


**```date_reference```** :&ensp;<code>datetime.date</code>
:   Date de référence


**```valeur_base```** :&ensp;<code>float</code>
:   Valeur de base de l'indice ex: 100 ou 1000


**```comment```** :&ensp;<code>str</code>
:   Commentaire sur l'indice, généralement le descriptif long publiquement publié


**```ticker```** :&ensp;<code>str</code>
:   Mnémonique


**```id_devise```** :&ensp;<code>int</code>
:   ID de la Devise de publication de l'indice


**```id_pays```** :&ensp;<code>int</code>
:   ID du Pays de cotation de l'indice






    
#### Methods


    
##### Method `add` {#hexagon.repository.store.RepoIndice.add}



    
> `def add(self, indice_instance: hexagon.infrastructure.db.models.market.Indice)`




    
##### Method `delete` {#hexagon.repository.store.RepoIndice.delete}



    
> `def delete(self, indice_instance: hexagon.infrastructure.db.models.market.Indice)`




    
##### Method `histo_indice` {#hexagon.repository.store.RepoIndice.histo_indice}



    
> `def histo_indice(self, code_indice: str, debut: datetime.date = None, fin: datetime.date = None, align_with: list[str] | str = ['MASIRB', 'MASIRN', 'MASI', 'ESG10', 'MSI20', 'MBI', 'MBICT', 'MBIMT', 'MBIMLT', 'MBILT', 'MONIA'], include: str = 'BOTH') -> list[sqlalchemy.engine.row.Row]`




    
##### Method `histo_raw_mbi` {#hexagon.repository.store.RepoIndice.histo_raw_mbi}



    
> `def histo_raw_mbi(self, codes_strates: list[str] = None, debut: datetime.date = None, fin: datetime.date = None, include: str = 'BOTH') -> list[hexagon.infrastructure.db.models.market.RawMBI]`


Retourne l'historique de raw_mbi pour avoir les indicateurs qui n'interviennent pas
dans le calcul de la performance.


###### Args

**```codes_strates```** :&ensp;<code>list</code>, optional
:   Liste des strates voulues


**```debut```** :&ensp;<code>None</code>, optional
:   date de début


**```fin```** :&ensp;<code>None</code>, optional
:   date de fin



###### Returns

<code>list</code>
:   list[db.RawMBI], plain sqlalchemy result



###### Raises

<code>TypeError</code>
:   Si codes_strates not an instance of (<code>tuple</code>, <code>list</code>)



    
##### Method `strates_raw_mbi` {#hexagon.repository.store.RepoIndice.strates_raw_mbi}



    
> `def strates_raw_mbi(self, debut: datetime.date = None, fin: datetime.date = None, include: str = 'BOTH') -> list[str]`


Retourne la liste unique des strates présentes dans la table RawMBI entre 2 dates.
Les dates permettent de filtrer la présence des strates

###### Args

**```debut```** :&ensp;<code>None</code>, optional
:   date de début


**```fin```** :&ensp;<code>None</code>, optional
:   date de fin



    
##### Method `update` {#hexagon.repository.store.RepoIndice.update}



    
> `def update(self, indice_instance: hexagon.infrastructure.db.models.market.Indice, **kwargs: dict)`




    
### Class `RepoOperation` {#hexagon.repository.store.RepoOperation}



> `class RepoOperation(dbsession: sqlalchemy.orm.session.Session, logger: logging.Logger = None)`





    
#### Ancestors (in MRO)

* [hexagon.infrastructure.query_executor.QueryExecutor](#hexagon.infrastructure.query_executor.QueryExecutor)



    
#### Class variables


    
##### Variable `default_fonds_fields` {#hexagon.repository.store.RepoOperation.default_fonds_fields}



    
##### Variable `default_operation_fields` {#hexagon.repository.store.RepoOperation.default_operation_fields}



    
##### Variable `default_titre_fields` {#hexagon.repository.store.RepoOperation.default_titre_fields}



    
##### Variable `fonds_model` {#hexagon.repository.store.RepoOperation.fonds_model}

Modèle de donnée des Fonds


###### Attributes

**```id```** :&ensp;<code>int</code>
:   L'ID du Fonds


**```code```** :&ensp;<code>str</code>
:   Code du Fonds (MANAR Like)


**```code_isin```** :&ensp;<code>str</code>
:   Code ISIN du Fonds


**```description```** :&ensp;<code>str</code>
:   Description du Fonds


**```categorie```** :&ensp;<code>str</code>
:   categorie du Fonds (OMLT, ACT, ....)


**```periodicite```** :&ensp;<code>str</code>
:   periodicite du Fonds (Hebdo / Quotidien)


**```forme_juridique```** :&ensp;<code>str</code>
:   FCP / SICAV


**```type_gestion```** :&ensp;<code>str</code>
:   'D' pour dédié et 'P' pour Grand Publique


**```date_creation```** :&ensp;<code>datetime.date</code>
:   Date de création du Fonds


**```date_premiere_vl```** :&ensp;<code>datetime.date</code>
:   Date de première VL


**```date_premiere_souscription```** :&ensp;<code>datetime.date</code>
:   Date de première souscription


**```taux_fdg```** :&ensp;<code>float</code>
:   FDG de la SDG 'TTC'


**```taux_depositaire```** :&ensp;<code>float</code>
:   frais depositaire 'TTC'


**```taux_ammc```** :&ensp;<code>float</code>
:   frais AMMC 'TTC'


**```affectation_resultat```** :&ensp;<code>str</code>
:   'CAPI' - 'MIXTE' - 'DISTR'


**```code_sesam```** :&ensp;<code>str</code>
:   Code de déclaration du Fonds sur la plateforme SESAM


**```code_agrement```** :&ensp;<code>str</code>
:   Code de l'agrément du Fonds


**```promoteur```** :&ensp;<code>str</code>
:   Le promoteur du Fonds (dans le cas d'un Fonds Dédié)


**```created_at```** :&ensp;<code>datetime.datetime</code>
:   Date de création de l'enregistrement



    
##### Variable `operation_model` {#hexagon.repository.store.RepoOperation.operation_model}

Modèle de donnée des Opérations


###### Attributes

**```id```** :&ensp;<code>int</code>
:   L'ID de l'opération (Identique au numéro d'OP de MANAR)


**```id_fonds```** :&ensp;<code>int</code>
:   L'ID du Fonds


**```id_titre```** :&ensp;<code>int</code>
:   L'ID du Titre


**```poste_operation```** :&ensp;<code>str</code>
:   Poste de l'opération 'Foreign Key'


**```date_operation```** :&ensp;<code>date</code>
:   Date de l'opération


**```contrepartie```** :&ensp;<code>str</code>
:   Contrepartie de l'opération


**```coupon_couru_unitaire```** :&ensp;<code>float</code>
:   Coupon couru du sous-jacent (dans le cas des titres obligataires)


**```quantite```** :&ensp;<code>float</code>
:   Quantité des titres de l'opération


**```cours_unitaire```** :&ensp;<code>float</code>
:   Cours unitaire du sous-jacent


**```cours_operation_brut```** :&ensp;<code>float</code>
:   Montant brut Global de l'opération


**```cours_operation_net```** :&ensp;<code>float</code>
:   Montant net Global de l'opération


**```date_fin```** :&ensp;<code>date</code>
:   Date de fin de l'opération


**```date_rl```** :&ensp;<code>date</code>
:   Date de R/L de l'opération


**```depositaire```** :&ensp;<code>str</code>
:   Dépositaire de l'opération


**```devise_reglement```** :&ensp;<code>str</code>
:   Devise règlement de l'opération


**```duree```** :&ensp;<code>int</code>
:   Nombre de jours de l'opération (cas des opération temporaires)


**```frais_operation```** :&ensp;<code>float</code>
:   Frais de l'opération


**```interets```** :&ensp;<code>float</code>
:   Interets de l'opération (REPO/RREPO)


**```intermediaire```** :&ensp;<code>str</code>
:   Intermédiaire de l'opération (selon l'opération)


**```pmv_brut```** :&ensp;<code>float</code>
:   PMV brut des frais d'opération (lors de la vente)


**```pmv_net```** :&ensp;<code>float</code>
:   PMV net des frais d'opération (lors de la vente)


**```ppc```** :&ensp;<code>float</code>
:   Prix Pied de coupon (Clean Price)


**```spread_operation```** :&ensp;<code>float</code>
:   Spread de l'opération (Différence entre le taux de valo selon la courbe et le taux de l'opération/négoce)


**```taux_negoce```** :&ensp;<code>float</code>
:   Taux de négociation (Taux d'Achat/Vente)


**```taux_placement```** :&ensp;<code>float</code>
:   Taux de placement (REPO/RREPO)



**```titre```** :&ensp;<code>Titre</code>
:   Instance du Titre de l'opération *relationship*


**```fonds```** :&ensp;<code>Fonds</code>
:   Instance du Fonds de l'opération *relationship*


**```poste```** :&ensp;<code>PosteOperation</code>
:   Instance du Poste Operation de l'opération *relationship*



    
##### Variable `titre_model` {#hexagon.repository.store.RepoOperation.titre_model}

Modèle de donnée des titres


###### Attributes

**```id```** :&ensp;<code>int</code>
:   L'ID du Titre dans la BDD. *UNIQUE*


**```id_devise```** :&ensp;<code>int</code>
:   ID de la devise du titre


**```id_emetteur```** :&ensp;<code>int</code>
:   ID de l'émetteur du titre


**```id_pays```** :&ensp;<code>int</code>
:   ID du Pays d'émission du Titre


**```code```** :&ensp;<code>str</code>
:   code *UNIQUE* 6 chiffres, déduit depuis le code ISIN


**```isin```** :&ensp;<code>str</code>
:   Code *UNIQUE* ISIN du Titre, MAOOO2013449


**```description```** :&ensp;<code>str</code>
:   Description ou libelle du titre


**```nominal```** :&ensp;<code>float</code>
:   Le nominal du Titre


**```taux_facial```** :&ensp;<code>float</code>
:   Le taux Facial dans le cas des titres Obligs


**```emission```** :&ensp;<code>date</code>
:   Date d'émission du Titre


**```jouissance```** :&ensp;<code>date</code>
:   Date de jouissance du Titre


**```echeance```** :&ensp;<code>date</code>
:   Date d'échéancedu Titre


**```revision```** :&ensp;<code>date</code>
:   Date de révision du Titre, Doit être revisable


**```categorie```** :&ensp;<code>str</code>
:   Categorie du titre ou sous-classe


**```classe```** :&ensp;<code>str</code>
:   Classe du titre


**```cote```** :&ensp;<code>bool</code>
:   Si le titre est Coté, le cas des titres ACTIONS


**```garanti```** :&ensp;<code>bool</code>
:   Si le titre est garanti, cas des Offices (ONCF, ONDA, FEC)


**```methode_valo```** :&ensp;<code>str</code>
:   Voir la doc du Pricer


**```type_coupon```** :&ensp;<code>str</code>
:   Fix-Revisable


**```periodicite```** :&ensp;<code>str</code>
:   Periodicite du Titre. IN ('A', 'S', T', 'M')


**```qt_emise```** :&ensp;<code>float</code>
:   Quantité émise du titre


**```spread```** :&ensp;<code>float</code>
:   Le spread à l'émission


**```ticker```** :&ensp;<code>str</code>
:   Mnémonique pour les titres Actions






    
#### Methods


    
##### Method `historique` {#hexagon.repository.store.RepoOperation.historique}



    
> `def historique(self, code_fonds: list[str] = None, debut: datetime.date = None, fin: datetime.date = None, poste_operations: list[str] = None, *, operation_fields: list[str] = None, titre_fields: list[str] = None, fonds_fields: list[str] = None, include_dates: str = 'BOTH')`




    
##### Method `historique_transactions` {#hexagon.repository.store.RepoOperation.historique_transactions}



    
> `def historique_transactions(self, code_fonds: list[str] = None, debut: datetime.date = None, fin: datetime.date = None, poste_operations: list[str] = None, *, operation_fields: list[str] = None, titre_fields: list[str] = None, fonds_fields: list[str] = None, include_dates: str = 'BOTH')`




    
### Class `RepoPassif` {#hexagon.repository.store.RepoPassif}



> `class RepoPassif(dbsession: sqlalchemy.orm.session.Session, logger: logging.Logger = None)`





    
#### Ancestors (in MRO)

* [hexagon.infrastructure.query_executor.QueryExecutor](#hexagon.infrastructure.query_executor.QueryExecutor)



    
#### Class variables


    
##### Variable `POSTE_RACHAT` {#hexagon.repository.store.RepoPassif.POSTE_RACHAT}



    
##### Variable `POSTE_SOUS` {#hexagon.repository.store.RepoPassif.POSTE_SOUS}



    
##### Variable `histo_model` {#hexagon.repository.store.RepoPassif.histo_model}

Les opérations du passif concernent les S/R et les distributions des Fonds


###### Attributes

**```id```** :&ensp;<code>int</code>
:   L'ID de l'enregistrement


**```id_fonds```** :&ensp;<code>int</code>
:   L'ID du Fonds


**```poste_operation```** :&ensp;<code>str</code>
:   Poste de l'opération


**```date_operation```** :&ensp;<code>date</code>
:   Date de l'opération


**```description```** :&ensp;<code>str</code>
:   Libellé de l'opération


**```devise_reglement```** :&ensp;<code>str</code>
:   Devise du règlement


**```montant_operation```** :&ensp;<code>float</code>
:   Montant de l'opération


**```tiers```** :&ensp;<code>str</code>
:   Le Tiers/Contrepartie de l'opération



    
##### Variable `model` {#hexagon.repository.store.RepoPassif.model}

Modèle de donnée des Fonds


###### Attributes

**```id```** :&ensp;<code>int</code>
:   L'ID du Fonds


**```code```** :&ensp;<code>str</code>
:   Code du Fonds (MANAR Like)


**```code_isin```** :&ensp;<code>str</code>
:   Code ISIN du Fonds


**```description```** :&ensp;<code>str</code>
:   Description du Fonds


**```categorie```** :&ensp;<code>str</code>
:   categorie du Fonds (OMLT, ACT, ....)


**```periodicite```** :&ensp;<code>str</code>
:   periodicite du Fonds (Hebdo / Quotidien)


**```forme_juridique```** :&ensp;<code>str</code>
:   FCP / SICAV


**```type_gestion```** :&ensp;<code>str</code>
:   'D' pour dédié et 'P' pour Grand Publique


**```date_creation```** :&ensp;<code>datetime.date</code>
:   Date de création du Fonds


**```date_premiere_vl```** :&ensp;<code>datetime.date</code>
:   Date de première VL


**```date_premiere_souscription```** :&ensp;<code>datetime.date</code>
:   Date de première souscription


**```taux_fdg```** :&ensp;<code>float</code>
:   FDG de la SDG 'TTC'


**```taux_depositaire```** :&ensp;<code>float</code>
:   frais depositaire 'TTC'


**```taux_ammc```** :&ensp;<code>float</code>
:   frais AMMC 'TTC'


**```affectation_resultat```** :&ensp;<code>str</code>
:   'CAPI' - 'MIXTE' - 'DISTR'


**```code_sesam```** :&ensp;<code>str</code>
:   Code de déclaration du Fonds sur la plateforme SESAM


**```code_agrement```** :&ensp;<code>str</code>
:   Code de l'agrément du Fonds


**```promoteur```** :&ensp;<code>str</code>
:   Le promoteur du Fonds (dans le cas d'un Fonds Dédié)


**```created_at```** :&ensp;<code>datetime.datetime</code>
:   Date de création de l'enregistrement






    
#### Methods


    
##### Method `get_histo_sr` {#hexagon.repository.store.RepoPassif.get_histo_sr}



    
> `def get_histo_sr(self, debut: datetime.date, fin: datetime.date, code_fonds: list[str] = None, code_sr: list[str] = None, code_tiers: list[str] = None, include: str = 'BOTH') -> list`




    
##### Method `get_histo_sr_pondere` {#hexagon.repository.store.RepoPassif.get_histo_sr_pondere}



    
> `def get_histo_sr_pondere(self, debut: datetime.date, fin: datetime.date, date_pivot: datetime.date = None, base_ponderation: float = None, code_fonds: list[str] = None, code_sr: list[str] = None, code_tiers: list[str] = None, include: str = 'BOTH') -> list[dict]`




    
### Class `RepoTitre` {#hexagon.repository.store.RepoTitre}



> `class RepoTitre(dbsession: sqlalchemy.orm.session.Session, logger: logging.Logger = None)`





    
#### Ancestors (in MRO)

* [hexagon.infrastructure.query_executor.QueryExecutor](#hexagon.infrastructure.query_executor.QueryExecutor)



    
#### Class variables


    
##### Variable `histo_model` {#hexagon.repository.store.RepoTitre.histo_model}

Modèle de persistence des valeurs des titres à une date donnée

* Unique(date_marche, id_titre) *


###### Attributes

**```id```** :&ensp;<code>int</code>
:   L'ID de l'enregistrement au niveau de la BDD


**```id_titre```** :&ensp;<code>int</code>
:   L'ID du Titre


**```date_marche```** :&ensp;<code>date</code>
:   La date de valeur de la valorisation


**```date_reference```** :&ensp;<code>date</code>
:   same thing que date marche, sauf pour les titres valorisés à des fréquences différentes. ex: Trimestriellement


**```taux_courbe```** :&ensp;<code>float</code>
:   Le taux sur la courbe correspondant à la maturité résiduelle


**```taux_valorisation```** :&ensp;<code>float</code>
:   Le coupon couru d'un titre obligataire


**```spread_valorisation```** :&ensp;<code>float</code>
:   Le spread de valorisation d'un titre obligataire


**```coupon_couru```** :&ensp;<code>float</code>
:   Le coupon couru d'un titre obligataire


**```cours```** :&ensp;<code>float</code>
:   Le cours de valorisation à la date 'date_marche'


**```sensibilite```** :&ensp;<code>float</code>
:   La sensibilite d'un titre obligataire


**```convexite```** :&ensp;<code>float</code>
:   La convexite d'un titre obligataire


**```duration```** :&ensp;<code>float</code>
:   La duration d'un titre obligataire


**```devise_valorisation```** :&ensp;<code>str</code>
:   Devise en alpha 3


**```maturite_residuelle```** :&ensp;<code>int</code>
:   Maturité residuelle en jours



**```titre```** :&ensp;<code>Titre</code>
:   Instance du titre associé à la valeur



    
##### Variable `model` {#hexagon.repository.store.RepoTitre.model}

Modèle de donnée des titres


###### Attributes

**```id```** :&ensp;<code>int</code>
:   L'ID du Titre dans la BDD. *UNIQUE*


**```id_devise```** :&ensp;<code>int</code>
:   ID de la devise du titre


**```id_emetteur```** :&ensp;<code>int</code>
:   ID de l'émetteur du titre


**```id_pays```** :&ensp;<code>int</code>
:   ID du Pays d'émission du Titre


**```code```** :&ensp;<code>str</code>
:   code *UNIQUE* 6 chiffres, déduit depuis le code ISIN


**```isin```** :&ensp;<code>str</code>
:   Code *UNIQUE* ISIN du Titre, MAOOO2013449


**```description```** :&ensp;<code>str</code>
:   Description ou libelle du titre


**```nominal```** :&ensp;<code>float</code>
:   Le nominal du Titre


**```taux_facial```** :&ensp;<code>float</code>
:   Le taux Facial dans le cas des titres Obligs


**```emission```** :&ensp;<code>date</code>
:   Date d'émission du Titre


**```jouissance```** :&ensp;<code>date</code>
:   Date de jouissance du Titre


**```echeance```** :&ensp;<code>date</code>
:   Date d'échéancedu Titre


**```revision```** :&ensp;<code>date</code>
:   Date de révision du Titre, Doit être revisable


**```categorie```** :&ensp;<code>str</code>
:   Categorie du titre ou sous-classe


**```classe```** :&ensp;<code>str</code>
:   Classe du titre


**```cote```** :&ensp;<code>bool</code>
:   Si le titre est Coté, le cas des titres ACTIONS


**```garanti```** :&ensp;<code>bool</code>
:   Si le titre est garanti, cas des Offices (ONCF, ONDA, FEC)


**```methode_valo```** :&ensp;<code>str</code>
:   Voir la doc du Pricer


**```type_coupon```** :&ensp;<code>str</code>
:   Fix-Revisable


**```periodicite```** :&ensp;<code>str</code>
:   Periodicite du Titre. IN ('A', 'S', T', 'M')


**```qt_emise```** :&ensp;<code>float</code>
:   Quantité émise du titre


**```spread```** :&ensp;<code>float</code>
:   Le spread à l'émission


**```ticker```** :&ensp;<code>str</code>
:   Mnémonique pour les titres Actions






    
#### Methods


    
##### Method `historique` {#hexagon.repository.store.RepoTitre.historique}



    
> `def historique(self, code_titre: list[str] = None, debut: datetime.date = None, fin: datetime.date = None, etat_titre_fields: list[str] = None, titre_fields: list[str] = None, include: str = 'BOTH')`


Construit un historique des données des Titres cette fonction permet aussi de demander
    les données à une fréquence donnée afin de faciliter son exploitation.

    Si freq=debut=fin=None; Retourne tout l'historique avec toutes les dimensions des Titres et de leurs historiques.

    Fournir une liste des Titres pour restreindre le périmètre.

    La fonction permet de retourner les données de la BDD liées aux Titres à savoir:

    * cours: Cours des Titres sur un intervalle donné
    * date_marche: Date de valeur du Titre
    * devise_valorisation: Devise de valorisation du cours Titre
    * ETC, Voir le Schema de la BDD


###### Args

**```code_titre```** :&ensp;<code>list</code>, optional
:   Liste des codes des Titres


**```debut```** :&ensp;<code>None</code>, optional
:   Date de début de l'historique, *inclus*


**```fin```** :&ensp;<code>None</code>, optional
:   Date de fin de l'historique, *inclus*


**```etat_titre_fields```** :&ensp;<code>list</code>, optional
:   Liste des champs/colonnes des EtatTitre demandés


**```titre_fields```** :&ensp;<code>list</code>, optional
:   Liste des champs/dimensions des Titres demandés


**```freq```** :&ensp;<code>str</code>, optional
:   Fréquence de l'historique (H: Hebdo, Q: Quotidien)
    freq annule la date de Début



###### Returns

<code>list</code>
:   list[KeyedTuple] avec les champs demandés et fonds.code si non fourni



###### Raises

<code>ValueError</code>
:   Si un champs fourni ne figure pas au niveau du modèle de données dudit modèle





    
# Module `hexagon.utils` {#hexagon.utils}




    
## Sub-modules

* [hexagon.utils.app_utils](#hexagon.utils.app_utils)
* [hexagon.utils.attrs_util](#hexagon.utils.attrs_util)
* [hexagon.utils.collection_helpers](#hexagon.utils.collection_helpers)
* [hexagon.utils.dir_utils](#hexagon.utils.dir_utils)
* [hexagon.utils.enumerations](#hexagon.utils.enumerations)
* [hexagon.utils.exceptions](#hexagon.utils.exceptions)
* [hexagon.utils.stringify](#hexagon.utils.stringify)






    
# Module `hexagon.utils.app_utils` {#hexagon.utils.app_utils}






    
## Functions


    
### Function `arrondi` {#hexagon.utils.app_utils.arrondi}



    
> `def arrondi(digits: int) -> <built-in function callable>`


Génère une fonction décoratrice qui arrondit à 'n' chiffres après

la virgule le résultat de la fonction décorée.

```python
>>> @arrondi(2)  # contruit une fonction décoratrice à 2 chiffres après la virgule
>>> def myfunc():
>>>     return 3.456
>>>
>>> rv = myfunc()
>>> rv = 3.46
```



###### Args

digits (<code>int</code>): Nombre de décimales à arrondir

###### Returns

<code>callable</code>: la fonction d'arrondi générée et paramétrée à n chiffres

    
### Function `base_a` {#hexagon.utils.app_utils.base_a}



    
> `def base_a(_date: datetime.date, step: int = 0) -> int`


Retourne la base annuelle d'une date ou une date future en passant le nombre de jours step

Cette fonction est utilisée pour la détermination de la base annuelle des coupons des obligations.

    
### Function `check_periodicite_titre` {#hexagon.utils.app_utils.check_periodicite_titre}



    
> `def check_periodicite_titre(value: str) -> str`


Vérifie si la périodicité du titre est permise et retourne un des choix suivants:

- A: Annuelle
- S: Semestrielle
- T: Trimestrielle
- M: Mensuelle
- B: BiHebdo - 15 jours


###### Args

value (<code>str</code>): Périodicité en str

###### Returns

<code>str</code>: code périodicité 'CHOIX_PERIODICITE'

###### Raises

<code>ValueError</code>: Si la périodicité n'est pas permise

    
### Function `current_machine` {#hexagon.utils.app_utils.current_machine}



    
> `def current_machine(upper: bool = True) -> str`


Fournit le nom de la machine: os.environ['COMPUTERNAME']


###### Returns

<code>str</code>: Computer Name as in platform.

    
### Function `current_platform` {#hexagon.utils.app_utils.current_platform}



    
> `def current_platform(upper: bool = True) -> str`


Retourne la plateforme de l'Application, utlise platform.system()


###### Args

upper (<code>bool</code>, optional): Si True, retourne le système en MAJUSCULES

    
### Function `current_user` {#hexagon.utils.app_utils.current_user}



    
> `def current_user(upper: bool = True) -> str`


Retourne le nom d'utilisateur de la session: os.getlogin()


###### Returns

<code>str</code>: User Name as in os.getlogin().upper()

    
### Function `dotattrs` {#hexagon.utils.app_utils.dotattrs}



    
> `def dotattrs(*args) -> <built-in function callable>`


Fonction qui controune les limites de operator.attrgetter.
Pour les champs ayant un séparateur comme Point('.'), attrgetter considère chaque
part comme un objet and walks the dotpath: 'titre.classe' ==> obj.titre.classe ce qui n'est pas vrai pour les
structures à accès hybride comme namedTuple, Row, KeyedTuple et ceux créés par le framework attrs


###### Args

**```*args```**
:   Liste des attributs à extraire (must be unpacked!)



###### Returns

<code>callable</code>: Fonction d'accès aux champs définis

    
### Function `get_mois` {#hexagon.utils.app_utils.get_mois}



    
> `def get_mois(adate: datetime.date, *, num_mois: int = None) -> str`


Retourne le Mois en Français d'une date ou d'un numéro de mois.

Si num_mois est None prend le mois de la date

Si num_mois fourni Ne prend pas la date en considération et retourne la représentation du Mois de num_mois

Args:

- adate (<code>dt.date</code>): Date voulue
- num_mois (<code>int</code>, None): Le numéro du Mois (1=Janvier, 2=Février, 3=Mars, ...)


###### Returns

<code>str</code>: Représentation du numéro de Mois en Français: 2=Février, 3=Mars

###### Raises

<code>UtilsError</code>: Si num_mois n'est pas un mois valide

    
### Function `hexdigest` {#hexagon.utils.app_utils.hexdigest}



    
> `def hexdigest(str_value: str, string_size: int = 32) -> str`


Genere un uid selon l'algo SHA3 256 de 'string_size' depuis le début.

```python
>>> a = hexdigest('Mero89') = 'a7ffc6f8bf1ed76651c14756a061d662'
>>> len(a) = string_size
```


Args:

- str_value (<code>str</code>): Chaine en input
- string_size (int, optional=32): la taille de la chaine de caractères.


###### Returns

<code>str</code>: a string of 32 characters.

###### Raises

<code>UtilsError</code>: Si la variable n'est pas str, ou impossible à décoder.

    
### Function `hexuid` {#hexagon.utils.app_utils.hexuid}



    
> `def hexuid(n: int = 16, url_safe: bool = False) -> str`


Genere un uid selon l'algo SHA3 256 de 32 caractères .

```python
>>> a = hexuid() -> 'a7ffc6f8bf1ed76651c14756a061d662'
>>> len(a) = 32
```



###### Returns

<code>str</code>: a string of 32 characters.

    
### Function `humanize_field_name` {#hexagon.utils.app_utils.humanize_field_name}



    
> `def humanize_field_name(field_name: str) -> str`


Transforme les champs avec les underscore et tirets en champs en mode Title
date_marche => Date Marche. Au moins Plus présentable.


###### Args

field_name (<code>str</code>): champs à transformer

###### Returns

<code>str</code>: Champs en title

    
### Function `humanize_period` {#hexagon.utils.app_utils.humanize_period}



    
> `def humanize_period(period: dict, separator=':') -> str`


Transforme une période 'relativedelta' en une forme humaine
```python
>>> period = {'days': 15, months': 3, 'years': 2}
>>> humanize_period(period) => '15D:3M:2Y'
```


Args:

- period (<code>dict</code>): Période au sens relativedelta
- separator (<code>str</code>, optional): Description


###### Returns

<code>str</code>: Période humaine classique {'years': 3} -> '3Y'

    
### Function `is_cached` {#hexagon.utils.app_utils.is_cached}



    
> `def is_cached(anyfunc: <built-in function callable>) -> bool`


Check si la fonction est cachée par functools.lru_cache

en vérifiant la véracité de tous les attributs:
('cache_clear', 'cache_info', 'cache_parameters')


###### Args

anyfunc (<code>callable</code>): Any function

###### Returns

<code>bool</code>: True is cached

    
### Function `lower_string` {#hexagon.utils.app_utils.lower_string}



    
> `def lower_string(str_value: str) -> str`


Supprime les espaces aux extrémités et transforme en Minuscule une chaine de caractères (CdC)


###### Args

str_value (<code>str</code>): CdC à transformer en Lower Case

###### Returns

<code>str</code>: Lower Cased String, Chaine en Minuscule

    
### Function `none_sort` {#hexagon.utils.app_utils.none_sort}



    
> `def none_sort(iterable: collections.abc.Iterable, *, key: <built-in function callable> = None, reverse=False, none_last=True) -> collections.abc.Iterator`


Ordonne une liste d'objets python en prenant en considération Les NoneType.
se comporte similairement au sorting de python (key and reverse) et groupe les elements
ayant None soit au début soit vers la fin.

N.B: La fonction est de Dimension 1 (1D), elle ne classe qu'un element/attribut à la fois
ex: key=op.itemgetter(1, 3, 5) ou key=op.attrgetter('attr1', 'attr2', 'attr3')

Args:

- iterable (Iterable): Iterable (tuple, list, set, etc)
- key (<code>callable</code>, None): fonction à appliquer aux objets ex: op.attrgetter, op.itemgetter, etc
- reverse (<code>bool</code>, False): Ordre ascendant par défaut
- none_last (<code>bool</code>, True): Groupe les none vers la fin par défaut, False pour les placer au début


###### Returns

<code>Iterator</code>: <code>itertools.chain</code>, le choix de la structure de retour reste à la discretion de l'utilisateur

    
### Function `partition` {#hexagon.utils.app_utils.partition}



    
> `def partition(predicate: <built-in function callable>, iterable: collections.abc.Iterable) -> tuple[collections.abc.Iterator]`


Use a predicate to partition entries into false entries and true entries

    
### Function `reset_cache` {#hexagon.utils.app_utils.reset_cache}



    
> `def reset_cache(anyfunc: <built-in function callable>) -> bool`


Rafraichit le cache d'une fonction décorée par lru_cache


###### Args

anyfunc (<code>callable</code>): Any function

###### Returns

<code>bool</code>: True si le cache est rafraichi

###### Raises

<code>UtilsError</code>
:   Si la fonction n'est pas @lru_cache décorée



    
### Function `struct_periodes_from_str` {#hexagon.utils.app_utils.struct_periodes_from_str}



    
> `def struct_periodes_from_str(list_periodes: str, separator=',') -> list[str]`


Sépare une liste de périodes représentée par une chaine de caractères et
séparés par un séparateur <code>separator</code> en une liste de périodes.

Args:

- list_periodes (<code>str</code>): Liste des Périodes en chaine de caratères
- separator (<code>str</code>, optional): séprateur de la chaine de caratères


###### Returns

<code>list</code>[`str`]: Liste des périodes

    
### Function `timestamp` {#hexagon.utils.app_utils.timestamp}



    
> `def timestamp() -> datetime.datetime`


Retourne le timestamp

    
### Function `to_bool` {#hexagon.utils.app_utils.to_bool}



    
> `def to_bool(value: str) -> bool`


Convertit une cdc en booléen selon un mapping simple

```python
>>> to_bool('Y') -> True
>>> to_bool('O') -> True
```



###### Args

value (<code>str</code>): Une valeur décrivant un état booléan

###### Returns

<code>bool</code>: True ou False

    
### Function `to_date` {#hexagon.utils.app_utils.to_date}



    
> `def to_date(value: str) -> datetime.date`


Retourne une date depuis un string, format dd/mm/yyyy ou dd-mm-yyyy

    
### Function `to_datetime` {#hexagon.utils.app_utils.to_datetime}



    
> `def to_datetime(value: str) -> datetime.datetime`


Retourne un datetime depuis un string, format dd/mm/yyyy ou dd-mm-yyy

    
### Function `to_float` {#hexagon.utils.app_utils.to_float}



    
> `def to_float(float_value: str) -> float`


Valide un texte qui représente un nombre rationnel.


```python
>>> float('2.5') = 2.50  # float
```



###### Args

float_value (<code>str</code>): nombre en texte

###### Returns

<code>float</code>: Le nombre en unité de base

    
### Function `to_int` {#hexagon.utils.app_utils.to_int}



    
> `def to_int(int_value: str) -> int`


Valide un texte qui représente un nombre entier.


###### Args

int_value (<code>str</code>): nombre entier en texte

###### Returns

<code>int</code>: Le nombre entier en unité de base

    
### Function `to_pip` {#hexagon.utils.app_utils.to_pip}



    
> `def to_pip(pip_value: float) -> float`


Valide une valeur en points de base (pb).

```python
>>> to_pip(80) = 0.008  # 0.8%
```



###### Args

pip_value (<code>float</code>): Un taux/spread exprimé en point de base ou pip

###### Returns

<code>float</code>: Le spread en unité naturelle: 1pb = 1e-4

    
### Function `to_yield` {#hexagon.utils.app_utils.to_yield}



    
> `def to_yield(yield_value: float) -> float`


Valide une valeur en pourcentage.

```python
>>> to_yield(2.5) = 0.025  # 2.5 %
```



###### Args

yield_value (<code>float</code>): un pourcentage

###### Returns

<code>float</code>: Le pourcentage en unité naturelle: 2.5% = 0.025

    
### Function `truncate` {#hexagon.utils.app_utils.truncate}



    
> `def truncate(value: float, digits: int = 0)`


Tronque le float passé en paramètres à n chiffres après la virgule

    
### Function `upper_string` {#hexagon.utils.app_utils.upper_string}



    
> `def upper_string(str_value: str) -> str`


Supprime les espaces aux extrémités et transforme en Majuscule une chaine de caractères (CdC)


###### Args

str_value (<code>str</code>): CdC à transformer en Upper Case

###### Returns

<code>str</code>: Upper Cased String, Chaine en Majuscule

    
### Function `validate_methode_valo` {#hexagon.utils.app_utils.validate_methode_valo}



    
> `def validate_methode_valo(value: str) -> str`







    
# Module `hexagon.utils.attrs_util` {#hexagon.utils.attrs_util}

Helpers functions for attrs structs




    
## Functions


    
### Function `attrs_date_converter` {#hexagon.utils.attrs_util.attrs_date_converter}



    
> `def attrs_date_converter(converter: cattrs.converters.Converter = None) -> cattrs.converters.Converter`


Ajoute à un converter object les hooks pour structurer/destructurer un date.date object.

Unstructuring: Attrs object ==> dict
Structuring: dict => Attrs object

Yaaay, A Converter object acts for both ways. Qt is ringing a bell!

    
### Function `attrs_datetime_converter` {#hexagon.utils.attrs_util.attrs_datetime_converter}



    
> `def attrs_datetime_converter(converter: cattrs.converters.Converter = None) -> cattrs.converters.Converter`


Ajoute à un converter object les hooks pour structurer/destructurer un date.date object.

Unstructuring: Attrs object ==> dict
Structuring: dict => Attrs object

Yaaay, A Converter object acts for both ways. Qt is ringing a bell!

    
### Function `attrs_path_converter` {#hexagon.utils.attrs_util.attrs_path_converter}



    
> `def attrs_path_converter(converter: cattrs.converters.Converter = None) -> cattrs.converters.Converter`


Ajoute à un converter object les hooks pour structurer/destructurer un Path object.

Unstructuring: Attrs object ==> dict
Structuring: dict => Attrs object

Yaaay, A Converter object acts for both ways. Qt is ringing a bell!

    
### Function `attrs_yaml_converter` {#hexagon.utils.attrs_util.attrs_yaml_converter}



    
> `def attrs_yaml_converter(converter: cattrs.converters.Converter = None) -> cattrs.converters.Converter`


Construit un converter pour Les config Files cette fonction évoluera avec le temps.
Prend le converter passé et le configure pour supporter des types supplémentaires (Path, date, datetime)
retourne le même objet


###### Args

**```converter```** :&ensp;<code>cattrs.Converter</code>, optional
:   Configure et retourne le converter passé, crée un nouveau if None.



###### Returns

<code>cattrs.Converter</code>
:   attrs.Converter avec support de types supplémentaires python



    
### Function `get_value_object` {#hexagon.utils.attrs_util.get_value_object}



    
> `def get_value_object(attr_object, attr_name: str, attr_cls, keep_attribute: bool = False)`




    
### Function `structure_date` {#hexagon.utils.attrs_util.structure_date}



    
> `def structure_date(val: Any, _)`




    
### Function `structure_datetime` {#hexagon.utils.attrs_util.structure_datetime}



    
> `def structure_datetime(val: Any, _)`




    
### Function `structure_path` {#hexagon.utils.attrs_util.structure_path}



    
> `def structure_path(val: Any, _: Any)`




    
### Function `walk_attr_struct` {#hexagon.utils.attrs_util.walk_attr_struct}



    
> `def walk_attr_struct(attr_object, attr_cls, parents: list[str] = None) -> Generator[str, attr._make.Attribute, NoneType]`


Traverse un objet attrs et extrait les paramètres ayant la même classe que attr_cls
Retourne un generateur (dotpath de l'objet, et attribut object attrs.Attribute)
Le dotpath est généré relativement à l'objet de configuration attr_object

L'ordre des objets est en breadth first

###### Args

attr_object (BaseConfig()): Instance de configuration
**```attr_cls```** :&ensp;<code>BaseConfig</code>
:   Classe du paramètre à chercher


**```parents```** :&ensp;<code>list\[str]</code>, optional
:   Liste des attributs suivant l'ordre de
    leur hierarchie par rapport à l'objet de configuration



###### Returns

<code>Generator\[str, attrs.Attribute]</code>
:   (dotpah: str, attrs.Attribute)






    
# Module `hexagon.utils.collection_helpers` {#hexagon.utils.collection_helpers}

Helper functions pour grouper les collections d'Objets

* groupby_attr: Permet de grouper les listes d'Objets Python
    en passant un attribut en Chaines de caractères

* groupby_key: Permet de grouper les Iterable[dict] ou Iterable[list] en passant soit
    un key:str pour les Iterable[dict] ou key:int pour les Iterable[list]




    
## Functions


    
### Function `groupby_attr` {#hexagon.utils.collection_helpers.groupby_attr}



    
> `def groupby_attr(collection: collections.abc.Iterable[dict | list], group_attr: collections.abc.Iterable[str]) -> dict[str, collections.abc.Iterable]`


Groupe la collection d'objets en un Dict avec les valeurs du group_attr comme Keys


###### Args

**```collection```**
:   List of objects


**```group_attr```**
:   L'attribut à utiliser pour regrouper les elements.



###### Returns

<code>dict</code>
:   Les 'keys' sont les groupes et les 'values' sont une Iterable[Item].



    
### Function `groupby_key` {#hexagon.utils.collection_helpers.groupby_key}



    
> `def groupby_key(collection: collections.abc.Iterable[dict | list], group_key: collections.abc.Iterable[str | int]) -> dict[str, collections.abc.Iterable]`


Groupe la collection d'objets en un Dict avec les valeurs du group_key comme Keys

N.B: Dans le cas d'une Iterable[list] group_key peut prendre la valeur d'un Entier (int)


###### Args

**```collection```**
:   List of dict[]


**```group_key```**
:   clé à utiliser pour regrouper les elements.



###### Returns

<code>dict</code>
:   Les 'keys' sont les groupes et les 'values' une liste du dictionnaire.




    
## Classes


    
### Class `IterableAgg` {#hexagon.utils.collection_helpers.IterableAgg}



> `class IterableAgg(data: list[dict] = None)`


A more or less complete user-defined wrapper around list objects.


    
#### Ancestors (in MRO)

* [collections.UserList](#collections.UserList)
* [collections.abc.MutableSequence](#collections.abc.MutableSequence)
* [collections.abc.Sequence](#collections.abc.Sequence)
* [collections.abc.Reversible](#collections.abc.Reversible)
* [collections.abc.Collection](#collections.abc.Collection)
* [collections.abc.Sized](#collections.abc.Sized)
* [collections.abc.Iterable](#collections.abc.Iterable)
* [collections.abc.Container](#collections.abc.Container)





    
#### Static methods


    
##### `Method master_field_keyfunc` {#hexagon.utils.collection_helpers.IterableAgg.master_field_keyfunc}



    
> `def master_field_keyfunc(champs_somme: list[str] | str, *, root_name: str = 'root')`


Cette fonction crée une fonction clé pour qui permet de grouper les lignes de la structure de données

Dans le cas normal, nous groupons par les valeurs d'un champs
i.e: champs=catégorie, vals=cat1, cat2, cat3

mais si nous voulons aggréger un attribute sur toute la population,
une clé constante qui va tout grouper est nécessaire la clé candidate
est le champs lui même. i.e: champs=categorie, vals='categorie' ou 'root' si la clé n'est pas inférée


###### Args

champs_somme (str| list[str]): Champs ou liste de champs à aggréger
**```root_name```** :&ensp;<code>str</code>, optional
:   Le champs par défaut si la clé est absente



###### Returns

`callable(str| tuple[str]): Fonction d'accès qui reçoit un arg`
:   &nbsp;




    
#### Methods


    
##### Method `agg_sum` {#hexagon.utils.collection_helpers.IterableAgg.agg_sum}



    
> `def agg_sum(self, champs_agg: list[str], champs_somme: list[str]) -> dict`


Fonction générale de dispatch en fonction du type des arguments passés


###### Args

**```champs_agg```** :&ensp;<code>list\[str]</code>
:   Liste des Dimensions à utiliser


**```champs_somme```** :&ensp;<code>list\[str]</code>
:   Liste des mesures à utiliser



###### Returns

<code>Dict</code>
:   Mapping selon les arguments passés



    
##### Method `agg_sum_many_many` {#hexagon.utils.collection_helpers.IterableAgg.agg_sum_many_many}



    
> `def agg_sum_many_many(self, champs_agg: list[str], champs_somme: list[str]) -> dict`


Somme les valeurs selon plusieurs dimensions et plusieurs mesures


###### Args

**```champs_agg```** :&ensp;<code>list\[str]</code>
:   Liste des champs de dimension


**```champs_somme```** :&ensp;<code>list\[str]</code>
:   Liste des mesures à sommer



###### Returns

<code>dict\[tuple\[champs\_agg]</code>
:   list[champs_somme]]:  Mapping avec tuple des dimensions et liste des mesures sommés



    
##### Method `agg_sum_many_one` {#hexagon.utils.collection_helpers.IterableAgg.agg_sum_many_one}



    
> `def agg_sum_many_one(self, champs_agg: list[str], champs_somme: str) -> dict`


Somme les valeurs selon plusieurs dimensions et un attribut


###### Args

**```champs_agg```** :&ensp;<code>list\[str]</code>
:   Liste des champs de dimension


**```champs_somme```** :&ensp;<code>str</code>
:   mesures à sommer



###### Returns

<code>dict\[tuple\[champs\_agg]</code>
:   champs_somme]: Mapping avec tuple des dimensions et attribut sommé



    
##### Method `agg_sum_one_many` {#hexagon.utils.collection_helpers.IterableAgg.agg_sum_one_many}



    
> `def agg_sum_one_many(self, champs_agg: str, champs_somme: list[str]) -> dict`


Somme les valeurs selon une dimension et plusieurs mesures


###### Args

**```champs_agg```** :&ensp;<code>str</code>
:   Dimension à utiliser


**```champs_somme```** :&ensp;<code>list\[str]</code>
:   Liste des mesures à sommer



###### Returns

<code>dict\[champs\_agg</code>
:   list[champs_somme]]: Mapping avec dimension unique et liste des mesures sommés



    
##### Method `agg_sum_one_one` {#hexagon.utils.collection_helpers.IterableAgg.agg_sum_one_one}



    
> `def agg_sum_one_one(self, champs_agg: str, champs_somme: str) -> dict`


Somme les valeurs selon une dimension et un attribut


###### Args

**```champs_agg```** :&ensp;<code>str</code>
:   Dimension à utiliser


**```champs_somme```** :&ensp;<code>str</code>
:   Attribut à sommer



###### Returns

<code>dict\[champs\_agg</code>
:   champs_somme]: Mapping avec champs agg comme clé et champs_somme comme valeur



    
##### Method `count` {#hexagon.utils.collection_helpers.IterableAgg.count}



    
> `def count(self, champs_agg: str) -> collections.Counter`


Compte les éléments uniques de la liste


###### Args

**```champs_agg```** :&ensp;<code>str</code>
:   Le champs à utiliser pour le calendar



###### Returns

<code>Counter</code>
:   Counter object from collections



    
##### Method `keys` {#hexagon.utils.collection_helpers.IterableAgg.keys}



    
> `def keys(self) -> tuple[str]`


This class is a list. keys return the keys of the mapping data
by inspecting the first row or record.

    
##### Method `reduce_weight_subgroup` {#hexagon.utils.collection_helpers.IterableAgg.reduce_weight_subgroup}



    
> `def reduce_weight_subgroup(self, iterable_tuples: list[tuple]) -> list`




    
##### Method `summary_auto_sum_weight` {#hexagon.utils.collection_helpers.IterableAgg.summary_auto_sum_weight}



    
> `def summary_auto_sum_weight(self, champs_agg: list[str], champs_ponderation: str, weight_champs: list[str])`




    
##### Method `unique_values` {#hexagon.utils.collection_helpers.IterableAgg.unique_values}



    
> `def unique_values(self, champs_agg: str) -> set`


Equivalent à distinct de SQL


###### Args

**```champs_agg```** :&ensp;<code>str</code>
:   Le champs à utiliser pour le calendar



###### Returns

<code>set</code>
:   Ensemble des éléments uniques



    
##### Method `unzip_sum_multi` {#hexagon.utils.collection_helpers.IterableAgg.unzip_sum_multi}



    
> `def unzip_sum_multi(self, iterable_tuples: list[tuple]) -> tuple`






    
# Module `hexagon.utils.dir_utils` {#hexagon.utils.dir_utils}






    
## Functions


    
### Function `zip_folder_to_disk` {#hexagon.utils.dir_utils.zip_folder_to_disk}



    
> `def zip_folder_to_disk(dirpath: pathlib.Path, fpath: pathlib.Path, suffix=None)`


Compresse un Dossier présent dans l'OS en un fichier Zip


###### Args

**```dirpath```** :&ensp;<code>Path</code>
:   Chemin du dossier


**```fpath```** :&ensp;<code>Path</code>
:   Chemin du fichier Zip à générer


**```suffix```** :&ensp;<code>None</code>, optional
:   Si Fournie, Prend en considération seulement
    les fichiers ayant l'extension 'suffx'



###### Raises

<code>NotADirectoryError</code>
:   Si le Dossier fournit n'existe pas



    
### Function `zip_folder_to_memory` {#hexagon.utils.dir_utils.zip_folder_to_memory}



    
> `def zip_folder_to_memory(dirpath: pathlib.Path, suffix=None) -> _io.BytesIO`


Compresse un Dossier en un fichier Zip en mémoire.
La fonction utilise un fichier temporaire pour générer l'archive .ZIP via ZipFile
Le contenu du fichier temporaire est recopié en BytesIO et détruit via close()


###### Args

**```dirpath```** :&ensp;<code>Path</code>
:   Chemin du dossier


**```suffix```** :&ensp;<code>None</code>, optional
:   Si Fournie, Prend en considération seulement
    les fichiers ayant l'extension 'suffx'



###### Raises

<code>NotADirectoryError</code>
:   Si le Dossier fourni n'existe pas



###### Returns

<code>io.BytesIO</code>
:   Stream BytesIO avec le contenu du fichier Temporaire




    
## Classes


    
### Class `HexagonPlatformDirs` {#hexagon.utils.dir_utils.HexagonPlatformDirs}



> `class HexagonPlatformDirs(config: hexagon.utils.dir_utils.PlatformDirsConfig = None, **kwargs)`


Manager des dossiers classiques avec des accessors prédéfinis pour l'application.


#### Attributes

**```config```**
:   PlatformDirsConfig(
    appname, appauthor, version, data_folder_name,
    config_folder_name, downloads_folder_name


Instancie PlatformDirs depuis PlatFormDirsConfig
PlatformDirsConfig(
    appname: str = 'AppName'
    appauthor: str = None
    version: str = None
    data_folder_name: str = 'data'
    config_folder_name: str = 'config'
    downloads_folder_name: str = 'Downloads'
)


#### Args

**```config```** :&ensp;<code>[PlatformDirsConfig](#hexagon.utils.dir\_utils.PlatformDirsConfig "hexagon.utils.dir\_utils.PlatformDirsConfig")</code>, optional
:   Description


**```**kwargs```**
:   Paramètres pour PlatformDirs.




    
#### Ancestors (in MRO)

* [platformdirs.macos.MacOS](#platformdirs.macos.MacOS)
* [platformdirs.api.PlatformDirsABC](#platformdirs.api.PlatformDirsABC)
* [abc.ABC](#abc.ABC)




    
#### Instance variables


    
##### Variable `config_folder_name` {#hexagon.utils.dir_utils.HexagonPlatformDirs.config_folder_name}



    
##### Variable `data_folder_name` {#hexagon.utils.dir_utils.HexagonPlatformDirs.data_folder_name}



    
##### Variable `documents_folder_name` {#hexagon.utils.dir_utils.HexagonPlatformDirs.documents_folder_name}



    
##### Variable `downloads_folder_name` {#hexagon.utils.dir_utils.HexagonPlatformDirs.downloads_folder_name}



    
##### Variable `existing_folders` {#hexagon.utils.dir_utils.HexagonPlatformDirs.existing_folders}

Liste les dossiers gérés existants


###### Returns

<code>dict\[str, Path]</code>
:   Liste des dossiers existants avec leurs clés.



    
##### Variable `list_folders` {#hexagon.utils.dir_utils.HexagonPlatformDirs.list_folders}

Liste tous les dossiers utilisés par la plateforme.
Les dossiers Utilisateurs sont exclus des opérations de création et de suppression.


###### Returns

<code>dict\[str, Path]</code>
:   Liste des dossiers avec leurs clés.



    
##### Variable `managed_folders` {#hexagon.utils.dir_utils.HexagonPlatformDirs.managed_folders}

Liste les dossiers gérés selon le mapping prédéfini.
Les dossiers Utilisateur sont exclus
  - Dossier Home
  - Dossier Documents
  - Dossier Downloads

    
##### Variable `not_existing_folders` {#hexagon.utils.dir_utils.HexagonPlatformDirs.not_existing_folders}

Liste les dossiers gérés non existants


###### Returns

<code>dict\[str, Path]</code>
:   Liste des dossiers non existants avec leurs clés.



    
##### Variable `user_config_path` {#hexagon.utils.dir_utils.HexagonPlatformDirs.user_config_path}

Retourne le chemin du dossier de configuration de Hexagon.
Ajoute self.config_folder_name à super().user_config_path

    
##### Variable `user_data_path` {#hexagon.utils.dir_utils.HexagonPlatformDirs.user_data_path}

Retourne le chemin du dossier de données (Data) de Hexagon.
Ajoute self.data_folder_name=data à super().user_data_path

    
##### Variable `user_documents_path` {#hexagon.utils.dir_utils.HexagonPlatformDirs.user_documents_path}

Retourne le chemin du dossier Documents de l'utilisateur et
Ajoute self.documents_folder_name default=config.appname à 
super().user_documents_path retourne seulement le dossier principal documents

    
##### Variable `user_downloads_path` {#hexagon.utils.dir_utils.HexagonPlatformDirs.user_downloads_path}

Retourne Home Path + 'Downloads'. This is just a guess
Ajoute self.downloads_folder_name=Hexagon à Path.home()

    
##### Variable `user_home_path` {#hexagon.utils.dir_utils.HexagonPlatformDirs.user_home_path}

Retourne le chemin du dossier Home (Maison) de l'utilisateur

    
##### Variable `user_log_path` {#hexagon.utils.dir_utils.HexagonPlatformDirs.user_log_path}

Retourne le chemin du dossier des logs de Hexagon.
Fait appel à super().user_log_path.

    
##### Variable `user_runtime_path` {#hexagon.utils.dir_utils.HexagonPlatformDirs.user_runtime_path}

Retourne le chemin du dossier du runtime de Hexagon.
Fait appel à super().user_runtime_path.
Idéal pour stocker des fichiers dynamiques.
i.e: pickle files, états des logs (for example), ou un shelve if necessary



    
#### Methods


    
##### Method `create_managed_folders` {#hexagon.utils.dir_utils.HexagonPlatformDirs.create_managed_folders}



    
> `def create_managed_folders(self, recursive: bool = True)`




    
##### Method `create_not_existing_folders` {#hexagon.utils.dir_utils.HexagonPlatformDirs.create_not_existing_folders}



    
> `def create_not_existing_folders(self, recursive: bool = True)`


Crée les dossiers gérés non existants
Si dossier existant abandonne la création

###### Args

**```recursive```** :&ensp;<code>bool</code>, optional
:   Crée toute l'arborescence if True



###### Raises

<code>PlatformDirsError</code>
:   Problème d'accès ou de permissions.



    
##### Method `delete_managed_folders` {#hexagon.utils.dir_utils.HexagonPlatformDirs.delete_managed_folders}



    
> `def delete_managed_folders(self)`


Supprime les dossiers gérés s'ils sont vides. utilise managed_folders

    
##### Method `user_cache_path` {#hexagon.utils.dir_utils.HexagonPlatformDirs.user_cache_path}



    
> `def user_cache_path(self, cache_region: str = None) -> pathlib.Path`


Retourne le chemin du dossier de cache de Hexagon.
Fait appel à super().user_cache_path.

  • Passer 'cache_region' pour isoler des régions de cache en dossiers.
    Ajoute 'cache_region' à super().user_cache_path.

    
### Class `PlatformDirsConfig` {#hexagon.utils.dir_utils.PlatformDirsConfig}



> `class PlatformDirsConfig(appname: str = 'Hexagon', appauthor: str = None, version: str = None, data_folder_name: str = 'data', config_folder_name: str = 'config', downloads_folder_name: str = 'Downloads')`


Method generated by attrs for class PlatformDirsConfig.





    
#### Instance variables


    
##### Variable `appauthor` {#hexagon.utils.dir_utils.PlatformDirsConfig.appauthor}

Return an attribute of instance, which is of type owner.

    
##### Variable `appname` {#hexagon.utils.dir_utils.PlatformDirsConfig.appname}

Return an attribute of instance, which is of type owner.

    
##### Variable `config_folder_name` {#hexagon.utils.dir_utils.PlatformDirsConfig.config_folder_name}

Return an attribute of instance, which is of type owner.

    
##### Variable `data_folder_name` {#hexagon.utils.dir_utils.PlatformDirsConfig.data_folder_name}

Return an attribute of instance, which is of type owner.

    
##### Variable `downloads_folder_name` {#hexagon.utils.dir_utils.PlatformDirsConfig.downloads_folder_name}

Return an attribute of instance, which is of type owner.

    
##### Variable `version` {#hexagon.utils.dir_utils.PlatformDirsConfig.version}

Return an attribute of instance, which is of type owner.


    
#### Static methods


    
##### `Method default` {#hexagon.utils.dir_utils.PlatformDirsConfig.default}



    
> `def default()`







    
# Module `hexagon.utils.enumerations` {#hexagon.utils.enumerations}







    
## Classes


    
### Class `Include` {#hexagon.utils.enumerations.Include}



> `class Include(value, names=None, *, module=None, qualname=None, type=None, start=1, boundary=None)`


Enumère les inclusions des intervalles au sens mathématique.
Cette classe est à utiliser wherever we have to define an interval.


#### Attributes

**```BOTH```** :&ensp;<code>str</code>
:   Inclut les 2 Bornes [min-max]. Interalle [fermé-fermé]


**```LEFT```** :&ensp;<code>str</code>
:   Inclut La Borne gauche [min-max[. Interalle [fermé-ouvert[


**```RIGHT```** :&ensp;<code>str</code>
:   Inclut La Borne droite ]min-max]. Interalle ]ouvert-fermé]


**```NONE```** :&ensp;<code>str</code>
:   Exclut les 2 Bornes ]min-max[. Interalle ]ouvert-ouvert[




    
#### Ancestors (in MRO)

* [enum.StrEnum](#enum.StrEnum)
* [builtins.str](#builtins.str)
* [enum.ReprEnum](#enum.ReprEnum)
* [enum.Enum](#enum.Enum)



    
#### Class variables


    
##### Variable `BOTH` {#hexagon.utils.enumerations.Include.BOTH}



    
##### Variable `LEFT` {#hexagon.utils.enumerations.Include.LEFT}



    
##### Variable `NONE` {#hexagon.utils.enumerations.Include.NONE}



    
##### Variable `RIGHT` {#hexagon.utils.enumerations.Include.RIGHT}








    
# Module `hexagon.utils.exceptions` {#hexagon.utils.exceptions}







    
## Classes


    
### Class `EcheancierIntervalleError` {#hexagon.utils.exceptions.EcheancierIntervalleError}



> `class EcheancierIntervalleError(...)`


Echeancier mal défini


    
#### Ancestors (in MRO)

* [hexagon.utils.exceptions.IntervalleError](#hexagon.utils.exceptions.IntervalleError)
* [hexagon.utils.exceptions.UtilsError](#hexagon.utils.exceptions.UtilsError)
* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)






    
### Class `EcheancierPeriodeError` {#hexagon.utils.exceptions.EcheancierPeriodeError}



> `class EcheancierPeriodeError(...)`


Si la Construction de l'Echeancier comporte des Erreurs


    
#### Ancestors (in MRO)

* [hexagon.utils.exceptions.PeriodeError](#hexagon.utils.exceptions.PeriodeError)
* [hexagon.utils.exceptions.UtilsError](#hexagon.utils.exceptions.UtilsError)
* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)






    
### Class `IntervalleError` {#hexagon.utils.exceptions.IntervalleError}



> `class IntervalleError(...)`


Intervalle mal défini


    
#### Ancestors (in MRO)

* [hexagon.utils.exceptions.UtilsError](#hexagon.utils.exceptions.UtilsError)
* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)


    
#### Descendants

* [hexagon.utils.exceptions.EcheancierIntervalleError](#hexagon.utils.exceptions.EcheancierIntervalleError)





    
### Class `PeriodeError` {#hexagon.utils.exceptions.PeriodeError}



> `class PeriodeError(...)`


Période d'un Echéancier mal spécifiée


    
#### Ancestors (in MRO)

* [hexagon.utils.exceptions.UtilsError](#hexagon.utils.exceptions.UtilsError)
* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)


    
#### Descendants

* [hexagon.utils.exceptions.EcheancierPeriodeError](#hexagon.utils.exceptions.EcheancierPeriodeError)





    
### Class `PlatformDirsError` {#hexagon.utils.exceptions.PlatformDirsError}



> `class PlatformDirsError(...)`


Exceptions des configurations des dossiers de la plateforme Hexagon


    
#### Ancestors (in MRO)

* [hexagon.utils.exceptions.UtilsError](#hexagon.utils.exceptions.UtilsError)
* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)






    
### Class `UtilsError` {#hexagon.utils.exceptions.UtilsError}



> `class UtilsError(...)`


Exception générique du dossier Utils. Pour le regroupement des Exceptions


    
#### Ancestors (in MRO)

* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)


    
#### Descendants

* [hexagon.utils.exceptions.IntervalleError](#hexagon.utils.exceptions.IntervalleError)
* [hexagon.utils.exceptions.PeriodeError](#hexagon.utils.exceptions.PeriodeError)
* [hexagon.utils.exceptions.PlatformDirsError](#hexagon.utils.exceptions.PlatformDirsError)







    
# Module `hexagon.utils.stringify` {#hexagon.utils.stringify}






    
## Functions


    
### Function `skip_str` {#hexagon.utils.stringify.skip_str}



    
> `def skip_str(value: str)`




    
### Function `stringify` {#hexagon.utils.stringify.stringify}



    
> `def stringify(value: Any) -> str`




    
### Function `stringify_bool` {#hexagon.utils.stringify.stringify_bool}



    
> `def stringify_bool(value: bool) -> str`




    
### Function `stringify_csv` {#hexagon.utils.stringify.stringify_csv}



    
> `def stringify_csv(value: Any) -> str`




    
### Function `stringify_date_dmy` {#hexagon.utils.stringify.stringify_date_dmy}



    
> `def stringify_date_dmy(value: datetime.date) -> str`


Retourne une date au format jj/mm/aaaa

    
### Function `stringify_date_iso` {#hexagon.utils.stringify.stringify_date_iso}



    
> `def stringify_date_iso(value: datetime.date) -> str`




    
### Function `stringify_date_iso_none` {#hexagon.utils.stringify.stringify_date_iso_none}



    
> `def stringify_date_iso_none(value: datetime.date) -> str`




    
### Function `stringify_datetime_dmy` {#hexagon.utils.stringify.stringify_datetime_dmy}



    
> `def stringify_datetime_dmy(value: datetime.datetime) -> str`


Retourne un datetime au format jj/mm/aaaa hh:mm:ss

    
### Function `stringify_datetime_iso` {#hexagon.utils.stringify.stringify_datetime_iso}



    
> `def stringify_datetime_iso(value: datetime.datetime) -> str`




    
### Function `stringify_datetime_iso_none` {#hexagon.utils.stringify.stringify_datetime_iso_none}



    
> `def stringify_datetime_iso_none(value: datetime.datetime) -> str`




    
### Function `stringify_float` {#hexagon.utils.stringify.stringify_float}



    
> `def stringify_float(value: float) -> str`




    
### Function `stringify_float_csv` {#hexagon.utils.stringify.stringify_float_csv}



    
> `def stringify_float_csv(value: float) -> str`




    
### Function `stringify_int` {#hexagon.utils.stringify.stringify_int}



    
> `def stringify_int(value: int) -> str`




    
### Function `stringify_none_empty` {#hexagon.utils.stringify.stringify_none_empty}



    
> `def stringify_none_empty(value: None) -> str`




    
### Function `stringify_none_none` {#hexagon.utils.stringify.stringify_none_none}



    
> `def stringify_none_none(value: None) -> str`




    
### Function `stringify_path` {#hexagon.utils.stringify.stringify_path}



    
> `def stringify_path(value: pathlib.Path)`




    
### Function `stringify_percent` {#hexagon.utils.stringify.stringify_percent}



    
> `def stringify_percent(value: hexagon.utils.stringify.Percent) -> str`




    
### Function `stringify_pip` {#hexagon.utils.stringify.stringify_pip}



    
> `def stringify_pip(value: hexagon.utils.stringify.Percent2) -> str`





    
## Classes


    
### Class `Percent` {#hexagon.utils.stringify.Percent}



> `class Percent(x=0)`


Unité en pourcentage


    
#### Ancestors (in MRO)

* [builtins.float](#builtins.float)






    
### Class `Percent2` {#hexagon.utils.stringify.Percent2}



> `class Percent2(x=0)`


Unité du jargon financier: Représente "0.01 %", "pips" ou 'pip'


    
#### Ancestors (in MRO)

* [builtins.float](#builtins.float)








    
# Module `hexagon.veille` {#hexagon.veille}




    
## Sub-modules

* [hexagon.veille.analytics](#hexagon.veille.analytics)
* [hexagon.veille.entities](#hexagon.veille.entities)
* [hexagon.veille.range_analytics](#hexagon.veille.range_analytics)



    
## Functions


    
### Function `write_veille_analytics` {#hexagon.veille.write_veille_analytics}



    
> `def write_veille_analytics(fpath, debut, fin, *, date_ytd=None, audit=True, in_memory=False, dbsession) -> bytes`





    
## Classes


    
### Class `VeilleAnalytics` {#hexagon.veille.VeilleAnalytics}



> `class VeilleAnalytics(debut: datetime.date, fin: datetime.date, dbsession: sqlalchemy.orm.session.Session, date_ytd: datetime.date = None)`







    
#### Class variables


    
##### Variable `COLS_ANALYTICS` {#hexagon.veille.VeilleAnalytics.COLS_ANALYTICS}



    
##### Variable `STATS_MAP_YTD` {#hexagon.veille.VeilleAnalytics.STATS_MAP_YTD}





    
#### Static methods


    
##### `Method build_records_analytic` {#hexagon.veille.VeilleAnalytics.build_records_analytic}



    
> `def build_records_analytic(records_debut: dict, records_fin: dict, *, skip_actif_moyen: bool = False, dbsession) -> dict`


Construit les statistiques analytiques depuis 2 Mappings à 2 dates différentes
Afin de gérer les nouveaux et anciens Fonds, un index unifié entre les 2 dates est construit

* Si le fonds n'existe pas dans records debut: aum_debut = 0 et vl_debut = vl_fin
* Si le fonds n'existe pas dans records fin: aum_fin = 0 et vl_fin = vl_debut

Consolide et Calcule:
    * aum_debut, aum_fin
    * vl_debut, vl_fin
    * perf, , perf_prod, aum_moyen
    * effet_mkt, effet_sr, var_an


###### Args

**```records_debut```** :&ensp;<code>dict</code>
:   Market View debut


**```records_fin```** :&ensp;<code>dict</code>
:   Market View Fin



###### Returns

<code>dict</code>
:   dict[id: dict[]] Mapping par id du fonds des données calculées



    
##### `Method build_table` {#hexagon.veille.VeilleAnalytics.build_table}



    
> `def build_table(referentiel: dict, records_analytics: dict, *, as_df: bool = True) -> list | pandas.core.frame.DataFrame`


Construit une table en Zippant le referentiel avec les données analytiques afin de construire
une table analytique en Croisé-dynamique


###### Args

**```referentiel```** :&ensp;<code>dict</code>
:   Mapping du referentiel par ID du Fonds


**```records_analytics```** :&ensp;<code>dict</code>
:   Mapping des données analytiques par ID du Fonds


**```as_df```** :&ensp;`bool=True`
:   If True retourne un DataFrame sinon une list[dict[]]



###### Returns

`dict| pd.DataFrame: Table analytique selon le flag as_df`
:   &nbsp;



    
##### `Method build_table_from_records` {#hexagon.veille.VeilleAnalytics.build_table_from_records}



    
> `def build_table_from_records(referentiel: dict, records_analytics: dict, *, as_df: bool = True) -> list | pandas.core.frame.DataFrame`


Construit une table en Zippant le referentiel avec les données analytiques afin de construire
une table analytique en Croisé-dynamique


###### Args

**```referentiel```** :&ensp;<code>dict</code>
:   Mapping du referentiel par ID du Fonds


**```records_analytics```** :&ensp;<code>dict\[list\[dict]]</code>
:   Mapping des données analytiques par ID du Fonds


**```as_df```** :&ensp;`bool=True`
:   If True retourne un DataFrame sinon une list[dict[]]



###### Returns

`list| pd.DataFrame: Table analytique selon le flag as_df`
:   &nbsp;




    
#### Methods


    
##### Method `agg_vue_consolidee` {#hexagon.veille.VeilleAnalytics.agg_vue_consolidee}



    
> `def agg_vue_consolidee(self, by: list[str] = ['sdg'], *, unstack: bool = False) -> pandas.core.frame.DataFrame`




    
##### Method `build_vue_consolidee` {#hexagon.veille.VeilleAnalytics.build_vue_consolidee}



    
> `def build_vue_consolidee(self) -> pandas.core.frame.DataFrame`




    
##### Method `fonds_concurrents` {#hexagon.veille.VeilleAnalytics.fonds_concurrents}



    
> `def fonds_concurrents(self, ytd: bool = False) -> pandas.core.frame.DataFrame`




    
##### Method `get_all` {#hexagon.veille.VeilleAnalytics.get_all}



    
> `def get_all(self, *, audit: bool = True) -> list[tuple[str, pandas.core.frame.DataFrame]]`




    
##### Method `rub_collecte` {#hexagon.veille.VeilleAnalytics.rub_collecte}



    
> `def rub_collecte(self, by: list[str] = ['sdg'], *, unstack: bool = False) -> pandas.core.frame.DataFrame`




    
##### Method `rub_collecte_ytd` {#hexagon.veille.VeilleAnalytics.rub_collecte_ytd}



    
> `def rub_collecte_ytd(self, by: list[str] = ['sdg'], *, unstack: bool = False) -> pandas.core.frame.DataFrame`




    
##### Method `rub_palmares_effet_sr` {#hexagon.veille.VeilleAnalytics.rub_palmares_effet_sr}



    
> `def rub_palmares_effet_sr(self, nth: int = 10, asc: bool = True, *, dedie: bool = None, ytd: bool = True, divide_by: float = 1000000) -> pandas.core.frame.DataFrame`


Rubrique du palmares des collectes par OPCVM


###### Args

**```nth```** :&ensp;`int=10`, optional
:   Le nombre des OPCVM du palmares.


**```asc```** :&ensp;`bool=True`, optional
:   if True, retourne les 'nth' plus petits effet_sr. False: Les 'nth' plus grands effet_sr


**```dedie```** :&ensp;`bool=None`, optional
:   If True, retourne le palmares


**```ytd```** :&ensp;`bool=True`, optional
:   if True, retourne le palmares en YTD. False retourne le palmares sur la période



###### Returns

<code>pd.DataFrame</code>
:   Le palmares indexé par id_fonds et avec les dimensions ['sdg', 'categorie', 'type_gestion']



    
##### Method `rub_pdm` {#hexagon.veille.VeilleAnalytics.rub_pdm}



    
> `def rub_pdm(self, by: list[str] = ['sdg'], *, unstack: bool = False) -> pandas.core.frame.DataFrame`


Agrège les actifs gérés selon les dimensions passées.


###### Args

**```by```** :&ensp;<code>str</code> or <code>list</code>
:   Les dimensions d'aggrégation des mesures,
    pour plusieurs niveaux d'aggrégation passer une liste


**```unstack```** :&ensp;`bool=False`
:   Si vrai, convertit le dernier niveau en colonnes



###### Returns

<code>pd.DataFrame</code>
:   Tableau sur les actifs gérés



    
##### Method `rub_performance` {#hexagon.veille.VeilleAnalytics.rub_performance}



    
> `def rub_performance(self, by: list[str] = ['categorie'], *, unstack: bool = False) -> pandas.core.frame.DataFrame`




    
##### Method `rub_performance_ytd` {#hexagon.veille.VeilleAnalytics.rub_performance_ytd}



    
> `def rub_performance_ytd(self, by: list[str] = ['categorie'], *, unstack: bool = False) -> pandas.core.frame.DataFrame`




    
##### Method `rub_sdg_actif` {#hexagon.veille.VeilleAnalytics.rub_sdg_actif}



    
> `def rub_sdg_actif(self, by: list[str] = ['categorie'], *, unstack: bool = False) -> pandas.core.frame.DataFrame`


Construit la liste des actifs depuis les données internes de CKG


###### Args

**```by```** :&ensp;<code>list</code>
:   Niveaux d'agrégation


**```unstack```** :&ensp;`bool=False`
:   Si vrai, convertit le dernier niveau en colonnes



###### Returns

<code>TYPE</code>
:   Description



    
##### Method `rub_sdg_collecte` {#hexagon.veille.VeilleAnalytics.rub_sdg_collecte}



    
> `def rub_sdg_collecte(self, by: list[str] = ['categorie'], *, unstack: bool = False) -> pandas.core.frame.DataFrame`




    
##### Method `rub_sdg_collecte_ytd` {#hexagon.veille.VeilleAnalytics.rub_sdg_collecte_ytd}



    
> `def rub_sdg_collecte_ytd(self, by: list[str] = ['categorie'], *, unstack: bool = False) -> pandas.core.frame.DataFrame`




    
##### Method `rub_vue_periode` {#hexagon.veille.VeilleAnalytics.rub_vue_periode}



    
> `def rub_vue_periode(self, by: list[str] = ['sdg'], *, unstack: bool = False) -> pandas.core.frame.DataFrame`




    
##### Method `rub_vue_ytd` {#hexagon.veille.VeilleAnalytics.rub_vue_ytd}



    
> `def rub_vue_ytd(self, by: list[str] = ['sdg'], *, unstack: bool = False, rename: bool = False) -> pandas.core.frame.DataFrame`




    
##### Method `sortby` {#hexagon.veille.VeilleAnalytics.sortby}



    
> `def sortby(self, records: list[dict], by: list[str], tag: str = None) -> list[dict]`




    
### Class `VeilleAnalyticsError` {#hexagon.veille.VeilleAnalyticsError}



> `class VeilleAnalyticsError(...)`


Common base class for all non-exit exceptions.


    
#### Ancestors (in MRO)

* [builtins.Exception](#builtins.Exception)
* [builtins.BaseException](#builtins.BaseException)








    
# Module `hexagon.veille.analytics` {#hexagon.veille.analytics}






    
## Functions


    
### Function `write_to_excel` {#hexagon.veille.analytics.write_to_excel}



    
> `def write_to_excel(fpath: str, rubrique_liste: list[tuple[str, pandas.core.frame.DataFrame]], *, in_memory: bool = False) -> bytes`




    
### Function `write_veille_analytics` {#hexagon.veille.analytics.write_veille_analytics}



    
> `def write_veille_analytics(fpath, debut, fin, *, date_ytd=None, audit=True, in_memory=False, dbsession) -> bytes`





    
## Classes


    
### Class `VeilleAnalytics` {#hexagon.veille.analytics.VeilleAnalytics}



> `class VeilleAnalytics(debut: datetime.date, fin: datetime.date, dbsession: sqlalchemy.orm.session.Session, date_ytd: datetime.date = None)`







    
#### Class variables


    
##### Variable `COLS_ANALYTICS` {#hexagon.veille.analytics.VeilleAnalytics.COLS_ANALYTICS}



    
##### Variable `STATS_MAP_YTD` {#hexagon.veille.analytics.VeilleAnalytics.STATS_MAP_YTD}





    
#### Static methods


    
##### `Method build_records_analytic` {#hexagon.veille.analytics.VeilleAnalytics.build_records_analytic}



    
> `def build_records_analytic(records_debut: dict, records_fin: dict, *, skip_actif_moyen: bool = False, dbsession) -> dict`


Construit les statistiques analytiques depuis 2 Mappings à 2 dates différentes
Afin de gérer les nouveaux et anciens Fonds, un index unifié entre les 2 dates est construit

* Si le fonds n'existe pas dans records debut: aum_debut = 0 et vl_debut = vl_fin
* Si le fonds n'existe pas dans records fin: aum_fin = 0 et vl_fin = vl_debut

Consolide et Calcule:
    * aum_debut, aum_fin
    * vl_debut, vl_fin
    * perf, , perf_prod, aum_moyen
    * effet_mkt, effet_sr, var_an


###### Args

**```records_debut```** :&ensp;<code>dict</code>
:   Market View debut


**```records_fin```** :&ensp;<code>dict</code>
:   Market View Fin



###### Returns

<code>dict</code>
:   dict[id: dict[]] Mapping par id du fonds des données calculées



    
##### `Method build_table` {#hexagon.veille.analytics.VeilleAnalytics.build_table}



    
> `def build_table(referentiel: dict, records_analytics: dict, *, as_df: bool = True) -> list | pandas.core.frame.DataFrame`


Construit une table en Zippant le referentiel avec les données analytiques afin de construire
une table analytique en Croisé-dynamique


###### Args

**```referentiel```** :&ensp;<code>dict</code>
:   Mapping du referentiel par ID du Fonds


**```records_analytics```** :&ensp;<code>dict</code>
:   Mapping des données analytiques par ID du Fonds


**```as_df```** :&ensp;`bool=True`
:   If True retourne un DataFrame sinon une list[dict[]]



###### Returns

`dict| pd.DataFrame: Table analytique selon le flag as_df`
:   &nbsp;



    
##### `Method build_table_from_records` {#hexagon.veille.analytics.VeilleAnalytics.build_table_from_records}



    
> `def build_table_from_records(referentiel: dict, records_analytics: dict, *, as_df: bool = True) -> list | pandas.core.frame.DataFrame`


Construit une table en Zippant le referentiel avec les données analytiques afin de construire
une table analytique en Croisé-dynamique


###### Args

**```referentiel```** :&ensp;<code>dict</code>
:   Mapping du referentiel par ID du Fonds


**```records_analytics```** :&ensp;<code>dict\[list\[dict]]</code>
:   Mapping des données analytiques par ID du Fonds


**```as_df```** :&ensp;`bool=True`
:   If True retourne un DataFrame sinon une list[dict[]]



###### Returns

`list| pd.DataFrame: Table analytique selon le flag as_df`
:   &nbsp;




    
#### Methods


    
##### Method `agg_vue_consolidee` {#hexagon.veille.analytics.VeilleAnalytics.agg_vue_consolidee}



    
> `def agg_vue_consolidee(self, by: list[str] = ['sdg'], *, unstack: bool = False) -> pandas.core.frame.DataFrame`




    
##### Method `build_vue_consolidee` {#hexagon.veille.analytics.VeilleAnalytics.build_vue_consolidee}



    
> `def build_vue_consolidee(self) -> pandas.core.frame.DataFrame`




    
##### Method `fonds_concurrents` {#hexagon.veille.analytics.VeilleAnalytics.fonds_concurrents}



    
> `def fonds_concurrents(self, ytd: bool = False) -> pandas.core.frame.DataFrame`




    
##### Method `get_all` {#hexagon.veille.analytics.VeilleAnalytics.get_all}



    
> `def get_all(self, *, audit: bool = True) -> list[tuple[str, pandas.core.frame.DataFrame]]`




    
##### Method `rub_collecte` {#hexagon.veille.analytics.VeilleAnalytics.rub_collecte}



    
> `def rub_collecte(self, by: list[str] = ['sdg'], *, unstack: bool = False) -> pandas.core.frame.DataFrame`




    
##### Method `rub_collecte_ytd` {#hexagon.veille.analytics.VeilleAnalytics.rub_collecte_ytd}



    
> `def rub_collecte_ytd(self, by: list[str] = ['sdg'], *, unstack: bool = False) -> pandas.core.frame.DataFrame`




    
##### Method `rub_palmares_effet_sr` {#hexagon.veille.analytics.VeilleAnalytics.rub_palmares_effet_sr}



    
> `def rub_palmares_effet_sr(self, nth: int = 10, asc: bool = True, *, dedie: bool = None, ytd: bool = True, divide_by: float = 1000000) -> pandas.core.frame.DataFrame`


Rubrique du palmares des collectes par OPCVM


###### Args

**```nth```** :&ensp;`int=10`, optional
:   Le nombre des OPCVM du palmares.


**```asc```** :&ensp;`bool=True`, optional
:   if True, retourne les 'nth' plus petits effet_sr. False: Les 'nth' plus grands effet_sr


**```dedie```** :&ensp;`bool=None`, optional
:   If True, retourne le palmares


**```ytd```** :&ensp;`bool=True`, optional
:   if True, retourne le palmares en YTD. False retourne le palmares sur la période



###### Returns

<code>pd.DataFrame</code>
:   Le palmares indexé par id_fonds et avec les dimensions ['sdg', 'categorie', 'type_gestion']



    
##### Method `rub_pdm` {#hexagon.veille.analytics.VeilleAnalytics.rub_pdm}



    
> `def rub_pdm(self, by: list[str] = ['sdg'], *, unstack: bool = False) -> pandas.core.frame.DataFrame`


Agrège les actifs gérés selon les dimensions passées.


###### Args

**```by```** :&ensp;<code>str</code> or <code>list</code>
:   Les dimensions d'aggrégation des mesures,
    pour plusieurs niveaux d'aggrégation passer une liste


**```unstack```** :&ensp;`bool=False`
:   Si vrai, convertit le dernier niveau en colonnes



###### Returns

<code>pd.DataFrame</code>
:   Tableau sur les actifs gérés



    
##### Method `rub_performance` {#hexagon.veille.analytics.VeilleAnalytics.rub_performance}



    
> `def rub_performance(self, by: list[str] = ['categorie'], *, unstack: bool = False) -> pandas.core.frame.DataFrame`




    
##### Method `rub_performance_ytd` {#hexagon.veille.analytics.VeilleAnalytics.rub_performance_ytd}



    
> `def rub_performance_ytd(self, by: list[str] = ['categorie'], *, unstack: bool = False) -> pandas.core.frame.DataFrame`




    
##### Method `rub_sdg_actif` {#hexagon.veille.analytics.VeilleAnalytics.rub_sdg_actif}



    
> `def rub_sdg_actif(self, by: list[str] = ['categorie'], *, unstack: bool = False) -> pandas.core.frame.DataFrame`


Construit la liste des actifs depuis les données internes de CKG


###### Args

**```by```** :&ensp;<code>list</code>
:   Niveaux d'agrégation


**```unstack```** :&ensp;`bool=False`
:   Si vrai, convertit le dernier niveau en colonnes



###### Returns

<code>TYPE</code>
:   Description



    
##### Method `rub_sdg_collecte` {#hexagon.veille.analytics.VeilleAnalytics.rub_sdg_collecte}



    
> `def rub_sdg_collecte(self, by: list[str] = ['categorie'], *, unstack: bool = False) -> pandas.core.frame.DataFrame`




    
##### Method `rub_sdg_collecte_ytd` {#hexagon.veille.analytics.VeilleAnalytics.rub_sdg_collecte_ytd}



    
> `def rub_sdg_collecte_ytd(self, by: list[str] = ['categorie'], *, unstack: bool = False) -> pandas.core.frame.DataFrame`




    
##### Method `rub_vue_periode` {#hexagon.veille.analytics.VeilleAnalytics.rub_vue_periode}



    
> `def rub_vue_periode(self, by: list[str] = ['sdg'], *, unstack: bool = False) -> pandas.core.frame.DataFrame`




    
##### Method `rub_vue_ytd` {#hexagon.veille.analytics.VeilleAnalytics.rub_vue_ytd}



    
> `def rub_vue_ytd(self, by: list[str] = ['sdg'], *, unstack: bool = False, rename: bool = False) -> pandas.core.frame.DataFrame`




    
##### Method `sortby` {#hexagon.veille.analytics.VeilleAnalytics.sortby}



    
> `def sortby(self, records: list[dict], by: list[str], tag: str = None) -> list[dict]`






    
# Module `hexagon.veille.entities` {#hexagon.veille.entities}







    
## Classes


    
### Class `VeilleAggregate` {#hexagon.veille.entities.VeilleAggregate}



> `class VeilleAggregate(vuid: int = -1, refuid: int = -1, date_marche: datetime.date = None, ref: hexagon.veille.entities.VeilleFonds = None)`


Method generated by attrs for class VeilleAggregate.


    
#### Ancestors (in MRO)

* [hexagon.core.valueobjects.CoreValue](#hexagon.core.valueobjects.CoreValue)






    
### Class `VeilleFonds` {#hexagon.veille.entities.VeilleFonds}



> `class VeilleFonds(uid: int = -1, code: str = None, description: str = None, code_isin: str = None, categorie: hexagon.core.enumerations.CategorieFonds = OPCVM, periodicite: hexagon.core.enumerations.PeriodiciteVL = H, type_gestion: hexagon.core.enumerations.TypeGestion = ND, forme_juridique: hexagon.core.enumerations.FormeJuridique = FCP, promoteur: str = None)`


Method generated by attrs for class VeilleFonds.


    
#### Ancestors (in MRO)

* [hexagon.core.entities.CoreEntity](#hexagon.core.entities.CoreEntity)




    
#### Instance variables


    
##### Variable `categorie` {#hexagon.veille.entities.VeilleFonds.categorie}

Return an attribute of instance, which is of type owner.

    
##### Variable `code_isin` {#hexagon.veille.entities.VeilleFonds.code_isin}

Return an attribute of instance, which is of type owner.

    
##### Variable `forme_juridique` {#hexagon.veille.entities.VeilleFonds.forme_juridique}

Return an attribute of instance, which is of type owner.

    
##### Variable `periodicite` {#hexagon.veille.entities.VeilleFonds.periodicite}

Return an attribute of instance, which is of type owner.

    
##### Variable `promoteur` {#hexagon.veille.entities.VeilleFonds.promoteur}

Return an attribute of instance, which is of type owner.

    
##### Variable `type_gestion` {#hexagon.veille.entities.VeilleFonds.type_gestion}

Return an attribute of instance, which is of type owner.


    
#### Static methods


    
##### `Method converter_enums` {#hexagon.veille.entities.VeilleFonds.converter_enums}



    
> `def converter_enums(rowdict: dict) -> dict`




    
##### `Method from_uid` {#hexagon.veille.entities.VeilleFonds.from_uid}



    
> `def from_uid(uid: int, dbsession: sqlalchemy.orm.session.Session)`




    
##### `Method struct_query` {#hexagon.veille.entities.VeilleFonds.struct_query}



    
> `def struct_query() -> sqlalchemy.sql.selectable.Select`





    
### Class `VeilleValue` {#hexagon.veille.entities.VeilleValue}



> `class VeilleValue(vuid: int = -1, refuid: int = -1, date_marche: datetime.date = None, ref: hexagon.veille.entities.VeilleFonds = None, vl: float = None, actif_net: float = None)`


Method generated by attrs for class VeilleValue.


    
#### Ancestors (in MRO)

* [hexagon.core.valueobjects.CoreValue](#hexagon.core.valueobjects.CoreValue)




    
#### Instance variables


    
##### Variable `actif_net` {#hexagon.veille.entities.VeilleValue.actif_net}

Return an attribute of instance, which is of type owner.

    
##### Variable `vl` {#hexagon.veille.entities.VeilleValue.vl}

Return an attribute of instance, which is of type owner.





    
# Module `hexagon.veille.range_analytics` {#hexagon.veille.range_analytics}






    
## Functions


    
### Function `build_batch_analytics` {#hexagon.veille.range_analytics.build_batch_analytics}



    
> `def build_batch_analytics(debut: datetime.date, fin: datetime.date, periode: str = 'M', *, calendar: hexagon.calendrier.market.MarketCalendar = None, promoteurs: list[str] = None, as_df=True, dbsession=None)`


Construit les statistiques Path Dependent comme effet_mkt_moyen, effet_sr_moyen


###### Args

**```debut```** :&ensp;<code>datetime.date</code>
:   Date de début


**```fin```** :&ensp;<code>datetime.date</code>
:   Date de fin


**```periode```** :&ensp;<code>str</code>
:   one of 'H', 'M', 'T', 'S', 'A'



    
### Function `build_range_analytics` {#hexagon.veille.range_analytics.build_range_analytics}



    
> `def build_range_analytics(debut: datetime.date, fin: datetime.date, *, calendar: hexagon.calendrier.market.MarketCalendar = None, as_df=True, dbsession=None)`


Construit les statistiques Path Dependent comme effet_mkt_moyen, effet_sr_moyen


###### Args

**```debut```** :&ensp;<code>datetime.date</code>
:   Date de début


**```fin```** :&ensp;<code>datetime.date</code>
:   Date de fin





-----
Generated by *pdoc* 0.8.1 (<https://pdoc3.github.io>).
