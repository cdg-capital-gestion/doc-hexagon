# The dawn of Repository Pattern in Hexagon

Avec le temps, il est devenu apparent qu’une modélisation orientée "Repository Pattern" s’avère nécessaire.

Nous adopterons le schéma suivant:

## Repository Infrastructure (R.I):

Ce repository est le plus abstrait et aura comme responsabilité la communication avec les systèmes de persistance
tels que les SGBD, Filesystem, Cloud Storage ou Memory (à la Sqlite).

Le R.I doit être en mesure de faire la séparation entre les différents types de requêtes SELECT, INSERT, UPDATE, DELETE.

Ce repository doit offrir les interfaces publiques suivantes:

- execute_select: fonction qui execute les requêtes de selection/lecture
- execute_insert: fonction qui execute les requêtes/commandes d’insertion/création
- execute_update: fonction qui execute les requêtes/commandes de mise à jour
- execute_delete: fonction qui execute les requêtes/commandes de suppression des enregistrements
- execute_query: fonction générique de dispatch aux autres méthodes

Ces méthodes seront transverses à l’ensemble des implémentations concrètes qui seront utilisées au niveau des différents Domaines ou Modules.

## Selector Repository Domain (S.R.D):

Ce type de repository (S.R.D) sera responsable de construire les requêtes de base pour la création des Entités et des Value Objects du Domaine/Module en question.

Aussi, nous mettrons les filtres par défaut à ce niveau là.

Par exemple: 
    
    une table qui contient des Users avec un flag booléen "is_actif" sera toujours "True"
    si le Domaine/Module ne traite que les Users "Actifs"

Il est préférable d’avoir le mot 'select_' pour les méthodes du S.R.D

Ce repository sera très lourd dans la mesure où il devra gérer l’ensemble des paramètres candidats pour les différents scénarios.

## Repository Domain (R.D):

Enfin on arrive au repository tel que défini par le concept du D.D.D
Ce type de repository sera responsable de charger les instances des Entités/ValueObjects telles que définies par le Domain/Module.

Les méthodes seront explicitement nommées en se conformant au **"Jargon"** du Domaine Métier

tout en conveyant un sens unique sans ambiguïté quant à sa fonction.

A ce niveau là, nous serons peut être temptés par le DRY mais ce sera une mauvaise idée sur le long terme.

L’alternative sera d’écrire autant de méthodes et de fonctions que des nuances des concepts présentés par le Domaine.

Exemple:
    
    load_new_users, load_updated_users: Dans ce cas d’étude, seulement le champs utilisé comme filtre va différer mais nous devons écrire ces Fonctions.
    Le DRY est déjà inclus dans le S.R.D qui offrira le paramètre adéquat.

## Schema conceptuel:

L'implémentation adoptée pour Hexagon est basée/adaptée sur les possibilités qu'offre Python et spécialement sqlalchemy
C'est très concret pour l'instant, l'idée est de valider le design et de tester son scaling sur le temps.
C'est le bon moment vu que toute manière nous étions obligés de refactorer la couche d'accès à un système de persistance.

La route expérimentale (dans un environnement maitrisé a été favorisée dans ce chantier)

Le design Actuel est d'exploiter les inner functions de sqlalchemy afin de pouvoir monter un système sur 3 couches conceptuelles du "**Repository Pattern**"

#### Points Importants:

Cette implémentation prend comme acquis certaines fonctionnalités ou propriétés de sqlalchemy, la liste ci-dessous explique le pourquoi de la chose:

 - Possibilité de générer des requêtes en **PUR SQL** et selon le dialect choisi de la BDD (Postgres, Sqlite, Oracle, ...)
 - Possibilité de définir des requêtes indépendemment de la session ou de l'Engine
 - Possibilité de définir des schémas AD-Hoc à partir seulement d'un MetaData
 - La propriété du context manager lors de l'execution des requêtes
 - La propriété de composabilité interne de l'objet Query que ça soit un Select ou Update, etc
 - La propriété de modifier/construire l'objet query en mode **pipeline**
 - Une abondance de hooks de contrôle du flux d'exécution des requêtes via **l'Event Systèm** offert
 - Un design et un niveau de maintenance avangardiste par rapport à l'ensemble des ORM de tous langages de programmation confondus
 - Une suite de tests solide
 - Unit of Work pattern par session

#### Représentation sommaire (UML):

```mermaid
classDiagram
    RepositoryInfrastructure<|-- SelectorRepositoryDomain
    SelectorRepositoryDomain <|-- RepositoryDomain
    class RepositoryInfrastructure{
      +Session dbsession
      +Logger logger
      +str name = "R.I"
      +execute_select(query: Select)
      +execute_insert(query: Insert)
      +execute_update(query: Update)
      +execute_delete(query: Delete)
      +execute_query(query: Any)
    }
    class SelectorRepositoryDomain{
      +Session dbsession
      +Logger logger
      +str name = "S.R.D"
      +select_alion()
    }
    class RepositoryDomain{
      +Session dbsession
      +Logger logger
      +str name = "R.D"
      +load_lion_of_atlas()
    }
```
