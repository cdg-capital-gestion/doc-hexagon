# Module Calendrier

Le module calendrier est responsable des opérations de calcul des périodes relativement à une date marché.
L'implémentation actuelle adhère rigoureusement aux règles de l'ASFIM et des méthodes supplémentaires
de manipulation des dates ouvrables.

??? info "Périodicités des OPCVM"
  
    Les `OPCVM` ont deux périodicités de VL `Quotidienne` et `Hebdomadaire`.
    L'ASFIM définit une règle de calcul propre à chacune d'entre elles.

# MarketCalendar

::: hexagon.calendrier.market.MarketCalendar
    handler: python
