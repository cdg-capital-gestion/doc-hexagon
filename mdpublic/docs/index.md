# Présentation Hexagon - version **2023.04.1**

Hexagon est application orientée "Data", développée en interne au sein de CDG Capital Gestion
pour répondre principalement aux besoin des reporting OPCVM.

Dans ce sens et avec le temps Hexagon a pris une dimension plus large et doit répondre à d'autres besoins
vu qu'il produit déjà les chiffres nécessaires.

Chose qui a poussé à une réécriture totale d'Hexagon core en **Middle-End** c-à-d entre une librairie de fonctions et de classes et une application à part entière
avec les interactions et les règles de gestion.

L'approche adoptée est innovante et risquée en même temps - version '2023.04'-CalVer cependant les
résultats obtenus sont intéressants d'un point de vue "System Design" à savoir le fonctionnement en mode hybride:

- **Choix de Python (3.11+):**
    - Supporte un haut niveau d'abstraction facilitant la conception et la modélisation du système.
    - Facilite la traduction des concepts mathématiques grâce à la richesse
        des structures de données de base (`list`, `set`, `dict`, `deque`, `defaultdict`, `ChainMap`, `heapq`, `Enum`)
    - Spécification mathématiquement correcte des valeurs nulles "None" (Singleton et évalué à False)
    - Multi-paradigme: Supporte la programmation fonctionnelle, comportementale et Orientée objet, Asynchrone
    - Le concept des itérateurs est inhérent au design du langage
    - Supporte les fonctions, les coroutines et les générateurs as first class citizens
    - Riche écosystème de librairies pour des besoins spécifiques:
        (Webserver, Filesystem, Charting, Data Analysis, Scraping, WebRequests, GUI, DataBases, ...)
    - **Sqlalchemy:** **Le meilleur ORM tous langages confondus** une librairie hors norme qui mérite d'être cité.

- **Mode interactif:**
    - Accès aux fonctionnalités bas niveau d'Hexagon (Modules et domaines définis)
    - Execution interactive des fonctionnalités
    - Offre des capacités de développer des scripts personnalisés (Utilisateurs initiés)
    - Aide interactive pour tous les modules Hexagon
    - Simulation et Analytics

- **Domain Driven Design (DDD):**
    
    Généralement le DDD n'est pas utilisé pour un système analytique mais les principes qu'il prône restent valables.

    Afin d'atteindre le Middle-End, Hexagon a adapté le DDD en définissant le domaine des OPCVM et d'un marché financier
    à part entière (Fonds, Portefeuille, Dates, Indices, AMMC, Devise, Souscripteur, Opération, ....)
    
    Suite à ce refactoring (radical), Hexagon s'est doté des fonctionnalités ou plutôt des propriétés suivantes:
    
    - Définition claire et uniforme des entités mises en jeu dans Hexagon. Titre, Fonds, Performance, Percent, ...
    - Réduction des dépendances externes.
    - Possibilité de configurer chaque module at runtime en mode interactif sans avoir à redémarrer l'application
    - Utilisation de la théorie des ensembles pour les Données/Entités reférentiel (Fonds, Indice, Titre)
    - Meilleure implémentation du concept des "Assimilations" (X est assimilable à Y) pour les entités du référentiel:
        - Un Fonds est une entité unique mais joue des rôles différents selon le contexte (Titre, Contrepartie, Emetteur, OPCVM)
        C'est la raison principale derrière l'adoption du DDD, Hexagon définit des CoreEntities multirôles et à identité unique
    - Séparation des Converters et Adapters entre les boundaries de chaque domaine.
    - Application plus Testable (Chaque domaine définit son langage)
    - Possiblité de faire des simulations basées sur des scénarios.
    - Gestion des fichiers de configuration par store/contexte et séparation du code source en
        utilisant les dossiers par défaut de chaque OS:
        - Windows: ~Home/AppData/Local/Hexagon
        - Mac OSX: ~Home/Library/Application Support/Hexagon
        - Linux/Red Hat: ~Home/.config/Hexagon
    - Séparation claire entre les modèles conceptuels et leurs contreparties “données"
    - Optimisation de l'ordre de 30~80X plus rapide le tout en dessous de 200 Mo de RAM.
    - Ajout de Converters pour supporter les services applicatifs: **WEB, Desktop et API**
    - Support de la localisation des données exportées en CSV (Séparateur décimal, des milliers, devise locale)
    - Intefaçage plus général avec les besoins haut niveau (Application WEB, Application Desktop, API)
    - Support de Json, Yaml et TOML comme structures de fichiers.
    - Traçabilité des données générées (Ajout de propriétés par défaut aux fichiers Excel).


## Vue d'ensemble: Architecture (Physique et conceptuelle)

### Architecture Physique

Hexagon est composé d'une base de données et de son code source qui constitue sa librairie

- **Base De Données**

    La base de données Hexagon a été conçue afin de structurer,
    isoler et apporter une spécification claire aux données essentielles à son fonctionnement.
    Conçue en adhérant fortement aux règles des Formes normales (3-4NF), la BDD offre de meilleures
    performances et une administration simplifiée.

    **Fiche Technique:**

    - Type: PostgresSQL
    - Version: 14+
    - Extensions: HStore, JSON
    - Client: PGAdmin 4
    - ApplicationUser: hexagon_services
    - Fréquence d'alimentation: Hebdomadaire
    - Taille du Backup: 120-140 Mo
    - Taille de la BDD: 2 Go
    - Fréquence du Backup: Hebdomadaire
    - OS: Wondows/Linux/Mac OSX
    - Version Initiale: 11

- **Hexagon-src code-**:

    Le package Hexagon a été conçu pour implémenter la logique qui exploite les données brutes de la BDD
    afin de les transformer en valeurs exploitables selon le besoin.
    
    Il est composé de plusieurs modules qui se composent afin de répondre aux différents besoins,
    le package implémente aussi l'ensemble des fonctions helpers/utils du projet.

    **Fiche Technique:**

    - Language: Python
    - Version: 3.11+
    - Package Manager: Mamba, pip
    - Shell: Jupyter Qtconsole, ptpython
    - Versioning: Git
    - CollabPlatform: GitLab
    - Linters: Jedi, pyflakes, LSP-pylsp
    - DocGenerator: pdoc3
    - DBAccess: psycopg2 + sqlalchemy
    - Profiling: cProfile, snakeviz, pyinstrument, 
    - Main Dependencies: sqlalchemy, attrs, cattrs, python-dateutil, more-itertools, xlsxwriter, numpy
    - OS: Wondows/Linux/Mac OSX

### Architecture conceptuelle -Version Courte-

En résumé Les principaux modules d'Hexagon sont:

- `hexagon.infrastructure`:
    - `hexagon.infrastructure.db`:
    - `hexagon.infrastructure.config`:
- `hexagon.performance`:
- `hexagon.veille`:
- `hexagon.analytics`:
- `hexagon.etl`:
